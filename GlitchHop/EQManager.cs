﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EQManager : MonoBehaviour {

	public Color lerpStart;
	public Color lerpEnd;
	public Color lerpCurrent;

	public PlayerController player;
	public float t;

	private SpriteRenderer[] allEq;

	// Use this for initialization
	void Start () 
	{
		player = FindObjectOfType<PlayerController> ();
		allEq = GetComponentsInChildren<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		t = (1f / player.maxHP) * player.HP;
		lerpCurrent = Color.Lerp (lerpStart,lerpEnd,t);

		for (int i = 0; i < allEq.Length; i++)
		{
			allEq [i].color = lerpCurrent;
		}
	}
}
