﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class AIAgent : MonoBehaviour 
{

	//Choreography variables.
	public enum MOVEDIRECTION{UP=0, DOWN=1, LEFT=2, RIGHT=3, STOP=4, SEEK = 5, EVADE = 6, SHOOT = 7};
	private Vector3[] Directions = new Vector3[8];
	public MOVEDIRECTION[] PathSequence;

	//Enemy Stats 
	public int maxHP = 1;
	public int HP = 1;

	//Interaction variables
	private Transform ThisTransform;
	public PlayerController playerController;
	public MusicManager musicManager;
	public Transform playerTransform;
	public Vector3 playerVectorDelta;
	public GameObject projectile;

	//Movement logic variables
	public Vector3 velocity = new Vector3 (0f,0f);
	public bool canMove = true;
	public bool isMoving;
//	public bool isBlocking;
	public bool isSeeking;
	public float distanceTravelled;
	public float speed = 16f;
	public Vector3 originPos;
	public int SequenceIndex = 0;

	//Respawn logic
	public Vector3 respawnPoint;

	//For use in-editor: which Koreography Track are we using?
	public string koreoEventString;
	public int audioInt;

	void Awake ()
	{
		ThisTransform = GetComponent<Transform> ();
		respawnPoint = ThisTransform.position;
		Koreographer.Instance.RegisterForEvents(koreoEventString, DanceSequence); 
		playerController = FindObjectOfType<PlayerController> ();
		musicManager = FindObjectOfType<MusicManager> ();

		Directions [(int)MOVEDIRECTION.UP] = Vector3.up;
		Directions [(int)MOVEDIRECTION.DOWN] = Vector3.down;
		Directions [(int)MOVEDIRECTION.LEFT] = Vector3.left;
		Directions [(int)MOVEDIRECTION.RIGHT] = Vector3.right;
		Directions [(int)MOVEDIRECTION.STOP] = Vector3.zero;
		Directions [(int)MOVEDIRECTION.SEEK] = Vector3.zero;
		Directions [(int)MOVEDIRECTION.EVADE] = Vector3.zero;
		Directions [(int)MOVEDIRECTION.SHOOT] = Vector3.zero;

		SequenceIndex = PathSequence.Length - 1;
		velocity = Vector3.zero;
	}

	// Use this for initialization
	void OnEnable () 
	{

		ThisTransform.position = respawnPoint;

		originPos = ThisTransform.position;	

		

		//Fire up the coroutine.
		StartCoroutine (RunPathSequence ());
	}
		

	void Update ()
	{
		if (HP <= 0)
		{
			OnDeath ();
		}

		if (PathSequence[SequenceIndex] == MOVEDIRECTION.SHOOT)
		{
			Projectile projectileFire = projectile.GetComponent<Projectile> ();
			projectileFire.Fire (ThisTransform.position, Directions [(int)MOVEDIRECTION.SEEK]);
		}
	}

	void OnDeath () 
	{
		HP = maxHP;
		musicManager.EndAudio (audioInt);
		StopAllCoroutines ();
		gameObject.SetActive (false);
	}

	void DanceSequence (KoreographyEvent evt)
	{
		//print ("koreo detected");
		canMove = true;
		isSeeking = true;
		originPos = ThisTransform.position;	
		SequenceIndex++;
		if (SequenceIndex >= PathSequence.Length)
		{
			SequenceIndex = 0;
		}
	}

	public void EnemyTakesDamage ()
	{
		//print ("enemy ouch!");
		canMove = true;
		HP -= 1;
		ThisTransform.position = originPos + velocity;
		originPos = ThisTransform.position + playerController.velocity;
		velocity = playerController.velocity;
	}

	public IEnumerator RunPathSequence()
	{
		while (isMoving)
		{
			//canMove is TRUE on each beat, and FALSE on completed movement. 
			if (canMove)
			{
				//Set the velocity of the enemy to the value relative to its position in its path sequence.
				velocity = Directions [(int)PathSequence [(int)SequenceIndex]];
				distanceTravelled = Vector3.Distance (originPos, ThisTransform.position);

				//Override previous assignment if the enemy is blocking at any point.
//				if (isBlocking)
//				{
//					originPos += velocity;
//					velocity = velocity * -1f;
//				}

				if(canMove == true && isSeeking == true)
				{
					//Now assign the appropriate vector to SEEK.
					if (Mathf.Abs(playerVectorDelta.x) >= Mathf.Abs (playerVectorDelta.y))
					{
						Directions [(int)MOVEDIRECTION.SEEK] = Vector3.right * (Mathf.Sign (playerVectorDelta.x));
						isSeeking = false;
					}
					else if (Mathf.Abs(playerVectorDelta.x) < Mathf.Abs (playerVectorDelta.y))
					{
						Directions [(int)MOVEDIRECTION.SEEK] = Vector3.up * (Mathf.Sign (playerVectorDelta.y));
						isSeeking = false;
					}
					Directions [(int)MOVEDIRECTION.EVADE] = Directions [(int)MOVEDIRECTION.SEEK] * - 1f;
				}
			}

				//If the enemy has moved far enough...
				if (distanceTravelled >= 1f)
				{
					//Fix its position to where it should be...
					ThisTransform.position = originPos + velocity;
//					isBlocking = false;
					canMove = false;
					originPos = ThisTransform.position;
					//Then clamp it in place.
					velocity = Vector3.zero;
				}

				//Check if the player is farther away on the X or Y axis.
				playerVectorDelta = playerTransform.position - ThisTransform.position;
				//Only change this assignment if it doesn't screw with other logic.

				//After everything has been assigned, resolve the movement for this frame.
				ThisTransform.position += velocity * Time.deltaTime * speed;

				yield return null;
			}
			
		}
	}

