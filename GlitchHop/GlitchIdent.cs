﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class GlitchIdent : MonoBehaviour {

	public MusicManager musicManager;
	public AIAgent[] enemyArray;
	public int HP = 15;
	public BoxCollider2D col;
	public Sprite invulnSprite;
	public Sprite vulnSprite;
	private SpriteRenderer sprRender;
	public GameObject playerObject;
	public GameObject resyncing;
	public PlayerController player;
	public GameManager gameManager;
	public GameObject victoryUI;
	public Animator deathAnim;
	public AudioSource hurtNoise;

	public TextMesh HPCounter;

	public bool vulnerable;
	public bool rising = false;
	public string glitchKoreo;
	public string respawnKoreo;



	// Use this for initialization
	void Start () {
		col =	GetComponent<BoxCollider2D> ();
		sprRender = GetComponent<SpriteRenderer> ();
		gameManager = FindObjectOfType <GameManager> ();
		Koreographer.Instance.RegisterForEvents(glitchKoreo, SetRespawnBool);
		Koreographer.Instance.RegisterForEvents (respawnKoreo, RespawnEnemies);

		musicManager.AudioArray [5].volume = 0f;
	}

	// Update is called once per frame
	void Update () {

		HPCounter.text = HP.ToString();

		if (HP <= 0)
		{
			Victory ();
		}

		for (int i = 0 ; i <= enemyArray.Length -1 ; i++)
		{

			if (enemyArray [i].isActiveAndEnabled == false)
			{
				musicManager.EndAudio (enemyArray[i].audioInt);
			}

			if (enemyArray [i].isActiveAndEnabled == true)
			{
				sprRender.sprite = invulnSprite;
				vulnerable = false;
				col.enabled = false;
				break;
			}
			vulnerable = true;
			col.enabled = true;
			HPCounter.gameObject.SetActive (true);
			sprRender.sprite = vulnSprite;
		}
		if (HP == 11 || HP == 6)
		{
			HP--;
		}

		HPCounter.text = HP.ToString();

	}

	void SetRespawnBool(KoreographyEvent evt)
	{
		if (vulnerable == true && player.isDead == false)
		{
			rising = true;
			musicManager.AudioArray [5].volume = 1f;
		}
	}

	public void GlitchDamager()
	{
		HP -= 1;
		hurtNoise.Play ();

	}

	public void RespawnEnemies(KoreographyEvent evt)
	{
		
		if (rising == true && player.isDead == false)
		{
			for (int i = 0; i <= enemyArray.Length - 1; i++)
			{
				enemyArray [i].gameObject.SetActive(true);
				musicManager.StartAudio (enemyArray [i].audioInt);
			} 
			rising = false;
			musicManager.AudioArray [5].volume = 0f;
			player.OnRespawn ();
			resyncing.SetActive (false);
			StartCoroutine (player.PlayerMove());
		}
	}

	public void DespawnEnemies ()
	{
		for (int i = 0; i <= enemyArray.Length - 1; i++)
		{
			enemyArray [i].gameObject.SetActive(false);
			musicManager.EndAudio (enemyArray [i].audioInt);
		} 

	}

	void Victory()
	{
		deathAnim.SetBool ("isDead",true);
		player.isActive = false;
		musicManager.gameObject.SetActive (false);
		victoryUI.SetActive (true);
	}
}
