﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class EQRow : MonoBehaviour 
{

	private SpriteRenderer[] eqBars;
	public string koreoString;
	public int heightModifier;
	public float dropSpeed = 16f;

	public PlayerController player;

	private float updateFlt;
	private int updateInt;

	// Use this for initialization
	void Awake ()
	{
		eqBars = GetComponentsInChildren<SpriteRenderer> ();
		player = FindObjectOfType<PlayerController> ();
	}

	void Start () 
	{
		Koreographer.Instance.RegisterForEvents(koreoString, KoreoEvent);
	}
	
	// Update is called once per frame
	void Update () 
	{
		updateFlt += Time.deltaTime * dropSpeed;
		updateInt = Mathf.RoundToInt (updateFlt);

			for (int i = 0; i < updateInt + ((player.maxHP - player.HP)) + heightModifier && i < eqBars.Length - 1; i++)
			{
				eqBars [i].gameObject.SetActive (false);
			}

	}

	void KoreoEvent (KoreographyEvent evt)
	{
		for (int i = ((player.maxHP - player.HP) * 2) + heightModifier ; i < eqBars.Length && i < eqBars.Length -1; i++)
		{
			eqBars[i].gameObject.SetActive(true);
		}
		updateFlt = 0f;
	}
}
