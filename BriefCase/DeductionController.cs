﻿namespace VRTK
{
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

	public class DeductionController : MonoBehaviour 
	{
        public DeductionObject leftObject = null;
		public DeductionObject rightObject = null;

        //public DeductionObject[] Objects;

        public NarrationController nController;
		public int leftInt;
		public int rightInt; 
		public bool deduced = false;
	
		void Awake()
		{
			nController = FindObjectOfType<NarrationController> ();
		}

	// Update is called once per frame
		public void Deduce() 
		{
			print ("Deducing");
			if (leftObject == null || rightObject == null)
			{
				print ("You need to hold two objects to start deducing.");
				return;
			}

			if (deduced == false)
			{
				print ("Two deducable items confirmed: " + leftObject.gameObject.name + " and " + rightObject.gameObject.name);
				for (int i = 0;i<=leftObject.deductions.Length - 1;i++)
				{
					leftInt = leftObject.deductions [i];
					print ("New L-Loop: leftString = " + leftInt );
					for (int j = 0 ; j <= rightObject.deductions.Length - 1 ; j++)
					{
						rightInt = rightObject.deductions [j];
						print ("New R-Loop: rightString = " + rightInt );
						if (leftInt == rightInt)
						{
							print ("match found!");
							nController.PlayNarration (leftInt, true);
							leftObject.isDeduced = true;
							rightObject.isDeduced = true;
							return;
						}
					}
				}
			}
			print ("no deductions available!");
		}
	}
}
