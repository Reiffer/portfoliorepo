﻿//TITLE
//SUMMARY
//AUTHOR
//DATE-TIME of last modification

namespace VRTK
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class DeductionObject : VRTK_InteractableObject
	{

		public int[] deductions; 
		public DeductionController dController;
		public NarrationController nController;
		public bool isDeduced = false;
		public bool heardBefore = false; //AVOID bools where possible. Instead use: public int HeadBeforeCount;
		public int narrationIndex;
        //-------------------------
        //Function comment - describes function purpose
        void Start ()
		{
			dController = FindObjectOfType<DeductionController> ();
			nController = FindObjectOfType<NarrationController> ();
		}
        //-------------------------
        public override void StartUsing(VRTK_InteractUse usingObject)
		{
            //if(usingObject.gameObject.name.Equals("LeftController"))

            if (usingObject.gameObject.name == "LeftController")
			{
				print ("left deduce");

				dController.leftObject = this;
				dController.Deduce ();
            }
			if (usingObject.gameObject.name == "RightController")
			{
				print ("right deduce");

				dController.rightObject = this;
				dController.Deduce ();
            }
				
			if (isDeduced == false && heardBefore == false)
			{
				nController.PlayNarration (narrationIndex,false);
				heardBefore = true;
			}
			isDeduced = false;
		}
        //-------------------------
        public override void StopUsing(VRTK_InteractUse previousUsingObject)
		{

			base.StopUsing (previousUsingObject);

			if (previousUsingObject.gameObject.name == "LeftController")
			{
				dController.leftObject = null;
				dController.leftInt = 50;
			}

			if (previousUsingObject.gameObject.name == "RightController")
			{
				dController.rightObject = null;
				dController.rightInt = 50;
            }
		}
	}
}
