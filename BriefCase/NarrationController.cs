﻿//Research list:
//1). Unity Throw Exception
//2). Breakpoint conditions
//3). https://docs.unity3d.com/Manual/PlatformDependentCompilation.html
//4). Dependency Injection  http://www.what-could-possibly-go-wrong.com/dependency-injection-for-unity-part-1/
//5). Regex https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285
//6). Linq https://unity3d.college/2017/07/01/linq-unity-developers/
//7). JSON https://docs.unity3d.com/ScriptReference/JsonUtility.html

namespace VRTK
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class NarrationController : MonoBehaviour 
	{

		public DeductionController dController;
		public AudioSource[] narration;
		public int previousIndex = 0;

		void Awake()
		{
			narration = GetComponentsInChildren<AudioSource> ();
			dController = FindObjectOfType<DeductionController> ();
		}

		void Update()
		{
			if (narration[previousIndex].isPlaying == false)
			{
				dController.deduced = false;
			}
		}

		public void PlayNarration(int index, bool deduction)
		{
			if (index == 30 || index == 33 || index == 36 || index == 37)
			{
				deduction = false;
				float rng = Random.Range (6f, 8f);
				narration[Mathf.RoundToInt(rng)].Play();
			}

			if (deduction == true)
			{
				float rng = Random.Range (1f, 5f);
				narration[Mathf.RoundToInt(rng)].Play();
			}
			narration [index].Play ();
		}
	}
}
