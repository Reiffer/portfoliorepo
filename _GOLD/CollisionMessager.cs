﻿//Author: Tim Tryzbiak (Original WeaponCore.cs)
//Last Edit: Tom Reiffer 21/06/18
//Summary: A modified WeaponCore that behaves as a slow-moving projectile and teleports the owner on impact with a specific surface.
//         Throwing and activation of this object are handled in TSS_ThrowWeapon

using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using com.ootii.Actors.Combat;
using com.ootii.Geometry;
using com.ootii.MotionControllerPacks;
using com.ootii.Messages;
using com.ootii.Actors.AnimationControllers;
using NFTS.TimeManager;
using NFTS.CM;

namespace com.ootii.Actors.LifeCores
{
    public class CollisionMessager : ItemCore, IWeaponCore
    {
        /// <summary>
        /// Owner that we don't want to collide with
        /// </summary>
        public override GameObject Owner
        {
            get { return mOwner; }

            set
            {
                Collider[] lColliders = gameObject.GetComponents<Collider>();

                if (mOwner != null)
                {
                    Collider[] lOwnerColliders = mOwner.GetComponents<Collider>();
                    for (int i = 0; i < lColliders.Length; i++)
                    {
                        for (int j = 0; j < lOwnerColliders.Length; j++)
                        {
                            UnityEngine.Physics.IgnoreCollision(lColliders[i], lOwnerColliders[j], false);
                        }
                    }
                }

                if (value != null)
                {
                    Collider[] lOwnerColliders = value.GetComponents<Collider>();
                    for (int i = 0; i < lColliders.Length; i++)
                    {
                        for (int j = 0; j < lOwnerColliders.Length; j++)
                        {
                            UnityEngine.Physics.IgnoreCollision(lColliders[i], lOwnerColliders[j], true);
                        }
                    }
                }

                mOwner = value;
            }
        }

        ///<summary>
        ///Throwing motion that this weapon adheres to
        ///</summary>
        public TSS_ThrowWeapon ThrowWeaponMotion;

        /// <summary>
        /// Determine if we're actually swinging
        /// </summary>
        protected bool mIsActive = true;
        public virtual bool IsActive
        {
            get { return mIsActive; }

            set
            {
                mIsActive = value;

                //mAge = 0f;
                mImpactCount = 0;
                //mPositions.Clear();

                Rigidbody lRigidbody = gameObject.GetComponent<Rigidbody>();
                if (lRigidbody != null) { lRigidbody.detectCollisions = mIsActive; }

                // Enable or disable the colliders
                Collider[] lColliders = gameObject.GetComponents<Collider>();
                for (int i = 0; i < lColliders.Length; i++)
                {
                    lColliders[i].enabled = mIsActive;
                }

                // Clear the defenders
                if (mIsActive) { mDefenders.Clear(); }
            }
        }

        /// <summary>
        /// Determines if we're dealing with colliders or not
        /// </summary>
        protected bool mHasColliders = false;
        public bool HasColliders
        {
            get { return mHasColliders; }
        }

        //--------------------------------------------------------------------------
        //T Reiffer edit: Impact teleportation vars.

        //Gameobject to represent player while teleporting, and all rendered elements of owner.
        public GameObject WispPrefab;
        public GameObject OwnerRenderables;

        // Values for lerp.
        public Vector3 StartTransform;
        public Vector3 DestinationPosition;
        public float speed = 8f;
        public AnimationCurve TeleportCurve;

        //Queued motion for arrival animation.
        public MotionControllerMotion QueuedMotion;

        //Collision fx.
        public AudioSource CollisionNoise;
        public GameObject DamageSparkPrefab;
        public GameObject TeleSparkPrefab;

        //Sweet slowmo camera for dope cool moments. Unused in final build.
        public GameObject VCO_Throwing;
        //--------------------------------------------------------------------------

        /// <summary>
        /// Collider used by the item
        /// </summary>
        public Collider Collider
        {
            get
            {
                if (mHasColliders)
                {
                    return gameObject.GetComponent<Collider>();
                }

                return null;
            }
        }

        /// <summary>
        /// Minimum range the weapon can apply damage
        /// </summary>
        public float _MinRange = 0f;
        public virtual float MinRange
        {
            get { return _MinRange; }
            set { _MinRange = value; }
        }

        /// <summary>
        /// Maximum range the weapon can apply damage
        /// </summary>
        public float _MaxRange = 1f;
        public virtual float MaxRange
        {
            get { return _MaxRange; }
            set { _MaxRange = value; }
        }

        /// <summary>
        /// Min damage to use on impact
        /// </summary>
        public float _MinDamage = 50f;
        public virtual float MinDamage
        {
            get { return _MinDamage; }
            set { _MinDamage = value; }
        }

        /// <summary>
        /// Max damage to use on impact
        /// </summary>
        public float _MaxDamage = 50f;
        public virtual float MaxDamage
        {
            get { return _MaxDamage; }
            set { _MaxDamage = value; }
        }

        /// <summary>
        /// Min force multiplier to use on impact
        /// </summary>
        public float _MinImpactPower = 1f;
        public virtual float MinImpactPower
        {
            get { return _MinImpactPower; }
            set { _MinImpactPower = value; }
        }

        /// <summary>
        /// Max force multiplier to use on impact
        /// </summary>
        public float _MaxImpactPower = 1f;
        public virtual float MaxImpactPower
        {
            get { return _MaxImpactPower; }
            set { _MaxImpactPower = value; }
        }

        /// <summary>
        /// Attack style being used for the current attack
        /// </summary>
        protected ICombatStyle _CombatStyle = null;
        public virtual ICombatStyle mCombatStyle
        {
            get { return _CombatStyle; }
            set { _CombatStyle = value; }
        }

        /// <summary>
        /// Track the last hit that occurred
        /// </summary>
        protected CombatHit mLastHit = CombatHit.EMPTY;
        public CombatHit LastHit
        {
            get { return mLastHit; }
            set { mLastHit = value; }
        }


        /// <summary>
        /// Raised when the weapon actually impacts something
        /// </summary>
        [NonSerialized]
        public LifeCoreDelegate OnImpactEvent = null;

        /// <summary>
        /// Cache the transform for easier access
        /// </summary>
        protected Transform mTransform = null;

        // Used to track the movement of the weapon
        protected Vector3 mLastPosition;

        /// <summary>
        /// Number of impacts that occured this activation
        /// </summary>
        protected int mImpactCount = 0;

        // Track the GameObject that have been hit. We only want to hit each object once per swing
        protected List<GameObject> mDefenders = new List<GameObject>();

        /// <summary>
        /// Called before any updates are called
        /// </summary>
        protected virtual void Start()
        {
            mTransform = gameObject.transform;
            mLastPosition = mTransform.position;

            if (GetComponentInChildren<SpearCameraIdent>() != null)
            {
                VCO_Throwing = GetComponentInChildren<SpearCameraIdent>().gameObject;
                VCO_Throwing.SetActive(false);
            }


            DamageSparkPrefab = FindObjectOfType<VFXManager>().ParticleOneShots[1];
            TeleSparkPrefab = FindObjectOfType<VFXManager>().ParticleOneShots[1];

        Collider lCollider = gameObject.GetComponent<Collider>();
            mHasColliders = (lCollider != null);

            // Ensure we're not detecting collisions yet
        }

        public IEnumerator ProjectileInFlight(Vector3 TargetTransform)
        {
            //print(TargetTransform);
            CinemachineManager.VCO_SpearDodge = VCO_Throwing;
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
            rb.useGravity = false;
            Vector3 OrigPos = gameObject.transform.position;
            Vector3 Velocity = Vector3.Normalize(TargetTransform - OrigPos) * speed * 2;
            IsActive = true;
            while (true)
            {
                gameObject.transform.position += Velocity * Time.deltaTime;
                yield return null;
            }
        }

        IEnumerator Teleport()
        {
            QueuedMotion = null;
            float t = 0;
            StartTransform = new Vector3 (Owner.transform.position.x, Owner.transform.position.y, Owner.transform.position.z);
            DestinationPosition = gameObject.transform.position;
            OwnerRenderables.SetActive(false);
            GameObject newWisp = Instantiate(WispPrefab);
            newWisp.transform.position = OwnerRenderables.transform.position;
            while (t < 1f)
            {
                newWisp.transform.position = Vector3.Lerp(StartTransform, DestinationPosition, t);
                newWisp.transform.position = new Vector3(newWisp.transform.position.x,  newWisp.transform.position.y + TeleportCurve.Evaluate(t), newWisp.transform.position.z );
                Owner.transform.position = newWisp.transform.position;
                t += Time.deltaTime * speed;
                yield return null;
            }

            Destroy(newWisp);
            if (Owner.GetComponent<Elain_StateMachine>() != null)
            {
                Elain_StateMachine AI = Owner.GetComponent<Elain_StateMachine>();
                QueuedMotion = AI.WhichTeleportArrival();
                yield return null;
            }

            ThrowWeaponMotion.MotionController.Animator.SetTrigger("TeleportArrival");
            ThrowWeaponMotion.IsInterruptible = true;
            OwnerRenderables.SetActive(true);
            Owner.transform.position = DestinationPosition;
            CinemachineManager.SwitchCameraTo(CinemachineManager.VCO_Cached);
            ActivateArrivalMotion(QueuedMotion);
            StopAllCoroutines();
            yield break;
        }

        public void OnDisable()
        {
            IsActive = false;
            CinemachineManager.VCO_SpearDodge = null;
            StopAllCoroutines();
        }

        public void ActivateArrivalMotion(MotionControllerMotion Motion)
        {
            //Debug.Log("Activating arrival motion");
            if (Motion == null)
            {
                //Debug.Log("Arrival motion is null");
                return;
            }

            //Debug.Log("Activating queued motion: " + Motion + ", upon arrival");
            ThrowWeaponMotion.MotionController.ActivateMotion(QueuedMotion,11);
        }

        /// <summary>
        /// As the projectile moves, check if it collides with anything
        /// </summary>
        protected virtual void LateUpdate()
        {
            if (mIsActive)
            {
                // Track the position over time
                mLastPosition = mTransform.position;
            }
        }

        /// <summary>
        /// Amount of damage to apply this attack
        /// </summary>
        /// <param name="rPercent">Percentage of damage between min and max (use 0 to 1).</param>
        /// <param name="rMultiplier">Multiplier applied to the final damage.</param>
        /// <returns>Amount of damage this attack</returns>
        public virtual float GetAttackDamage(float rPercent = 1f, float rMultiplier = 1f)
        {
            return (_MinDamage + ((_MaxDamage - _MinDamage) * rPercent)) * rMultiplier;
        }

        /// <summary>
        /// Amount of impulse to apply this attack
        /// </summary>
        /// <param name="rPercent">Percentage of impulse between min and max (use 0 to 1).</param>
        /// <param name="rMultiplier">Multiplier applied to the final impulse.</param>
        /// <returns>Amount of impulse this attack</returns>
        /*
        public virtual float GetAttackImpactPower(float rPercent = 1f, float rMultiplier = 5f)
        {
            //return (_MinImpactPower + ((_MaxImpactPower - _MinImpactPower) * rPercent)) * rMultiplier;
            //TS 

        }
        */
        /// <summary>
        /// Test each of the combatants to determine if an impact occured
        /// </summary>
        /// <param name="rCombatTargets">Targets who we may be impacting</param>
        /// <param name="rAttackStyle">ICombatStyle that details the combat style being used.</param>
        /// <returns>The number of impacts that occurred</returns>
        public virtual int TestImpact(List<CombatTarget> rCombatTargets, ICombatStyle rAttackStyle = null)
        {

            mImpactCount = 0;

            float lMaxReach = 0f;

            if (mOwner != null)
            {
                ICombatant lCombatant = mOwner.GetComponent<ICombatant>();
                if (lCombatant != null) { lMaxReach = lCombatant.MaxMeleeReach; }
            }

            for (int i = 0; i < rCombatTargets.Count; i++)
            {
                CombatTarget lTarget = rCombatTargets[i];

                // Stop if we don't have a valid target
                if (lTarget == CombatTarget.EMPTY) { continue; }

                // Stop if we already hit the game object
                if (mDefenders.Contains(lTarget.Collider.gameObject)) { continue; }

                // Stop if we're out of range
                float lDistance = Vector3.Distance(lTarget.ClosestPoint, mTransform.position);
                if (lDistance > _MaxRange + lMaxReach) { continue; }

                Vector3 lVector = (mTransform.position - mLastPosition).normalized;
                if (lVector.sqrMagnitude == 0 && mOwner != null) { lVector = mOwner.transform.forward; }

                mLastHit.Collider = lTarget.Collider;
                mLastHit.Point = lTarget.ClosestPoint;
                mLastHit.Normal = -lVector;
                mLastHit.Vector = lVector;
                mLastHit.Distance = lTarget.Distance;
                mLastHit.Index = mImpactCount;

                OnImpact(mLastHit, rAttackStyle);
            }

            return mImpactCount;
        }

        /// <summary>
        /// Raised when the impact occurs
        /// </summary>
        /// <param name="rHitInfo">CombatHit structure detailing the hit information.</param>
        /// <param name="rAttackStyle">ICombatStyle that details the combat style being used.</param>
        protected virtual void OnImpact(CombatHit rHitInfo, ICombatStyle rAttackStyle = null)
        {

            if (!IsActive) { return; }
            // Track the defenders so we don't hit more than once
            if (rHitInfo.Collider.gameObject.layer == 8)
            {
                print("Collision!");
                Rigidbody rb = gameObject.GetComponent<Rigidbody>();
                rb.velocity = Vector3.zero;
                rb.isKinematic = true;
                rb.useGravity = false;
                IsActive = false;
                //CollisionNoise.Play();
                Instantiate(TeleSparkPrefab);
                StopAllCoroutines();
                StartCoroutine(Teleport());
            }

            if (rHitInfo.Collider.gameObject.layer == 9)
            {
                SlowTimeManager.bIsAttacking = false;
            }

            if (rHitInfo.Collider.gameObject.GetComponent<Combatant>() != null)
            {
                if (rHitInfo.Collider.gameObject.GetComponent<Combatant>().IsDodging == false)
                {
                    mDefenders.Add(rHitInfo.Collider.gameObject);
                    Instantiate(DamageSparkPrefab);
                    //CollisionNoise.Play();
                    // If we get here, there's an impact
                    mImpactCount++;

                    // Extract out information about the hit
                    Transform lHitTransform = GetClosestTransform(rHitInfo.Point, rHitInfo.Collider.transform);
                    Vector3 lHitDirection = Quaternion.Inverse(lHitTransform.rotation) * (rHitInfo.Point - lHitTransform.position).normalized;

                    // Put together the combat info. This will will be modified over time
                    CombatMessage lMessage = CombatMessage.Allocate();
                    lMessage.Attacker = mOwner;
                    lMessage.Defender = rHitInfo.Collider.gameObject;
                    lMessage.Weapon = this;
                    lMessage.Damage = GetAttackDamage(1f, (rAttackStyle != null ? rAttackStyle.DamageModifier : 1f));
                    lMessage.HitPoint = rHitInfo.Point;
                    lMessage.HitDirection = lHitDirection;
                    lMessage.HitVector = rHitInfo.Vector;
                    lMessage.HitTransform = lHitTransform;
                    // Grab cores for processing
                    ActorCore lAttackerCore = (mOwner != null ? mOwner.GetComponentInParents(typeof(ActorCore)) as ActorCore : null);
                    ActorCore lDefenderCore = rHitInfo.Collider.gameObject.GetComponentInParents(typeof(ActorCore)) as ActorCore;

                    // Pre-Attack
                    lMessage.ID = CombatMessage.MSG_ATTACKER_ATTACKED;

                    if (lAttackerCore != null)
                    {
                        lAttackerCore.SendMessage(lMessage);
                    }

#if USE_MESSAGE_DISPATCHER || OOTII_MD
            MessageDispatcher.SendMessage(lMessage);
#endif

                    // Attack Defender
                    lMessage.ID = CombatMessage.MSG_DEFENDER_ATTACKED;

                    if (lDefenderCore != null)
                    {
                        ICombatant lDefenderCombatant = rHitInfo.Collider.gameObject.GetComponentInParents(typeof(ICombatant)) as ICombatant;
                        if (lDefenderCombatant != null)
                        {
                            lMessage.HitDirection = Quaternion.Inverse(lDefenderCore.Transform.rotation) * (rHitInfo.Point - lDefenderCombatant.CombatOrigin).normalized;
                        }

                        lDefenderCore.SendMessage(lMessage);

#if USE_MESSAGE_DISPATCHER || OOTII_MD
                MessageDispatcher.SendMessage(lMessage);
#endif
                    }
                    else
                    {
                        lMessage.HitDirection = Quaternion.Inverse(lHitTransform.rotation) * (rHitInfo.Point - lHitTransform.position).normalized;

                        IDamageable lDefenderDamageable = rHitInfo.Collider.gameObject.GetComponentInParents(typeof(IDamageable)) as IDamageable;
                        if (lDefenderDamageable != null)
                        {
                            lDefenderDamageable.OnDamaged(lMessage);
                        }

                        Rigidbody lRigidBody = rHitInfo.Collider.gameObject.GetComponentInParents(typeof(Rigidbody)) as Rigidbody;
                        if (lRigidBody != null)
                        {
                            lRigidBody.AddForceAtPosition(rHitInfo.Vector * lMessage.ImpactPower, rHitInfo.Point, ForceMode.Impulse);
                        }
                    }

                    // Attacker response
                    if (lAttackerCore != null)
                    {
                        lAttackerCore.SendMessage(lMessage);

#if USE_MESSAGE_DISPATCHER || OOTII_MD
                MessageDispatcher.SendMessage(lMessage);
#endif
                    }

                    // Finish up any impact processing (like sound)
                    OnImpactComplete(lMessage);
                }
                // Release the combatant to the pool
                //CombatMessage.Release(lMessage);
            }
        }

        /// <summary>
        /// Raised when the weapon hit is verified. This way the weapon can adjust the combat message, play sounds, etc.
        /// </summary>
        /// <param name="rCombatMessage">Message that contains information about the hit</param>
        public virtual void OnImpactComplete(CombatMessage rCombatMessage)
        {
            //com.ootii.Utilities.Debug.Log.FileWrite((mOwner != null ? mOwner.name + "." : "") + "WeaponCore.OnHit()");
            rCombatMessage.Release();
        }

        /// <summary>
        /// Starts the recursive function for the closest transform to the specified point
        /// </summary>
        /// <param name="rPosition">Reference point for for the closest transform</param>
        /// <param name="rCollider">Transform that represents the collision</param>
        /// <returns></returns>
        public virtual Transform GetClosestTransform(Vector3 rPosition, Transform rCollider)
        {
            // Find the anchor's root transform
            Transform lActorTransform = rCollider;
            //while (lActorTransform.parent != null) { lActorTransform = lActorTransform.parent; }

            // Grab the closest body transform
            float lMinDistance = float.MaxValue;
            Transform lMinTransform = lActorTransform;
            GetClosestTransform(rPosition, lActorTransform, ref lMinDistance, ref lMinTransform);

            // Return it
            return lMinTransform;
        }

        /// <summary>
        /// Find the closes transform to the hit position. This is what we'll attach the projectile to
        /// </summary>
        /// <param name="rPosition">Hit position</param>
        /// <param name="rTransform">Transform to be tested</param>
        /// <param name="rMinDistance">Current min distance between the hit position and closest transform</param>
        /// <param name="rMinTransform">Closest transform</param>
        protected virtual void GetClosestTransform(Vector3 rPosition, Transform rTransform, ref float rMinDistance, ref Transform rMinTransform)
        {
            // Limit what we'll connect to
            if (rTransform.name.Contains("connector")) { return; }
            if (rTransform.gameObject.GetComponent<IWeaponCore>() != null) { return; }

            // If this transform is closer to the hit position, use it
            float lDistance = Vector3.Distance(rPosition, rTransform.position);
            if (lDistance < rMinDistance)
            {
                rMinDistance = lDistance;
                rMinTransform = rTransform;
            }

            // Check if any child transform is closer to the hit position
            for (int i = 0; i < rTransform.childCount; i++)
            {
                GetClosestTransform(rPosition, rTransform.GetChild(i), ref rMinDistance, ref rMinTransform);
            }
        }

        /// <summary>
        /// Determines if the "descendant" transform is a child (or grand child)
        /// of the "parent" transform.
        /// </summary>
        /// <param name="rParent"></param>
        /// <param name="rTest"></param>
        /// <returns></returns>
        protected bool IsDescendant(Transform rParent, Transform rDescendant)
        {
            if (rParent == null) { return false; }

            Transform lDescendantParent = rDescendant;
            while (lDescendantParent != null)
            {
                if (lDescendantParent == rParent) { return true; }
                lDescendantParent = lDescendantParent.parent;
            }

            return false;
        }

        /// <summary>
        /// Capture Unity's collision event. We use triggers since IsKinematic Rigidbodies don't
        /// raise collisions... only triggers.
        /// </summary>
        public virtual void OnTriggerEnter(Collider rCollider)
        {
            if (!mIsActive) { return; }

            // Get out if we already hit this game object
            if (mDefenders.Contains(rCollider.gameObject)) { return; }

            // Ensure we're not hitting ourselves
            if (mOwner != null)
            {
                if (rCollider.gameObject == mOwner) { return; }

                IWeaponCore lWeaponCore = rCollider.gameObject.GetComponentInParents(typeof(IWeaponCore)) as IWeaponCore;
                if (lWeaponCore != null && lWeaponCore.Owner == mOwner) { return; }

                if (IsDescendant(mOwner.transform, rCollider.transform)) { return; }
            }

            Vector3 lClosestPoint = GeometryExt.ClosestPoint(mLastPosition, rCollider);
            if (lClosestPoint != Vector3Ext.Null)
            {
                if (mTransform == null)
                {
                    mTransform = GetComponent<Transform>();
                }
                Vector3 lVector = (mTransform.position - mLastPosition).normalized;
                if (lVector.sqrMagnitude == 0 && mOwner != null) { lVector = mOwner.transform.forward; }

                mLastHit.Collider = rCollider;
                mLastHit.Point = lClosestPoint;
                mLastHit.Normal = -lVector;
                mLastHit.Vector = lVector;
                mLastHit.Distance = 0f;
                mLastHit.Index = mImpactCount;
                OnImpact(mLastHit, _CombatStyle);
            }
        }

        /// <summary>
        /// Capture Unity's collision event
        /// </summary>
        protected virtual void OnTriggerStay(Collider rCollider)
        {
            //com.ootii.Utilities.Debug.Log.FileWrite(transform.name + ".OnTriggerStay(" + rCollider.name + ")");

            if (!mIsActive) { return; }
        }

        /// <summary>
        /// Capture Unity's collision event
        /// </summary>
        protected virtual void OnTriggerExit(Collider rCollider)
        {
            //com.ootii.Utilities.Debug.Log.FileWrite(transform.name + ".OnTriggerExit(" + rCollider.name + ")");

            if (!mIsActive) { return; }
        }

        
    }

}
