﻿using UnityEngine;
using com.ootii.Actors.Combat;
using NFTS.TimeManager;
using com.ootii.MotionControllerPacks;
using com.ootii.Helpers;
using com.ootii.Actors.LifeCores;
using NFTS.CM;
using com.ootii.Actors.Attributes;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.ootii.Actors.AnimationControllers
{
    /// <summary>
    /// Dodge roll left & right, hop and duck based on x/y input during lock-on combat.
    /// </summary>
    [MotionName("CombatEvade")]
    [MotionDescription("Press button at correct time to dodge and prepare to counter, or enter a 'whiffed' state")]
    public class CombatEvade : PSS_MotionBase
    {
        // Different types of dodges we can perform, along with a "whiff" and default start value.
        public const int PHASE_UNKNOWN = 0;
        public const int PHASE_START = 99900;
        public const int PARAM_STEPLEFT = 1;
        public const int PARAM_STEPRIGHT = 2;
        public const int PARAM_DUCK = 3;
        public const int PARAM_BACK = 4;
        public const int PARAM_HOP = 5;
        public const int PARAM_FORTH = 6;
        public const int PARAM_SLIPL = 7;
        public const int PARAM_SLIPR = 8;
        public const int PARAM_SLIPB = 9;
        public const int PARAM_BLINKEVADE = 11;
        public const int PARAM_WHIFFED = -1;

    
        // Declaring the player and AI to be assigned to later.
        private Elain_StateMachine AISource = null;
        private PlayerManager PlayerSource = null;
        public int mChainAttackStyleIndex = -1;

        // Event strings for the OnAnimationEvent function to listen for.
        public const string EVENT_COUNTERSTART = "counterstart";
        public const string EVENT_COUNTEREND = "counterend";

        // Can we chain out of this motion into a new one?
        public bool mIsChainActive = false;

        /// <summary>
        /// Do not rely on verification here.
        /// </summary>
        public override bool VerifyTransition
        {
            get
            {
                return false;
            }
        }


        /// <summary>
        /// Phase ID to send to the animator when the motion activates.
        /// </summary>
        public int _PhaseID = 0;
        public int PhaseID
        {
            get { return _PhaseID; }
            set { _PhaseID = value; }
        }

        /// <summary>
        /// Normalized time of the animation to exit the motion.
        /// </summary>
        public float _ExitTime = 1f;
        public float ExitTime
        {
            get { return _ExitTime; }
            set { _ExitTime = value; }
        }

        /// <summary>
        /// State we'll use to text the exit time with. If blank, it will simply
        /// be the first state we've entered.
        /// </summary>
        public string _ExitState = "";
        public string ExitState
        {
            get { return _ExitState; }
            set { _ExitState = value; }
        }

        /// <summary>
        /// Determines if we exit when the input alias is released
        /// </summary>
        public bool _ExitOnRelease = false;
        public bool ExitOnRelease
        {
            get { return _ExitOnRelease; }
            set { _ExitOnRelease = value; }
        }

        public int selectedDodgeParam = 0;

        /// <summary>
        /// Determines if we disable gravity on activation and re-enable it on de-activation
        /// </summary>
        public bool _DisableGravity = false;
        public bool DisableGravity
        {
            get { return _DisableGravity; }
            set { _DisableGravity = value; }
        }

        /// <summary>
        /// Determines if we disable root motion rotations
        /// </summary>
        public bool _DisableRootMotionRotation = false;
        public bool DisableRootMotionRotation
        {
            get { return _DisableRootMotionRotation; }
            set { _DisableRootMotionRotation = value; }
        }

        /// <summary>
        /// Determines if we disable root motion movement
        /// </summary>
        public bool _DisableRootMotionMovement = false;
        public bool DisableRootMotionMovement
        {
            get { return _DisableRootMotionMovement; }
            set { _DisableRootMotionMovement = value; }
        }

        /// <summary>
        /// Hash for the exit state we'll use to deactivate the motion
        /// </summary>
        private int mExitStateID = 0;

        /// <summary>
        /// Determines if we've actually entered the first animation
        /// </summary>
        private bool mHasEnteredAnimatorState = false;

        /// <summary>
        /// Determine if gravity was enabled previously.
        /// </summary>
        private bool mWasGravityEnabled = true;

        /// <summary>
        /// Default constructor
        /// </summary>
        public CombatEvade()
            : base()
        {
            _Priority = 10;
            mIsStartable = true;
        }

        /// <summary>
        /// Controller constructor
        /// </summary>
        /// <param name="rController">Controller the motion belongs to</param>
        public CombatEvade(MotionController rController)
            : base(rController)
        {
            _Priority = 10;
            mIsStartable = true;
        }

        public override void Awake()
        {
            base.Awake();

            // Find the combatant to pass combat data to and from, and then 

            mCombatant = mMotionController.gameObject.GetComponent<ICombatant>();
            if (mMotionController.gameObject.tag != "Player")
            {
                AISource = mMotionController.gameObject.GetComponent<Elain_StateMachine>();
            }
            else
            {
               PlayerSource = mMotionController.gameObject.GetComponent<PlayerManager>();
            }

        }

        /// <summary>
        /// Tests if this motion should be started. However, the motion
        /// isn't actually started.
        /// </summary>
        /// <returns></returns>
        public override bool TestActivate()
        {
            if (!mIsStartable)
            {
                return false;
            }

            if (!mMotionController.IsGrounded)
            {
                return false;
            }

            // Ensure we have input to test
            if (mMotionController.InputSource == null)
            {
                return false;
            }

            if (mCombatant.IsTargetLocked == false)
            {
                return false;
            }

            // Test the input
            if (_ActionAlias.Length > 0 && mMotionController.InputSource.IsJustPressed(_ActionAlias))
            {
                mHasEnteredAnimatorState = false;
                //Debug.Log("Test Activate True");
                return true;
            }

            // Return the final result
            return false;
        }

        /// <summary>
        /// Tests if the motion should continue. If it shouldn't, the motion
        /// is typically disabled
        /// </summary>
        /// <returns>Boolean that determines if the motion continues</returns>
        public override bool TestUpdate()
        {

            // Ensure we're in the animation
            if (mIsAnimatorActive)
            {
                // We could be in the transition that's getting us to the animation. So, we need
                // to ensure the transition ID is clear. That means we're in a state
                if (mMotionLayer._AnimatorTransitionID == 0)
                {
                    mHasEnteredAnimatorState = true;
                }
            }

            // Once we know we're in the animation state, we can see if it's time to exit
            if (mHasEnteredAnimatorState)
            {
                // If we're in the exit state...
                if (mExitStateID == 0 || mMotionLayer._AnimatorStateID == mExitStateID)
                {

                    // TS 22/03/18 Changes dodge to also check for the exit tag, not only the exit time. 
                    // Fixes bug with AI cancelling dodge midway through animation.
                    // If the exit time has come, and if we're in the exit state.
                    if (mMotionLayer._AnimatorStateNormalizedTime >= _ExitTime && mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.IsTag("Exit"))
                    {
                        return false;
                    }
                }

                // Check if the alias was released and if we should exit
                if (_ExitOnRelease && _ActionAlias.Length > 0 && mMotionController.InputSource.IsReleased(_ActionAlias))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called to start the specific motion. If the motion
        /// were something like 'jump', this would start the jumping process
        /// </summary>
        /// <param name="rPrevMotion">Motion that this motion is taking over from</param>
        public override bool Activate(MotionControllerMotion rPrevMotion)
        {
            //if (AISource != null) { Debug.Log("Dodge activated"); }
            SlowTimeManager.SetGlobalSpeedNormal();
            mIsChainActive = false;
            if (!SlowTimeManager.bIsAttacking)
            {
                mParameter = -1;
                mCombatant.IsDodging = false;
            }
            else
            {
                mCombatant.IsDodging = true;
            }
            
            //Otherwise, we take the input from the animator and determine motion phase from there.

            //Debug.Log(mParameter);

            mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, PHASE_START,mParameter, true);

            if (mParameter == 11 || mParameter == -1)
            {
                mCombatant.ForceActorRotation = false;
            }

            // Disable gravity if needed
            if (_DisableGravity)
            {
                mWasGravityEnabled = mActorController._IsGravityEnabled;
                mActorController.IsGravityEnabled = false;
            }

            // Return
            //SlowTimeManager.DodgeTrigger(selectedDodgeParam);
            return base.Activate(rPrevMotion);
        }

        /// <summary>
        /// Called to stop the motion. If the motion is stopable. Some motions
        /// like jump cannot be stopped early
        /// </summary>
        public override void Deactivate()
        {
            // Re-enable gravity if needed
            if (_DisableGravity)
            {
                mActorController.IsGravityEnabled = mWasGravityEnabled;
            }

            if (PlayerSource && VFXManager.VFX.isCounterGlowing) { VFXManager.CounterDeactive(); }

            if (PlayerSource) { TutorialManager.DodgeTutorialDone(); }
            mCombatant.ForceActorRotation = true;
            SlowTimeManager.SetGlobalSpeedNormal();
            mCombatant.IsDodging = false;

            // Finish the deactivation process
            base.Deactivate();
        }

        

        /// <summary>
        /// Allows the motion to modify the root-motion velocities before they are applied. 
        /// 
        /// NOTE:
        /// Be careful when removing rotations as some transitions will want rotations even 
        /// if the state they are transitioning from don't.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        /// <param name="rVelocityDelta">Root-motion linear velocity relative to the actor's forward</param>
        /// <param name="rRotationDelta">Root-motion rotational velocity</param>
        /// <returns></returns>
        public override void UpdateRootMotion(float rDeltaTime, int rUpdateIndex, ref Vector3 rVelocityDelta, ref Quaternion rRotationDelta)
        {
            mMovement = Vector3.zero;
            mRotation = Quaternion.identity;
        }

        /// <summary>
        /// Updates the motion over time. This is called by the controller
        /// every update cycle so animations and stages can be updated.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        public override void Update(float rDeltaTime, int rUpdateIndex)
        {

            if (mMotionController.InputSource.IsJustPressed(ActionAlias))
            {
                RepeatMotion();
            }

            if (mIsChainActive)
            {
                //Debug.Log("CHAIN IS ACTIVE");

                BasicMeleeAttack mBasicMeleeAttack = mMotionController.GetMotion<BasicMeleeAttack>();

                if (mMotionController.InputSource.IsJustPressed(mBasicMeleeAttack.ActionAlias))
                {
                    VFXManager.FireOneShot(3,mMotionController.gameObject.transform);
                    mChainAttackStyleIndex = Mathf.RoundToInt(Random.Range(12, 16));
                    //Debug.Log("Entering into counter with parameter " + mChainAttackStyleIndex);
                    mIsChainActive = false;
                    mMotionController.ActivateMotion(mBasicMeleeAttack, mChainAttackStyleIndex);

                        TutorialManager.IsFirstCounter = false;
                        TutorialManager.CounterTutorialDone();
                        SlowTimeManager.SetGlobalSpeedNormal();

                }
                if (mMotionController.InputSource.IsJustPressed(mBasicMeleeAttack.BreakoutActionAlias))
                {
                    if (GameManager.CurrentGameState != GameManager.GAMESTATE.INTERIOR) { return; }
                    BasicAttributes PlayerAttributes = mMotionController.gameObject.GetComponent<BasicAttributes>();

                    if (PlayerAttributes.GetAttributeValue<int>("GOLD") <= 0)
                    {
                        return;
                    }
                    TutorialManager.HeavyCounterDone();
                    mChainAttackStyleIndex = 16;
                    mIsChainActive = false;
                    FMODUnity.RuntimeManager.PlayOneShot("event:/FX/Interior/Combat/Abel/Smash Attack/Dry/Smash Attack Build",mMotionController.gameObject.transform.position);
                    mMotionController.ActivateMotion(mBasicMeleeAttack,mChainAttackStyleIndex);
                }
            }
        }

        public void RepeatMotion()
        {
            mCombatant.ForceActorRotation = true;
            //Debug.Log("Dodge resetting!");
            mIsChainActive = false;
            mCombatant.IsDodging = true;
            if (!SlowTimeManager.bIsAttacking)
            {
                mCombatant.ForceActorRotation = false;
                mCombatant.IsDodging = false;
                mParameter = PARAM_WHIFFED;
            }
            else if (AISource != null && AISource.CurrentCombatState != Elain_StateMachine.COMBATSTATE.THROW && mParameter == 0)
            {
                //Debug.Log("Player's captured combat form = " + AISource.PassCapturedCombatForm());
                mParameter = AISource.PassCapturedCombatForm();

            }
            else
            {
                //Debug.Log("AI's captured combat form = " + PlayerSource.PassCapturedCombatForm());
                mParameter = PlayerSource.PassCapturedCombatForm();
            }

            
            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START, mParameter,true);
        }

        public override void OnAnimationEvent(AnimationEvent rEvent)
        {
            if (rEvent == null) { return; }

            // If the animation is running backwards (say in response to a block), don't fire.
#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
#else
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.speed <= 0f) { return; }
#endif

            if (StringHelper.CleanString(rEvent.stringParameter) == CombatEvade.EVENT_COUNTERSTART)
            {
                if (PlayerSource)
                {
                    if (PlayerSource.GetComponent<MotionController>().ActiveMotion != PlayerSource.GetComponent<MotionController>().GetMotion<PSS_Damaged>()) { VFXManager.CounterActive(); }
                    
                    if (TutorialManager.IsFirstCounter)
                    {
                        TutorialManager.CounterTutorial();
                        TutorialManager.DodgeTutorialDone();
                        SlowTimeManager.SetGlobalSpeedSlow();
                        TutorialManager.IsFirstCounter = false;

                    }
                    BasicAttributes PlayerAttributes = mMotionController.gameObject.GetComponent<BasicAttributes>();

                    if (PlayerAttributes.GetAttributeValue<int>("GOLD") > 0 && TutorialManager.IsFirstHeavyCounter)
                    {
                        TutorialManager.HeavyCounter();
                        SlowTimeManager.SetGlobalSpeedSlow();
                        TutorialManager.IsFirstHeavyCounter = false;
                    }
                }
                
                //Debug.Log("CounterEvent TRIGGERED");
                mIsChainActive = true;
                mChainAttackStyleIndex = rEvent.intParameter;
                if (AISource != null)
                {
                    if (AISource.GateDodge)
                    {
                        BasicMeleeAttack BMA = mMotionController.GetMotion<BasicMeleeAttack>();
                        mChainAttackStyleIndex = Mathf.RoundToInt(Random.Range(12, 16));
                        Debug.Log("Entering into counter with parameter " + mChainAttackStyleIndex);
                        mIsChainActive = false;
                        mMotionController.ActivateMotion(BMA, mChainAttackStyleIndex);
                        return;
                    }
                    if (AISource.AIDodgeChain > AISource.MaxCounters)
                    {
                        AISource.PPM.StopCoroutine(AISource.PPM.VignetteLerper());
                        AISource.PPM.ResetVignIntensity();
                        return;
                    }
                    AISource.PPM.StartCoroutine(AISource.PPM.VignetteLerper());
                    BasicMeleeAttack mBasicMeleeAttack = mMotionController.GetMotion<BasicMeleeAttack>();
                    mChainAttackStyleIndex = Mathf.RoundToInt(Random.Range(12, 16));
                    //Debug.Log("Entering into counter with parameter " + mChainAttackStyleIndex);
                    mIsChainActive = false;
                    mMotionController.ActivateMotion(mBasicMeleeAttack, mChainAttackStyleIndex);
                }
            }

            else if (StringHelper.CleanString(rEvent.stringParameter) == CombatEvade.EVENT_COUNTEREND)
            {
                if (PlayerSource)
                {
                    VFXManager.CounterDeactive();
                }
                //Debug.Log("CounterEventEnd TRIGGERED");
                mIsChainActive = false;
            }
        }



        // **************************************************************************************************
        // Following properties and function only valid while editing
        // **************************************************************************************************

#if UNITY_EDITOR

            /// <summary>
            /// Allow the constraint to render it's own GUI
            /// </summary>
            /// <returns>Reports if the object's value was changed</returns>
        public override bool OnInspectorGUI()
        {
            bool lIsDirty = false;

            string lNewActionAlias = EditorGUILayout.TextField(new GUIContent("Action Alias", "Action alias that triggers the motion."), ActionAlias, GUILayout.MinWidth(30));
            if (lNewActionAlias != ActionAlias)
            {
                lIsDirty = true;
                ActionAlias = lNewActionAlias;
            }

            int lNewPhaseID = EditorGUILayout.IntField(new GUIContent("Phase ID", "Phase ID to pass to the animation during activation."), PhaseID, GUILayout.MinWidth(30));
            if (lNewPhaseID != PhaseID)
            {
                lIsDirty = true;
                PhaseID = lNewPhaseID;
            }

            GUILayout.Space(5);

            float lNewExitTime = EditorGUILayout.FloatField(new GUIContent("Exit Time", "Normalized time to exit the motion. Typically this is '1' to exit at the end of the animation we entered."), ExitTime, GUILayout.MinWidth(30));
            if (lNewExitTime != ExitTime)
            {
                lIsDirty = true;
                ExitTime = lNewExitTime;
            }

            string lNewExitState = EditorGUILayout.TextField(new GUIContent("Exit State", "Full path of the state we'll use for the Exit Time. If no path exists, we'll use the first state we enter as the default. For example: Base Layer.Utilities-SM.IdlePose"), ExitState, GUILayout.MinWidth(30));
            if (lNewExitState != ExitState)
            {
                lIsDirty = true;
                ExitState = lNewExitState;
            }

            bool lNewExitOnRelease = EditorGUILayout.Toggle(new GUIContent("Exit On Release", "Determines if we disable the motion when the input that activated the motion is released."), ExitOnRelease, GUILayout.MinWidth(30));
            if (lNewExitOnRelease != ExitOnRelease)
            {
                lIsDirty = true;
                ExitOnRelease = lNewExitOnRelease;
            }

            GUILayout.Space(5);

            bool lNewDisableGravity = EditorGUILayout.Toggle(new GUIContent("Disable Gravity", "Determines if we disable the gravity when the motion is activated."), DisableGravity, GUILayout.MinWidth(30));
            if (lNewDisableGravity != DisableGravity)
            {
                lIsDirty = true;
                DisableGravity = lNewDisableGravity;
            }

            bool lNewDisableRootMotionRotation = EditorGUILayout.Toggle(new GUIContent("Disable RM Rotation", "Determines if we disable the rotation caused by root-motion when this motion is activated."), DisableRootMotionRotation, GUILayout.MinWidth(30));
            if (lNewDisableRootMotionRotation != DisableRootMotionRotation)
            {
                lIsDirty = true;
                DisableRootMotionRotation = lNewDisableRootMotionRotation;
            }

            bool lNewDisableRootMotionMovement = EditorGUILayout.Toggle(new GUIContent("Disable RM Movement", "Determines if we disable the movement caused by root-motion when this motion is activated."), DisableRootMotionMovement, GUILayout.MinWidth(30));
            if (lNewDisableRootMotionMovement != DisableRootMotionMovement)
            {
                lIsDirty = true;
                DisableRootMotionMovement = lNewDisableRootMotionMovement;
            }

            return lIsDirty;
        }
#endif

        #region Auto-Generated
        // ************************************ START AUTO GENERATED ************************************

        /// <summary>
        /// These declarations go inside the class so you can test for which state
        /// and transitions are active. Testing hash values is much faster than strings.
        /// </summary>
        public int STATE_Start = -1;
        public int STATE_KB_m_LowKickRound_R = -1;
        public int STATE_01_StepLeft = -1;
        public int STATE_Idle = -1;
        public int STATE_02_StepRight = -1;
        public int STATE_11_BlinkEvade = -1;
        public int STATE_09_KB_m_LeanBack = -1;
        public int STATE_07_KB_m_Slip_L = -1;
        public int STATE_08_KB_m_Slip_R = -1;
        public int STATE_05_RootJump = -1;
        public int STATE_1_Whiff = -1;
        public int TRANS_AnyState_01_StepLeft = -1;
        public int TRANS_EntryState_01_StepLeft = -1;
        public int TRANS_AnyState_02_StepRight = -1;
        public int TRANS_EntryState_02_StepRight = -1;
        public int TRANS_AnyState_11_BlinkEvade = -1;
        public int TRANS_EntryState_11_BlinkEvade = -1;
        public int TRANS_AnyState_09_KB_m_LeanBack = -1;
        public int TRANS_EntryState_09_KB_m_LeanBack = -1;
        public int TRANS_AnyState_05_RootJump = -1;
        public int TRANS_EntryState_05_RootJump = -1;
        public int TRANS_AnyState_07_KB_m_Slip_L = -1;
        public int TRANS_EntryState_07_KB_m_Slip_L = -1;
        public int TRANS_AnyState_08_KB_m_Slip_R = -1;
        public int TRANS_EntryState_08_KB_m_Slip_R = -1;
        public int TRANS_AnyState_1_Whiff = -1;
        public int TRANS_EntryState_1_Whiff = -1;
        public int TRANS_01_StepLeft_Idle = -1;
        public int TRANS_02_StepRight_Idle = -1;
        public int TRANS_11_BlinkEvade_Idle = -1;
        public int TRANS_09_KB_m_LeanBack_Idle = -1;
        public int TRANS_07_KB_m_Slip_L_Idle = -1;
        public int TRANS_08_KB_m_Slip_R_Idle = -1;
        public int TRANS_05_RootJump_Idle = -1;
        public int TRANS_1_Whiff_Idle = -1;

        /// <summary>
        /// Determines if we're using auto-generated code
        /// </summary>
        public override bool HasAutoGeneratedCode
        {
            get { return true; }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsInMotionState
        {
            get
            {
                int lStateID = mMotionLayer._AnimatorStateID;
                int lTransitionID = mMotionLayer._AnimatorTransitionID;

                if (lTransitionID == 0)
                {
                    if (lStateID == STATE_Start) { return true; }
                    if (lStateID == STATE_KB_m_LowKickRound_R) { return true; }
                    if (lStateID == STATE_01_StepLeft) { return true; }
                    if (lStateID == STATE_Idle) { return true; }
                    if (lStateID == STATE_02_StepRight) { return true; }
                    if (lStateID == STATE_11_BlinkEvade) { return true; }
                    if (lStateID == STATE_09_KB_m_LeanBack) { return true; }
                    if (lStateID == STATE_07_KB_m_Slip_L) { return true; }
                    if (lStateID == STATE_08_KB_m_Slip_R) { return true; }
                    if (lStateID == STATE_05_RootJump) { return true; }
                    if (lStateID == STATE_1_Whiff) { return true; }
                }

                if (lTransitionID == TRANS_AnyState_01_StepLeft) { return true; }
                if (lTransitionID == TRANS_EntryState_01_StepLeft) { return true; }
                if (lTransitionID == TRANS_AnyState_02_StepRight) { return true; }
                if (lTransitionID == TRANS_EntryState_02_StepRight) { return true; }
                if (lTransitionID == TRANS_AnyState_11_BlinkEvade) { return true; }
                if (lTransitionID == TRANS_EntryState_11_BlinkEvade) { return true; }
                if (lTransitionID == TRANS_AnyState_09_KB_m_LeanBack) { return true; }
                if (lTransitionID == TRANS_EntryState_09_KB_m_LeanBack) { return true; }
                if (lTransitionID == TRANS_AnyState_05_RootJump) { return true; }
                if (lTransitionID == TRANS_EntryState_05_RootJump) { return true; }
                if (lTransitionID == TRANS_AnyState_07_KB_m_Slip_L) { return true; }
                if (lTransitionID == TRANS_EntryState_07_KB_m_Slip_L) { return true; }
                if (lTransitionID == TRANS_AnyState_08_KB_m_Slip_R) { return true; }
                if (lTransitionID == TRANS_EntryState_08_KB_m_Slip_R) { return true; }
                if (lTransitionID == TRANS_AnyState_1_Whiff) { return true; }
                if (lTransitionID == TRANS_EntryState_1_Whiff) { return true; }
                if (lTransitionID == TRANS_AnyState_02_StepRight) { return true; }
                if (lTransitionID == TRANS_EntryState_02_StepRight) { return true; }
                if (lTransitionID == TRANS_AnyState_07_KB_m_Slip_L) { return true; }
                if (lTransitionID == TRANS_EntryState_07_KB_m_Slip_L) { return true; }
                if (lTransitionID == TRANS_AnyState_09_KB_m_LeanBack) { return true; }
                if (lTransitionID == TRANS_EntryState_09_KB_m_LeanBack) { return true; }
                if (lTransitionID == TRANS_AnyState_01_StepLeft) { return true; }
                if (lTransitionID == TRANS_EntryState_01_StepLeft) { return true; }
                if (lTransitionID == TRANS_01_StepLeft_Idle) { return true; }
                if (lTransitionID == TRANS_02_StepRight_Idle) { return true; }
                if (lTransitionID == TRANS_11_BlinkEvade_Idle) { return true; }
                if (lTransitionID == TRANS_09_KB_m_LeanBack_Idle) { return true; }
                if (lTransitionID == TRANS_07_KB_m_Slip_L_Idle) { return true; }
                if (lTransitionID == TRANS_08_KB_m_Slip_R_Idle) { return true; }
                if (lTransitionID == TRANS_05_RootJump_Idle) { return true; }
                if (lTransitionID == TRANS_1_Whiff_Idle) { return true; }
                return false;
            }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID)
        {
            if (rStateID == STATE_Start) { return true; }
            if (rStateID == STATE_KB_m_LowKickRound_R) { return true; }
            if (rStateID == STATE_01_StepLeft) { return true; }
            if (rStateID == STATE_Idle) { return true; }
            if (rStateID == STATE_02_StepRight) { return true; }
            if (rStateID == STATE_11_BlinkEvade) { return true; }
            if (rStateID == STATE_09_KB_m_LeanBack) { return true; }
            if (rStateID == STATE_07_KB_m_Slip_L) { return true; }
            if (rStateID == STATE_08_KB_m_Slip_R) { return true; }
            if (rStateID == STATE_05_RootJump) { return true; }
            if (rStateID == STATE_1_Whiff) { return true; }
            return false;
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID, int rTransitionID)
        {
            if (rTransitionID == 0)
            {
                if (rStateID == STATE_Start) { return true; }
                if (rStateID == STATE_KB_m_LowKickRound_R) { return true; }
                if (rStateID == STATE_01_StepLeft) { return true; }
                if (rStateID == STATE_Idle) { return true; }
                if (rStateID == STATE_02_StepRight) { return true; }
                if (rStateID == STATE_11_BlinkEvade) { return true; }
                if (rStateID == STATE_09_KB_m_LeanBack) { return true; }
                if (rStateID == STATE_07_KB_m_Slip_L) { return true; }
                if (rStateID == STATE_08_KB_m_Slip_R) { return true; }
                if (rStateID == STATE_05_RootJump) { return true; }
                if (rStateID == STATE_1_Whiff) { return true; }
            }

            if (rTransitionID == TRANS_AnyState_01_StepLeft) { return true; }
            if (rTransitionID == TRANS_EntryState_01_StepLeft) { return true; }
            if (rTransitionID == TRANS_AnyState_02_StepRight) { return true; }
            if (rTransitionID == TRANS_EntryState_02_StepRight) { return true; }
            if (rTransitionID == TRANS_AnyState_11_BlinkEvade) { return true; }
            if (rTransitionID == TRANS_EntryState_11_BlinkEvade) { return true; }
            if (rTransitionID == TRANS_AnyState_09_KB_m_LeanBack) { return true; }
            if (rTransitionID == TRANS_EntryState_09_KB_m_LeanBack) { return true; }
            if (rTransitionID == TRANS_AnyState_05_RootJump) { return true; }
            if (rTransitionID == TRANS_EntryState_05_RootJump) { return true; }
            if (rTransitionID == TRANS_AnyState_07_KB_m_Slip_L) { return true; }
            if (rTransitionID == TRANS_EntryState_07_KB_m_Slip_L) { return true; }
            if (rTransitionID == TRANS_AnyState_08_KB_m_Slip_R) { return true; }
            if (rTransitionID == TRANS_EntryState_08_KB_m_Slip_R) { return true; }
            if (rTransitionID == TRANS_AnyState_1_Whiff) { return true; }
            if (rTransitionID == TRANS_EntryState_1_Whiff) { return true; }
            if (rTransitionID == TRANS_AnyState_02_StepRight) { return true; }
            if (rTransitionID == TRANS_EntryState_02_StepRight) { return true; }
            if (rTransitionID == TRANS_AnyState_07_KB_m_Slip_L) { return true; }
            if (rTransitionID == TRANS_EntryState_07_KB_m_Slip_L) { return true; }
            if (rTransitionID == TRANS_AnyState_09_KB_m_LeanBack) { return true; }
            if (rTransitionID == TRANS_EntryState_09_KB_m_LeanBack) { return true; }
            if (rTransitionID == TRANS_AnyState_01_StepLeft) { return true; }
            if (rTransitionID == TRANS_EntryState_01_StepLeft) { return true; }
            if (rTransitionID == TRANS_01_StepLeft_Idle) { return true; }
            if (rTransitionID == TRANS_02_StepRight_Idle) { return true; }
            if (rTransitionID == TRANS_11_BlinkEvade_Idle) { return true; }
            if (rTransitionID == TRANS_09_KB_m_LeanBack_Idle) { return true; }
            if (rTransitionID == TRANS_07_KB_m_Slip_L_Idle) { return true; }
            if (rTransitionID == TRANS_08_KB_m_Slip_R_Idle) { return true; }
            if (rTransitionID == TRANS_05_RootJump_Idle) { return true; }
            if (rTransitionID == TRANS_1_Whiff_Idle) { return true; }
            return false;
        }

        /// <summary>
        /// Preprocess any animator data so the motion can use it later
        /// </summary>
        public override void LoadAnimatorData()
        {
            string lLayer = mMotionController.Animator.GetLayerName(mMotionLayer._AnimatorLayerIndex);
            TRANS_AnyState_01_StepLeft = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.01_StepLeft");
            TRANS_EntryState_01_StepLeft = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.01_StepLeft");
            TRANS_AnyState_02_StepRight = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.02_StepRight");
            TRANS_EntryState_02_StepRight = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.02_StepRight");
            TRANS_AnyState_11_BlinkEvade = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.11_Blink-Evade");
            TRANS_EntryState_11_BlinkEvade = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.11_Blink-Evade");
            TRANS_AnyState_09_KB_m_LeanBack = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.09_KB_m_LeanBack");
            TRANS_EntryState_09_KB_m_LeanBack = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.09_KB_m_LeanBack");
            TRANS_AnyState_05_RootJump = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.05_RootJump");
            TRANS_EntryState_05_RootJump = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.05_RootJump");
            TRANS_AnyState_07_KB_m_Slip_L = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.07_KB_m_Slip_L");
            TRANS_EntryState_07_KB_m_Slip_L = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.07_KB_m_Slip_L");
            TRANS_AnyState_08_KB_m_Slip_R = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.08_KB_m_Slip_R");
            TRANS_EntryState_08_KB_m_Slip_R = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.08_KB_m_Slip_R");
            TRANS_AnyState_1_Whiff = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.-1_Whiff");
            TRANS_EntryState_1_Whiff = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.-1_Whiff");
            TRANS_AnyState_02_StepRight = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.02_StepRight");
            TRANS_EntryState_02_StepRight = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.02_StepRight");
            TRANS_AnyState_07_KB_m_Slip_L = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.07_KB_m_Slip_L");
            TRANS_EntryState_07_KB_m_Slip_L = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.07_KB_m_Slip_L");
            TRANS_AnyState_09_KB_m_LeanBack = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.09_KB_m_LeanBack");
            TRANS_EntryState_09_KB_m_LeanBack = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.09_KB_m_LeanBack");
            TRANS_AnyState_01_StepLeft = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".CombatEvade-SM.01_StepLeft");
            TRANS_EntryState_01_StepLeft = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".CombatEvade-SM.01_StepLeft");
            STATE_Start = mMotionController.AddAnimatorName("" + lLayer + ".Start");
            STATE_KB_m_LowKickRound_R = mMotionController.AddAnimatorName("" + lLayer + ".KB_m_LowKickRound_R");
            STATE_01_StepLeft = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.01_StepLeft");
            TRANS_01_StepLeft_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.01_StepLeft -> " + lLayer + ".CombatEvade-SM.Idle");
            STATE_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.Idle");
            STATE_02_StepRight = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.02_StepRight");
            TRANS_02_StepRight_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.02_StepRight -> " + lLayer + ".CombatEvade-SM.Idle");
            STATE_11_BlinkEvade = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.11_Blink-Evade");
            TRANS_11_BlinkEvade_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.11_Blink-Evade -> " + lLayer + ".CombatEvade-SM.Idle");
            STATE_09_KB_m_LeanBack = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.09_KB_m_LeanBack");
            TRANS_09_KB_m_LeanBack_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.09_KB_m_LeanBack -> " + lLayer + ".CombatEvade-SM.Idle");
            STATE_07_KB_m_Slip_L = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.07_KB_m_Slip_L");
            TRANS_07_KB_m_Slip_L_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.07_KB_m_Slip_L -> " + lLayer + ".CombatEvade-SM.Idle");
            STATE_08_KB_m_Slip_R = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.08_KB_m_Slip_R");
            TRANS_08_KB_m_Slip_R_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.08_KB_m_Slip_R -> " + lLayer + ".CombatEvade-SM.Idle");
            STATE_05_RootJump = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.05_RootJump");
            TRANS_05_RootJump_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.05_RootJump -> " + lLayer + ".CombatEvade-SM.Idle");
            STATE_1_Whiff = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.-1_Whiff");
            TRANS_1_Whiff_Idle = mMotionController.AddAnimatorName("" + lLayer + ".CombatEvade-SM.-1_Whiff -> " + lLayer + ".CombatEvade-SM.Idle");
        }

#if UNITY_EDITOR

        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
        protected override void CreateStateMachine()
        {
            int rLayerIndex = mMotionLayer._AnimatorLayerIndex;
            MotionController rMotionController = mMotionController;

            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_108212 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "CombatEvade-SM");
            if (lSSM_108212 == null) { lSSM_108212 = lLayerStateMachine.AddStateMachine("CombatEvade-SM", new Vector3(672, -228, 0)); }

            UnityEditor.Animations.AnimatorState lState_108730 = MotionControllerMotion.EditorFindState(lSSM_108212, "01_StepLeft");
            if (lState_108730 == null) { lState_108730 = lSSM_108212.AddState("01_StepLeft", new Vector3(240, 36, 0)); }
            lState_108730.speed = 1f;
            lState_108730.mirror = false;
            lState_108730.tag = "";
            lState_108730.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Dodge_L");

            UnityEditor.Animations.AnimatorState lState_109892 = MotionControllerMotion.EditorFindState(lSSM_108212, "Idle");
            if (lState_109892 == null) { lState_109892 = lSSM_108212.AddState("Idle", new Vector3(468, 36, 0)); }
            lState_109892.speed = 1f;
            lState_109892.mirror = false;
            lState_109892.tag = "Exit";
            lState_109892.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/RPG Character Animation Pack/Animations/Unarmed/RPG-Character@Unarmed-IdlePose.FBX", "Unarmed-Idle");

            UnityEditor.Animations.AnimatorState lState_108732 = MotionControllerMotion.EditorFindState(lSSM_108212, "02_StepRight");
            if (lState_108732 == null) { lState_108732 = lSSM_108212.AddState("02_StepRight", new Vector3(696, 24, 0)); }
            lState_108732.speed = 1f;
            lState_108732.mirror = false;
            lState_108732.tag = "";
            lState_108732.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Dodge_R");

            UnityEditor.Animations.AnimatorState lState_108794 = MotionControllerMotion.EditorFindState(lSSM_108212, "11_Blink-Evade");
            if (lState_108794 == null) { lState_108794 = lSSM_108212.AddState("11_Blink-Evade", new Vector3(804, 204, 0)); }
            lState_108794.speed = 0.8f;
            lState_108794.mirror = false;
            lState_108794.tag = "";
            lState_108794.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Teleportation/Front Twist Flip (1).fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_108798 = MotionControllerMotion.EditorFindState(lSSM_108212, "09_KB_m_LeanBack");
            if (lState_108798 == null) { lState_108798 = lSSM_108212.AddState("09_KB_m_LeanBack", new Vector3(468, 228, 0)); }
            lState_108798.speed = 1f;
            lState_108798.mirror = false;
            lState_108798.tag = "";
            lState_108798.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Blocks.fbx", "KB_p_Duck");

            UnityEditor.Animations.AnimatorState lState_108802 = MotionControllerMotion.EditorFindState(lSSM_108212, "07_KB_m_Slip_L");
            if (lState_108802 == null) { lState_108802 = lSSM_108212.AddState("07_KB_m_Slip_L", new Vector3(240, -24, 0)); }
            lState_108802.speed = 1f;
            lState_108802.mirror = false;
            lState_108802.tag = "";
            lState_108802.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Blocks.fbx", "KB_m_Duck_L");

            UnityEditor.Animations.AnimatorState lState_108804 = MotionControllerMotion.EditorFindState(lSSM_108212, "08_KB_m_Slip_R");
            if (lState_108804 == null) { lState_108804 = lSSM_108212.AddState("08_KB_m_Slip_R", new Vector3(696, -24, 0)); }
            lState_108804.speed = 1f;
            lState_108804.mirror = false;
            lState_108804.tag = "";
            lState_108804.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Blocks.fbx", "KB_m_Duck_R");

            UnityEditor.Animations.AnimatorState lState_108800 = MotionControllerMotion.EditorFindState(lSSM_108212, "05_RootJump");
            if (lState_108800 == null) { lState_108800 = lSSM_108212.AddState("05_RootJump", new Vector3(468, -132, 0)); }
            lState_108800.speed = 1f;
            lState_108800.mirror = false;
            lState_108800.tag = "";
            lState_108800.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/RPG Character Animation Pack/Animations/Unarmed/RPG-Character@Unarmed-EvadeHop.FBX", "Unarmed-Jump");

            UnityEditor.Animations.AnimatorState lState_108806 = MotionControllerMotion.EditorFindState(lSSM_108212, "-1_Whiff");
            if (lState_108806 == null) { lState_108806 = lSSM_108212.AddState("-1_Whiff", new Vector3(132, 204, 0)); }
            lState_108806.speed = 0.8f;
            lState_108806.mirror = false;
            lState_108806.tag = "";
            lState_108806.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Leg Sweep.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108432 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108730, 0);
            if (lAnyTransition_108432 == null) { lAnyTransition_108432 = lLayerStateMachine.AddAnyStateTransition(lState_108730); }
            lAnyTransition_108432.isExit = false;
            lAnyTransition_108432.hasExitTime = false;
            lAnyTransition_108432.hasFixedDuration = true;
            lAnyTransition_108432.exitTime = 0f;
            lAnyTransition_108432.duration = 0f;
            lAnyTransition_108432.offset = 0.04032839f;
            lAnyTransition_108432.mute = false;
            lAnyTransition_108432.solo = false;
            lAnyTransition_108432.canTransitionToSelf = false;
            lAnyTransition_108432.orderedInterruption = true;
            lAnyTransition_108432.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108432.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108432.RemoveCondition(lAnyTransition_108432.conditions[i]); }
            lAnyTransition_108432.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108432.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 110f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108434 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108732, 0);
            if (lAnyTransition_108434 == null) { lAnyTransition_108434 = lLayerStateMachine.AddAnyStateTransition(lState_108732); }
            lAnyTransition_108434.isExit = false;
            lAnyTransition_108434.hasExitTime = false;
            lAnyTransition_108434.hasFixedDuration = true;
            lAnyTransition_108434.exitTime = 1.647726E-08f;
            lAnyTransition_108434.duration = 0f;
            lAnyTransition_108434.offset = 0f;
            lAnyTransition_108434.mute = false;
            lAnyTransition_108434.solo = false;
            lAnyTransition_108434.canTransitionToSelf = false;
            lAnyTransition_108434.orderedInterruption = true;
            lAnyTransition_108434.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108434.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108434.RemoveCondition(lAnyTransition_108434.conditions[i]); }
            lAnyTransition_108434.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108434.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108500 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108794, 0);
            if (lAnyTransition_108500 == null) { lAnyTransition_108500 = lLayerStateMachine.AddAnyStateTransition(lState_108794); }
            lAnyTransition_108500.isExit = false;
            lAnyTransition_108500.hasExitTime = false;
            lAnyTransition_108500.hasFixedDuration = true;
            lAnyTransition_108500.exitTime = 4.678276E-08f;
            lAnyTransition_108500.duration = 0.04999995f;
            lAnyTransition_108500.offset = 0.2780116f;
            lAnyTransition_108500.mute = false;
            lAnyTransition_108500.solo = false;
            lAnyTransition_108500.canTransitionToSelf = false;
            lAnyTransition_108500.orderedInterruption = true;
            lAnyTransition_108500.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108500.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108500.RemoveCondition(lAnyTransition_108500.conditions[i]); }
            lAnyTransition_108500.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108500.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 11f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108504 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108798, 0);
            if (lAnyTransition_108504 == null) { lAnyTransition_108504 = lLayerStateMachine.AddAnyStateTransition(lState_108798); }
            lAnyTransition_108504.isExit = false;
            lAnyTransition_108504.hasExitTime = false;
            lAnyTransition_108504.hasFixedDuration = true;
            lAnyTransition_108504.exitTime = 1.549597E-08f;
            lAnyTransition_108504.duration = 0f;
            lAnyTransition_108504.offset = 0f;
            lAnyTransition_108504.mute = false;
            lAnyTransition_108504.solo = false;
            lAnyTransition_108504.canTransitionToSelf = false;
            lAnyTransition_108504.orderedInterruption = true;
            lAnyTransition_108504.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108504.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108504.RemoveCondition(lAnyTransition_108504.conditions[i]); }
            lAnyTransition_108504.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108504.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 103f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108506 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108800, 0);
            if (lAnyTransition_108506 == null) { lAnyTransition_108506 = lLayerStateMachine.AddAnyStateTransition(lState_108800); }
            lAnyTransition_108506.isExit = false;
            lAnyTransition_108506.hasExitTime = false;
            lAnyTransition_108506.hasFixedDuration = true;
            lAnyTransition_108506.exitTime = 0f;
            lAnyTransition_108506.duration = 0f;
            lAnyTransition_108506.offset = 0f;
            lAnyTransition_108506.mute = false;
            lAnyTransition_108506.solo = false;
            lAnyTransition_108506.canTransitionToSelf = false;
            lAnyTransition_108506.orderedInterruption = true;
            lAnyTransition_108506.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108506.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108506.RemoveCondition(lAnyTransition_108506.conditions[i]); }
            lAnyTransition_108506.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108506.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 112f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108508 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108802, 0);
            if (lAnyTransition_108508 == null) { lAnyTransition_108508 = lLayerStateMachine.AddAnyStateTransition(lState_108802); }
            lAnyTransition_108508.isExit = false;
            lAnyTransition_108508.hasExitTime = false;
            lAnyTransition_108508.hasFixedDuration = true;
            lAnyTransition_108508.exitTime = 0f;
            lAnyTransition_108508.duration = 0f;
            lAnyTransition_108508.offset = 0f;
            lAnyTransition_108508.mute = false;
            lAnyTransition_108508.solo = false;
            lAnyTransition_108508.canTransitionToSelf = false;
            lAnyTransition_108508.orderedInterruption = true;
            lAnyTransition_108508.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108508.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108508.RemoveCondition(lAnyTransition_108508.conditions[i]); }
            lAnyTransition_108508.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108508.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108510 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108804, 0);
            if (lAnyTransition_108510 == null) { lAnyTransition_108510 = lLayerStateMachine.AddAnyStateTransition(lState_108804); }
            lAnyTransition_108510.isExit = false;
            lAnyTransition_108510.hasExitTime = false;
            lAnyTransition_108510.hasFixedDuration = true;
            lAnyTransition_108510.exitTime = 0.1268878f;
            lAnyTransition_108510.duration = 0f;
            lAnyTransition_108510.offset = 0f;
            lAnyTransition_108510.mute = false;
            lAnyTransition_108510.solo = false;
            lAnyTransition_108510.canTransitionToSelf = false;
            lAnyTransition_108510.orderedInterruption = true;
            lAnyTransition_108510.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108510.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108510.RemoveCondition(lAnyTransition_108510.conditions[i]); }
            lAnyTransition_108510.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108510.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 102f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108512 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108806, 0);
            if (lAnyTransition_108512 == null) { lAnyTransition_108512 = lLayerStateMachine.AddAnyStateTransition(lState_108806); }
            lAnyTransition_108512.isExit = false;
            lAnyTransition_108512.hasExitTime = false;
            lAnyTransition_108512.hasFixedDuration = true;
            lAnyTransition_108512.exitTime = 0.7070456f;
            lAnyTransition_108512.duration = 0.1961262f;
            lAnyTransition_108512.offset = 0.4409941f;
            lAnyTransition_108512.mute = false;
            lAnyTransition_108512.solo = false;
            lAnyTransition_108512.canTransitionToSelf = false;
            lAnyTransition_108512.orderedInterruption = true;
            lAnyTransition_108512.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108512.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108512.RemoveCondition(lAnyTransition_108512.conditions[i]); }
            lAnyTransition_108512.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108512.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, -1f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108524 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108732, 1);
            if (lAnyTransition_108524 == null) { lAnyTransition_108524 = lLayerStateMachine.AddAnyStateTransition(lState_108732); }
            lAnyTransition_108524.isExit = false;
            lAnyTransition_108524.hasExitTime = false;
            lAnyTransition_108524.hasFixedDuration = true;
            lAnyTransition_108524.exitTime = 0f;
            lAnyTransition_108524.duration = 0f;
            lAnyTransition_108524.offset = 0f;
            lAnyTransition_108524.mute = false;
            lAnyTransition_108524.solo = false;
            lAnyTransition_108524.canTransitionToSelf = false;
            lAnyTransition_108524.orderedInterruption = true;
            lAnyTransition_108524.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108524.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108524.RemoveCondition(lAnyTransition_108524.conditions[i]); }
            lAnyTransition_108524.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108524.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 101f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108526 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108802, 1);
            if (lAnyTransition_108526 == null) { lAnyTransition_108526 = lLayerStateMachine.AddAnyStateTransition(lState_108802); }
            lAnyTransition_108526.isExit = false;
            lAnyTransition_108526.hasExitTime = false;
            lAnyTransition_108526.hasFixedDuration = true;
            lAnyTransition_108526.exitTime = 2.211542E-08f;
            lAnyTransition_108526.duration = 0f;
            lAnyTransition_108526.offset = 0f;
            lAnyTransition_108526.mute = false;
            lAnyTransition_108526.solo = false;
            lAnyTransition_108526.canTransitionToSelf = false;
            lAnyTransition_108526.orderedInterruption = true;
            lAnyTransition_108526.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108526.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108526.RemoveCondition(lAnyTransition_108526.conditions[i]); }
            lAnyTransition_108526.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108526.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 111f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108528 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108798, 1);
            if (lAnyTransition_108528 == null) { lAnyTransition_108528 = lLayerStateMachine.AddAnyStateTransition(lState_108798); }
            lAnyTransition_108528.isExit = false;
            lAnyTransition_108528.hasExitTime = false;
            lAnyTransition_108528.hasFixedDuration = true;
            lAnyTransition_108528.exitTime = 0f;
            lAnyTransition_108528.duration = 0f;
            lAnyTransition_108528.offset = 0f;
            lAnyTransition_108528.mute = false;
            lAnyTransition_108528.solo = false;
            lAnyTransition_108528.canTransitionToSelf = false;
            lAnyTransition_108528.orderedInterruption = true;
            lAnyTransition_108528.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108528.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108528.RemoveCondition(lAnyTransition_108528.conditions[i]); }
            lAnyTransition_108528.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108528.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 113f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108530 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108730, 1);
            if (lAnyTransition_108530 == null) { lAnyTransition_108530 = lLayerStateMachine.AddAnyStateTransition(lState_108730); }
            lAnyTransition_108530.isExit = false;
            lAnyTransition_108530.hasExitTime = false;
            lAnyTransition_108530.hasFixedDuration = true;
            lAnyTransition_108530.exitTime = 0f;
            lAnyTransition_108530.duration = 0f;
            lAnyTransition_108530.offset = 0f;
            lAnyTransition_108530.mute = false;
            lAnyTransition_108530.solo = false;
            lAnyTransition_108530.canTransitionToSelf = false;
            lAnyTransition_108530.orderedInterruption = true;
            lAnyTransition_108530.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108530.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108530.RemoveCondition(lAnyTransition_108530.conditions[i]); }
            lAnyTransition_108530.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108530.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 105f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lTransition_109894 = MotionControllerMotion.EditorFindTransition(lState_108730, lState_109892, 0);
            if (lTransition_109894 == null) { lTransition_109894 = lState_108730.AddTransition(lState_109892); }
            lTransition_109894.isExit = false;
            lTransition_109894.hasExitTime = true;
            lTransition_109894.hasFixedDuration = true;
            lTransition_109894.exitTime = 0.3485394f;
            lTransition_109894.duration = 0.01554191f;
            lTransition_109894.offset = 1.36198f;
            lTransition_109894.mute = false;
            lTransition_109894.solo = false;
            lTransition_109894.canTransitionToSelf = true;
            lTransition_109894.orderedInterruption = true;
            lTransition_109894.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109894.conditions.Length - 1; i >= 0; i--) { lTransition_109894.RemoveCondition(lTransition_109894.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109896 = MotionControllerMotion.EditorFindTransition(lState_108732, lState_109892, 0);
            if (lTransition_109896 == null) { lTransition_109896 = lState_108732.AddTransition(lState_109892); }
            lTransition_109896.isExit = false;
            lTransition_109896.hasExitTime = true;
            lTransition_109896.hasFixedDuration = true;
            lTransition_109896.exitTime = 0.361057f;
            lTransition_109896.duration = 0.03010701f;
            lTransition_109896.offset = 0f;
            lTransition_109896.mute = false;
            lTransition_109896.solo = false;
            lTransition_109896.canTransitionToSelf = true;
            lTransition_109896.orderedInterruption = true;
            lTransition_109896.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109896.conditions.Length - 1; i >= 0; i--) { lTransition_109896.RemoveCondition(lTransition_109896.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109898 = MotionControllerMotion.EditorFindTransition(lState_108794, lState_109892, 0);
            if (lTransition_109898 == null) { lTransition_109898 = lState_108794.AddTransition(lState_109892); }
            lTransition_109898.isExit = false;
            lTransition_109898.hasExitTime = true;
            lTransition_109898.hasFixedDuration = true;
            lTransition_109898.exitTime = 0.7224014f;
            lTransition_109898.duration = 0.1009254f;
            lTransition_109898.offset = 28.12615f;
            lTransition_109898.mute = false;
            lTransition_109898.solo = false;
            lTransition_109898.canTransitionToSelf = true;
            lTransition_109898.orderedInterruption = true;
            lTransition_109898.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109898.conditions.Length - 1; i >= 0; i--) { lTransition_109898.RemoveCondition(lTransition_109898.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109900 = MotionControllerMotion.EditorFindTransition(lState_108798, lState_109892, 0);
            if (lTransition_109900 == null) { lTransition_109900 = lState_108798.AddTransition(lState_109892); }
            lTransition_109900.isExit = false;
            lTransition_109900.hasExitTime = true;
            lTransition_109900.hasFixedDuration = true;
            lTransition_109900.exitTime = 0.7457627f;
            lTransition_109900.duration = 0.25f;
            lTransition_109900.offset = 0f;
            lTransition_109900.mute = false;
            lTransition_109900.solo = false;
            lTransition_109900.canTransitionToSelf = true;
            lTransition_109900.orderedInterruption = true;
            lTransition_109900.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109900.conditions.Length - 1; i >= 0; i--) { lTransition_109900.RemoveCondition(lTransition_109900.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109902 = MotionControllerMotion.EditorFindTransition(lState_108802, lState_109892, 0);
            if (lTransition_109902 == null) { lTransition_109902 = lState_108802.AddTransition(lState_109892); }
            lTransition_109902.isExit = false;
            lTransition_109902.hasExitTime = true;
            lTransition_109902.hasFixedDuration = true;
            lTransition_109902.exitTime = 0.6938776f;
            lTransition_109902.duration = 0.25f;
            lTransition_109902.offset = 0f;
            lTransition_109902.mute = false;
            lTransition_109902.solo = false;
            lTransition_109902.canTransitionToSelf = true;
            lTransition_109902.orderedInterruption = true;
            lTransition_109902.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109902.conditions.Length - 1; i >= 0; i--) { lTransition_109902.RemoveCondition(lTransition_109902.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109904 = MotionControllerMotion.EditorFindTransition(lState_108804, lState_109892, 0);
            if (lTransition_109904 == null) { lTransition_109904 = lState_108804.AddTransition(lState_109892); }
            lTransition_109904.isExit = false;
            lTransition_109904.hasExitTime = true;
            lTransition_109904.hasFixedDuration = true;
            lTransition_109904.exitTime = 0.7321429f;
            lTransition_109904.duration = 0.25f;
            lTransition_109904.offset = 0f;
            lTransition_109904.mute = false;
            lTransition_109904.solo = false;
            lTransition_109904.canTransitionToSelf = true;
            lTransition_109904.orderedInterruption = true;
            lTransition_109904.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109904.conditions.Length - 1; i >= 0; i--) { lTransition_109904.RemoveCondition(lTransition_109904.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109906 = MotionControllerMotion.EditorFindTransition(lState_108800, lState_109892, 0);
            if (lTransition_109906 == null) { lTransition_109906 = lState_108800.AddTransition(lState_109892); }
            lTransition_109906.isExit = false;
            lTransition_109906.hasExitTime = true;
            lTransition_109906.hasFixedDuration = true;
            lTransition_109906.exitTime = 0.918029f;
            lTransition_109906.duration = 0.04744691f;
            lTransition_109906.offset = 2.242554f;
            lTransition_109906.mute = false;
            lTransition_109906.solo = false;
            lTransition_109906.canTransitionToSelf = true;
            lTransition_109906.orderedInterruption = true;
            lTransition_109906.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109906.conditions.Length - 1; i >= 0; i--) { lTransition_109906.RemoveCondition(lTransition_109906.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109908 = MotionControllerMotion.EditorFindTransition(lState_108806, lState_109892, 0);
            if (lTransition_109908 == null) { lTransition_109908 = lState_108806.AddTransition(lState_109892); }
            lTransition_109908.isExit = false;
            lTransition_109908.hasExitTime = true;
            lTransition_109908.hasFixedDuration = true;
            lTransition_109908.exitTime = 0.9878153f;
            lTransition_109908.duration = 0.0343138f;
            lTransition_109908.offset = 47.69421f;
            lTransition_109908.mute = false;
            lTransition_109908.solo = false;
            lTransition_109908.canTransitionToSelf = true;
            lTransition_109908.orderedInterruption = true;
            lTransition_109908.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109908.conditions.Length - 1; i >= 0; i--) { lTransition_109908.RemoveCondition(lTransition_109908.conditions[i]); }


            // Run any post processing after creating the state machine
            OnStateMachineCreated();
        }

#endif

        // ************************************ END AUTO GENERATED ************************************
        #endregion


        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
#if UNITY_EDITOR
        public static void ExtendBasicMotion(MotionController rMotionController, int rLayerIndex)
        {
            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_108212 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "CombatEvade-SM");
            if (lSSM_108212 == null) { lSSM_108212 = lLayerStateMachine.AddStateMachine("CombatEvade-SM", new Vector3(672, -228, 0)); }

            UnityEditor.Animations.AnimatorState lState_108730 = MotionControllerMotion.EditorFindState(lSSM_108212, "01_StepLeft");
            if (lState_108730 == null) { lState_108730 = lSSM_108212.AddState("01_StepLeft", new Vector3(240, 36, 0)); }
            lState_108730.speed = 1f;
            lState_108730.mirror = false;
            lState_108730.tag = "";
            lState_108730.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Dodge_L");

            UnityEditor.Animations.AnimatorState lState_109892 = MotionControllerMotion.EditorFindState(lSSM_108212, "Idle");
            if (lState_109892 == null) { lState_109892 = lSSM_108212.AddState("Idle", new Vector3(468, 36, 0)); }
            lState_109892.speed = 1f;
            lState_109892.mirror = false;
            lState_109892.tag = "Exit";
            lState_109892.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/RPG Character Animation Pack/Animations/Unarmed/RPG-Character@Unarmed-IdlePose.FBX", "Unarmed-Idle");

            UnityEditor.Animations.AnimatorState lState_108732 = MotionControllerMotion.EditorFindState(lSSM_108212, "02_StepRight");
            if (lState_108732 == null) { lState_108732 = lSSM_108212.AddState("02_StepRight", new Vector3(696, 24, 0)); }
            lState_108732.speed = 1f;
            lState_108732.mirror = false;
            lState_108732.tag = "";
            lState_108732.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Dodge_R");

            UnityEditor.Animations.AnimatorState lState_108794 = MotionControllerMotion.EditorFindState(lSSM_108212, "11_Blink-Evade");
            if (lState_108794 == null) { lState_108794 = lSSM_108212.AddState("11_Blink-Evade", new Vector3(804, 204, 0)); }
            lState_108794.speed = 0.8f;
            lState_108794.mirror = false;
            lState_108794.tag = "";
            lState_108794.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Teleportation/Front Twist Flip (1).fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_108798 = MotionControllerMotion.EditorFindState(lSSM_108212, "09_KB_m_LeanBack");
            if (lState_108798 == null) { lState_108798 = lSSM_108212.AddState("09_KB_m_LeanBack", new Vector3(468, 228, 0)); }
            lState_108798.speed = 1f;
            lState_108798.mirror = false;
            lState_108798.tag = "";
            lState_108798.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Blocks.fbx", "KB_p_Duck");

            UnityEditor.Animations.AnimatorState lState_108802 = MotionControllerMotion.EditorFindState(lSSM_108212, "07_KB_m_Slip_L");
            if (lState_108802 == null) { lState_108802 = lSSM_108212.AddState("07_KB_m_Slip_L", new Vector3(240, -24, 0)); }
            lState_108802.speed = 1f;
            lState_108802.mirror = false;
            lState_108802.tag = "";
            lState_108802.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Blocks.fbx", "KB_m_Duck_L");

            UnityEditor.Animations.AnimatorState lState_108804 = MotionControllerMotion.EditorFindState(lSSM_108212, "08_KB_m_Slip_R");
            if (lState_108804 == null) { lState_108804 = lSSM_108212.AddState("08_KB_m_Slip_R", new Vector3(696, -24, 0)); }
            lState_108804.speed = 1f;
            lState_108804.mirror = false;
            lState_108804.tag = "";
            lState_108804.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Blocks.fbx", "KB_m_Duck_R");

            UnityEditor.Animations.AnimatorState lState_108800 = MotionControllerMotion.EditorFindState(lSSM_108212, "05_RootJump");
            if (lState_108800 == null) { lState_108800 = lSSM_108212.AddState("05_RootJump", new Vector3(468, -132, 0)); }
            lState_108800.speed = 1f;
            lState_108800.mirror = false;
            lState_108800.tag = "";
            lState_108800.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/RPG Character Animation Pack/Animations/Unarmed/RPG-Character@Unarmed-EvadeHop.FBX", "Unarmed-Jump");

            UnityEditor.Animations.AnimatorState lState_108806 = MotionControllerMotion.EditorFindState(lSSM_108212, "-1_Whiff");
            if (lState_108806 == null) { lState_108806 = lSSM_108212.AddState("-1_Whiff", new Vector3(132, 204, 0)); }
            lState_108806.speed = 0.8f;
            lState_108806.mirror = false;
            lState_108806.tag = "";
            lState_108806.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Leg Sweep.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108432 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108730, 0);
            if (lAnyTransition_108432 == null) { lAnyTransition_108432 = lLayerStateMachine.AddAnyStateTransition(lState_108730); }
            lAnyTransition_108432.isExit = false;
            lAnyTransition_108432.hasExitTime = false;
            lAnyTransition_108432.hasFixedDuration = true;
            lAnyTransition_108432.exitTime = 0f;
            lAnyTransition_108432.duration = 0f;
            lAnyTransition_108432.offset = 0.04032839f;
            lAnyTransition_108432.mute = false;
            lAnyTransition_108432.solo = false;
            lAnyTransition_108432.canTransitionToSelf = false;
            lAnyTransition_108432.orderedInterruption = true;
            lAnyTransition_108432.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108432.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108432.RemoveCondition(lAnyTransition_108432.conditions[i]); }
            lAnyTransition_108432.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108432.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 110f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108434 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108732, 0);
            if (lAnyTransition_108434 == null) { lAnyTransition_108434 = lLayerStateMachine.AddAnyStateTransition(lState_108732); }
            lAnyTransition_108434.isExit = false;
            lAnyTransition_108434.hasExitTime = false;
            lAnyTransition_108434.hasFixedDuration = true;
            lAnyTransition_108434.exitTime = 1.647726E-08f;
            lAnyTransition_108434.duration = 0f;
            lAnyTransition_108434.offset = 0f;
            lAnyTransition_108434.mute = false;
            lAnyTransition_108434.solo = false;
            lAnyTransition_108434.canTransitionToSelf = false;
            lAnyTransition_108434.orderedInterruption = true;
            lAnyTransition_108434.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108434.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108434.RemoveCondition(lAnyTransition_108434.conditions[i]); }
            lAnyTransition_108434.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108434.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108500 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108794, 0);
            if (lAnyTransition_108500 == null) { lAnyTransition_108500 = lLayerStateMachine.AddAnyStateTransition(lState_108794); }
            lAnyTransition_108500.isExit = false;
            lAnyTransition_108500.hasExitTime = false;
            lAnyTransition_108500.hasFixedDuration = true;
            lAnyTransition_108500.exitTime = 4.678276E-08f;
            lAnyTransition_108500.duration = 0.04999995f;
            lAnyTransition_108500.offset = 0.2780116f;
            lAnyTransition_108500.mute = false;
            lAnyTransition_108500.solo = false;
            lAnyTransition_108500.canTransitionToSelf = false;
            lAnyTransition_108500.orderedInterruption = true;
            lAnyTransition_108500.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108500.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108500.RemoveCondition(lAnyTransition_108500.conditions[i]); }
            lAnyTransition_108500.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108500.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 11f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108504 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108798, 0);
            if (lAnyTransition_108504 == null) { lAnyTransition_108504 = lLayerStateMachine.AddAnyStateTransition(lState_108798); }
            lAnyTransition_108504.isExit = false;
            lAnyTransition_108504.hasExitTime = false;
            lAnyTransition_108504.hasFixedDuration = true;
            lAnyTransition_108504.exitTime = 1.549597E-08f;
            lAnyTransition_108504.duration = 0f;
            lAnyTransition_108504.offset = 0f;
            lAnyTransition_108504.mute = false;
            lAnyTransition_108504.solo = false;
            lAnyTransition_108504.canTransitionToSelf = false;
            lAnyTransition_108504.orderedInterruption = true;
            lAnyTransition_108504.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108504.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108504.RemoveCondition(lAnyTransition_108504.conditions[i]); }
            lAnyTransition_108504.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108504.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 103f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108506 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108800, 0);
            if (lAnyTransition_108506 == null) { lAnyTransition_108506 = lLayerStateMachine.AddAnyStateTransition(lState_108800); }
            lAnyTransition_108506.isExit = false;
            lAnyTransition_108506.hasExitTime = false;
            lAnyTransition_108506.hasFixedDuration = true;
            lAnyTransition_108506.exitTime = 0f;
            lAnyTransition_108506.duration = 0f;
            lAnyTransition_108506.offset = 0f;
            lAnyTransition_108506.mute = false;
            lAnyTransition_108506.solo = false;
            lAnyTransition_108506.canTransitionToSelf = false;
            lAnyTransition_108506.orderedInterruption = true;
            lAnyTransition_108506.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108506.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108506.RemoveCondition(lAnyTransition_108506.conditions[i]); }
            lAnyTransition_108506.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108506.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 112f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108508 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108802, 0);
            if (lAnyTransition_108508 == null) { lAnyTransition_108508 = lLayerStateMachine.AddAnyStateTransition(lState_108802); }
            lAnyTransition_108508.isExit = false;
            lAnyTransition_108508.hasExitTime = false;
            lAnyTransition_108508.hasFixedDuration = true;
            lAnyTransition_108508.exitTime = 0f;
            lAnyTransition_108508.duration = 0f;
            lAnyTransition_108508.offset = 0f;
            lAnyTransition_108508.mute = false;
            lAnyTransition_108508.solo = false;
            lAnyTransition_108508.canTransitionToSelf = false;
            lAnyTransition_108508.orderedInterruption = true;
            lAnyTransition_108508.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108508.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108508.RemoveCondition(lAnyTransition_108508.conditions[i]); }
            lAnyTransition_108508.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108508.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108510 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108804, 0);
            if (lAnyTransition_108510 == null) { lAnyTransition_108510 = lLayerStateMachine.AddAnyStateTransition(lState_108804); }
            lAnyTransition_108510.isExit = false;
            lAnyTransition_108510.hasExitTime = false;
            lAnyTransition_108510.hasFixedDuration = true;
            lAnyTransition_108510.exitTime = 0.1268878f;
            lAnyTransition_108510.duration = 0f;
            lAnyTransition_108510.offset = 0f;
            lAnyTransition_108510.mute = false;
            lAnyTransition_108510.solo = false;
            lAnyTransition_108510.canTransitionToSelf = false;
            lAnyTransition_108510.orderedInterruption = true;
            lAnyTransition_108510.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108510.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108510.RemoveCondition(lAnyTransition_108510.conditions[i]); }
            lAnyTransition_108510.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108510.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 102f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108512 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108806, 0);
            if (lAnyTransition_108512 == null) { lAnyTransition_108512 = lLayerStateMachine.AddAnyStateTransition(lState_108806); }
            lAnyTransition_108512.isExit = false;
            lAnyTransition_108512.hasExitTime = false;
            lAnyTransition_108512.hasFixedDuration = true;
            lAnyTransition_108512.exitTime = 0.7070456f;
            lAnyTransition_108512.duration = 0.1961262f;
            lAnyTransition_108512.offset = 0.4409941f;
            lAnyTransition_108512.mute = false;
            lAnyTransition_108512.solo = false;
            lAnyTransition_108512.canTransitionToSelf = false;
            lAnyTransition_108512.orderedInterruption = true;
            lAnyTransition_108512.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108512.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108512.RemoveCondition(lAnyTransition_108512.conditions[i]); }
            lAnyTransition_108512.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108512.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, -1f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108524 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108732, 1);
            if (lAnyTransition_108524 == null) { lAnyTransition_108524 = lLayerStateMachine.AddAnyStateTransition(lState_108732); }
            lAnyTransition_108524.isExit = false;
            lAnyTransition_108524.hasExitTime = false;
            lAnyTransition_108524.hasFixedDuration = true;
            lAnyTransition_108524.exitTime = 0f;
            lAnyTransition_108524.duration = 0f;
            lAnyTransition_108524.offset = 0f;
            lAnyTransition_108524.mute = false;
            lAnyTransition_108524.solo = false;
            lAnyTransition_108524.canTransitionToSelf = false;
            lAnyTransition_108524.orderedInterruption = true;
            lAnyTransition_108524.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108524.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108524.RemoveCondition(lAnyTransition_108524.conditions[i]); }
            lAnyTransition_108524.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108524.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 101f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108526 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108802, 1);
            if (lAnyTransition_108526 == null) { lAnyTransition_108526 = lLayerStateMachine.AddAnyStateTransition(lState_108802); }
            lAnyTransition_108526.isExit = false;
            lAnyTransition_108526.hasExitTime = false;
            lAnyTransition_108526.hasFixedDuration = true;
            lAnyTransition_108526.exitTime = 2.211542E-08f;
            lAnyTransition_108526.duration = 0f;
            lAnyTransition_108526.offset = 0f;
            lAnyTransition_108526.mute = false;
            lAnyTransition_108526.solo = false;
            lAnyTransition_108526.canTransitionToSelf = false;
            lAnyTransition_108526.orderedInterruption = true;
            lAnyTransition_108526.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108526.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108526.RemoveCondition(lAnyTransition_108526.conditions[i]); }
            lAnyTransition_108526.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108526.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 111f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108528 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108798, 1);
            if (lAnyTransition_108528 == null) { lAnyTransition_108528 = lLayerStateMachine.AddAnyStateTransition(lState_108798); }
            lAnyTransition_108528.isExit = false;
            lAnyTransition_108528.hasExitTime = false;
            lAnyTransition_108528.hasFixedDuration = true;
            lAnyTransition_108528.exitTime = 0f;
            lAnyTransition_108528.duration = 0f;
            lAnyTransition_108528.offset = 0f;
            lAnyTransition_108528.mute = false;
            lAnyTransition_108528.solo = false;
            lAnyTransition_108528.canTransitionToSelf = false;
            lAnyTransition_108528.orderedInterruption = true;
            lAnyTransition_108528.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108528.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108528.RemoveCondition(lAnyTransition_108528.conditions[i]); }
            lAnyTransition_108528.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108528.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 113f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_108530 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_108730, 1);
            if (lAnyTransition_108530 == null) { lAnyTransition_108530 = lLayerStateMachine.AddAnyStateTransition(lState_108730); }
            lAnyTransition_108530.isExit = false;
            lAnyTransition_108530.hasExitTime = false;
            lAnyTransition_108530.hasFixedDuration = true;
            lAnyTransition_108530.exitTime = 0f;
            lAnyTransition_108530.duration = 0f;
            lAnyTransition_108530.offset = 0f;
            lAnyTransition_108530.mute = false;
            lAnyTransition_108530.solo = false;
            lAnyTransition_108530.canTransitionToSelf = false;
            lAnyTransition_108530.orderedInterruption = true;
            lAnyTransition_108530.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_108530.conditions.Length - 1; i >= 0; i--) { lAnyTransition_108530.RemoveCondition(lAnyTransition_108530.conditions[i]); }
            lAnyTransition_108530.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99900f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_108530.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 105f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lTransition_109894 = MotionControllerMotion.EditorFindTransition(lState_108730, lState_109892, 0);
            if (lTransition_109894 == null) { lTransition_109894 = lState_108730.AddTransition(lState_109892); }
            lTransition_109894.isExit = false;
            lTransition_109894.hasExitTime = true;
            lTransition_109894.hasFixedDuration = true;
            lTransition_109894.exitTime = 0.3485394f;
            lTransition_109894.duration = 0.01554191f;
            lTransition_109894.offset = 1.36198f;
            lTransition_109894.mute = false;
            lTransition_109894.solo = false;
            lTransition_109894.canTransitionToSelf = true;
            lTransition_109894.orderedInterruption = true;
            lTransition_109894.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109894.conditions.Length - 1; i >= 0; i--) { lTransition_109894.RemoveCondition(lTransition_109894.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109896 = MotionControllerMotion.EditorFindTransition(lState_108732, lState_109892, 0);
            if (lTransition_109896 == null) { lTransition_109896 = lState_108732.AddTransition(lState_109892); }
            lTransition_109896.isExit = false;
            lTransition_109896.hasExitTime = true;
            lTransition_109896.hasFixedDuration = true;
            lTransition_109896.exitTime = 0.361057f;
            lTransition_109896.duration = 0.03010701f;
            lTransition_109896.offset = 0f;
            lTransition_109896.mute = false;
            lTransition_109896.solo = false;
            lTransition_109896.canTransitionToSelf = true;
            lTransition_109896.orderedInterruption = true;
            lTransition_109896.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109896.conditions.Length - 1; i >= 0; i--) { lTransition_109896.RemoveCondition(lTransition_109896.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109898 = MotionControllerMotion.EditorFindTransition(lState_108794, lState_109892, 0);
            if (lTransition_109898 == null) { lTransition_109898 = lState_108794.AddTransition(lState_109892); }
            lTransition_109898.isExit = false;
            lTransition_109898.hasExitTime = true;
            lTransition_109898.hasFixedDuration = true;
            lTransition_109898.exitTime = 0.7224014f;
            lTransition_109898.duration = 0.1009254f;
            lTransition_109898.offset = 28.12615f;
            lTransition_109898.mute = false;
            lTransition_109898.solo = false;
            lTransition_109898.canTransitionToSelf = true;
            lTransition_109898.orderedInterruption = true;
            lTransition_109898.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109898.conditions.Length - 1; i >= 0; i--) { lTransition_109898.RemoveCondition(lTransition_109898.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109900 = MotionControllerMotion.EditorFindTransition(lState_108798, lState_109892, 0);
            if (lTransition_109900 == null) { lTransition_109900 = lState_108798.AddTransition(lState_109892); }
            lTransition_109900.isExit = false;
            lTransition_109900.hasExitTime = true;
            lTransition_109900.hasFixedDuration = true;
            lTransition_109900.exitTime = 0.7457627f;
            lTransition_109900.duration = 0.25f;
            lTransition_109900.offset = 0f;
            lTransition_109900.mute = false;
            lTransition_109900.solo = false;
            lTransition_109900.canTransitionToSelf = true;
            lTransition_109900.orderedInterruption = true;
            lTransition_109900.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109900.conditions.Length - 1; i >= 0; i--) { lTransition_109900.RemoveCondition(lTransition_109900.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109902 = MotionControllerMotion.EditorFindTransition(lState_108802, lState_109892, 0);
            if (lTransition_109902 == null) { lTransition_109902 = lState_108802.AddTransition(lState_109892); }
            lTransition_109902.isExit = false;
            lTransition_109902.hasExitTime = true;
            lTransition_109902.hasFixedDuration = true;
            lTransition_109902.exitTime = 0.6938776f;
            lTransition_109902.duration = 0.25f;
            lTransition_109902.offset = 0f;
            lTransition_109902.mute = false;
            lTransition_109902.solo = false;
            lTransition_109902.canTransitionToSelf = true;
            lTransition_109902.orderedInterruption = true;
            lTransition_109902.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109902.conditions.Length - 1; i >= 0; i--) { lTransition_109902.RemoveCondition(lTransition_109902.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109904 = MotionControllerMotion.EditorFindTransition(lState_108804, lState_109892, 0);
            if (lTransition_109904 == null) { lTransition_109904 = lState_108804.AddTransition(lState_109892); }
            lTransition_109904.isExit = false;
            lTransition_109904.hasExitTime = true;
            lTransition_109904.hasFixedDuration = true;
            lTransition_109904.exitTime = 0.7321429f;
            lTransition_109904.duration = 0.25f;
            lTransition_109904.offset = 0f;
            lTransition_109904.mute = false;
            lTransition_109904.solo = false;
            lTransition_109904.canTransitionToSelf = true;
            lTransition_109904.orderedInterruption = true;
            lTransition_109904.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109904.conditions.Length - 1; i >= 0; i--) { lTransition_109904.RemoveCondition(lTransition_109904.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109906 = MotionControllerMotion.EditorFindTransition(lState_108800, lState_109892, 0);
            if (lTransition_109906 == null) { lTransition_109906 = lState_108800.AddTransition(lState_109892); }
            lTransition_109906.isExit = false;
            lTransition_109906.hasExitTime = true;
            lTransition_109906.hasFixedDuration = true;
            lTransition_109906.exitTime = 0.918029f;
            lTransition_109906.duration = 0.04744691f;
            lTransition_109906.offset = 2.242554f;
            lTransition_109906.mute = false;
            lTransition_109906.solo = false;
            lTransition_109906.canTransitionToSelf = true;
            lTransition_109906.orderedInterruption = true;
            lTransition_109906.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109906.conditions.Length - 1; i >= 0; i--) { lTransition_109906.RemoveCondition(lTransition_109906.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_109908 = MotionControllerMotion.EditorFindTransition(lState_108806, lState_109892, 0);
            if (lTransition_109908 == null) { lTransition_109908 = lState_108806.AddTransition(lState_109892); }
            lTransition_109908.isExit = false;
            lTransition_109908.hasExitTime = true;
            lTransition_109908.hasFixedDuration = true;
            lTransition_109908.exitTime = 0.9878153f;
            lTransition_109908.duration = 0.0343138f;
            lTransition_109908.offset = 47.69421f;
            lTransition_109908.mute = false;
            lTransition_109908.solo = false;
            lTransition_109908.canTransitionToSelf = true;
            lTransition_109908.orderedInterruption = true;
            lTransition_109908.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_109908.conditions.Length - 1; i >= 0; i--) { lTransition_109908.RemoveCondition(lTransition_109908.conditions[i]); }

        }
#endif

    }
}
