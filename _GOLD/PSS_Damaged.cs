﻿using UnityEngine;
using com.ootii.Actors.AnimationControllers;
using com.ootii.Actors.Combat;
using com.ootii.Geometry;
using com.ootii.Messages;
using com.ootii.Actors.LifeCores;
using com.ootii.Helpers;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.ootii.MotionControllerPacks
{
    /// <summary>
    /// Adventure style walk/run that uses Mixamo's bow animations.
    /// </summary>
    [MotionName("PSS - Damaged")]
    [MotionDescription("Support animations for being hit that work with the Sword & Shield pack.")]
    public class PSS_Damaged : PSS_MotionBase
    {
        // Enum values for the motion
        public const int PHASE_UNKNOWN = 0;
        public const int PHASE_START = 32050;
        public const int PHASE_OUT = 32052;

        public const string EVENT_CONTINUE = "multistatecontinue";

        //For curbing the AI's animation-cancelling shenanigans.
        Elain_StateMachine AISource;

        public BasicMeleeAttack EnemyBMA;

        /// <summary>
        /// Default constructor
        /// </summary>
        public PSS_Damaged()
            : base()
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE;

            _Priority = 30;
            _ActionAlias = "";

#if UNITY_EDITOR
            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "PSS_Utilities-SM"; }
#endif
        }

        /// <summary>
        /// Controller constructor
        /// </summary>
        /// <param name="rController">Controller the motion belongs to</param>
        public PSS_Damaged(MotionController rController)
            : base(rController)
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE;

            _Priority = 30;
            _ActionAlias = "";

#if UNITY_EDITOR
            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "PSS_Utilities-SM"; }
#endif
        }

        /// <summary>
        /// Allows for any processing after the motion has been deserialized
        /// </summary>
        public override void Awake()
        {
            base.Awake();
            if (mMotionController.gameObject.GetComponent<Elain_StateMachine>() != null)
            {
                AISource = mMotionController.gameObject.GetComponent<Elain_StateMachine>();
            }
        }

        /// <summary>
        /// Tests if this motion should be started. However, the motion
        /// isn't actually started.
        /// </summary>
        /// <returns></returns>
        public override bool TestActivate()
        {
            if (!mIsStartable) { return false; }
            if (!mMotionController.IsGrounded) { return false; }
            //if (mActorController.State.Stance != EnumControllerStance.COMBAT_MELEE_SWORD_SHIELD) { return false; }

            if (_ActionAlias.Length > 0 && mMotionController._InputSource != null)
            {
                if (mMotionController._InputSource.IsJustPressed(_ActionAlias))
                {
                    return true;
                }
            }

            // Stop
            return false;
        }

        /// <summary>
        /// Tests if the motion should continue. If it shouldn't, the motion
        /// is typically disabled
        /// </summary>
        /// <returns>Boolean that determines if the motion continues</returns>
        public override bool TestUpdate()
        {
            if (mIsActivatedFrame) {/* Debug.Log("in activated frame, returning true");*/ return true; }
            //if (!mMotionController.IsGrounded) { Debug.Log("not grounded, deactivating!"); return false; }
            //if (mActorController.State.Stance != EnumControllerStance.COMBAT_MELEE_SWORD_SHIELD) { return false; }
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.IsTag("Exit"))
            {
                //Debug.Log("In EXIT state");
                return false;
            }
            // Ensure we're in the animation
            if (mIsAnimatorActive && !IsInMotionState)
            {
                //Debug.Log("Animator is active and we're not in the motion state");
                return false;
            }

            // Stay in
            return true;
        }

        /// <summary>
        /// Raised when a motion is being interrupted by another motion
        /// </summary>
        /// <param name="rMotion">Motion doing the interruption</param>
        /// <returns>Boolean determining if it can be interrupted</returns>
        public override bool TestInterruption(MotionControllerMotion rMotion)
        {
            return true;
        }

        /// <summary>
        /// Called to start the specific motion. If the motion
        /// were something like 'jump', this would start the jumping process
        /// </summary>
        /// <param name="rPrevMotion">Motion that this motion is taking over from</param>
        public override bool Activate(MotionControllerMotion rPrevMotion)
        {
            // Activate the motion
            ////Debug.Log("Activating PSS_Damaged in the MotionController");

            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START, mParameter, true);
            // Return
            return base.Activate(rPrevMotion);

        }

        /// <summary>
        /// Called to stop the motion. If the motion is stopable. Some motions
        /// like jump cannot be stopped early
        /// </summary>
        public override void Deactivate()
        {
            ////Debug.Log("Deactivating PSS_Damaged");
            if (AISource)
            {
                    AISource.isInterruptable = true;
                    AISource.CurrentCombatState = AISource.DecideNextState();
                
            }
            // Finish the deactivation process
            base.Deactivate();

        }

        /// <summary>
        /// Allows the motion to modify the root-motion velocities before they are applied. 
        /// 
        /// NOTE:
        /// Be careful when removing rotations as some transitions will want rotations even 
        /// if the state they are transitioning from don't.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        /// <param name="rVelocityDelta">Root-motion linear velocity relative to the actor's forward</param>
        /// <param name="rRotationDelta">Root-motion rotational velocity</param>
        /// <returns></returns>
        public override void UpdateRootMotion(float rDeltaTime, int rUpdateIndex, ref Vector3 rMovement, ref Quaternion rRotation)
        {
        }

        /// <summary>
        /// Updates the motion over time. This is called by the controller
        /// every update cycle so animations and stages can be updated.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        public override void Update(float rDeltaTime, int rUpdateIndex)
        {
            base.Update(rDeltaTime, rUpdateIndex);
        }

        public void RepeatHitStun(CombatMessage rMessage)
        {
            //Debug.Log("Hitstun resetting!");
            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START, mParameter);
        }

        public override void OnAnimationEvent(AnimationEvent rEvent)
        {

            if (rEvent == null) { return; }

            // If the animation is running backwards (say in response to a block), don't fire.
#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
#else
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.speed <= 0f) { return; }
#endif

            // Test the event type
            if (rEvent.stringParameter.Length == 0 || StringHelper.CleanString(rEvent.stringParameter) == PSS_Damaged.EVENT_CONTINUE)
            {
                mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_OUT, false);
            }

            base.OnAnimationEvent(rEvent);
        }

        /// <summary>
        /// Look at the incoming message to determine if it means we should react
        /// </summary>
        /// <param name="rMessage"></param>
        public override void OnMessageReceived(IMessage rMessage)
        {
            ////Debug.Log("PSS_Damaged: IMessage received");
            if (rMessage == null) { return; }


            CombatMessage lCombatMessage = rMessage as CombatMessage;

            //if (lCombatMessage.CombatStyle != null) { //Debug.Log("PSS_Damaged: Message's CombatStyle.Form reads as: " + lCombatMessage.CombatStyle.Form); }

            /*if (rMessage is DamageMessage)
            {
                ////Debug.Log("PSS_Damaged: Message confirmed as DamageMessage");
            }*/

            if (lCombatMessage != null)
            {
                // Attack messages
                if (lCombatMessage.Attacker == mMotionController.gameObject)
                {
                    return;
                }
                // Defender messages
                else if (lCombatMessage.Defender == mMotionController.gameObject)
                {
                    CombatEvade combatEvade = mMotionController.GetMotion<CombatEvade>();

                    if (lCombatMessage.ID == CombatMessage.MSG_DEFENDER_DAMAGED)
                    {
                        mIsActive = true;
                        Vector3 lLocalPosition = mMotionController._Transform.InverseTransformPoint(lCombatMessage.HitPoint);
                        Vector3 lLocalDirection = (lLocalPosition - Vector3.zero).normalized;
                        float lAttackAngle = Vector3Ext.HorizontalAngleTo(Vector3.forward, lLocalDirection, Vector3.up);
                        mParameter = lCombatMessage.Attacker.GetComponent<ActorCore>().CurrentAttackForm;
                        //Debug.Log("Damaged motion Parameter reads: " + mParameter);

                        if (mMotionController.ActiveMotion != this)
                        {
                            //Debug.Log("Active motion is not Damaged, activating!");
                            mMotionController.ActivateMotion(this, mParameter);
                        }
                        else
                        {
                            RepeatHitStun(lCombatMessage);
                        }

                        lCombatMessage.IsHandled = true;
                        lCombatMessage.Recipient = this;
                    }
                }
            }
        }

        // **************************************************************************************************
        // Following properties and function only valid while editing
        // **************************************************************************************************

#if UNITY_EDITOR
        /// <summary>
        /// Allow the motion to render it's own GUI
        /// </summary>
        public override bool OnInspectorGUI()
        {
            bool lIsDirty = false;

            return lIsDirty;
        }

#endif

        #region Auto-Generated
        // ************************************ START AUTO GENERATED ************************************

        /// <summary>
        /// These declarations go inside the class so you can test for which state
        /// and transitions are active. Testing hash values is much faster than strings.
        /// </summary>
        public int STATE_Start = -1;
        public int STATE_Death_0 = -1;
        public int STATE_Death_180 = -1;
        public int STATE_Damaged_100 = -1;
        public int STATE_PSS_IdlePose = -1;
        public int STATE_Damaged_Trip_R = -1;
        public int STATE_Damaged_Stun_00 = -1;
        public int STATE_Damaged_300 = -1;
        public int STATE_Damaged_Stun_304 = -1;
        public int STATE_Damaged_Stun_02 = -1;
        public int STATE_Damaged_Punted = -1;
        public int STATE_Getup = -1;
        public int STATE_Damaged_99 = -1;
        public int STATE_Damaged_101 = -1;
        public int STATE_Damaged_102 = -1;
        public int STATE_Damaged_103 = -1;
        public int STATE_Damaged_104 = -1;
        public int STATE_Damaged_111 = -1;
        public int STATE_Damaged_303 = -1;
        public int STATE_StumbleBack = -1;
        public int STATE_Damaged_200 = -1;
        public int STATE_Damaged_301 = -1;
        public int STATE_KB_MidKO_Powerful = -1;
        public int TRANS_AnyState_Damaged_100 = -1;
        public int TRANS_EntryState_Damaged_100 = -1;
        public int TRANS_AnyState_Death_0 = -1;
        public int TRANS_EntryState_Death_0 = -1;
        public int TRANS_AnyState_Death_180 = -1;
        public int TRANS_EntryState_Death_180 = -1;
        public int TRANS_AnyState_Damaged_Trip_R = -1;
        public int TRANS_EntryState_Damaged_Trip_R = -1;
        public int TRANS_AnyState_Damaged_Punted = -1;
        public int TRANS_EntryState_Damaged_Punted = -1;
        public int TRANS_AnyState_Damaged_300 = -1;
        public int TRANS_EntryState_Damaged_300 = -1;
        public int TRANS_AnyState_Damaged_Stun_02 = -1;
        public int TRANS_EntryState_Damaged_Stun_02 = -1;
        public int TRANS_AnyState_Damaged_Stun_304 = -1;
        public int TRANS_EntryState_Damaged_Stun_304 = -1;
        public int TRANS_AnyState_Damaged_Stun_00 = -1;
        public int TRANS_EntryState_Damaged_Stun_00 = -1;
        public int TRANS_AnyState_Damaged_99 = -1;
        public int TRANS_EntryState_Damaged_99 = -1;
        public int TRANS_AnyState_Damaged_101 = -1;
        public int TRANS_EntryState_Damaged_101 = -1;
        public int TRANS_AnyState_Damaged_102 = -1;
        public int TRANS_EntryState_Damaged_102 = -1;
        public int TRANS_AnyState_Damaged_103 = -1;
        public int TRANS_EntryState_Damaged_103 = -1;
        public int TRANS_AnyState_Damaged_104 = -1;
        public int TRANS_EntryState_Damaged_104 = -1;
        public int TRANS_AnyState_Damaged_111 = -1;
        public int TRANS_EntryState_Damaged_111 = -1;
        public int TRANS_AnyState_Damaged_303 = -1;
        public int TRANS_EntryState_Damaged_303 = -1;
        public int TRANS_AnyState_Damaged_200 = -1;
        public int TRANS_EntryState_Damaged_200 = -1;
        public int TRANS_AnyState_Damaged_301 = -1;
        public int TRANS_EntryState_Damaged_301 = -1;
        public int TRANS_AnyState_KB_MidKO_Powerful = -1;
        public int TRANS_EntryState_KB_MidKO_Powerful = -1;
        public int TRANS_Damaged_100_PSS_IdlePose = -1;
        public int TRANS_Damaged_Trip_R_Getup = -1;
        public int TRANS_Damaged_Stun_00_PSS_IdlePose = -1;
        public int TRANS_Damaged_300_PSS_IdlePose = -1;
        public int TRANS_Damaged_Stun_304_Getup = -1;
        public int TRANS_Damaged_Stun_02_PSS_IdlePose = -1;
        public int TRANS_Damaged_Punted_Getup = -1;
        public int TRANS_Getup_PSS_IdlePose = -1;
        public int TRANS_Damaged_99_StumbleBack = -1;
        public int TRANS_Damaged_101_PSS_IdlePose = -1;
        public int TRANS_Damaged_102_PSS_IdlePose = -1;
        public int TRANS_Damaged_103_PSS_IdlePose = -1;
        public int TRANS_Damaged_104_PSS_IdlePose = -1;
        public int TRANS_Damaged_111_PSS_IdlePose = -1;
        public int TRANS_Damaged_303_Getup = -1;
        public int TRANS_StumbleBack_PSS_IdlePose = -1;
        public int TRANS_Damaged_200_PSS_IdlePose = -1;
        public int TRANS_Damaged_301_Damaged_300 = -1;
        public int TRANS_KB_MidKO_Powerful_Getup = -1;

        /// <summary>
        /// Determines if we're using auto-generated code
        /// </summary>
        public override bool HasAutoGeneratedCode
        {
            get { return true; }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsInMotionState
        {
            get
            {
                int lStateID = mMotionLayer._AnimatorStateID;
                int lTransitionID = mMotionLayer._AnimatorTransitionID;

                if (lTransitionID == 0)
                {
                    if (lStateID == STATE_Start) { return true; }
                    if (lStateID == STATE_Death_0) { return true; }
                    if (lStateID == STATE_Death_180) { return true; }
                    if (lStateID == STATE_Damaged_100) { return true; }
                    if (lStateID == STATE_PSS_IdlePose) { return true; }
                    if (lStateID == STATE_Damaged_Trip_R) { return true; }
                    if (lStateID == STATE_Damaged_Stun_00) { return true; }
                    if (lStateID == STATE_Damaged_300) { return true; }
                    if (lStateID == STATE_Damaged_Stun_304) { return true; }
                    if (lStateID == STATE_Damaged_Stun_02) { return true; }
                    if (lStateID == STATE_Damaged_Punted) { return true; }
                    if (lStateID == STATE_Getup) { return true; }
                    if (lStateID == STATE_Damaged_99) { return true; }
                    if (lStateID == STATE_Damaged_101) { return true; }
                    if (lStateID == STATE_Damaged_102) { return true; }
                    if (lStateID == STATE_Damaged_103) { return true; }
                    if (lStateID == STATE_Damaged_104) { return true; }
                    if (lStateID == STATE_Damaged_111) { return true; }
                    if (lStateID == STATE_Damaged_303) { return true; }
                    if (lStateID == STATE_StumbleBack) { return true; }
                    if (lStateID == STATE_Damaged_200) { return true; }
                    if (lStateID == STATE_Damaged_301) { return true; }
                    if (lStateID == STATE_KB_MidKO_Powerful) { return true; }
                }

                if (lTransitionID == TRANS_AnyState_Damaged_100) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_100) { return true; }
                if (lTransitionID == TRANS_AnyState_Death_0) { return true; }
                if (lTransitionID == TRANS_EntryState_Death_0) { return true; }
                if (lTransitionID == TRANS_AnyState_Death_180) { return true; }
                if (lTransitionID == TRANS_EntryState_Death_180) { return true; }
                if (lTransitionID == TRANS_AnyState_Death_180) { return true; }
                if (lTransitionID == TRANS_EntryState_Death_180) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_Trip_R) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_Trip_R) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_Punted) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_Punted) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_300) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_300) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_Stun_02) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_Stun_02) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_Stun_304) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_Stun_304) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_Stun_00) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_Stun_00) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_99) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_99) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_101) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_101) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_102) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_102) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_103) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_103) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_104) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_104) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_111) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_111) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_303) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_303) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_200) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_200) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_301) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_301) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_Punted) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_Punted) { return true; }
                if (lTransitionID == TRANS_AnyState_Damaged_303) { return true; }
                if (lTransitionID == TRANS_EntryState_Damaged_303) { return true; }
                if (lTransitionID == TRANS_AnyState_KB_MidKO_Powerful) { return true; }
                if (lTransitionID == TRANS_EntryState_KB_MidKO_Powerful) { return true; }
                if (lTransitionID == TRANS_AnyState_KB_MidKO_Powerful) { return true; }
                if (lTransitionID == TRANS_EntryState_KB_MidKO_Powerful) { return true; }
                if (lTransitionID == TRANS_Damaged_100_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_Trip_R_Getup) { return true; }
                if (lTransitionID == TRANS_Damaged_Stun_00_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_300_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_Stun_304_Getup) { return true; }
                if (lTransitionID == TRANS_Damaged_Stun_02_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_Punted_Getup) { return true; }
                if (lTransitionID == TRANS_Getup_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_99_StumbleBack) { return true; }
                if (lTransitionID == TRANS_Damaged_101_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_102_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_103_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_104_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_111_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_303_Getup) { return true; }
                if (lTransitionID == TRANS_StumbleBack_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_200_PSS_IdlePose) { return true; }
                if (lTransitionID == TRANS_Damaged_301_Damaged_300) { return true; }
                if (lTransitionID == TRANS_KB_MidKO_Powerful_Getup) { return true; }
                return false;
            }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID)
        {
            if (rStateID == STATE_Start) { return true; }
            if (rStateID == STATE_Death_0) { return true; }
            if (rStateID == STATE_Death_180) { return true; }
            if (rStateID == STATE_Damaged_100) { return true; }
            if (rStateID == STATE_PSS_IdlePose) { return true; }
            if (rStateID == STATE_Damaged_Trip_R) { return true; }
            if (rStateID == STATE_Damaged_Stun_00) { return true; }
            if (rStateID == STATE_Damaged_300) { return true; }
            if (rStateID == STATE_Damaged_Stun_304) { return true; }
            if (rStateID == STATE_Damaged_Stun_02) { return true; }
            if (rStateID == STATE_Damaged_Punted) { return true; }
            if (rStateID == STATE_Getup) { return true; }
            if (rStateID == STATE_Damaged_99) { return true; }
            if (rStateID == STATE_Damaged_101) { return true; }
            if (rStateID == STATE_Damaged_102) { return true; }
            if (rStateID == STATE_Damaged_103) { return true; }
            if (rStateID == STATE_Damaged_104) { return true; }
            if (rStateID == STATE_Damaged_111) { return true; }
            if (rStateID == STATE_Damaged_303) { return true; }
            if (rStateID == STATE_StumbleBack) { return true; }
            if (rStateID == STATE_Damaged_200) { return true; }
            if (rStateID == STATE_Damaged_301) { return true; }
            if (rStateID == STATE_KB_MidKO_Powerful) { return true; }
            return false;
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID, int rTransitionID)
        {
            if (rTransitionID == 0)
            {
                if (rStateID == STATE_Start) { return true; }
                if (rStateID == STATE_Death_0) { return true; }
                if (rStateID == STATE_Death_180) { return true; }
                if (rStateID == STATE_Damaged_100) { return true; }
                if (rStateID == STATE_PSS_IdlePose) { return true; }
                if (rStateID == STATE_Damaged_Trip_R) { return true; }
                if (rStateID == STATE_Damaged_Stun_00) { return true; }
                if (rStateID == STATE_Damaged_300) { return true; }
                if (rStateID == STATE_Damaged_Stun_304) { return true; }
                if (rStateID == STATE_Damaged_Stun_02) { return true; }
                if (rStateID == STATE_Damaged_Punted) { return true; }
                if (rStateID == STATE_Getup) { return true; }
                if (rStateID == STATE_Damaged_99) { return true; }
                if (rStateID == STATE_Damaged_101) { return true; }
                if (rStateID == STATE_Damaged_102) { return true; }
                if (rStateID == STATE_Damaged_103) { return true; }
                if (rStateID == STATE_Damaged_104) { return true; }
                if (rStateID == STATE_Damaged_111) { return true; }
                if (rStateID == STATE_Damaged_303) { return true; }
                if (rStateID == STATE_StumbleBack) { return true; }
                if (rStateID == STATE_Damaged_200) { return true; }
                if (rStateID == STATE_Damaged_301) { return true; }
                if (rStateID == STATE_KB_MidKO_Powerful) { return true; }
            }

            if (rTransitionID == TRANS_AnyState_Damaged_100) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_100) { return true; }
            if (rTransitionID == TRANS_AnyState_Death_0) { return true; }
            if (rTransitionID == TRANS_EntryState_Death_0) { return true; }
            if (rTransitionID == TRANS_AnyState_Death_180) { return true; }
            if (rTransitionID == TRANS_EntryState_Death_180) { return true; }
            if (rTransitionID == TRANS_AnyState_Death_180) { return true; }
            if (rTransitionID == TRANS_EntryState_Death_180) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_Trip_R) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_Trip_R) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_Punted) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_Punted) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_300) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_300) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_Stun_02) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_Stun_02) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_Stun_304) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_Stun_304) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_Stun_00) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_Stun_00) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_99) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_99) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_101) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_101) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_102) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_102) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_103) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_103) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_104) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_104) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_111) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_111) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_303) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_303) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_200) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_200) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_301) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_301) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_Punted) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_Punted) { return true; }
            if (rTransitionID == TRANS_AnyState_Damaged_303) { return true; }
            if (rTransitionID == TRANS_EntryState_Damaged_303) { return true; }
            if (rTransitionID == TRANS_AnyState_KB_MidKO_Powerful) { return true; }
            if (rTransitionID == TRANS_EntryState_KB_MidKO_Powerful) { return true; }
            if (rTransitionID == TRANS_AnyState_KB_MidKO_Powerful) { return true; }
            if (rTransitionID == TRANS_EntryState_KB_MidKO_Powerful) { return true; }
            if (rTransitionID == TRANS_Damaged_100_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_Trip_R_Getup) { return true; }
            if (rTransitionID == TRANS_Damaged_Stun_00_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_300_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_Stun_304_Getup) { return true; }
            if (rTransitionID == TRANS_Damaged_Stun_02_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_Punted_Getup) { return true; }
            if (rTransitionID == TRANS_Getup_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_99_StumbleBack) { return true; }
            if (rTransitionID == TRANS_Damaged_101_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_102_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_103_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_104_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_111_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_303_Getup) { return true; }
            if (rTransitionID == TRANS_StumbleBack_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_200_PSS_IdlePose) { return true; }
            if (rTransitionID == TRANS_Damaged_301_Damaged_300) { return true; }
            if (rTransitionID == TRANS_KB_MidKO_Powerful_Getup) { return true; }
            return false;
        }

        /// <summary>
        /// Preprocess any animator data so the motion can use it later
        /// </summary>
        public override void LoadAnimatorData()
        {
            string lLayer = mMotionController.Animator.GetLayerName(mMotionLayer._AnimatorLayerIndex);
            TRANS_AnyState_Damaged_100 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_100");
            TRANS_EntryState_Damaged_100 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_100");
            TRANS_AnyState_Death_0 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Death_0");
            TRANS_EntryState_Death_0 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Death_0");
            TRANS_AnyState_Death_180 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Death_180");
            TRANS_EntryState_Death_180 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Death_180");
            TRANS_AnyState_Death_180 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Death_180");
            TRANS_EntryState_Death_180 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Death_180");
            TRANS_AnyState_Damaged_Trip_R = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_Trip_R");
            TRANS_EntryState_Damaged_Trip_R = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_Trip_R");
            TRANS_AnyState_Damaged_Punted = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_Punted");
            TRANS_EntryState_Damaged_Punted = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_Punted");
            TRANS_AnyState_Damaged_300 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_300");
            TRANS_EntryState_Damaged_300 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_300");
            TRANS_AnyState_Damaged_Stun_02 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_Stun_02");
            TRANS_EntryState_Damaged_Stun_02 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_Stun_02");
            TRANS_AnyState_Damaged_Stun_304 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_Stun_304");
            TRANS_EntryState_Damaged_Stun_304 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_Stun_304");
            TRANS_AnyState_Damaged_Stun_00 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_Stun_00");
            TRANS_EntryState_Damaged_Stun_00 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_Stun_00");
            TRANS_AnyState_Damaged_99 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_99");
            TRANS_EntryState_Damaged_99 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_99");
            TRANS_AnyState_Damaged_101 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_101");
            TRANS_EntryState_Damaged_101 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_101");
            TRANS_AnyState_Damaged_102 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_102");
            TRANS_EntryState_Damaged_102 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_102");
            TRANS_AnyState_Damaged_103 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_103");
            TRANS_EntryState_Damaged_103 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_103");
            TRANS_AnyState_Damaged_104 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_104");
            TRANS_EntryState_Damaged_104 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_104");
            TRANS_AnyState_Damaged_111 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_111");
            TRANS_EntryState_Damaged_111 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_111");
            TRANS_AnyState_Damaged_303 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_303");
            TRANS_EntryState_Damaged_303 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_303");
            TRANS_AnyState_Damaged_200 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_200");
            TRANS_EntryState_Damaged_200 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_200");
            TRANS_AnyState_Damaged_301 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_301");
            TRANS_EntryState_Damaged_301 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_301");
            TRANS_AnyState_Damaged_Punted = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_Punted");
            TRANS_EntryState_Damaged_Punted = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_Punted");
            TRANS_AnyState_Damaged_303 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.Damaged_303");
            TRANS_EntryState_Damaged_303 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.Damaged_303");
            TRANS_AnyState_KB_MidKO_Powerful = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.KB_MidKO_Powerful");
            TRANS_EntryState_KB_MidKO_Powerful = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.KB_MidKO_Powerful");
            TRANS_AnyState_KB_MidKO_Powerful = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".PSS_Utilities-SM.KB_MidKO_Powerful");
            TRANS_EntryState_KB_MidKO_Powerful = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".PSS_Utilities-SM.KB_MidKO_Powerful");
            STATE_Start = mMotionController.AddAnimatorName("" + lLayer + ".Start");
            STATE_Death_0 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Death_0");
            STATE_Death_180 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Death_180");
            STATE_Damaged_100 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_100");
            TRANS_Damaged_100_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_100 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_Trip_R = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Trip_R");
            TRANS_Damaged_Trip_R_Getup = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Trip_R -> " + lLayer + ".PSS_Utilities-SM.Getup");
            STATE_Damaged_Stun_00 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Stun_00");
            TRANS_Damaged_Stun_00_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Stun_00 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_300 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_300");
            TRANS_Damaged_300_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_300 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_Stun_304 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Stun_304");
            TRANS_Damaged_Stun_304_Getup = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Stun_304 -> " + lLayer + ".PSS_Utilities-SM.Getup");
            STATE_Damaged_Stun_02 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Stun_02");
            TRANS_Damaged_Stun_02_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Stun_02 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_Punted = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Punted");
            TRANS_Damaged_Punted_Getup = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_Punted -> " + lLayer + ".PSS_Utilities-SM.Getup");
            STATE_Getup = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Getup");
            TRANS_Getup_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Getup -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_99 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_99");
            TRANS_Damaged_99_StumbleBack = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_99 -> " + lLayer + ".PSS_Utilities-SM.StumbleBack");
            STATE_Damaged_101 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_101");
            TRANS_Damaged_101_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_101 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_102 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_102");
            TRANS_Damaged_102_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_102 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_103 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_103");
            TRANS_Damaged_103_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_103 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_104 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_104");
            TRANS_Damaged_104_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_104 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_111 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_111");
            TRANS_Damaged_111_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_111 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_303 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_303");
            TRANS_Damaged_303_Getup = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_303 -> " + lLayer + ".PSS_Utilities-SM.Getup");
            STATE_StumbleBack = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.StumbleBack");
            TRANS_StumbleBack_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.StumbleBack -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_200 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_200");
            TRANS_Damaged_200_PSS_IdlePose = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_200 -> " + lLayer + ".PSS_Utilities-SM.PSS_IdlePose");
            STATE_Damaged_301 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_301");
            TRANS_Damaged_301_Damaged_300 = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.Damaged_301 -> " + lLayer + ".PSS_Utilities-SM.Damaged_300");
            STATE_KB_MidKO_Powerful = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.KB_MidKO_Powerful");
            TRANS_KB_MidKO_Powerful_Getup = mMotionController.AddAnimatorName("" + lLayer + ".PSS_Utilities-SM.KB_MidKO_Powerful -> " + lLayer + ".PSS_Utilities-SM.Getup");
        }

#if UNITY_EDITOR

        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
        protected override void CreateStateMachine()
        {
            int rLayerIndex = mMotionLayer._AnimatorLayerIndex;
            MotionController rMotionController = mMotionController;

            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_113772 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "PSS_Utilities-SM");
            if (lSSM_113772 == null) { lSSM_113772 = lLayerStateMachine.AddStateMachine("PSS_Utilities-SM", new Vector3(672, -324, 0)); }

            UnityEditor.Animations.AnimatorState lState_114346 = MotionControllerMotion.EditorFindState(lSSM_113772, "Death_0");
            if (lState_114346 == null) { lState_114346 = lSSM_113772.AddState("Death_0", new Vector3(-204, -12, 0)); }
            lState_114346.speed = 1f;
            lState_114346.mirror = false;
            lState_114346.tag = "";
            lState_114346.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_HighKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_114348 = MotionControllerMotion.EditorFindState(lSSM_113772, "Death_180");
            if (lState_114348 == null) { lState_114348 = lSSM_113772.AddState("Death_180", new Vector3(-204, 48, 0)); }
            lState_114348.speed = 1f;
            lState_114348.mirror = false;
            lState_114348.tag = "";
            lState_114348.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_MidKO_Back");

            UnityEditor.Animations.AnimatorState lState_114344 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_100");
            if (lState_114344 == null) { lState_114344 = lSSM_113772.AddState("Damaged_100", new Vector3(-216, -432, 0)); }
            lState_114344.speed = 1f;
            lState_114344.mirror = false;
            lState_114344.tag = "";
            lState_114344.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighFront_Weak");

            UnityEditor.Animations.AnimatorState lState_115540 = MotionControllerMotion.EditorFindState(lSSM_113772, "PSS_IdlePose");
            if (lState_115540 == null) { lState_115540 = lSSM_113772.AddState("PSS_IdlePose", new Vector3(420, -300, 0)); }
            lState_115540.speed = 1f;
            lState_115540.mirror = false;
            lState_115540.tag = "Exit";
            lState_115540.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Idle_3");

            UnityEditor.Animations.AnimatorState lState_114350 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Trip_R");
            if (lState_114350 == null) { lState_114350 = lSSM_113772.AddState("Damaged_Trip_R", new Vector3(264, 132, 0)); }
            lState_114350.speed = 1f;
            lState_114350.mirror = false;
            lState_114350.tag = "";
            lState_114350.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_LowKO_R");

            UnityEditor.Animations.AnimatorState lState_114360 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Stun_00");
            if (lState_114360 == null) { lState_114360 = lSSM_113772.AddState("Damaged_Stun_00", new Vector3(168, -168, 0)); }
            lState_114360.speed = 1f;
            lState_114360.mirror = false;
            lState_114360.tag = "";
            lState_114360.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidFront_Stagger");

            UnityEditor.Animations.AnimatorState lState_114354 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_300");
            if (lState_114354 == null) { lState_114354 = lSSM_113772.AddState("Damaged_300", new Vector3(288, -120, 0)); }
            lState_114354.speed = 1f;
            lState_114354.mirror = false;
            lState_114354.tag = "";
            lState_114354.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_HighFront_Stagger");

            UnityEditor.Animations.AnimatorState lState_114358 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Stun_304");
            if (lState_114358 == null) { lState_114358 = lSSM_113772.AddState("Damaged_Stun_304", new Vector3(396, -12, 0)); }
            lState_114358.speed = 1f;
            lState_114358.mirror = false;
            lState_114358.tag = "";
            lState_114358.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_UpperKO");

            UnityEditor.Animations.AnimatorState lState_114356 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Stun_02");
            if (lState_114356 == null) { lState_114356 = lSSM_113772.AddState("Damaged_Stun_02", new Vector3(372, -84, 0)); }
            lState_114356.speed = 1f;
            lState_114356.mirror = false;
            lState_114356.tag = "";
            lState_114356.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidRight_Stagger");

            UnityEditor.Animations.AnimatorState lState_114352 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Punted");
            if (lState_114352 == null) { lState_114352 = lSSM_113772.AddState("Damaged_Punted", new Vector3(264, 72, 0)); }
            lState_114352.speed = 1f;
            lState_114352.mirror = false;
            lState_114352.tag = "";
            lState_114352.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_HighKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_115542 = MotionControllerMotion.EditorFindState(lSSM_113772, "Getup");
            if (lState_115542 == null) { lState_115542 = lSSM_113772.AddState("Getup", new Vector3(576, 84, 0)); }
            lState_115542.speed = 1f;
            lState_115542.mirror = false;
            lState_115542.tag = "";
            lState_115542.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_LayBack_Roll");

            UnityEditor.Animations.AnimatorState lState_114362 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_99");
            if (lState_114362 == null) { lState_114362 = lSSM_113772.AddState("Damaged_99", new Vector3(-216, -468, 0)); }
            lState_114362.speed = 1f;
            lState_114362.mirror = false;
            lState_114362.tag = "";
            lState_114362.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_TopKO");

            UnityEditor.Animations.AnimatorState lState_114364 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_101");
            if (lState_114364 == null) { lState_114364 = lSSM_113772.AddState("Damaged_101", new Vector3(-216, -396, 0)); }
            lState_114364.speed = 1f;
            lState_114364.mirror = false;
            lState_114364.tag = "";
            lState_114364.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighUpper_Weak");

            UnityEditor.Animations.AnimatorState lState_114366 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_102");
            if (lState_114366 == null) { lState_114366 = lSSM_113772.AddState("Damaged_102", new Vector3(-216, -360, 0)); }
            lState_114366.speed = 1f;
            lState_114366.mirror = false;
            lState_114366.tag = "";
            lState_114366.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighUpper_Weak");

            UnityEditor.Animations.AnimatorState lState_114368 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_103");
            if (lState_114368 == null) { lState_114368 = lSSM_113772.AddState("Damaged_103", new Vector3(-216, -324, 0)); }
            lState_114368.speed = 1f;
            lState_114368.mirror = false;
            lState_114368.tag = "";
            lState_114368.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighUpper_Weak");

            UnityEditor.Animations.AnimatorState lState_114370 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_104");
            if (lState_114370 == null) { lState_114370 = lSSM_113772.AddState("Damaged_104", new Vector3(-216, -288, 0)); }
            lState_114370.speed = 1f;
            lState_114370.mirror = false;
            lState_114370.tag = "";
            lState_114370.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidFront_Stagger");

            UnityEditor.Animations.AnimatorState lState_114372 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_111");
            if (lState_114372 == null) { lState_114372 = lSSM_113772.AddState("Damaged_111", new Vector3(-216, -228, 0)); }
            lState_114372.speed = 1f;
            lState_114372.mirror = false;
            lState_114372.tag = "";
            lState_114372.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidLeft_Weak");

            UnityEditor.Animations.AnimatorState lState_114374 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_303");
            if (lState_114374 == null) { lState_114374 = lSSM_113772.AddState("Damaged_303", new Vector3(-216, -156, 0)); }
            lState_114374.speed = 1f;
            lState_114374.mirror = true;
            lState_114374.tag = "";
            lState_114374.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_HighKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_115544 = MotionControllerMotion.EditorFindState(lSSM_113772, "StumbleBack");
            if (lState_115544 == null) { lState_115544 = lSSM_113772.AddState("StumbleBack", new Vector3(178.351f, -492.3941f, 0)); }
            lState_115544.speed = 1f;
            lState_115544.mirror = false;
            lState_115544.tag = "";
            lState_115544.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Dodging Back.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114380 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_200");
            if (lState_114380 == null) { lState_114380 = lSSM_113772.AddState("Damaged_200", new Vector3(-204, -96, 0)); }
            lState_114380.speed = 1f;
            lState_114380.mirror = true;
            lState_114380.tag = "";
            lState_114380.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_MidKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_114408 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_301");
            if (lState_114408 == null) { lState_114408 = lSSM_113772.AddState("Damaged_301", new Vector3(48, 108, 0)); }
            lState_114408.speed = 1f;
            lState_114408.mirror = false;
            lState_114408.tag = "";
            lState_114408.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_UpperKO");

            UnityEditor.Animations.AnimatorState lState_114420 = MotionControllerMotion.EditorFindState(lSSM_113772, "KB_MidKO_Powerful");
            if (lState_114420 == null) { lState_114420 = lSSM_113772.AddState("KB_MidKO_Powerful", new Vector3(240, 204, 0)); }
            lState_114420.speed = 1f;
            lState_114420.mirror = false;
            lState_114420.tag = "";
            lState_114420.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_MidKO_Powerful");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114016 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114344, 0);
            if (lAnyTransition_114016 == null) { lAnyTransition_114016 = lLayerStateMachine.AddAnyStateTransition(lState_114344); }
            lAnyTransition_114016.isExit = false;
            lAnyTransition_114016.hasExitTime = false;
            lAnyTransition_114016.hasFixedDuration = true;
            lAnyTransition_114016.exitTime = 0f;
            lAnyTransition_114016.duration = 0f;
            lAnyTransition_114016.offset = 0f;
            lAnyTransition_114016.mute = false;
            lAnyTransition_114016.solo = false;
            lAnyTransition_114016.canTransitionToSelf = false;
            lAnyTransition_114016.orderedInterruption = true;
            lAnyTransition_114016.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114016.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114016.RemoveCondition(lAnyTransition_114016.conditions[i]); }
            lAnyTransition_114016.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114016.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114018 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114346, 0);
            if (lAnyTransition_114018 == null) { lAnyTransition_114018 = lLayerStateMachine.AddAnyStateTransition(lState_114346); }
            lAnyTransition_114018.isExit = false;
            lAnyTransition_114018.hasExitTime = false;
            lAnyTransition_114018.hasFixedDuration = true;
            lAnyTransition_114018.exitTime = 0.9f;
            lAnyTransition_114018.duration = 0.1f;
            lAnyTransition_114018.offset = 0f;
            lAnyTransition_114018.mute = false;
            lAnyTransition_114018.solo = false;
            lAnyTransition_114018.canTransitionToSelf = true;
            lAnyTransition_114018.orderedInterruption = true;
            lAnyTransition_114018.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114018.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114018.RemoveCondition(lAnyTransition_114018.conditions[i]); }
            lAnyTransition_114018.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32051f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114018.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Greater, -100f, "L" + rLayerIndex + "MotionParameter");
            lAnyTransition_114018.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Less, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114020 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114348, 0);
            if (lAnyTransition_114020 == null) { lAnyTransition_114020 = lLayerStateMachine.AddAnyStateTransition(lState_114348); }
            lAnyTransition_114020.isExit = false;
            lAnyTransition_114020.hasExitTime = false;
            lAnyTransition_114020.hasFixedDuration = true;
            lAnyTransition_114020.exitTime = 0.9f;
            lAnyTransition_114020.duration = 0.1f;
            lAnyTransition_114020.offset = 0f;
            lAnyTransition_114020.mute = false;
            lAnyTransition_114020.solo = false;
            lAnyTransition_114020.canTransitionToSelf = true;
            lAnyTransition_114020.orderedInterruption = true;
            lAnyTransition_114020.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114020.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114020.RemoveCondition(lAnyTransition_114020.conditions[i]); }
            lAnyTransition_114020.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32051f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114020.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Less, -100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114022 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114348, 1);
            if (lAnyTransition_114022 == null) { lAnyTransition_114022 = lLayerStateMachine.AddAnyStateTransition(lState_114348); }
            lAnyTransition_114022.isExit = false;
            lAnyTransition_114022.hasExitTime = false;
            lAnyTransition_114022.hasFixedDuration = true;
            lAnyTransition_114022.exitTime = 0.9f;
            lAnyTransition_114022.duration = 0.1f;
            lAnyTransition_114022.offset = 0f;
            lAnyTransition_114022.mute = false;
            lAnyTransition_114022.solo = false;
            lAnyTransition_114022.canTransitionToSelf = true;
            lAnyTransition_114022.orderedInterruption = true;
            lAnyTransition_114022.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114022.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114022.RemoveCondition(lAnyTransition_114022.conditions[i]); }
            lAnyTransition_114022.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32051f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114022.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Greater, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114024 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114350, 0);
            if (lAnyTransition_114024 == null) { lAnyTransition_114024 = lLayerStateMachine.AddAnyStateTransition(lState_114350); }
            lAnyTransition_114024.isExit = false;
            lAnyTransition_114024.hasExitTime = false;
            lAnyTransition_114024.hasFixedDuration = true;
            lAnyTransition_114024.exitTime = 0f;
            lAnyTransition_114024.duration = 0f;
            lAnyTransition_114024.offset = 0f;
            lAnyTransition_114024.mute = false;
            lAnyTransition_114024.solo = false;
            lAnyTransition_114024.canTransitionToSelf = false;
            lAnyTransition_114024.orderedInterruption = true;
            lAnyTransition_114024.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114024.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114024.RemoveCondition(lAnyTransition_114024.conditions[i]); }
            lAnyTransition_114024.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114024.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 112f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114026 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114352, 0);
            if (lAnyTransition_114026 == null) { lAnyTransition_114026 = lLayerStateMachine.AddAnyStateTransition(lState_114352); }
            lAnyTransition_114026.isExit = false;
            lAnyTransition_114026.hasExitTime = false;
            lAnyTransition_114026.hasFixedDuration = false;
            lAnyTransition_114026.exitTime = 0f;
            lAnyTransition_114026.duration = 0f;
            lAnyTransition_114026.offset = 0f;
            lAnyTransition_114026.mute = false;
            lAnyTransition_114026.solo = false;
            lAnyTransition_114026.canTransitionToSelf = false;
            lAnyTransition_114026.orderedInterruption = true;
            lAnyTransition_114026.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114026.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114026.RemoveCondition(lAnyTransition_114026.conditions[i]); }
            lAnyTransition_114026.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114026.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 105f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114028 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114354, 0);
            if (lAnyTransition_114028 == null) { lAnyTransition_114028 = lLayerStateMachine.AddAnyStateTransition(lState_114354); }
            lAnyTransition_114028.isExit = false;
            lAnyTransition_114028.hasExitTime = false;
            lAnyTransition_114028.hasFixedDuration = true;
            lAnyTransition_114028.exitTime = 0f;
            lAnyTransition_114028.duration = 0f;
            lAnyTransition_114028.offset = 0.05644388f;
            lAnyTransition_114028.mute = false;
            lAnyTransition_114028.solo = false;
            lAnyTransition_114028.canTransitionToSelf = false;
            lAnyTransition_114028.orderedInterruption = true;
            lAnyTransition_114028.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114028.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114028.RemoveCondition(lAnyTransition_114028.conditions[i]); }
            lAnyTransition_114028.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114028.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 300f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114030 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114356, 0);
            if (lAnyTransition_114030 == null) { lAnyTransition_114030 = lLayerStateMachine.AddAnyStateTransition(lState_114356); }
            lAnyTransition_114030.isExit = false;
            lAnyTransition_114030.hasExitTime = false;
            lAnyTransition_114030.hasFixedDuration = true;
            lAnyTransition_114030.exitTime = 0f;
            lAnyTransition_114030.duration = 0.05f;
            lAnyTransition_114030.offset = 0f;
            lAnyTransition_114030.mute = false;
            lAnyTransition_114030.solo = false;
            lAnyTransition_114030.canTransitionToSelf = false;
            lAnyTransition_114030.orderedInterruption = true;
            lAnyTransition_114030.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114030.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114030.RemoveCondition(lAnyTransition_114030.conditions[i]); }
            lAnyTransition_114030.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114030.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 0f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114032 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114358, 0);
            if (lAnyTransition_114032 == null) { lAnyTransition_114032 = lLayerStateMachine.AddAnyStateTransition(lState_114358); }
            lAnyTransition_114032.isExit = false;
            lAnyTransition_114032.hasExitTime = false;
            lAnyTransition_114032.hasFixedDuration = true;
            lAnyTransition_114032.exitTime = 0f;
            lAnyTransition_114032.duration = 0f;
            lAnyTransition_114032.offset = 0.05644388f;
            lAnyTransition_114032.mute = false;
            lAnyTransition_114032.solo = false;
            lAnyTransition_114032.canTransitionToSelf = true;
            lAnyTransition_114032.orderedInterruption = true;
            lAnyTransition_114032.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114032.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114032.RemoveCondition(lAnyTransition_114032.conditions[i]); }
            lAnyTransition_114032.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114032.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 304f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114034 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114360, 0);
            if (lAnyTransition_114034 == null) { lAnyTransition_114034 = lLayerStateMachine.AddAnyStateTransition(lState_114360); }
            lAnyTransition_114034.isExit = false;
            lAnyTransition_114034.hasExitTime = false;
            lAnyTransition_114034.hasFixedDuration = true;
            lAnyTransition_114034.exitTime = 0f;
            lAnyTransition_114034.duration = 0.05f;
            lAnyTransition_114034.offset = 0f;
            lAnyTransition_114034.mute = false;
            lAnyTransition_114034.solo = false;
            lAnyTransition_114034.canTransitionToSelf = false;
            lAnyTransition_114034.orderedInterruption = true;
            lAnyTransition_114034.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114034.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114034.RemoveCondition(lAnyTransition_114034.conditions[i]); }
            lAnyTransition_114034.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114034.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 110f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114036 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114362, 0);
            if (lAnyTransition_114036 == null) { lAnyTransition_114036 = lLayerStateMachine.AddAnyStateTransition(lState_114362); }
            lAnyTransition_114036.isExit = false;
            lAnyTransition_114036.hasExitTime = false;
            lAnyTransition_114036.hasFixedDuration = true;
            lAnyTransition_114036.exitTime = 0f;
            lAnyTransition_114036.duration = 0f;
            lAnyTransition_114036.offset = 0f;
            lAnyTransition_114036.mute = false;
            lAnyTransition_114036.solo = false;
            lAnyTransition_114036.canTransitionToSelf = false;
            lAnyTransition_114036.orderedInterruption = true;
            lAnyTransition_114036.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114036.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114036.RemoveCondition(lAnyTransition_114036.conditions[i]); }
            lAnyTransition_114036.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114036.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114038 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114364, 0);
            if (lAnyTransition_114038 == null) { lAnyTransition_114038 = lLayerStateMachine.AddAnyStateTransition(lState_114364); }
            lAnyTransition_114038.isExit = false;
            lAnyTransition_114038.hasExitTime = false;
            lAnyTransition_114038.hasFixedDuration = true;
            lAnyTransition_114038.exitTime = 0f;
            lAnyTransition_114038.duration = 0f;
            lAnyTransition_114038.offset = 0f;
            lAnyTransition_114038.mute = false;
            lAnyTransition_114038.solo = false;
            lAnyTransition_114038.canTransitionToSelf = false;
            lAnyTransition_114038.orderedInterruption = true;
            lAnyTransition_114038.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114038.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114038.RemoveCondition(lAnyTransition_114038.conditions[i]); }
            lAnyTransition_114038.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114038.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 101f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114040 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114366, 0);
            if (lAnyTransition_114040 == null) { lAnyTransition_114040 = lLayerStateMachine.AddAnyStateTransition(lState_114366); }
            lAnyTransition_114040.isExit = false;
            lAnyTransition_114040.hasExitTime = false;
            lAnyTransition_114040.hasFixedDuration = true;
            lAnyTransition_114040.exitTime = 0f;
            lAnyTransition_114040.duration = 0f;
            lAnyTransition_114040.offset = 0f;
            lAnyTransition_114040.mute = false;
            lAnyTransition_114040.solo = false;
            lAnyTransition_114040.canTransitionToSelf = false;
            lAnyTransition_114040.orderedInterruption = true;
            lAnyTransition_114040.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114040.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114040.RemoveCondition(lAnyTransition_114040.conditions[i]); }
            lAnyTransition_114040.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114040.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 102f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114042 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114368, 0);
            if (lAnyTransition_114042 == null) { lAnyTransition_114042 = lLayerStateMachine.AddAnyStateTransition(lState_114368); }
            lAnyTransition_114042.isExit = false;
            lAnyTransition_114042.hasExitTime = false;
            lAnyTransition_114042.hasFixedDuration = true;
            lAnyTransition_114042.exitTime = 0f;
            lAnyTransition_114042.duration = 0f;
            lAnyTransition_114042.offset = 0f;
            lAnyTransition_114042.mute = false;
            lAnyTransition_114042.solo = false;
            lAnyTransition_114042.canTransitionToSelf = false;
            lAnyTransition_114042.orderedInterruption = true;
            lAnyTransition_114042.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114042.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114042.RemoveCondition(lAnyTransition_114042.conditions[i]); }
            lAnyTransition_114042.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114042.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 103f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114044 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114370, 0);
            if (lAnyTransition_114044 == null) { lAnyTransition_114044 = lLayerStateMachine.AddAnyStateTransition(lState_114370); }
            lAnyTransition_114044.isExit = false;
            lAnyTransition_114044.hasExitTime = false;
            lAnyTransition_114044.hasFixedDuration = true;
            lAnyTransition_114044.exitTime = 0f;
            lAnyTransition_114044.duration = 0f;
            lAnyTransition_114044.offset = 0f;
            lAnyTransition_114044.mute = false;
            lAnyTransition_114044.solo = false;
            lAnyTransition_114044.canTransitionToSelf = false;
            lAnyTransition_114044.orderedInterruption = true;
            lAnyTransition_114044.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114044.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114044.RemoveCondition(lAnyTransition_114044.conditions[i]); }
            lAnyTransition_114044.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114044.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 104f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114046 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114372, 0);
            if (lAnyTransition_114046 == null) { lAnyTransition_114046 = lLayerStateMachine.AddAnyStateTransition(lState_114372); }
            lAnyTransition_114046.isExit = false;
            lAnyTransition_114046.hasExitTime = false;
            lAnyTransition_114046.hasFixedDuration = true;
            lAnyTransition_114046.exitTime = 0f;
            lAnyTransition_114046.duration = 0f;
            lAnyTransition_114046.offset = 0f;
            lAnyTransition_114046.mute = false;
            lAnyTransition_114046.solo = false;
            lAnyTransition_114046.canTransitionToSelf = true;
            lAnyTransition_114046.orderedInterruption = true;
            lAnyTransition_114046.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114046.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114046.RemoveCondition(lAnyTransition_114046.conditions[i]); }
            lAnyTransition_114046.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114046.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 111f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114048 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114374, 0);
            if (lAnyTransition_114048 == null) { lAnyTransition_114048 = lLayerStateMachine.AddAnyStateTransition(lState_114374); }
            lAnyTransition_114048.isExit = false;
            lAnyTransition_114048.hasExitTime = false;
            lAnyTransition_114048.hasFixedDuration = false;
            lAnyTransition_114048.exitTime = 1.035465E-08f;
            lAnyTransition_114048.duration = 0f;
            lAnyTransition_114048.offset = 0.05644388f;
            lAnyTransition_114048.mute = false;
            lAnyTransition_114048.solo = false;
            lAnyTransition_114048.canTransitionToSelf = true;
            lAnyTransition_114048.orderedInterruption = true;
            lAnyTransition_114048.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114048.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114048.RemoveCondition(lAnyTransition_114048.conditions[i]); }
            lAnyTransition_114048.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114048.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 303f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114054 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114380, 0);
            if (lAnyTransition_114054 == null) { lAnyTransition_114054 = lLayerStateMachine.AddAnyStateTransition(lState_114380); }
            lAnyTransition_114054.isExit = false;
            lAnyTransition_114054.hasExitTime = false;
            lAnyTransition_114054.hasFixedDuration = true;
            lAnyTransition_114054.exitTime = 0f;
            lAnyTransition_114054.duration = 0.05f;
            lAnyTransition_114054.offset = 0f;
            lAnyTransition_114054.mute = false;
            lAnyTransition_114054.solo = false;
            lAnyTransition_114054.canTransitionToSelf = false;
            lAnyTransition_114054.orderedInterruption = true;
            lAnyTransition_114054.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114054.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114054.RemoveCondition(lAnyTransition_114054.conditions[i]); }
            lAnyTransition_114054.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114054.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 200f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114090 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114408, 0);
            if (lAnyTransition_114090 == null) { lAnyTransition_114090 = lLayerStateMachine.AddAnyStateTransition(lState_114408); }
            lAnyTransition_114090.isExit = false;
            lAnyTransition_114090.hasExitTime = false;
            lAnyTransition_114090.hasFixedDuration = true;
            lAnyTransition_114090.exitTime = 2.497404E-09f;
            lAnyTransition_114090.duration = 0f;
            lAnyTransition_114090.offset = 0.05644388f;
            lAnyTransition_114090.mute = false;
            lAnyTransition_114090.solo = false;
            lAnyTransition_114090.canTransitionToSelf = false;
            lAnyTransition_114090.orderedInterruption = true;
            lAnyTransition_114090.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114090.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114090.RemoveCondition(lAnyTransition_114090.conditions[i]); }
            lAnyTransition_114090.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114090.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 301f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114092 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114352, 1);
            if (lAnyTransition_114092 == null) { lAnyTransition_114092 = lLayerStateMachine.AddAnyStateTransition(lState_114352); }
            lAnyTransition_114092.isExit = false;
            lAnyTransition_114092.hasExitTime = false;
            lAnyTransition_114092.hasFixedDuration = false;
            lAnyTransition_114092.exitTime = 0f;
            lAnyTransition_114092.duration = 0f;
            lAnyTransition_114092.offset = 0f;
            lAnyTransition_114092.mute = false;
            lAnyTransition_114092.solo = false;
            lAnyTransition_114092.canTransitionToSelf = false;
            lAnyTransition_114092.orderedInterruption = true;
            lAnyTransition_114092.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114092.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114092.RemoveCondition(lAnyTransition_114092.conditions[i]); }
            lAnyTransition_114092.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114092.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 302f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114094 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114374, 1);
            if (lAnyTransition_114094 == null) { lAnyTransition_114094 = lLayerStateMachine.AddAnyStateTransition(lState_114374); }
            lAnyTransition_114094.isExit = false;
            lAnyTransition_114094.hasExitTime = false;
            lAnyTransition_114094.hasFixedDuration = false;
            lAnyTransition_114094.exitTime = 0f;
            lAnyTransition_114094.duration = 0f;
            lAnyTransition_114094.offset = 0.05644388f;
            lAnyTransition_114094.mute = false;
            lAnyTransition_114094.solo = false;
            lAnyTransition_114094.canTransitionToSelf = true;
            lAnyTransition_114094.orderedInterruption = true;
            lAnyTransition_114094.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114094.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114094.RemoveCondition(lAnyTransition_114094.conditions[i]); }
            lAnyTransition_114094.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114094.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 302f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114116 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114420, 0);
            if (lAnyTransition_114116 == null) { lAnyTransition_114116 = lLayerStateMachine.AddAnyStateTransition(lState_114420); }
            lAnyTransition_114116.isExit = false;
            lAnyTransition_114116.hasExitTime = false;
            lAnyTransition_114116.hasFixedDuration = true;
            lAnyTransition_114116.exitTime = 0f;
            lAnyTransition_114116.duration = 0.05f;
            lAnyTransition_114116.offset = 0f;
            lAnyTransition_114116.mute = false;
            lAnyTransition_114116.solo = false;
            lAnyTransition_114116.canTransitionToSelf = true;
            lAnyTransition_114116.orderedInterruption = true;
            lAnyTransition_114116.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114116.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114116.RemoveCondition(lAnyTransition_114116.conditions[i]); }
            lAnyTransition_114116.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114116.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 200f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114118 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114420, 1);
            if (lAnyTransition_114118 == null) { lAnyTransition_114118 = lLayerStateMachine.AddAnyStateTransition(lState_114420); }
            lAnyTransition_114118.isExit = false;
            lAnyTransition_114118.hasExitTime = false;
            lAnyTransition_114118.hasFixedDuration = true;
            lAnyTransition_114118.exitTime = 0.75f;
            lAnyTransition_114118.duration = 0.25f;
            lAnyTransition_114118.offset = 0f;
            lAnyTransition_114118.mute = false;
            lAnyTransition_114118.solo = false;
            lAnyTransition_114118.canTransitionToSelf = true;
            lAnyTransition_114118.orderedInterruption = true;
            lAnyTransition_114118.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114118.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114118.RemoveCondition(lAnyTransition_114118.conditions[i]); }
            lAnyTransition_114118.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114118.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 400f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lTransition_115546 = MotionControllerMotion.EditorFindTransition(lState_114344, lState_115540, 0);
            if (lTransition_115546 == null) { lTransition_115546 = lState_114344.AddTransition(lState_115540); }
            lTransition_115546.isExit = false;
            lTransition_115546.hasExitTime = true;
            lTransition_115546.hasFixedDuration = true;
            lTransition_115546.exitTime = 0.5847073f;
            lTransition_115546.duration = 0.09999999f;
            lTransition_115546.offset = 0f;
            lTransition_115546.mute = false;
            lTransition_115546.solo = false;
            lTransition_115546.canTransitionToSelf = true;
            lTransition_115546.orderedInterruption = true;
            lTransition_115546.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115546.conditions.Length - 1; i >= 0; i--) { lTransition_115546.RemoveCondition(lTransition_115546.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115548 = MotionControllerMotion.EditorFindTransition(lState_114350, lState_115542, 0);
            if (lTransition_115548 == null) { lTransition_115548 = lState_114350.AddTransition(lState_115542); }
            lTransition_115548.isExit = false;
            lTransition_115548.hasExitTime = true;
            lTransition_115548.hasFixedDuration = true;
            lTransition_115548.exitTime = 0.972064f;
            lTransition_115548.duration = 0.2500001f;
            lTransition_115548.offset = 0f;
            lTransition_115548.mute = false;
            lTransition_115548.solo = false;
            lTransition_115548.canTransitionToSelf = true;
            lTransition_115548.orderedInterruption = true;
            lTransition_115548.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115548.conditions.Length - 1; i >= 0; i--) { lTransition_115548.RemoveCondition(lTransition_115548.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115550 = MotionControllerMotion.EditorFindTransition(lState_114360, lState_115540, 0);
            if (lTransition_115550 == null) { lTransition_115550 = lState_114360.AddTransition(lState_115540); }
            lTransition_115550.isExit = false;
            lTransition_115550.hasExitTime = true;
            lTransition_115550.hasFixedDuration = true;
            lTransition_115550.exitTime = 0.5467574f;
            lTransition_115550.duration = 0.2499998f;
            lTransition_115550.offset = 0f;
            lTransition_115550.mute = false;
            lTransition_115550.solo = false;
            lTransition_115550.canTransitionToSelf = true;
            lTransition_115550.orderedInterruption = true;
            lTransition_115550.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115550.conditions.Length - 1; i >= 0; i--) { lTransition_115550.RemoveCondition(lTransition_115550.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115552 = MotionControllerMotion.EditorFindTransition(lState_114354, lState_115540, 0);
            if (lTransition_115552 == null) { lTransition_115552 = lState_114354.AddTransition(lState_115540); }
            lTransition_115552.isExit = false;
            lTransition_115552.hasExitTime = true;
            lTransition_115552.hasFixedDuration = true;
            lTransition_115552.exitTime = 0.2967281f;
            lTransition_115552.duration = 0.2499998f;
            lTransition_115552.offset = 0.145962f;
            lTransition_115552.mute = false;
            lTransition_115552.solo = false;
            lTransition_115552.canTransitionToSelf = true;
            lTransition_115552.orderedInterruption = true;
            lTransition_115552.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115552.conditions.Length - 1; i >= 0; i--) { lTransition_115552.RemoveCondition(lTransition_115552.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115554 = MotionControllerMotion.EditorFindTransition(lState_114358, lState_115542, 0);
            if (lTransition_115554 == null) { lTransition_115554 = lState_114358.AddTransition(lState_115542); }
            lTransition_115554.isExit = false;
            lTransition_115554.hasExitTime = true;
            lTransition_115554.hasFixedDuration = true;
            lTransition_115554.exitTime = 0.8863636f;
            lTransition_115554.duration = 0.25f;
            lTransition_115554.offset = 0f;
            lTransition_115554.mute = false;
            lTransition_115554.solo = false;
            lTransition_115554.canTransitionToSelf = true;
            lTransition_115554.orderedInterruption = true;
            lTransition_115554.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115554.conditions.Length - 1; i >= 0; i--) { lTransition_115554.RemoveCondition(lTransition_115554.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115556 = MotionControllerMotion.EditorFindTransition(lState_114356, lState_115540, 0);
            if (lTransition_115556 == null) { lTransition_115556 = lState_114356.AddTransition(lState_115540); }
            lTransition_115556.isExit = false;
            lTransition_115556.hasExitTime = true;
            lTransition_115556.hasFixedDuration = true;
            lTransition_115556.exitTime = 0.3847882f;
            lTransition_115556.duration = 0.2499998f;
            lTransition_115556.offset = 0f;
            lTransition_115556.mute = false;
            lTransition_115556.solo = false;
            lTransition_115556.canTransitionToSelf = true;
            lTransition_115556.orderedInterruption = true;
            lTransition_115556.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115556.conditions.Length - 1; i >= 0; i--) { lTransition_115556.RemoveCondition(lTransition_115556.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115558 = MotionControllerMotion.EditorFindTransition(lState_114352, lState_115542, 0);
            if (lTransition_115558 == null) { lTransition_115558 = lState_114352.AddTransition(lState_115542); }
            lTransition_115558.isExit = false;
            lTransition_115558.hasExitTime = true;
            lTransition_115558.hasFixedDuration = true;
            lTransition_115558.exitTime = 1.345912f;
            lTransition_115558.duration = 0.25f;
            lTransition_115558.offset = 0f;
            lTransition_115558.mute = false;
            lTransition_115558.solo = false;
            lTransition_115558.canTransitionToSelf = true;
            lTransition_115558.orderedInterruption = true;
            lTransition_115558.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115558.conditions.Length - 1; i >= 0; i--) { lTransition_115558.RemoveCondition(lTransition_115558.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115560 = MotionControllerMotion.EditorFindTransition(lState_115542, lState_115540, 0);
            if (lTransition_115560 == null) { lTransition_115560 = lState_115542.AddTransition(lState_115540); }
            lTransition_115560.isExit = false;
            lTransition_115560.hasExitTime = true;
            lTransition_115560.hasFixedDuration = true;
            lTransition_115560.exitTime = 0.7f;
            lTransition_115560.duration = 0.25f;
            lTransition_115560.offset = 0f;
            lTransition_115560.mute = false;
            lTransition_115560.solo = false;
            lTransition_115560.canTransitionToSelf = true;
            lTransition_115560.orderedInterruption = true;
            lTransition_115560.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115560.conditions.Length - 1; i >= 0; i--) { lTransition_115560.RemoveCondition(lTransition_115560.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115562 = MotionControllerMotion.EditorFindTransition(lState_114362, lState_115544, 0);
            if (lTransition_115562 == null) { lTransition_115562 = lState_114362.AddTransition(lState_115544); }
            lTransition_115562.isExit = false;
            lTransition_115562.hasExitTime = true;
            lTransition_115562.hasFixedDuration = true;
            lTransition_115562.exitTime = 0.1313023f;
            lTransition_115562.duration = 0.25f;
            lTransition_115562.offset = 0.1362922f;
            lTransition_115562.mute = false;
            lTransition_115562.solo = false;
            lTransition_115562.canTransitionToSelf = true;
            lTransition_115562.orderedInterruption = true;
            lTransition_115562.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115562.conditions.Length - 1; i >= 0; i--) { lTransition_115562.RemoveCondition(lTransition_115562.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115564 = MotionControllerMotion.EditorFindTransition(lState_114364, lState_115540, 0);
            if (lTransition_115564 == null) { lTransition_115564 = lState_114364.AddTransition(lState_115540); }
            lTransition_115564.isExit = false;
            lTransition_115564.hasExitTime = true;
            lTransition_115564.hasFixedDuration = true;
            lTransition_115564.exitTime = 0.2315253f;
            lTransition_115564.duration = 0.25f;
            lTransition_115564.offset = 0f;
            lTransition_115564.mute = false;
            lTransition_115564.solo = false;
            lTransition_115564.canTransitionToSelf = true;
            lTransition_115564.orderedInterruption = true;
            lTransition_115564.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115564.conditions.Length - 1; i >= 0; i--) { lTransition_115564.RemoveCondition(lTransition_115564.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115566 = MotionControllerMotion.EditorFindTransition(lState_114366, lState_115540, 0);
            if (lTransition_115566 == null) { lTransition_115566 = lState_114366.AddTransition(lState_115540); }
            lTransition_115566.isExit = false;
            lTransition_115566.hasExitTime = true;
            lTransition_115566.hasFixedDuration = true;
            lTransition_115566.exitTime = 0.7916667f;
            lTransition_115566.duration = 0.25f;
            lTransition_115566.offset = 0f;
            lTransition_115566.mute = false;
            lTransition_115566.solo = false;
            lTransition_115566.canTransitionToSelf = true;
            lTransition_115566.orderedInterruption = true;
            lTransition_115566.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115566.conditions.Length - 1; i >= 0; i--) { lTransition_115566.RemoveCondition(lTransition_115566.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115568 = MotionControllerMotion.EditorFindTransition(lState_114368, lState_115540, 0);
            if (lTransition_115568 == null) { lTransition_115568 = lState_114368.AddTransition(lState_115540); }
            lTransition_115568.isExit = false;
            lTransition_115568.hasExitTime = true;
            lTransition_115568.hasFixedDuration = true;
            lTransition_115568.exitTime = 0.7916667f;
            lTransition_115568.duration = 0.25f;
            lTransition_115568.offset = 0f;
            lTransition_115568.mute = false;
            lTransition_115568.solo = false;
            lTransition_115568.canTransitionToSelf = true;
            lTransition_115568.orderedInterruption = true;
            lTransition_115568.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115568.conditions.Length - 1; i >= 0; i--) { lTransition_115568.RemoveCondition(lTransition_115568.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115570 = MotionControllerMotion.EditorFindTransition(lState_114370, lState_115540, 0);
            if (lTransition_115570 == null) { lTransition_115570 = lState_114370.AddTransition(lState_115540); }
            lTransition_115570.isExit = false;
            lTransition_115570.hasExitTime = true;
            lTransition_115570.hasFixedDuration = true;
            lTransition_115570.exitTime = 0.90625f;
            lTransition_115570.duration = 0.25f;
            lTransition_115570.offset = 0f;
            lTransition_115570.mute = false;
            lTransition_115570.solo = false;
            lTransition_115570.canTransitionToSelf = true;
            lTransition_115570.orderedInterruption = true;
            lTransition_115570.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115570.conditions.Length - 1; i >= 0; i--) { lTransition_115570.RemoveCondition(lTransition_115570.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115572 = MotionControllerMotion.EditorFindTransition(lState_114372, lState_115540, 0);
            if (lTransition_115572 == null) { lTransition_115572 = lState_114372.AddTransition(lState_115540); }
            lTransition_115572.isExit = false;
            lTransition_115572.hasExitTime = true;
            lTransition_115572.hasFixedDuration = true;
            lTransition_115572.exitTime = 0.7857143f;
            lTransition_115572.duration = 0.25f;
            lTransition_115572.offset = 0f;
            lTransition_115572.mute = false;
            lTransition_115572.solo = false;
            lTransition_115572.canTransitionToSelf = true;
            lTransition_115572.orderedInterruption = true;
            lTransition_115572.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115572.conditions.Length - 1; i >= 0; i--) { lTransition_115572.RemoveCondition(lTransition_115572.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115574 = MotionControllerMotion.EditorFindTransition(lState_114374, lState_115542, 0);
            if (lTransition_115574 == null) { lTransition_115574 = lState_114374.AddTransition(lState_115542); }
            lTransition_115574.isExit = false;
            lTransition_115574.hasExitTime = true;
            lTransition_115574.hasFixedDuration = true;
            lTransition_115574.exitTime = 0.8672566f;
            lTransition_115574.duration = 0.25f;
            lTransition_115574.offset = 0f;
            lTransition_115574.mute = false;
            lTransition_115574.solo = false;
            lTransition_115574.canTransitionToSelf = true;
            lTransition_115574.orderedInterruption = true;
            lTransition_115574.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115574.conditions.Length - 1; i >= 0; i--) { lTransition_115574.RemoveCondition(lTransition_115574.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115576 = MotionControllerMotion.EditorFindTransition(lState_115544, lState_115540, 0);
            if (lTransition_115576 == null) { lTransition_115576 = lState_115544.AddTransition(lState_115540); }
            lTransition_115576.isExit = false;
            lTransition_115576.hasExitTime = true;
            lTransition_115576.hasFixedDuration = true;
            lTransition_115576.exitTime = 0.4935692f;
            lTransition_115576.duration = 0.2499995f;
            lTransition_115576.offset = 0f;
            lTransition_115576.mute = false;
            lTransition_115576.solo = false;
            lTransition_115576.canTransitionToSelf = true;
            lTransition_115576.orderedInterruption = true;
            lTransition_115576.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115576.conditions.Length - 1; i >= 0; i--) { lTransition_115576.RemoveCondition(lTransition_115576.conditions[i]); }
            lTransition_115576.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32052f, "L" + rLayerIndex + "MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lTransition_115578 = MotionControllerMotion.EditorFindTransition(lState_114380, lState_115540, 0);
            if (lTransition_115578 == null) { lTransition_115578 = lState_114380.AddTransition(lState_115540); }
            lTransition_115578.isExit = false;
            lTransition_115578.hasExitTime = true;
            lTransition_115578.hasFixedDuration = true;
            lTransition_115578.exitTime = 0.90625f;
            lTransition_115578.duration = 0.25f;
            lTransition_115578.offset = 0f;
            lTransition_115578.mute = false;
            lTransition_115578.solo = false;
            lTransition_115578.canTransitionToSelf = true;
            lTransition_115578.orderedInterruption = true;
            lTransition_115578.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115578.conditions.Length - 1; i >= 0; i--) { lTransition_115578.RemoveCondition(lTransition_115578.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115580 = MotionControllerMotion.EditorFindTransition(lState_114408, lState_114354, 0);
            if (lTransition_115580 == null) { lTransition_115580 = lState_114408.AddTransition(lState_114354); }
            lTransition_115580.isExit = false;
            lTransition_115580.hasExitTime = true;
            lTransition_115580.hasFixedDuration = true;
            lTransition_115580.exitTime = 0.1615123f;
            lTransition_115580.duration = 0.4819526f;
            lTransition_115580.offset = 0f;
            lTransition_115580.mute = false;
            lTransition_115580.solo = false;
            lTransition_115580.canTransitionToSelf = true;
            lTransition_115580.orderedInterruption = true;
            lTransition_115580.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115580.conditions.Length - 1; i >= 0; i--) { lTransition_115580.RemoveCondition(lTransition_115580.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115582 = MotionControllerMotion.EditorFindTransition(lState_114420, lState_115542, 0);
            if (lTransition_115582 == null) { lTransition_115582 = lState_114420.AddTransition(lState_115542); }
            lTransition_115582.isExit = false;
            lTransition_115582.hasExitTime = true;
            lTransition_115582.hasFixedDuration = true;
            lTransition_115582.exitTime = 0.8611111f;
            lTransition_115582.duration = 0.25f;
            lTransition_115582.offset = 0f;
            lTransition_115582.mute = false;
            lTransition_115582.solo = false;
            lTransition_115582.canTransitionToSelf = true;
            lTransition_115582.orderedInterruption = true;
            lTransition_115582.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115582.conditions.Length - 1; i >= 0; i--) { lTransition_115582.RemoveCondition(lTransition_115582.conditions[i]); }


            // Run any post processing after creating the state machine
            OnStateMachineCreated();
        }

#endif

        // ************************************ END AUTO GENERATED ************************************
        #endregion
#if UNITY_EDITOR
        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
        public static void ExtendBasicMotion(MotionController rMotionController, int rLayerIndex)
        {
            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_113772 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "PSS_Utilities-SM");
            if (lSSM_113772 == null) { lSSM_113772 = lLayerStateMachine.AddStateMachine("PSS_Utilities-SM", new Vector3(672, -324, 0)); }

            UnityEditor.Animations.AnimatorState lState_114346 = MotionControllerMotion.EditorFindState(lSSM_113772, "Death_0");
            if (lState_114346 == null) { lState_114346 = lSSM_113772.AddState("Death_0", new Vector3(-204, -12, 0)); }
            lState_114346.speed = 1f;
            lState_114346.mirror = false;
            lState_114346.tag = "";
            lState_114346.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_HighKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_114348 = MotionControllerMotion.EditorFindState(lSSM_113772, "Death_180");
            if (lState_114348 == null) { lState_114348 = lSSM_113772.AddState("Death_180", new Vector3(-204, 48, 0)); }
            lState_114348.speed = 1f;
            lState_114348.mirror = false;
            lState_114348.tag = "";
            lState_114348.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_MidKO_Back");

            UnityEditor.Animations.AnimatorState lState_114344 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_100");
            if (lState_114344 == null) { lState_114344 = lSSM_113772.AddState("Damaged_100", new Vector3(-216, -432, 0)); }
            lState_114344.speed = 1f;
            lState_114344.mirror = false;
            lState_114344.tag = "";
            lState_114344.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighFront_Weak");

            UnityEditor.Animations.AnimatorState lState_115540 = MotionControllerMotion.EditorFindState(lSSM_113772, "PSS_IdlePose");
            if (lState_115540 == null) { lState_115540 = lSSM_113772.AddState("PSS_IdlePose", new Vector3(420, -300, 0)); }
            lState_115540.speed = 1f;
            lState_115540.mirror = false;
            lState_115540.tag = "Exit";
            lState_115540.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Idle_3");

            UnityEditor.Animations.AnimatorState lState_114350 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Trip_R");
            if (lState_114350 == null) { lState_114350 = lSSM_113772.AddState("Damaged_Trip_R", new Vector3(264, 132, 0)); }
            lState_114350.speed = 1f;
            lState_114350.mirror = false;
            lState_114350.tag = "";
            lState_114350.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_LowKO_R");

            UnityEditor.Animations.AnimatorState lState_114360 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Stun_00");
            if (lState_114360 == null) { lState_114360 = lSSM_113772.AddState("Damaged_Stun_00", new Vector3(168, -168, 0)); }
            lState_114360.speed = 1f;
            lState_114360.mirror = false;
            lState_114360.tag = "";
            lState_114360.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidFront_Stagger");

            UnityEditor.Animations.AnimatorState lState_114354 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_300");
            if (lState_114354 == null) { lState_114354 = lSSM_113772.AddState("Damaged_300", new Vector3(288, -120, 0)); }
            lState_114354.speed = 1f;
            lState_114354.mirror = false;
            lState_114354.tag = "";
            lState_114354.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_HighFront_Stagger");

            UnityEditor.Animations.AnimatorState lState_114358 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Stun_304");
            if (lState_114358 == null) { lState_114358 = lSSM_113772.AddState("Damaged_Stun_304", new Vector3(396, -12, 0)); }
            lState_114358.speed = 1f;
            lState_114358.mirror = false;
            lState_114358.tag = "";
            lState_114358.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_UpperKO");

            UnityEditor.Animations.AnimatorState lState_114356 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Stun_02");
            if (lState_114356 == null) { lState_114356 = lSSM_113772.AddState("Damaged_Stun_02", new Vector3(372, -84, 0)); }
            lState_114356.speed = 1f;
            lState_114356.mirror = false;
            lState_114356.tag = "";
            lState_114356.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidRight_Stagger");

            UnityEditor.Animations.AnimatorState lState_114352 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_Punted");
            if (lState_114352 == null) { lState_114352 = lSSM_113772.AddState("Damaged_Punted", new Vector3(264, 72, 0)); }
            lState_114352.speed = 1f;
            lState_114352.mirror = false;
            lState_114352.tag = "";
            lState_114352.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_HighKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_115542 = MotionControllerMotion.EditorFindState(lSSM_113772, "Getup");
            if (lState_115542 == null) { lState_115542 = lSSM_113772.AddState("Getup", new Vector3(576, 84, 0)); }
            lState_115542.speed = 1f;
            lState_115542.mirror = false;
            lState_115542.tag = "";
            lState_115542.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_LayBack_Roll");

            UnityEditor.Animations.AnimatorState lState_114362 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_99");
            if (lState_114362 == null) { lState_114362 = lSSM_113772.AddState("Damaged_99", new Vector3(-216, -468, 0)); }
            lState_114362.speed = 1f;
            lState_114362.mirror = false;
            lState_114362.tag = "";
            lState_114362.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_TopKO");

            UnityEditor.Animations.AnimatorState lState_114364 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_101");
            if (lState_114364 == null) { lState_114364 = lSSM_113772.AddState("Damaged_101", new Vector3(-216, -396, 0)); }
            lState_114364.speed = 1f;
            lState_114364.mirror = false;
            lState_114364.tag = "";
            lState_114364.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighUpper_Weak");

            UnityEditor.Animations.AnimatorState lState_114366 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_102");
            if (lState_114366 == null) { lState_114366 = lSSM_113772.AddState("Damaged_102", new Vector3(-216, -360, 0)); }
            lState_114366.speed = 1f;
            lState_114366.mirror = false;
            lState_114366.tag = "";
            lState_114366.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighUpper_Weak");

            UnityEditor.Animations.AnimatorState lState_114368 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_103");
            if (lState_114368 == null) { lState_114368 = lSSM_113772.AddState("Damaged_103", new Vector3(-216, -324, 0)); }
            lState_114368.speed = 1f;
            lState_114368.mirror = false;
            lState_114368.tag = "";
            lState_114368.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_p_HighUpper_Weak");

            UnityEditor.Animations.AnimatorState lState_114370 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_104");
            if (lState_114370 == null) { lState_114370 = lSSM_113772.AddState("Damaged_104", new Vector3(-216, -288, 0)); }
            lState_114370.speed = 1f;
            lState_114370.mirror = false;
            lState_114370.tag = "";
            lState_114370.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidFront_Stagger");

            UnityEditor.Animations.AnimatorState lState_114372 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_111");
            if (lState_114372 == null) { lState_114372 = lSSM_113772.AddState("Damaged_111", new Vector3(-216, -228, 0)); }
            lState_114372.speed = 1f;
            lState_114372.mirror = false;
            lState_114372.tag = "";
            lState_114372.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Hits.fbx", "KB_Hit_m_MidLeft_Weak");

            UnityEditor.Animations.AnimatorState lState_114374 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_303");
            if (lState_114374 == null) { lState_114374 = lSSM_113772.AddState("Damaged_303", new Vector3(-216, -156, 0)); }
            lState_114374.speed = 1f;
            lState_114374.mirror = true;
            lState_114374.tag = "";
            lState_114374.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_HighKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_115544 = MotionControllerMotion.EditorFindState(lSSM_113772, "StumbleBack");
            if (lState_115544 == null) { lState_115544 = lSSM_113772.AddState("StumbleBack", new Vector3(178.351f, -492.3941f, 0)); }
            lState_115544.speed = 1f;
            lState_115544.mirror = false;
            lState_115544.tag = "";
            lState_115544.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Dodging Back.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114380 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_200");
            if (lState_114380 == null) { lState_114380 = lSSM_113772.AddState("Damaged_200", new Vector3(-204, -96, 0)); }
            lState_114380.speed = 1f;
            lState_114380.mirror = true;
            lState_114380.tag = "";
            lState_114380.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_MidKO_Powerful");

            UnityEditor.Animations.AnimatorState lState_114408 = MotionControllerMotion.EditorFindState(lSSM_113772, "Damaged_301");
            if (lState_114408 == null) { lState_114408 = lSSM_113772.AddState("Damaged_301", new Vector3(48, 108, 0)); }
            lState_114408.speed = 1f;
            lState_114408.mirror = false;
            lState_114408.tag = "";
            lState_114408.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_UpperKO");

            UnityEditor.Animations.AnimatorState lState_114420 = MotionControllerMotion.EditorFindState(lSSM_113772, "KB_MidKO_Powerful");
            if (lState_114420 == null) { lState_114420 = lSSM_113772.AddState("KB_MidKO_Powerful", new Vector3(240, 204, 0)); }
            lState_114420.speed = 1f;
            lState_114420.mirror = false;
            lState_114420.tag = "";
            lState_114420.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_KOs.fbx", "KB_MidKO_Powerful");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114016 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114344, 0);
            if (lAnyTransition_114016 == null) { lAnyTransition_114016 = lLayerStateMachine.AddAnyStateTransition(lState_114344); }
            lAnyTransition_114016.isExit = false;
            lAnyTransition_114016.hasExitTime = false;
            lAnyTransition_114016.hasFixedDuration = true;
            lAnyTransition_114016.exitTime = 0f;
            lAnyTransition_114016.duration = 0f;
            lAnyTransition_114016.offset = 0f;
            lAnyTransition_114016.mute = false;
            lAnyTransition_114016.solo = false;
            lAnyTransition_114016.canTransitionToSelf = false;
            lAnyTransition_114016.orderedInterruption = true;
            lAnyTransition_114016.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114016.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114016.RemoveCondition(lAnyTransition_114016.conditions[i]); }
            lAnyTransition_114016.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114016.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114018 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114346, 0);
            if (lAnyTransition_114018 == null) { lAnyTransition_114018 = lLayerStateMachine.AddAnyStateTransition(lState_114346); }
            lAnyTransition_114018.isExit = false;
            lAnyTransition_114018.hasExitTime = false;
            lAnyTransition_114018.hasFixedDuration = true;
            lAnyTransition_114018.exitTime = 0.9f;
            lAnyTransition_114018.duration = 0.1f;
            lAnyTransition_114018.offset = 0f;
            lAnyTransition_114018.mute = false;
            lAnyTransition_114018.solo = false;
            lAnyTransition_114018.canTransitionToSelf = true;
            lAnyTransition_114018.orderedInterruption = true;
            lAnyTransition_114018.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114018.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114018.RemoveCondition(lAnyTransition_114018.conditions[i]); }
            lAnyTransition_114018.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32051f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114018.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Greater, -100f, "L" + rLayerIndex + "MotionParameter");
            lAnyTransition_114018.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Less, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114020 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114348, 0);
            if (lAnyTransition_114020 == null) { lAnyTransition_114020 = lLayerStateMachine.AddAnyStateTransition(lState_114348); }
            lAnyTransition_114020.isExit = false;
            lAnyTransition_114020.hasExitTime = false;
            lAnyTransition_114020.hasFixedDuration = true;
            lAnyTransition_114020.exitTime = 0.9f;
            lAnyTransition_114020.duration = 0.1f;
            lAnyTransition_114020.offset = 0f;
            lAnyTransition_114020.mute = false;
            lAnyTransition_114020.solo = false;
            lAnyTransition_114020.canTransitionToSelf = true;
            lAnyTransition_114020.orderedInterruption = true;
            lAnyTransition_114020.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114020.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114020.RemoveCondition(lAnyTransition_114020.conditions[i]); }
            lAnyTransition_114020.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32051f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114020.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Less, -100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114022 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114348, 1);
            if (lAnyTransition_114022 == null) { lAnyTransition_114022 = lLayerStateMachine.AddAnyStateTransition(lState_114348); }
            lAnyTransition_114022.isExit = false;
            lAnyTransition_114022.hasExitTime = false;
            lAnyTransition_114022.hasFixedDuration = true;
            lAnyTransition_114022.exitTime = 0.9f;
            lAnyTransition_114022.duration = 0.1f;
            lAnyTransition_114022.offset = 0f;
            lAnyTransition_114022.mute = false;
            lAnyTransition_114022.solo = false;
            lAnyTransition_114022.canTransitionToSelf = true;
            lAnyTransition_114022.orderedInterruption = true;
            lAnyTransition_114022.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114022.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114022.RemoveCondition(lAnyTransition_114022.conditions[i]); }
            lAnyTransition_114022.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32051f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114022.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Greater, 100f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114024 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114350, 0);
            if (lAnyTransition_114024 == null) { lAnyTransition_114024 = lLayerStateMachine.AddAnyStateTransition(lState_114350); }
            lAnyTransition_114024.isExit = false;
            lAnyTransition_114024.hasExitTime = false;
            lAnyTransition_114024.hasFixedDuration = true;
            lAnyTransition_114024.exitTime = 0f;
            lAnyTransition_114024.duration = 0f;
            lAnyTransition_114024.offset = 0f;
            lAnyTransition_114024.mute = false;
            lAnyTransition_114024.solo = false;
            lAnyTransition_114024.canTransitionToSelf = false;
            lAnyTransition_114024.orderedInterruption = true;
            lAnyTransition_114024.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114024.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114024.RemoveCondition(lAnyTransition_114024.conditions[i]); }
            lAnyTransition_114024.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114024.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 112f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114026 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114352, 0);
            if (lAnyTransition_114026 == null) { lAnyTransition_114026 = lLayerStateMachine.AddAnyStateTransition(lState_114352); }
            lAnyTransition_114026.isExit = false;
            lAnyTransition_114026.hasExitTime = false;
            lAnyTransition_114026.hasFixedDuration = false;
            lAnyTransition_114026.exitTime = 0f;
            lAnyTransition_114026.duration = 0f;
            lAnyTransition_114026.offset = 0f;
            lAnyTransition_114026.mute = false;
            lAnyTransition_114026.solo = false;
            lAnyTransition_114026.canTransitionToSelf = false;
            lAnyTransition_114026.orderedInterruption = true;
            lAnyTransition_114026.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114026.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114026.RemoveCondition(lAnyTransition_114026.conditions[i]); }
            lAnyTransition_114026.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114026.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 105f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114028 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114354, 0);
            if (lAnyTransition_114028 == null) { lAnyTransition_114028 = lLayerStateMachine.AddAnyStateTransition(lState_114354); }
            lAnyTransition_114028.isExit = false;
            lAnyTransition_114028.hasExitTime = false;
            lAnyTransition_114028.hasFixedDuration = true;
            lAnyTransition_114028.exitTime = 0f;
            lAnyTransition_114028.duration = 0f;
            lAnyTransition_114028.offset = 0.05644388f;
            lAnyTransition_114028.mute = false;
            lAnyTransition_114028.solo = false;
            lAnyTransition_114028.canTransitionToSelf = false;
            lAnyTransition_114028.orderedInterruption = true;
            lAnyTransition_114028.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114028.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114028.RemoveCondition(lAnyTransition_114028.conditions[i]); }
            lAnyTransition_114028.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114028.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 300f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114030 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114356, 0);
            if (lAnyTransition_114030 == null) { lAnyTransition_114030 = lLayerStateMachine.AddAnyStateTransition(lState_114356); }
            lAnyTransition_114030.isExit = false;
            lAnyTransition_114030.hasExitTime = false;
            lAnyTransition_114030.hasFixedDuration = true;
            lAnyTransition_114030.exitTime = 0f;
            lAnyTransition_114030.duration = 0.05f;
            lAnyTransition_114030.offset = 0f;
            lAnyTransition_114030.mute = false;
            lAnyTransition_114030.solo = false;
            lAnyTransition_114030.canTransitionToSelf = false;
            lAnyTransition_114030.orderedInterruption = true;
            lAnyTransition_114030.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114030.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114030.RemoveCondition(lAnyTransition_114030.conditions[i]); }
            lAnyTransition_114030.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114030.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 0f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114032 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114358, 0);
            if (lAnyTransition_114032 == null) { lAnyTransition_114032 = lLayerStateMachine.AddAnyStateTransition(lState_114358); }
            lAnyTransition_114032.isExit = false;
            lAnyTransition_114032.hasExitTime = false;
            lAnyTransition_114032.hasFixedDuration = true;
            lAnyTransition_114032.exitTime = 0f;
            lAnyTransition_114032.duration = 0f;
            lAnyTransition_114032.offset = 0.05644388f;
            lAnyTransition_114032.mute = false;
            lAnyTransition_114032.solo = false;
            lAnyTransition_114032.canTransitionToSelf = true;
            lAnyTransition_114032.orderedInterruption = true;
            lAnyTransition_114032.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114032.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114032.RemoveCondition(lAnyTransition_114032.conditions[i]); }
            lAnyTransition_114032.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114032.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 304f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114034 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114360, 0);
            if (lAnyTransition_114034 == null) { lAnyTransition_114034 = lLayerStateMachine.AddAnyStateTransition(lState_114360); }
            lAnyTransition_114034.isExit = false;
            lAnyTransition_114034.hasExitTime = false;
            lAnyTransition_114034.hasFixedDuration = true;
            lAnyTransition_114034.exitTime = 0f;
            lAnyTransition_114034.duration = 0.05f;
            lAnyTransition_114034.offset = 0f;
            lAnyTransition_114034.mute = false;
            lAnyTransition_114034.solo = false;
            lAnyTransition_114034.canTransitionToSelf = false;
            lAnyTransition_114034.orderedInterruption = true;
            lAnyTransition_114034.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114034.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114034.RemoveCondition(lAnyTransition_114034.conditions[i]); }
            lAnyTransition_114034.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114034.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 110f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114036 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114362, 0);
            if (lAnyTransition_114036 == null) { lAnyTransition_114036 = lLayerStateMachine.AddAnyStateTransition(lState_114362); }
            lAnyTransition_114036.isExit = false;
            lAnyTransition_114036.hasExitTime = false;
            lAnyTransition_114036.hasFixedDuration = true;
            lAnyTransition_114036.exitTime = 0f;
            lAnyTransition_114036.duration = 0f;
            lAnyTransition_114036.offset = 0f;
            lAnyTransition_114036.mute = false;
            lAnyTransition_114036.solo = false;
            lAnyTransition_114036.canTransitionToSelf = false;
            lAnyTransition_114036.orderedInterruption = true;
            lAnyTransition_114036.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114036.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114036.RemoveCondition(lAnyTransition_114036.conditions[i]); }
            lAnyTransition_114036.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114036.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114038 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114364, 0);
            if (lAnyTransition_114038 == null) { lAnyTransition_114038 = lLayerStateMachine.AddAnyStateTransition(lState_114364); }
            lAnyTransition_114038.isExit = false;
            lAnyTransition_114038.hasExitTime = false;
            lAnyTransition_114038.hasFixedDuration = true;
            lAnyTransition_114038.exitTime = 0f;
            lAnyTransition_114038.duration = 0f;
            lAnyTransition_114038.offset = 0f;
            lAnyTransition_114038.mute = false;
            lAnyTransition_114038.solo = false;
            lAnyTransition_114038.canTransitionToSelf = false;
            lAnyTransition_114038.orderedInterruption = true;
            lAnyTransition_114038.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114038.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114038.RemoveCondition(lAnyTransition_114038.conditions[i]); }
            lAnyTransition_114038.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114038.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 101f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114040 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114366, 0);
            if (lAnyTransition_114040 == null) { lAnyTransition_114040 = lLayerStateMachine.AddAnyStateTransition(lState_114366); }
            lAnyTransition_114040.isExit = false;
            lAnyTransition_114040.hasExitTime = false;
            lAnyTransition_114040.hasFixedDuration = true;
            lAnyTransition_114040.exitTime = 0f;
            lAnyTransition_114040.duration = 0f;
            lAnyTransition_114040.offset = 0f;
            lAnyTransition_114040.mute = false;
            lAnyTransition_114040.solo = false;
            lAnyTransition_114040.canTransitionToSelf = false;
            lAnyTransition_114040.orderedInterruption = true;
            lAnyTransition_114040.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114040.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114040.RemoveCondition(lAnyTransition_114040.conditions[i]); }
            lAnyTransition_114040.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114040.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 102f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114042 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114368, 0);
            if (lAnyTransition_114042 == null) { lAnyTransition_114042 = lLayerStateMachine.AddAnyStateTransition(lState_114368); }
            lAnyTransition_114042.isExit = false;
            lAnyTransition_114042.hasExitTime = false;
            lAnyTransition_114042.hasFixedDuration = true;
            lAnyTransition_114042.exitTime = 0f;
            lAnyTransition_114042.duration = 0f;
            lAnyTransition_114042.offset = 0f;
            lAnyTransition_114042.mute = false;
            lAnyTransition_114042.solo = false;
            lAnyTransition_114042.canTransitionToSelf = false;
            lAnyTransition_114042.orderedInterruption = true;
            lAnyTransition_114042.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114042.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114042.RemoveCondition(lAnyTransition_114042.conditions[i]); }
            lAnyTransition_114042.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114042.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 103f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114044 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114370, 0);
            if (lAnyTransition_114044 == null) { lAnyTransition_114044 = lLayerStateMachine.AddAnyStateTransition(lState_114370); }
            lAnyTransition_114044.isExit = false;
            lAnyTransition_114044.hasExitTime = false;
            lAnyTransition_114044.hasFixedDuration = true;
            lAnyTransition_114044.exitTime = 0f;
            lAnyTransition_114044.duration = 0f;
            lAnyTransition_114044.offset = 0f;
            lAnyTransition_114044.mute = false;
            lAnyTransition_114044.solo = false;
            lAnyTransition_114044.canTransitionToSelf = false;
            lAnyTransition_114044.orderedInterruption = true;
            lAnyTransition_114044.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114044.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114044.RemoveCondition(lAnyTransition_114044.conditions[i]); }
            lAnyTransition_114044.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114044.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 104f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114046 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114372, 0);
            if (lAnyTransition_114046 == null) { lAnyTransition_114046 = lLayerStateMachine.AddAnyStateTransition(lState_114372); }
            lAnyTransition_114046.isExit = false;
            lAnyTransition_114046.hasExitTime = false;
            lAnyTransition_114046.hasFixedDuration = true;
            lAnyTransition_114046.exitTime = 0f;
            lAnyTransition_114046.duration = 0f;
            lAnyTransition_114046.offset = 0f;
            lAnyTransition_114046.mute = false;
            lAnyTransition_114046.solo = false;
            lAnyTransition_114046.canTransitionToSelf = true;
            lAnyTransition_114046.orderedInterruption = true;
            lAnyTransition_114046.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114046.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114046.RemoveCondition(lAnyTransition_114046.conditions[i]); }
            lAnyTransition_114046.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114046.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 111f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114048 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114374, 0);
            if (lAnyTransition_114048 == null) { lAnyTransition_114048 = lLayerStateMachine.AddAnyStateTransition(lState_114374); }
            lAnyTransition_114048.isExit = false;
            lAnyTransition_114048.hasExitTime = false;
            lAnyTransition_114048.hasFixedDuration = false;
            lAnyTransition_114048.exitTime = 1.035465E-08f;
            lAnyTransition_114048.duration = 0f;
            lAnyTransition_114048.offset = 0.05644388f;
            lAnyTransition_114048.mute = false;
            lAnyTransition_114048.solo = false;
            lAnyTransition_114048.canTransitionToSelf = true;
            lAnyTransition_114048.orderedInterruption = true;
            lAnyTransition_114048.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114048.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114048.RemoveCondition(lAnyTransition_114048.conditions[i]); }
            lAnyTransition_114048.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114048.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 303f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114054 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114380, 0);
            if (lAnyTransition_114054 == null) { lAnyTransition_114054 = lLayerStateMachine.AddAnyStateTransition(lState_114380); }
            lAnyTransition_114054.isExit = false;
            lAnyTransition_114054.hasExitTime = false;
            lAnyTransition_114054.hasFixedDuration = true;
            lAnyTransition_114054.exitTime = 0f;
            lAnyTransition_114054.duration = 0.05f;
            lAnyTransition_114054.offset = 0f;
            lAnyTransition_114054.mute = false;
            lAnyTransition_114054.solo = false;
            lAnyTransition_114054.canTransitionToSelf = false;
            lAnyTransition_114054.orderedInterruption = true;
            lAnyTransition_114054.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114054.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114054.RemoveCondition(lAnyTransition_114054.conditions[i]); }
            lAnyTransition_114054.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114054.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 200f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114090 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114408, 0);
            if (lAnyTransition_114090 == null) { lAnyTransition_114090 = lLayerStateMachine.AddAnyStateTransition(lState_114408); }
            lAnyTransition_114090.isExit = false;
            lAnyTransition_114090.hasExitTime = false;
            lAnyTransition_114090.hasFixedDuration = true;
            lAnyTransition_114090.exitTime = 2.497404E-09f;
            lAnyTransition_114090.duration = 0f;
            lAnyTransition_114090.offset = 0.05644388f;
            lAnyTransition_114090.mute = false;
            lAnyTransition_114090.solo = false;
            lAnyTransition_114090.canTransitionToSelf = false;
            lAnyTransition_114090.orderedInterruption = true;
            lAnyTransition_114090.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114090.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114090.RemoveCondition(lAnyTransition_114090.conditions[i]); }
            lAnyTransition_114090.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114090.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 301f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114092 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114352, 1);
            if (lAnyTransition_114092 == null) { lAnyTransition_114092 = lLayerStateMachine.AddAnyStateTransition(lState_114352); }
            lAnyTransition_114092.isExit = false;
            lAnyTransition_114092.hasExitTime = false;
            lAnyTransition_114092.hasFixedDuration = false;
            lAnyTransition_114092.exitTime = 0f;
            lAnyTransition_114092.duration = 0f;
            lAnyTransition_114092.offset = 0f;
            lAnyTransition_114092.mute = false;
            lAnyTransition_114092.solo = false;
            lAnyTransition_114092.canTransitionToSelf = false;
            lAnyTransition_114092.orderedInterruption = true;
            lAnyTransition_114092.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114092.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114092.RemoveCondition(lAnyTransition_114092.conditions[i]); }
            lAnyTransition_114092.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114092.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 302f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114094 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114374, 1);
            if (lAnyTransition_114094 == null) { lAnyTransition_114094 = lLayerStateMachine.AddAnyStateTransition(lState_114374); }
            lAnyTransition_114094.isExit = false;
            lAnyTransition_114094.hasExitTime = false;
            lAnyTransition_114094.hasFixedDuration = false;
            lAnyTransition_114094.exitTime = 0f;
            lAnyTransition_114094.duration = 0f;
            lAnyTransition_114094.offset = 0.05644388f;
            lAnyTransition_114094.mute = false;
            lAnyTransition_114094.solo = false;
            lAnyTransition_114094.canTransitionToSelf = true;
            lAnyTransition_114094.orderedInterruption = true;
            lAnyTransition_114094.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114094.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114094.RemoveCondition(lAnyTransition_114094.conditions[i]); }
            lAnyTransition_114094.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114094.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 302f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114116 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114420, 0);
            if (lAnyTransition_114116 == null) { lAnyTransition_114116 = lLayerStateMachine.AddAnyStateTransition(lState_114420); }
            lAnyTransition_114116.isExit = false;
            lAnyTransition_114116.hasExitTime = false;
            lAnyTransition_114116.hasFixedDuration = true;
            lAnyTransition_114116.exitTime = 0f;
            lAnyTransition_114116.duration = 0.05f;
            lAnyTransition_114116.offset = 0f;
            lAnyTransition_114116.mute = false;
            lAnyTransition_114116.solo = false;
            lAnyTransition_114116.canTransitionToSelf = true;
            lAnyTransition_114116.orderedInterruption = true;
            lAnyTransition_114116.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114116.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114116.RemoveCondition(lAnyTransition_114116.conditions[i]); }
            lAnyTransition_114116.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114116.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 200f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114118 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114420, 1);
            if (lAnyTransition_114118 == null) { lAnyTransition_114118 = lLayerStateMachine.AddAnyStateTransition(lState_114420); }
            lAnyTransition_114118.isExit = false;
            lAnyTransition_114118.hasExitTime = false;
            lAnyTransition_114118.hasFixedDuration = true;
            lAnyTransition_114118.exitTime = 0.75f;
            lAnyTransition_114118.duration = 0.25f;
            lAnyTransition_114118.offset = 0f;
            lAnyTransition_114118.mute = false;
            lAnyTransition_114118.solo = false;
            lAnyTransition_114118.canTransitionToSelf = true;
            lAnyTransition_114118.orderedInterruption = true;
            lAnyTransition_114118.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114118.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114118.RemoveCondition(lAnyTransition_114118.conditions[i]); }
            lAnyTransition_114118.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32050f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114118.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 400f, "L" + rLayerIndex + "MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lTransition_115546 = MotionControllerMotion.EditorFindTransition(lState_114344, lState_115540, 0);
            if (lTransition_115546 == null) { lTransition_115546 = lState_114344.AddTransition(lState_115540); }
            lTransition_115546.isExit = false;
            lTransition_115546.hasExitTime = true;
            lTransition_115546.hasFixedDuration = true;
            lTransition_115546.exitTime = 0.5847073f;
            lTransition_115546.duration = 0.09999999f;
            lTransition_115546.offset = 0f;
            lTransition_115546.mute = false;
            lTransition_115546.solo = false;
            lTransition_115546.canTransitionToSelf = true;
            lTransition_115546.orderedInterruption = true;
            lTransition_115546.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115546.conditions.Length - 1; i >= 0; i--) { lTransition_115546.RemoveCondition(lTransition_115546.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115548 = MotionControllerMotion.EditorFindTransition(lState_114350, lState_115542, 0);
            if (lTransition_115548 == null) { lTransition_115548 = lState_114350.AddTransition(lState_115542); }
            lTransition_115548.isExit = false;
            lTransition_115548.hasExitTime = true;
            lTransition_115548.hasFixedDuration = true;
            lTransition_115548.exitTime = 0.972064f;
            lTransition_115548.duration = 0.2500001f;
            lTransition_115548.offset = 0f;
            lTransition_115548.mute = false;
            lTransition_115548.solo = false;
            lTransition_115548.canTransitionToSelf = true;
            lTransition_115548.orderedInterruption = true;
            lTransition_115548.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115548.conditions.Length - 1; i >= 0; i--) { lTransition_115548.RemoveCondition(lTransition_115548.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115550 = MotionControllerMotion.EditorFindTransition(lState_114360, lState_115540, 0);
            if (lTransition_115550 == null) { lTransition_115550 = lState_114360.AddTransition(lState_115540); }
            lTransition_115550.isExit = false;
            lTransition_115550.hasExitTime = true;
            lTransition_115550.hasFixedDuration = true;
            lTransition_115550.exitTime = 0.5467574f;
            lTransition_115550.duration = 0.2499998f;
            lTransition_115550.offset = 0f;
            lTransition_115550.mute = false;
            lTransition_115550.solo = false;
            lTransition_115550.canTransitionToSelf = true;
            lTransition_115550.orderedInterruption = true;
            lTransition_115550.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115550.conditions.Length - 1; i >= 0; i--) { lTransition_115550.RemoveCondition(lTransition_115550.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115552 = MotionControllerMotion.EditorFindTransition(lState_114354, lState_115540, 0);
            if (lTransition_115552 == null) { lTransition_115552 = lState_114354.AddTransition(lState_115540); }
            lTransition_115552.isExit = false;
            lTransition_115552.hasExitTime = true;
            lTransition_115552.hasFixedDuration = true;
            lTransition_115552.exitTime = 0.2967281f;
            lTransition_115552.duration = 0.2499998f;
            lTransition_115552.offset = 0.145962f;
            lTransition_115552.mute = false;
            lTransition_115552.solo = false;
            lTransition_115552.canTransitionToSelf = true;
            lTransition_115552.orderedInterruption = true;
            lTransition_115552.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115552.conditions.Length - 1; i >= 0; i--) { lTransition_115552.RemoveCondition(lTransition_115552.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115554 = MotionControllerMotion.EditorFindTransition(lState_114358, lState_115542, 0);
            if (lTransition_115554 == null) { lTransition_115554 = lState_114358.AddTransition(lState_115542); }
            lTransition_115554.isExit = false;
            lTransition_115554.hasExitTime = true;
            lTransition_115554.hasFixedDuration = true;
            lTransition_115554.exitTime = 0.8863636f;
            lTransition_115554.duration = 0.25f;
            lTransition_115554.offset = 0f;
            lTransition_115554.mute = false;
            lTransition_115554.solo = false;
            lTransition_115554.canTransitionToSelf = true;
            lTransition_115554.orderedInterruption = true;
            lTransition_115554.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115554.conditions.Length - 1; i >= 0; i--) { lTransition_115554.RemoveCondition(lTransition_115554.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115556 = MotionControllerMotion.EditorFindTransition(lState_114356, lState_115540, 0);
            if (lTransition_115556 == null) { lTransition_115556 = lState_114356.AddTransition(lState_115540); }
            lTransition_115556.isExit = false;
            lTransition_115556.hasExitTime = true;
            lTransition_115556.hasFixedDuration = true;
            lTransition_115556.exitTime = 0.3847882f;
            lTransition_115556.duration = 0.2499998f;
            lTransition_115556.offset = 0f;
            lTransition_115556.mute = false;
            lTransition_115556.solo = false;
            lTransition_115556.canTransitionToSelf = true;
            lTransition_115556.orderedInterruption = true;
            lTransition_115556.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115556.conditions.Length - 1; i >= 0; i--) { lTransition_115556.RemoveCondition(lTransition_115556.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115558 = MotionControllerMotion.EditorFindTransition(lState_114352, lState_115542, 0);
            if (lTransition_115558 == null) { lTransition_115558 = lState_114352.AddTransition(lState_115542); }
            lTransition_115558.isExit = false;
            lTransition_115558.hasExitTime = true;
            lTransition_115558.hasFixedDuration = true;
            lTransition_115558.exitTime = 1.345912f;
            lTransition_115558.duration = 0.25f;
            lTransition_115558.offset = 0f;
            lTransition_115558.mute = false;
            lTransition_115558.solo = false;
            lTransition_115558.canTransitionToSelf = true;
            lTransition_115558.orderedInterruption = true;
            lTransition_115558.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115558.conditions.Length - 1; i >= 0; i--) { lTransition_115558.RemoveCondition(lTransition_115558.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115560 = MotionControllerMotion.EditorFindTransition(lState_115542, lState_115540, 0);
            if (lTransition_115560 == null) { lTransition_115560 = lState_115542.AddTransition(lState_115540); }
            lTransition_115560.isExit = false;
            lTransition_115560.hasExitTime = true;
            lTransition_115560.hasFixedDuration = true;
            lTransition_115560.exitTime = 0.7f;
            lTransition_115560.duration = 0.25f;
            lTransition_115560.offset = 0f;
            lTransition_115560.mute = false;
            lTransition_115560.solo = false;
            lTransition_115560.canTransitionToSelf = true;
            lTransition_115560.orderedInterruption = true;
            lTransition_115560.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115560.conditions.Length - 1; i >= 0; i--) { lTransition_115560.RemoveCondition(lTransition_115560.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115562 = MotionControllerMotion.EditorFindTransition(lState_114362, lState_115544, 0);
            if (lTransition_115562 == null) { lTransition_115562 = lState_114362.AddTransition(lState_115544); }
            lTransition_115562.isExit = false;
            lTransition_115562.hasExitTime = true;
            lTransition_115562.hasFixedDuration = true;
            lTransition_115562.exitTime = 0.1313023f;
            lTransition_115562.duration = 0.25f;
            lTransition_115562.offset = 0.1362922f;
            lTransition_115562.mute = false;
            lTransition_115562.solo = false;
            lTransition_115562.canTransitionToSelf = true;
            lTransition_115562.orderedInterruption = true;
            lTransition_115562.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115562.conditions.Length - 1; i >= 0; i--) { lTransition_115562.RemoveCondition(lTransition_115562.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115564 = MotionControllerMotion.EditorFindTransition(lState_114364, lState_115540, 0);
            if (lTransition_115564 == null) { lTransition_115564 = lState_114364.AddTransition(lState_115540); }
            lTransition_115564.isExit = false;
            lTransition_115564.hasExitTime = true;
            lTransition_115564.hasFixedDuration = true;
            lTransition_115564.exitTime = 0.2315253f;
            lTransition_115564.duration = 0.25f;
            lTransition_115564.offset = 0f;
            lTransition_115564.mute = false;
            lTransition_115564.solo = false;
            lTransition_115564.canTransitionToSelf = true;
            lTransition_115564.orderedInterruption = true;
            lTransition_115564.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115564.conditions.Length - 1; i >= 0; i--) { lTransition_115564.RemoveCondition(lTransition_115564.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115566 = MotionControllerMotion.EditorFindTransition(lState_114366, lState_115540, 0);
            if (lTransition_115566 == null) { lTransition_115566 = lState_114366.AddTransition(lState_115540); }
            lTransition_115566.isExit = false;
            lTransition_115566.hasExitTime = true;
            lTransition_115566.hasFixedDuration = true;
            lTransition_115566.exitTime = 0.7916667f;
            lTransition_115566.duration = 0.25f;
            lTransition_115566.offset = 0f;
            lTransition_115566.mute = false;
            lTransition_115566.solo = false;
            lTransition_115566.canTransitionToSelf = true;
            lTransition_115566.orderedInterruption = true;
            lTransition_115566.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115566.conditions.Length - 1; i >= 0; i--) { lTransition_115566.RemoveCondition(lTransition_115566.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115568 = MotionControllerMotion.EditorFindTransition(lState_114368, lState_115540, 0);
            if (lTransition_115568 == null) { lTransition_115568 = lState_114368.AddTransition(lState_115540); }
            lTransition_115568.isExit = false;
            lTransition_115568.hasExitTime = true;
            lTransition_115568.hasFixedDuration = true;
            lTransition_115568.exitTime = 0.7916667f;
            lTransition_115568.duration = 0.25f;
            lTransition_115568.offset = 0f;
            lTransition_115568.mute = false;
            lTransition_115568.solo = false;
            lTransition_115568.canTransitionToSelf = true;
            lTransition_115568.orderedInterruption = true;
            lTransition_115568.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115568.conditions.Length - 1; i >= 0; i--) { lTransition_115568.RemoveCondition(lTransition_115568.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115570 = MotionControllerMotion.EditorFindTransition(lState_114370, lState_115540, 0);
            if (lTransition_115570 == null) { lTransition_115570 = lState_114370.AddTransition(lState_115540); }
            lTransition_115570.isExit = false;
            lTransition_115570.hasExitTime = true;
            lTransition_115570.hasFixedDuration = true;
            lTransition_115570.exitTime = 0.90625f;
            lTransition_115570.duration = 0.25f;
            lTransition_115570.offset = 0f;
            lTransition_115570.mute = false;
            lTransition_115570.solo = false;
            lTransition_115570.canTransitionToSelf = true;
            lTransition_115570.orderedInterruption = true;
            lTransition_115570.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115570.conditions.Length - 1; i >= 0; i--) { lTransition_115570.RemoveCondition(lTransition_115570.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115572 = MotionControllerMotion.EditorFindTransition(lState_114372, lState_115540, 0);
            if (lTransition_115572 == null) { lTransition_115572 = lState_114372.AddTransition(lState_115540); }
            lTransition_115572.isExit = false;
            lTransition_115572.hasExitTime = true;
            lTransition_115572.hasFixedDuration = true;
            lTransition_115572.exitTime = 0.7857143f;
            lTransition_115572.duration = 0.25f;
            lTransition_115572.offset = 0f;
            lTransition_115572.mute = false;
            lTransition_115572.solo = false;
            lTransition_115572.canTransitionToSelf = true;
            lTransition_115572.orderedInterruption = true;
            lTransition_115572.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115572.conditions.Length - 1; i >= 0; i--) { lTransition_115572.RemoveCondition(lTransition_115572.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115574 = MotionControllerMotion.EditorFindTransition(lState_114374, lState_115542, 0);
            if (lTransition_115574 == null) { lTransition_115574 = lState_114374.AddTransition(lState_115542); }
            lTransition_115574.isExit = false;
            lTransition_115574.hasExitTime = true;
            lTransition_115574.hasFixedDuration = true;
            lTransition_115574.exitTime = 0.8672566f;
            lTransition_115574.duration = 0.25f;
            lTransition_115574.offset = 0f;
            lTransition_115574.mute = false;
            lTransition_115574.solo = false;
            lTransition_115574.canTransitionToSelf = true;
            lTransition_115574.orderedInterruption = true;
            lTransition_115574.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115574.conditions.Length - 1; i >= 0; i--) { lTransition_115574.RemoveCondition(lTransition_115574.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115576 = MotionControllerMotion.EditorFindTransition(lState_115544, lState_115540, 0);
            if (lTransition_115576 == null) { lTransition_115576 = lState_115544.AddTransition(lState_115540); }
            lTransition_115576.isExit = false;
            lTransition_115576.hasExitTime = true;
            lTransition_115576.hasFixedDuration = true;
            lTransition_115576.exitTime = 0.4935692f;
            lTransition_115576.duration = 0.2499995f;
            lTransition_115576.offset = 0f;
            lTransition_115576.mute = false;
            lTransition_115576.solo = false;
            lTransition_115576.canTransitionToSelf = true;
            lTransition_115576.orderedInterruption = true;
            lTransition_115576.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115576.conditions.Length - 1; i >= 0; i--) { lTransition_115576.RemoveCondition(lTransition_115576.conditions[i]); }
            lTransition_115576.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32052f, "L" + rLayerIndex + "MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lTransition_115578 = MotionControllerMotion.EditorFindTransition(lState_114380, lState_115540, 0);
            if (lTransition_115578 == null) { lTransition_115578 = lState_114380.AddTransition(lState_115540); }
            lTransition_115578.isExit = false;
            lTransition_115578.hasExitTime = true;
            lTransition_115578.hasFixedDuration = true;
            lTransition_115578.exitTime = 0.90625f;
            lTransition_115578.duration = 0.25f;
            lTransition_115578.offset = 0f;
            lTransition_115578.mute = false;
            lTransition_115578.solo = false;
            lTransition_115578.canTransitionToSelf = true;
            lTransition_115578.orderedInterruption = true;
            lTransition_115578.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115578.conditions.Length - 1; i >= 0; i--) { lTransition_115578.RemoveCondition(lTransition_115578.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115580 = MotionControllerMotion.EditorFindTransition(lState_114408, lState_114354, 0);
            if (lTransition_115580 == null) { lTransition_115580 = lState_114408.AddTransition(lState_114354); }
            lTransition_115580.isExit = false;
            lTransition_115580.hasExitTime = true;
            lTransition_115580.hasFixedDuration = true;
            lTransition_115580.exitTime = 0.1615123f;
            lTransition_115580.duration = 0.4819526f;
            lTransition_115580.offset = 0f;
            lTransition_115580.mute = false;
            lTransition_115580.solo = false;
            lTransition_115580.canTransitionToSelf = true;
            lTransition_115580.orderedInterruption = true;
            lTransition_115580.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115580.conditions.Length - 1; i >= 0; i--) { lTransition_115580.RemoveCondition(lTransition_115580.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115582 = MotionControllerMotion.EditorFindTransition(lState_114420, lState_115542, 0);
            if (lTransition_115582 == null) { lTransition_115582 = lState_114420.AddTransition(lState_115542); }
            lTransition_115582.isExit = false;
            lTransition_115582.hasExitTime = true;
            lTransition_115582.hasFixedDuration = true;
            lTransition_115582.exitTime = 0.8611111f;
            lTransition_115582.duration = 0.25f;
            lTransition_115582.offset = 0f;
            lTransition_115582.mute = false;
            lTransition_115582.solo = false;
            lTransition_115582.canTransitionToSelf = true;
            lTransition_115582.orderedInterruption = true;
            lTransition_115582.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115582.conditions.Length - 1; i >= 0; i--) { lTransition_115582.RemoveCondition(lTransition_115582.conditions[i]); }

        }
#endif

    }
}
