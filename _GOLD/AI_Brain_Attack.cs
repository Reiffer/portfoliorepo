﻿//Author: Thomas Reiffer
//Last Edit: Thomas Reiffer 18/09/18
//Summary: Scaffolding for putting together Elain's hand-to-hand combat behaviour in-editor. Contains decision heuristic, combo struct and attack struct. 
//         Called by the ATTACKING state in Elain's FSM.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Actors.AnimationControllers;
using com.ootii.Actors.Combat;


//-------------------------------------------------------------------------------------------

/// <summary>
/// Single strike within AI_Combo.ListOfAttacksInCombo.
/// </summary>
[System.Serializable]
public struct AI_AttackStyle
{
    public enum ATTACKSTYLE {
                                AXE = 0,
                                COMBO_1 = 1,
                                COMBO_2 = 2,
                                COMBO_3 = 3,
                                COMBO_4 = 4,
                                BREAKOUT_KNEE = 5,
                                BREAKOUT_SPRING = 6,
                                BREAKOUT_SWEEP = 7,
                                BREAKOUT_HEEL = 8,
                                SUPERMAN = 9,
                                PUSHKICK = 10
                            }

    public ATTACKSTYLE ThisAttackStyle;

    public int ReturnAttackStyleInt()
    {
        //Debug.Log(ThisAttackStyle);
        return (int)ThisAttackStyle;
    }
}

//-------------------------------------------------------------------------------------------

/// <summary>
/// Contains an array of AI_AttackStyles, with modifiers for aggression, pressure and distance (contained within CombatDataCenter.cs).
/// </summary>
[System.Serializable]
public struct AI_Combo
{
    //This is populated in-editor.
    public AI_AttackStyle[] ListOfAttacksInCombo;

    //If false, will not be considered.
    public bool isEnabled;

    //Unsigned values only.
    public float AggressionModifier;
    public float PressureModifier;

    //Maximum and minimum distance between two combatants for combo to be considered.
    public float DistanceCeiling;
    public float DistanceFloor;
    
    //Minimum difficulty value that the combo will be considered after.
    public float DifficultyFloor;
    
    //Used for 
    [System.NonSerialized]
    public float DecisionScore;

    public void SetEnabled(bool value)
    {
        isEnabled = value;
    }
    
}

//-------------------------------------------------------------------------------------------

/// <summary>
/// Decisionmaking centre for combos - contains all available combos and functionality for evaluating them using information from the CombatDataCentre.
/// </summary>
public class AI_Brain_Attack : MonoBehaviour
{

    public AI_Combo[] ComboList;

//----------------------------------------------

    //For gameplay testing purposes.
    public void EnableAllAttacks()
    {
        foreach (AI_Combo combo in ComboList)
        {
            combo.SetEnabled(true);
        }
    }

//----------------------------------------------

    /// <summary>
    /// Returns the combo that the heuristic considers the most appropriate of the enabled combos.
    /// </summary>
    /// <returns>AI_Combo</returns>
    public AI_Combo TakeAction()
    {
        //Generate decisionscore for combos.
        for (int i = 0; i < ComboList.Length; i++)
        {
            if (ComboList[i].isEnabled)
            {
                ComboList[i].DecisionScore = GenerateDecisionScore(ComboList[i].AggressionModifier, 
                                                                   ComboList[i].PressureModifier, 
                                                                   ComboList[i].DifficultyFloor, 
                                                                   ComboList[i].DistanceFloor, 
                                                                   ComboList[i].DistanceCeiling);
            }
        }

        //Find highest DecisionScore.
        int bestIndex = 0;
        for (int i = 0; i < ComboList.Length; i++)
        {
            if (ComboList[i].isEnabled)
            {
                int newIndex = i;
                if (ComboList[i].DecisionScore > ComboList[bestIndex].DecisionScore)
                {
                    bestIndex = newIndex;
                }
                else if (ComboList[i].DecisionScore == ComboList[bestIndex].DecisionScore)
                {
                    if (ComboList[i].DifficultyFloor > ComboList[bestIndex].DifficultyFloor)
                    {
                        bestIndex = newIndex;
                    }
                }
            }
        }

        //Clear out decision scores.
        for (int i = 0; i < ComboList.Length; i++)
        {
            if (ComboList[i].isEnabled)
            {
                ComboList[i].DecisionScore = 0;
            }
        }

        //Give Elain's ATTACKING state the combo to perform.
        return ComboList[bestIndex];
    }

//----------------------------------------------

    public float GenerateDecisionScore(float AggroMod, float PressureMod, float DifficultyMin, float DistanceMin, float DistanceMax)
    {
        float DecisionScore;

        //If we can't perform this combo for any reason, exit.
        if (CombatDataCentre.DistanceBetweenCombatants <= DistanceMin) { DecisionScore = -20; return DecisionScore; }
        if (CombatDataCentre.DistanceBetweenCombatants >= DistanceMax) { DecisionScore = -20; return DecisionScore; }
        if (CombatDataCentre.AIDifficulty <= DifficultyMin) { DecisionScore = -20; return DecisionScore; }

        //If the combo is possible, generate and return decision score.
        DecisionScore = ((CombatDataCentre.AIAggression * AggroMod) + (CombatDataCentre.AIPressure * PressureMod));

        return DecisionScore;
    }

}
