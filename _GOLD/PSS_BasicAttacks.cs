﻿using System;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Actors.AnimationControllers;
using com.ootii.Actors.Combat;
using com.ootii.Actors.LifeCores;
using com.ootii.Data.Serializers;
using com.ootii.Helpers;
using com.ootii.Geometry;
using com.ootii.Messages;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace com.ootii.MotionControllerPacks
{
    /// <summary>
    /// Basic motion for a sword attack
    /// </summary>
    [MotionName("PSS - Basic Attacks")]
    [MotionDescription("Basic sword attacks using Mixamo's Pro Sword and Shield animations.")]
    public class PSS_BasicAttacks : PSS_MotionBase
    {
        /// <summary>
        /// Preallocates string for the event tests
        /// </summary>
        public static string EVENT_BEGIN_FOA = "beginfoa";
        public static string EVENT_END_FOA = "endfoa";
        public static string EVENT_BEGIN_CHAIN = "beginchain";
        public static string EVENT_END_CHAIN = "endchain";
        public static string EVENT_HIT = "hit";

        /// <summary>
        /// Trigger values for th emotion
        /// </summary>
        public const int PHASE_UNKNOWN = 0;
        public const int PHASE_START = 32040;
        public const int PHASE_START_CHAIN = 32041;
        public const int PHASE_START_INTERRUPT = 32045;

        /// <summary>
        /// Determines if the impact test is done continuously through the FOA
        /// </summary>
        public bool _TestContinuously = true;
        public bool TestContinuously
        {
            get { return _TestContinuously; }
            set { _TestContinuously = value; }
        }

        /// <summary>
        /// Desired degrees of rotation per second
        /// </summary>
        public float _RotationSpeed = 360f;
        public float RotationSpeed
        {
            get { return _RotationSpeed; }

            set
            {
                _RotationSpeed = value;
                mDegreesPer60FPSTick = _RotationSpeed / 60f;
            }
        }

        /// <summary>
        /// Index of the default attack style to use
        /// </summary>
        public int _DefaultAttackStyleIndex = -1;
        public int DefaultAttackStyleIndex
        {
            get { return _DefaultAttackStyleIndex; }
            set { _DefaultAttackStyleIndex = value; }
        }

        /// <summary>
        /// Index of the current attack style
        /// </summary>
        protected int mAttackStyleIndex = 0;
        public int AttackStyleIndex
        {
            get { return mAttackStyleIndex; }

            set
            {
                mAttackStyleIndex = value;
                if (mAttackStyleIndex < 0 || mAttackStyleIndex >= AttackStyles.Count) { mAttackStyleIndex = 0; }
            }
        }

        /// <summary>
        /// Attack styles built into the motion. This allows us to create different 
        /// capabilities and tie them to motions.
        /// </summary>
        public List<AttackStyle> AttackStyles = new List<AttackStyle>();

        /// <summary>
        /// Active attack style
        /// </summary>
        public AttackStyle AttackStyle
        {
            get
            {
                if (mAttackStyleIndex >= 0 && mAttackStyleIndex < AttackStyles.Count)
                {
                    return AttackStyles[mAttackStyleIndex];
                }

                return null;
            }
        }

        /// <summary>
        /// Determines if we can chain the next attack
        /// </summary>
        protected bool mIsChainActive = false;

        /// <summary>
        /// The next attack style in the chain
        /// </summary>
        protected int mChainAttackStyleIndex = -1;

        /// <summary>
        /// Keeps us from having to re-allocate
        /// </summary>
        protected List<CombatTarget> mCombatTargets = new List<CombatTarget>();

        /// <summary>
        /// Determines if the weapon is currently equipped
        /// </summary>
        //private bool mIsEquipped = false;

        /// <summary>
        /// Determines if the attack hit
        /// </summary>
        protected bool mHasHit = false;

        /// <summary>
        /// Determines if the attack has been interrupted or not
        /// </summary>
        protected bool mIsInterrupted = false;

        /// <summary>
        /// Speed we'll actually apply to the rotation. This is essencially the
        /// number of degrees per tick assuming we're running at 60 FPS
        /// </summary>
        protected float mDegreesPer60FPSTick = 1f;

        /// <summary>
        /// INTERNAL ONLY - Allows us to serialize our attack styles
        /// </summary>
        protected string mAttackStyleDefinitions = "";
        public string AttackStyleDefinitions
        {
            get { return mAttackStyleDefinitions; }
            set { mAttackStyleDefinitions = value; }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public PSS_BasicAttacks()
            : base()
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE;

            _Priority = 16;
            _ActionAlias = "Combat Attack";

#if UNITY_EDITOR
            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "PSS_BasicAttacks-SM"; }
#endif
        }

        /// <summary>
        /// Controller constructor
        /// </summary>
        /// <param name="rController">Controller the motion belongs to</param>
        public PSS_BasicAttacks(MotionController rController)
            : base(rController)
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE;

            _Priority = 16;
            _ActionAlias = "Combat Attack";

#if UNITY_EDITOR
            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "PSS_BasicAttacks-SM"; }
#endif
        }

        /// <summary>
        /// Awake is called after all objects are initialized so you can safely speak to other objects. This is where
        /// reference can be associated.
        /// </summary>
        public override void Awake()
        {
            base.Awake();

            // Default the speed we'll use to rotate
            mDegreesPer60FPSTick = _RotationSpeed / 60f;

            // Default the attack style
            if (mCombatant != null)
            {
                if (mAttackStyleIndex < 0 && AttackStyles.Count > 0) { mAttackStyleIndex = 0; }
                mCombatant.CombatStyle = AttackStyle;
            }

#if UNITY_EDITOR

            // Recreate our list for rendering
            InstantiateAttackStyleList();

#endif
        }

        /// <summary>
        /// Tests if this motion should be started. However, the motion
        /// isn't actually started.
        /// </summary>
        /// <returns></returns>
        public override bool TestActivate()
        {
            if (!mIsStartable)
            {
                return false;
            }

            if (!mActorController.IsGrounded)
            {
                return false;
            }

            // This is only valid if we're in combat mode
            if (mMotionController.Stance != EnumControllerStance.COMBAT_MELEE_SWORD_SHIELD)
            {
                return false;
            }

            // Check if we've been activated
            if (_ActionAlias.Length > 0 && mMotionController._InputSource != null)
            {
                if (mMotionController._InputSource.IsJustPressed(_ActionAlias))
                {
                    mParameter = _DefaultAttackStyleIndex;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Tests if the motion should continue. If it shouldn't, the motion
        /// is typically disabled
        /// </summary>
        /// <returns></returns>
        public override bool TestUpdate()
        {
            if (mIsActivatedFrame) { return true; }

            // This is only valid if we're in combat mode
            if (mMotionController.Stance != EnumControllerStance.COMBAT_MELEE_SWORD_SHIELD)
            {
                return false;
            }

            // Ensure we're actually in our animation state
            if (mIsAnimatorActive)
            {
                if (!IsInMotionState)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Called to start the specific motion. If the motion
        /// were something like 'jump', this would start the jumping process
        /// </summary>
        /// <param name="rPrevMotion">Motion that this motion is taking over from</param>
        public override bool Activate(MotionControllerMotion rPrevMotion)
        {
            mHasHit = false;
            mIsInterrupted = false;
            mIsChainActive = false;
            mChainAttackStyleIndex = -1;

            if (AttackStyles.Count <= 0) { return false; }

            mAttackStyleIndex = _DefaultAttackStyleIndex;

            // If a swing was specified (through the parameter), use it
            if (mParameter >= 0 && mParameter < AttackStyles.Count)
            {
                mAttackStyleIndex = mParameter;
            }
            // Randomize
            else if (mParameter == -1)
            {
                mAttackStyleIndex = UnityEngine.Random.Range(0, AttackStyles.Count);
            }

            // Ensure we have a valid index
            if (mAttackStyleIndex < 0) { mAttackStyleIndex = 0; }

            AttackStyle lAttackStyle = AttackStyle;
            if (lAttackStyle == null) { return false; }

            // Allow the combat style to be modified before the attack occurs
            IActorCore lActorCore = mMotionController.gameObject.GetComponent<ActorCore>();
            if (lActorCore != null)
            {
                CombatMessage lCombatMessage = CombatMessage.Allocate();
                lCombatMessage.ID = CombatMessage.MSG_ATTACKER_PRE_ATTACK;
                lCombatMessage.Attacker = mMotionController.gameObject;
                lCombatMessage.CombatStyle = lAttackStyle;

                lActorCore.SendMessage(lCombatMessage);

                CombatMessage.Release(lCombatMessage);
            }

            //// Ensure our combatant is setup
            //if (mCombatant != null)
            //{
            //    mCombatant.CombatStyle = AttackStyle;
            //    if (!mCombatant.OnAttackActivated(this))
            //    {
            //        return false;
            //    }
            //}

            // Run the approapriate attack
            int lAttackStyleParameter = (lAttackStyle != null ? lAttackStyle.Form : 0);
            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START, lAttackStyleParameter, true);

            // Now, activate the motion
            return base.Activate(rPrevMotion);
        }

        /// <summary>
        /// Called to interrupt the motion if it is currently active. This
        /// gives the motion a chance to stop itself how it sees fit. The motion
        /// may simply ignore the call.
        /// </summary>
        /// <param name="rParameter">Any value you wish to pass</param>
        /// <returns>Boolean determining if the motion accepts the interruption. It doesn't mean it will deactivate.</returns>
        public override bool Interrupt(object rParameter)
        {
            if (mIsActive && !mIsInterrupted)
            {
                mIsInterrupted = true;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Raised when we shut the motion down
        /// </summary>
        public override void Deactivate()
        {
            // Don't hold onto our targets
            mCombatTargets.Clear();

            // Continue
            base.Deactivate();
        }

        /// <summary>
        /// Allows the motion to modify the velocity before it is applied.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        /// <param name="rMovement">Amount of movement caused by root motion this frame</param>
        /// <param name="rRotation">Amount of rotation caused by root motion this frame</param>
        /// <returns></returns>
        public override void UpdateRootMotion(float rDeltaTime, int rUpdateIndex, ref Vector3 rMovement, ref Quaternion rRotation)
        {
            int lStateID = mMotionLayer._AnimatorStateID;
            int lTransitionID = mMotionLayer._AnimatorTransitionID;

            if (lTransitionID == TRANS_AnyState_SpinSlash || lStateID == STATE_SpinSlash)
            {
                rMovement.x = 0f;
                rMovement.z = 0f;
            }
            else if (lTransitionID == TRANS_AnyState_JumpSlash || lStateID == STATE_JumpSlash)
            {
                rMovement.x = 0f;
                rMovement.z = 0f;
            }
            else if (lTransitionID == TRANS_AnyState_ComboSpinSlash || lStateID == STATE_ComboSpinSlash)
            {
                rMovement.x = 0f;
                rMovement.z = 0f;
            }
            else if (lTransitionID == TRANS_AnyState_Slash || lStateID == STATE_Slash)
            {
                rMovement = Vector3.zero;
                rRotation = Quaternion.identity;
            }
            else if (lTransitionID == TRANS_AnyState_BackSlash || lStateID == STATE_BackSlash)
            {
                rMovement = Vector3.zero;
                rRotation = Quaternion.identity;
            }
            else if (lStateID == STATE_LowSlash)
            {
                rMovement = Vector3.zero;
                rRotation = Quaternion.identity;
            }
            else if (lTransitionID == TRANS_AnyState_PommelBash || lStateID == STATE_PommelBash)
            {
                rMovement = Vector3.zero;
                rRotation = Quaternion.identity;
            }
        }

        /// <summary>
        /// Updates the motion over time. This is called by the controller
        /// every update cycle so animations and stages can be updated.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        public override void Update(float rDeltaTime, int rUpdateIndex)
        {
            mMovement = Vector3.zero;
            mRotation = Quaternion.identity;

            // Ensure we face the target if we're meant to
            if (mCombatant != null && mCombatant.IsTargetLocked)
            {
                RotateCameraToTarget(mCombatant.Target, _ToTargetCameraRotationSpeed);
                RotateToTarget(mCombatant.Target, _ToTargetRotationSpeed, rDeltaTime, ref mRotation);
            }

            // Clear the motion phase if we need to
            if (IsInBlockedState())
            {
                mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, 0);
            }

            if (IsInChainState())
            {
                mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, 0);
            }

            // Check if we've got a hit while the weapon is active
            if (_TestContinuously && mAttackStyleIndex >= 0 && !mIsInterrupted)
            {
                if (mCombatant != null && mCombatant.PrimaryWeapon != null)
                {
                    IWeaponCore lWeapon = mCombatant.PrimaryWeapon;
                    if (lWeapon.IsActive && !lWeapon.HasColliders)
                    {
                        int lTargetCount = mCombatant.QueryCombatTargets(AttackStyles[mAttackStyleIndex], mCombatTargets);

                        if (lTargetCount > 0)
                        {
                            WeaponCore lWeaponCore = lWeapon as WeaponCore;
                            if (lWeaponCore != null)
                            {
                                int lImpactCount = lWeaponCore.TestImpact(mCombatTargets, AttackStyles[mAttackStyleIndex]);
                                if (lImpactCount > 0) { lWeapon.IsActive = false; }
                            }
                        }

                        mCombatTargets.Clear();
                    }
                }
            }

            // Check if we are chaining an attack
            if (mIsChainActive && mChainAttackStyleIndex >= 0 && !mIsInterrupted)
            {
                if (_ActionAlias.Length > 0 && mMotionController._InputSource != null)
                {
                    if (mMotionController._InputSource.IsJustPressed(_ActionAlias))
                    {
                        //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " chained");

                        mHasHit = false;
                        mIsChainActive = false;

                        mAttackStyleIndex = mChainAttackStyleIndex;

                        int lAttackStyleParameter = (AttackStyle != null ? AttackStyle.Form : 0);
                        mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START_CHAIN, lAttackStyleParameter);
                    }
                }
            }

            // Move to the true idle. We do this so IsActive stays true while we transition to PSS_Idle.IdlePose
            if (mMotionLayer._AnimatorStateID == STATE_Idle && mMotionLayer._AnimatorTransitionID == 0)
            {
                mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, PSS_Idle.PHASE_START, true);
            }

#if UNITY_EDITOR
            if (ShowDebug)
            {
                if (mCombatant != null)
                {
                    IWeaponCore lWeaponCore = mCombatant.PrimaryWeapon as IWeaponCore;
                    if (lWeaponCore != null)
                    {
                        Color lColor = (mHasHit ? Color.red : (lWeaponCore.IsActive ? Color.green : Color.gray));
                        DrawWeaponDebug(mCombatant.PrimaryWeapon, AttackStyle, lColor);
                    }
                }

                if (mIsChainActive)
                {
                    Graphics.GraphicsManager.DrawSolidCircle(mMotionController._Transform.position + (mMotionController._Transform.up * 0.1f), 0.5f, Color.green);
                }
            }
#endif

            // Allow the base class to render debug info
            base.Update(rDeltaTime, rUpdateIndex);
        }

        /// <summary>
        /// Raised by the animation when an event occurs
        /// </summary>
        public override void OnAnimationEvent(AnimationEvent rEvent)
        {
            if (rEvent == null) { return; }

            // If the animation is running backwards (say in response to a block), don't fire.
#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
#else
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.speed <= 0f) { return; }
#endif

            // Test the event type
            if (rEvent.stringParameter.Length == 0 || StringHelper.CleanString(rEvent.stringParameter) == PSS_BasicAttacks.EVENT_HIT)
            {
                // Test if we can cause damage
                if (mAttackStyleIndex >= 0 && !mIsInterrupted && !_TestContinuously && mCombatant != null)
                {
                    IWeaponCore lWeapon = mCombatant.PrimaryWeapon;
                    if (lWeapon != null && !lWeapon.HasColliders)
                    {
                        int lTargetCount = mCombatant.QueryCombatTargets(AttackStyles[mAttackStyleIndex], mCombatTargets);

                        if (lTargetCount > 0)
                        {
                            WeaponCore lWeaponCore = lWeapon as WeaponCore;
                            if (lWeaponCore != null)
                            {
                                lWeaponCore.TestImpact(mCombatTargets);
                            }
                        }

                        mCombatTargets.Clear();
                    }
                }
            }
            // Determine if we are starting the attack
            else if (StringHelper.CleanString(rEvent.stringParameter) == PSS_BasicAttacks.EVENT_BEGIN_FOA)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " begin foa");
                if (mCombatant != null && mCombatant.PrimaryWeapon != null)
                {
                    mCombatant.PrimaryWeapon.IsActive = true;

                    SwordCore lSword = mCombatant.PrimaryWeapon as SwordCore;
                    if (lSword != null) { lSword.OnStartSwing(null); }
                }
            }
            // Determine if we are ending the attack
            else if (StringHelper.CleanString(rEvent.stringParameter) == PSS_BasicAttacks.EVENT_END_FOA)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " end foa");
                if (mCombatant != null && mCombatant.PrimaryWeapon != null)
                {
                    mCombatant.PrimaryWeapon.IsActive = false;

                    SwordCore lSword = mCombatant.PrimaryWeapon as SwordCore;
                    if (lSword != null) { lSword.OnEndSwing(null); }
                }
            }
            // Determine if we are beginning a chain
            else if (StringHelper.CleanString(rEvent.stringParameter) == PSS_BasicAttacks.EVENT_BEGIN_CHAIN)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " begin chain");
                mIsChainActive = true;
                mChainAttackStyleIndex = rEvent.intParameter;
            }
            // Determine if we are ending
            else if (StringHelper.CleanString(rEvent.stringParameter) == PSS_BasicAttacks.EVENT_END_CHAIN)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " end chain");
                mIsChainActive = false;
            }
        }

        /// <summary>
        /// Raised by the controller when a message is received
        /// </summary>
        public override void OnMessageReceived(IMessage rMessage)
        {
            if (rMessage == null) { return; }
            if (mMotionController.Stance != EnumControllerStance.COMBAT_MELEE_SWORD_SHIELD) { return; }

            CombatMessage lCombatMessage = rMessage as CombatMessage;
            if (lCombatMessage != null)
            {
                // Attack messages
                if (lCombatMessage.Attacker == mMotionController.gameObject)
                {
                    // Call for an attack
                    if (rMessage.ID == CombatMessage.MSG_COMBATANT_ATTACK)
                    {
                        if (!mIsActive && mMotionLayer._AnimatorTransitionID == 0)
                        {
                            int lAttackStyleIndex = (lCombatMessage.StyleIndex >= 0 ? lCombatMessage.StyleIndex : _DefaultAttackStyleIndex);

                            if (lCombatMessage.CombatStyle != null)
                            {
                                for (int i = 0; i < AttackStyles.Count; i++)
                                {
                                    if (AttackStyles[i] == lCombatMessage.CombatStyle)
                                    {
                                        lAttackStyleIndex = i;
                                        break;
                                    }
                                }
                            }

                            lCombatMessage.IsHandled = true;
                            lCombatMessage.Recipient = this;
                            lCombatMessage.Damage = lCombatMessage.Damage * (lAttackStyleIndex >= 0 && lAttackStyleIndex < AttackStyles.Count ? AttackStyles[lAttackStyleIndex].DamageModifier : 1f);
                            mMotionController.ActivateMotion(this, lAttackStyleIndex);
                        }
                    }
                    // Attack has been activated and impacts
                    else if (rMessage.ID == CombatMessage.MSG_ATTACKER_ATTACKED)
                    {
                        if (mIsActive)
                        {
                            lCombatMessage.CombatStyle.Form = Form;
                            lCombatMessage.Damage = lCombatMessage.Damage * (AttackStyle != null ? AttackStyle.DamageModifier : 1f);
                        }
                    }
                    // Gives us a chance to respond to the defender's reaction (post attack)
                    else if (rMessage.ID == CombatMessage.MSG_DEFENDER_ATTACKED_BLOCKED)
                    {
                        if (mIsActive)
                        {
                            mIsInterrupted = true;
                            mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, PHASE_START_INTERRUPT);

                            // Ensure the weapon isn't active any more.
                            if (mCombatant.PrimaryWeapon != null)
                            {
                                mCombatant.PrimaryWeapon.IsActive = false;
                            }
                        }
                    }
                    // Final hit (post attack)
                    else if (rMessage.ID == CombatMessage.MSG_DEFENDER_ATTACKED_PARRIED ||
                             rMessage.ID == CombatMessage.MSG_DEFENDER_DAMAGED || 
                             rMessage.ID == CombatMessage.MSG_DEFENDER_KILLED)
                    {
                        OnWeaponHit(lCombatMessage, lCombatMessage.Weapon);
                    }
                }
                // Defender messages
                else if (lCombatMessage.Defender == mMotionController.gameObject)
                {
                }
            }
        }

        /// <summary>
        /// Determines what happens when a hit occurs. We could ignore it, reduce damage, etc.
        /// </summary>
        /// <param name="rCombatMessage"></param>
        protected virtual void OnWeaponHit(CombatMessage rCombatMessage, IWeaponCore rWeaponCore)
        {
            //com.ootii.Utilities.Debug.Log.FileWrite(mMotionController._Transform.name + ".OnWeaponHit()");

            mHasHit = true;

            if (rWeaponCore != null)
            {
                WeaponCore lWeaponCore = rWeaponCore as WeaponCore;
                if (lWeaponCore != null)
                {
                    lWeaponCore.OnImpactComplete(rCombatMessage);
                }
            }
        }

        /// <summary>
        /// Test if we're in a blocked state
        /// </summary>
        /// <returns>Determines if we're in a blocked state</returns>
        protected bool IsInBlockedState()
        {
            int lStateID = mMotionLayer._AnimatorStateID;
            if (lStateID == STATE_Blocked) { return true; }
            if (lStateID == STATE_Blocked2) { return true; }

            int lTransitionID = mMotionLayer._AnimatorTransitionID;
            if (lTransitionID == TRANS_Slash_Blocked) { return true; }
            if (lTransitionID == TRANS_BackSlash_Blocked2) { return true; }
            if (lTransitionID == TRANS_SpinSlash_Blocked2) { return true; }
            if (lTransitionID == TRANS_LowSlash_Blocked) { return true; }

            return false;
        }

        /// <summary>
        /// Test if we're in a chain state
        /// </summary>
        /// <returns>Determines if we're in a blocked state</returns>
        protected bool IsInChainState()
        {
            int lTransitionID = mMotionLayer._AnimatorTransitionID;
            if (lTransitionID == TRANS_Slash_BackSlash) { return true; }
            if (lTransitionID == TRANS_BackSlash_Slash) { return true; }

            return false;
        }

        /// <summary>
        /// Creates a JSON string that represents the motion's serialized state. We
        /// do this since Unity can't handle putting lists of derived objects into
        /// prefabs.
        /// </summary>
        /// <returns>JSON string representing the object</returns>
        public override string SerializeMotion()
        {
            // Serialize our attack styles
            if (AttackStyles != null && AttackStyles.Count > 0)
            {
                mAttackStyleDefinitions = JSONSerializer.SerializeValue("AttackStyles", AttackStyles);
                mAttackStyleDefinitions = mAttackStyleDefinitions.Replace("\"", "\\\"");
            }
            else
            {
                mAttackStyleDefinitions = "";
            }

            // Now serialize everything
            return base.SerializeMotion();
        }

        /// <summary>
        /// Gieven a JSON string that is the definition of the object, we parse
        /// out the properties and set them.
        /// </summary>
        /// <param name="rDefinition">JSON string</param>
        public override void DeserializeMotion(string rDefinition)
        {
            // Deserialize everything
            base.DeserializeMotion(rDefinition);

            // Now deserialize our attack styles
            if (mAttackStyleDefinitions.Length > 0)
            {
                try
                {
                    AttackStyles = JSONSerializer.DeserializeValue<List<AttackStyle>>(mAttackStyleDefinitions);
                }
                catch
                {
                    AttackStyles.Clear();
                    mAttackStyleDefinitions = "";
                }
            }
        }

#region Editor Functions

        // **************************************************************************************************
        // Following properties and function only valid while editing
        // **************************************************************************************************

#if UNITY_EDITOR

        /// <summary>
        /// Attack style list we'll modify
        /// </summary>
        public ReorderableList mAttackStyleList;

        /// <summary>
        /// Allows us to re-open the last selected motor
        /// </summary>
        public int mEditorAttackStyleIndex = 0;

        /// <summary>
        /// Determines if the motion is dirty
        /// </summary>
        public bool mIsDirty = false;

        /// <summary>
        /// Reset to default values. Reset is called when the user hits the Reset button in the Inspector's 
        /// context menu or when adding the component the first time. This function is only called in editor mode.
        /// </summary>
        public override void Reset()
        {
            AttackStyleListReset();
        }

        /// <summary>
        /// Allow the constraint to render it's own GUI
        /// </summary>
        /// <returns>Reports if the object's value was changed</returns>
        public override bool OnInspectorGUI()
        {
            mIsDirty = false;

            if (EditorHelper.TextField("Attack Alias", "Action alias that is required to trigger the attack.", ActionAlias, mMotionController))
            {
                mIsDirty = true;
                ActionAlias = EditorHelper.FieldStringValue;
            }

            if (EditorHelper.FloatField("Rotation Speed", "Degrees per second to rotate the actor.", RotationSpeed, mMotionController))
            {
                mIsDirty = true;
                RotationSpeed = EditorHelper.FieldFloatValue;
            }

            GUILayout.Space(5f);

            if (EditorHelper.IntField("Default Attack Style", "Default attack style index to use.", DefaultAttackStyleIndex, mMotionController))
            {
                mIsDirty = true;
                DefaultAttackStyleIndex = EditorHelper.FieldIntValue;
            }

            if (EditorHelper.BoolField("Test Continuously", "Determines if we test for impact continuously between the field-of-attack or just at the animation event.", TestContinuously, mMotionController))
            {
                mIsDirty = true;
                TestContinuously = EditorHelper.FieldBoolValue;
            }

            GUILayout.Space(5f);

            EditorGUILayout.LabelField("Attack Styles", EditorStyles.boldLabel, GUILayout.Height(16f));

            GUILayout.BeginVertical(EditorHelper.GroupBox);
            EditorHelper.DrawInspectorDescription("Attack Styles determine how different attack animations play and the effect.", MessageType.None);

            mAttackStyleList.DoLayoutList();

            if (mAttackStyleList.index >= 0)
            {
                GUILayout.Space(5f);
                GUILayout.BeginVertical(EditorHelper.Box);

                bool lListIsDirty = DrawAttackStyleDetailItem(AttackStyles[mAttackStyleList.index]);
                if (lListIsDirty) { mIsDirty = true; }

                GUILayout.EndVertical();
            }

            EditorGUILayout.EndVertical();

            if (mIsDirty)
            {
                // Serialize our attack styles
                if (AttackStyles != null && AttackStyles.Count > 0)
                {
                    mAttackStyleDefinitions = JSONSerializer.SerializeValue("AttackStyles", AttackStyles);
                    mAttackStyleDefinitions = mAttackStyleDefinitions.Replace("\"", "\\\"");
                }
                else
                {
                    mAttackStyleDefinitions = "";
                }

                InternalEditorUtility.RepaintAllViews();
            }

            return mIsDirty;
        }

        /// <summary>
        /// Enables the Editor to handle an event in the scene view.
        /// </summary>
        public override void OnSceneGUI()
        {
            if (Application.isPlaying) { return; }

            Combatant lCombatant = mMotionController.gameObject.GetComponent<Combatant>();
            if (lCombatant != null)
            {
                if (mAttackStyleList != null && mAttackStyleList.index >= 0 && mAttackStyleList.index < AttackStyles.Count)
                {
                    AttackStyle lAttackStyle = AttackStyles[mAttackStyleList.index];

                    float lMinReach = (lAttackStyle.MinRange > 0f ? lAttackStyle.MinRange : lCombatant.MinMeleeReach + 0f);
                    float lMaxReach = (lAttackStyle.MaxRange > 0f ? lAttackStyle.MaxRange : lCombatant.MaxMeleeReach + 1f);
                    Graphics.GraphicsManager.DrawSolidFrustum(lCombatant.CombatOrigin, lCombatant.transform.rotation * Quaternion.LookRotation(lAttackStyle.Forward, lCombatant.transform.up), lAttackStyle.HorizontalFOA, lAttackStyle.VerticalFOA, lMinReach, lMaxReach, Color.gray);
                }
            }
        }

#region Attack Styles

        /// <summary>
        /// Create the reorderable list
        /// </summary>
        private void InstantiateAttackStyleList()
        {
            if (mAttackStyleList != null)
            {
                mAttackStyleList.drawHeaderCallback -= DrawAttackStyleListHeader;
                mAttackStyleList.drawFooterCallback -= DrawAttackStyleListFooter;
                mAttackStyleList.drawElementCallback -= DrawAttackStyleListItem;
                mAttackStyleList.onAddCallback -= OnAttackStyleListItemAdd;
                mAttackStyleList.onRemoveCallback -= OnAttackStyleListItemRemove;
                mAttackStyleList.onSelectCallback -= OnAttackStyleListItemSelect;
                mAttackStyleList.onReorderCallback -= OnAttackStyleListReorder;
                mAttackStyleList.list.Clear();
            }

            mAttackStyleList = new ReorderableList(AttackStyles, typeof(AttackStyle), true, true, true, true);
            mAttackStyleList.drawHeaderCallback += DrawAttackStyleListHeader;
            mAttackStyleList.drawFooterCallback += DrawAttackStyleListFooter;
            mAttackStyleList.drawElementCallback += DrawAttackStyleListItem;
            mAttackStyleList.onAddCallback += OnAttackStyleListItemAdd;
            mAttackStyleList.onRemoveCallback += OnAttackStyleListItemRemove;
            mAttackStyleList.onSelectCallback += OnAttackStyleListItemSelect;
            mAttackStyleList.onReorderCallback += OnAttackStyleListReorder;
            mAttackStyleList.headerHeight = 18f;
            mAttackStyleList.footerHeight = 17f;

            if (mEditorAttackStyleIndex >= 0 && mEditorAttackStyleIndex < mAttackStyleList.count)
            {
                mAttackStyleList.index = mEditorAttackStyleIndex;

                SceneView.RepaintAll();
                InternalEditorUtility.RepaintAllViews();
            }
        }

        /// <summary>
        /// Header for the list
        /// </summary>
        /// <param name="rRect"></param>
        private void DrawAttackStyleListHeader(Rect rRect)
        {
            EditorGUI.LabelField(rRect, "Attack Styles");

            Rect lClickRect = new Rect(30f, rRect.y, rRect.width - 45f, rRect.height);
            if (GUI.Button(lClickRect, "", EditorStyles.label))
            {
                mAttackStyleList.index = -1;
                OnAttackStyleListItemSelect(mAttackStyleList);
            }

            Rect lResetRect = new Rect(lClickRect.x + lClickRect.width + 3f, rRect.y + 2f, 45f, rRect.height - 3f);
            if (GUI.Button(lResetRect, "Reset", EditorStyles.miniButton))
            {
                mIsDirty = true;
                mAttackStyleList.index = -1;
                AttackStyleListReset();
            }

            //Rect lNoteRect = new Rect(rRect.width + 19f, rRect.y, 11f, rRect.height);
            //EditorGUI.LabelField(lNoteRect, "X", EditorStyles.miniLabel);
        }

        /// <summary>
        /// Allows us to draw each AttackStyle in the list
        /// </summary>
        /// <param name="rRect"></param>
        /// <param name="rIndex"></param>
        /// <param name="rIsActive"></param>
        /// <param name="rIsFocused"></param>
        private void DrawAttackStyleListItem(Rect rRect, int rIndex, bool rIsActive, bool rIsFocused)
        {
            if (rIndex < AttackStyles.Count)
            {
                AttackStyle lAttackStyle = AttackStyles[rIndex];

                rRect.y += 2;

                Rect lItemRect = new Rect(rRect.x, rRect.y, rRect.width, EditorGUIUtility.singleLineHeight);
                EditorGUI.LabelField(lItemRect, lAttackStyle.Name);
            }
        }

        /// <summary>
        /// Footer for the list
        /// </summary>
        /// <param name="rRect"></param>
        private void DrawAttackStyleListFooter(Rect rRect)
        {
            Rect lAddRect = new Rect(rRect.x + rRect.width - 28 - 28 - 1, rRect.y + 1, 28, 15);
            if (GUI.Button(lAddRect, new GUIContent("+", "Add Style."), EditorStyles.miniButtonLeft)) { OnAttackStyleListItemAdd(mAttackStyleList); }

            Rect lDeleteRect = new Rect(lAddRect.x + lAddRect.width, lAddRect.y, 28, 15);
            if (GUI.Button(lDeleteRect, new GUIContent("-", "Delete Style."), EditorStyles.miniButtonRight)) { OnAttackStyleListItemRemove(mAttackStyleList); };
        }

        /// <summary>
        /// Allows us to add to a list
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListItemAdd(ReorderableList rList)
        {
            AttackStyle lAttackStyle = new AttackStyle();
            //lAttackStyle.ParameterID = AttackStyles.Count;

            AttackStyles.Add(lAttackStyle);

            mAttackStyleList.index = AttackStyles.Count - 1;
            OnAttackStyleListItemSelect(rList);

            mIsDirty = true;
        }

        /// <summary>
        /// Allows us process when a list is selected
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListItemSelect(ReorderableList rList)
        {
            mEditorAttackStyleIndex = rList.index;

            SceneView.RepaintAll();
            InternalEditorUtility.RepaintAllViews();
        }

        /// <summary>
        /// Allows us to stop before removing the item
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListItemRemove(ReorderableList rList)
        {
            if (EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete the item?", "Yes", "No"))
            {
                int rIndex = rList.index;

                rList.index--;
                AttackStyles.RemoveAt(rIndex);

                OnAttackStyleListItemSelect(rList);

                mIsDirty = true;
            }
        }

        /// <summary>
        /// Allows us to process after the motions are reordered
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListReorder(ReorderableList rList)
        {
            mIsDirty = true;
        }

        /// <summary>
        /// Renders the currently selected step
        /// </summary>
        /// <param name="rStep"></param>
        private bool DrawAttackStyleDetailItem(AttackStyle rAttackStyle)
        {
            bool lIsDirty = false;

            EditorHelper.DrawSmallTitle(rAttackStyle.Name.Length > 0 ? rAttackStyle.Name : "Attack Style");

            if (EditorHelper.TextField("Name", "ID of the attack style.", rAttackStyle.Name, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Name = EditorHelper.FieldStringValue;
            }

            if (EditorHelper.IntField("Form", "Motion Parameter ID to use with the animator.", rAttackStyle.Form, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Form = EditorHelper.FieldIntValue;
            }

            if (EditorHelper.BoolField("Is Blockable", "Determines if the attack can be blocked", rAttackStyle.IsInterruptible, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.IsInterruptible = EditorHelper.FieldBoolValue;
            }

            GUILayout.Space(5f);

            if (EditorHelper.Vector3Field("Attack Forward", "Normalized center of the attack. FOA values are based on this.", rAttackStyle.Forward, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Forward = EditorHelper.FieldVector3Value.normalized;
            }

            EditorGUILayout.BeginHorizontal();

            EditorHelper.LabelField("Field of Attack", "Horizontal and vertical field of attack when colliders are not used.", EditorGUIUtility.labelWidth - 4f);

            if (EditorHelper.FloatField(rAttackStyle.HorizontalFOA, "Horizontal FOA", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.HorizontalFOA = EditorHelper.FieldFloatValue;
            }

            if (EditorHelper.FloatField(rAttackStyle.VerticalFOA, "Vertical FOA", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.VerticalFOA = EditorHelper.FieldFloatValue;
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorHelper.LabelField("Range", "Min and Max range of the attack. Set to 0 to use combatant + weapon range.", EditorGUIUtility.labelWidth - 4f);

            if (EditorHelper.FloatField(rAttackStyle.MinRange, "Min Range", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.MinRange = EditorHelper.FieldFloatValue;
            }

            if (EditorHelper.FloatField(rAttackStyle.MaxRange, "Max Range", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.MaxRange = EditorHelper.FieldFloatValue;
            }

            EditorGUILayout.EndHorizontal();

            if (EditorHelper.FloatField("Damage Modifier", "Multiplier that this style applies to the weapon's damage.", rAttackStyle.DamageModifier, mMotionController))
            {
                mIsDirty = true;
                rAttackStyle.DamageModifier = EditorHelper.FieldFloatValue;
            }

            if (lIsDirty)
            {
                SceneView.RepaintAll();
                InternalEditorUtility.RepaintAllViews();
            }

            return lIsDirty;
        }

        /// <summary>
        /// Reset to default values. 
        /// </summary>
        private void AttackStyleListReset()
        {
            AttackStyles.Clear();

            AttackStyle lStyle = new AttackStyle();
            lStyle.Name = "Forward Slash";
            lStyle.Form = 0;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 70f;
            lStyle.VerticalFOA = 40f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Back Slash";
            lStyle.Form = 1;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 160f;
            lStyle.VerticalFOA = 30f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Spin Slash";
            lStyle.Form = 2;
            lStyle.Forward = new Vector3(0f, -0.24f, 0.968f);
            lStyle.HorizontalFOA = 210f;
            lStyle.VerticalFOA = 20f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Low Forward Slash";
            lStyle.Form = 3;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 120f;
            lStyle.VerticalFOA = 30f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Pommel Blash";
            lStyle.Form = 4;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 30f;
            lStyle.VerticalFOA = 30f;
            lStyle.MinRange = 0.1f;
            lStyle.MaxRange = 1.0f;
            lStyle.DamageModifier = 0.5f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Jump Slash";
            lStyle.Form = 5;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 20f;
            lStyle.VerticalFOA = 50f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Jump Spin Slash";
            lStyle.Form = 6;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 70f;
            lStyle.VerticalFOA = 40f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = false;
            AttackStyles.Add(lStyle);
        }

#endregion

#endif

#endregion

#region Auto-Generated
        // ************************************ START AUTO GENERATED ************************************

        /// <summary>
        /// These declarations go inside the class so you can test for which state
        /// and transitions are active. Testing hash values is much faster than strings.
        /// </summary>
        public static int STATE_Slash = -1;
        public static int STATE_BackSlash = -1;
        public static int STATE_Idle = -1;
        public static int STATE_LowSlash = -1;
        public static int STATE_JumpSlash = -1;
        public static int STATE_SpinSlash = -1;
        public static int STATE_PommelBash = -1;
        public static int STATE_ComboSpinSlash = -1;
        public static int STATE_CrouchIn = -1;
        public static int STATE_CrouchOut = -1;
        public static int STATE_Blocked = -1;
        public static int STATE_Blocked2 = -1;
        public static int TRANS_AnyState_Slash = -1;
        public static int TRANS_EntryState_Slash = -1;
        public static int TRANS_AnyState_BackSlash = -1;
        public static int TRANS_EntryState_BackSlash = -1;
        public static int TRANS_AnyState_JumpSlash = -1;
        public static int TRANS_EntryState_JumpSlash = -1;
        public static int TRANS_AnyState_SpinSlash = -1;
        public static int TRANS_EntryState_SpinSlash = -1;
        public static int TRANS_AnyState_PommelBash = -1;
        public static int TRANS_EntryState_PommelBash = -1;
        public static int TRANS_AnyState_ComboSpinSlash = -1;
        public static int TRANS_EntryState_ComboSpinSlash = -1;
        public static int TRANS_AnyState_CrouchIn = -1;
        public static int TRANS_EntryState_CrouchIn = -1;
        public static int TRANS_Slash_Idle = -1;
        public static int TRANS_Slash_Blocked = -1;
        public static int TRANS_Slash_BackSlash = -1;
        public static int TRANS_BackSlash_Idle = -1;
        public static int TRANS_BackSlash_Slash = -1;
        public static int TRANS_BackSlash_Blocked2 = -1;
        public static int TRANS_LowSlash_CrouchOut = -1;
        public static int TRANS_LowSlash_Blocked = -1;
        public static int TRANS_JumpSlash_Idle = -1;
        public static int TRANS_SpinSlash_Idle = -1;
        public static int TRANS_SpinSlash_Blocked2 = -1;
        public static int TRANS_PommelBash_Idle = -1;
        public static int TRANS_ComboSpinSlash_Idle = -1;
        public static int TRANS_CrouchIn_LowSlash = -1;
        public static int TRANS_CrouchOut_Idle = -1;
        public static int TRANS_Blocked_Idle = -1;
        public static int TRANS_Blocked2_Idle = -1;

        /// <summary>
        /// Determines if we're using auto-generated code
        /// </summary>
        public override bool HasAutoGeneratedCode
        {
            get { return true; }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsInMotionState
        {
            get
            {
                int lStateID = mMotionLayer._AnimatorStateID;
                int lTransitionID = mMotionLayer._AnimatorTransitionID;

                if (lStateID == STATE_Slash) { return true; }
                if (lStateID == STATE_BackSlash) { return true; }
                if (lStateID == STATE_Idle) { return true; }
                if (lStateID == STATE_LowSlash) { return true; }
                if (lStateID == STATE_JumpSlash) { return true; }
                if (lStateID == STATE_SpinSlash) { return true; }
                if (lStateID == STATE_PommelBash) { return true; }
                if (lStateID == STATE_ComboSpinSlash) { return true; }
                if (lStateID == STATE_CrouchIn) { return true; }
                if (lStateID == STATE_CrouchOut) { return true; }
                if (lStateID == STATE_Blocked) { return true; }
                if (lStateID == STATE_Blocked2) { return true; }
                if (lTransitionID == TRANS_AnyState_Slash) { return true; }
                if (lTransitionID == TRANS_EntryState_Slash) { return true; }
                if (lTransitionID == TRANS_AnyState_BackSlash) { return true; }
                if (lTransitionID == TRANS_EntryState_BackSlash) { return true; }
                if (lTransitionID == TRANS_AnyState_JumpSlash) { return true; }
                if (lTransitionID == TRANS_EntryState_JumpSlash) { return true; }
                if (lTransitionID == TRANS_AnyState_SpinSlash) { return true; }
                if (lTransitionID == TRANS_EntryState_SpinSlash) { return true; }
                if (lTransitionID == TRANS_AnyState_PommelBash) { return true; }
                if (lTransitionID == TRANS_EntryState_PommelBash) { return true; }
                if (lTransitionID == TRANS_AnyState_ComboSpinSlash) { return true; }
                if (lTransitionID == TRANS_EntryState_ComboSpinSlash) { return true; }
                if (lTransitionID == TRANS_AnyState_CrouchIn) { return true; }
                if (lTransitionID == TRANS_EntryState_CrouchIn) { return true; }
                if (lTransitionID == TRANS_Slash_Idle) { return true; }
                if (lTransitionID == TRANS_Slash_Blocked) { return true; }
                if (lTransitionID == TRANS_Slash_BackSlash) { return true; }
                if (lTransitionID == TRANS_BackSlash_Idle) { return true; }
                if (lTransitionID == TRANS_BackSlash_Slash) { return true; }
                if (lTransitionID == TRANS_BackSlash_Blocked2) { return true; }
                if (lTransitionID == TRANS_LowSlash_CrouchOut) { return true; }
                if (lTransitionID == TRANS_LowSlash_Blocked) { return true; }
                if (lTransitionID == TRANS_JumpSlash_Idle) { return true; }
                if (lTransitionID == TRANS_SpinSlash_Idle) { return true; }
                if (lTransitionID == TRANS_SpinSlash_Blocked2) { return true; }
                if (lTransitionID == TRANS_PommelBash_Idle) { return true; }
                if (lTransitionID == TRANS_ComboSpinSlash_Idle) { return true; }
                if (lTransitionID == TRANS_CrouchIn_LowSlash) { return true; }
                if (lTransitionID == TRANS_CrouchOut_Idle) { return true; }
                if (lTransitionID == TRANS_Blocked_Idle) { return true; }
                if (lTransitionID == TRANS_Blocked2_Idle) { return true; }
                return false;
            }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID)
        {
            if (rStateID == STATE_Slash) { return true; }
            if (rStateID == STATE_BackSlash) { return true; }
            if (rStateID == STATE_Idle) { return true; }
            if (rStateID == STATE_LowSlash) { return true; }
            if (rStateID == STATE_JumpSlash) { return true; }
            if (rStateID == STATE_SpinSlash) { return true; }
            if (rStateID == STATE_PommelBash) { return true; }
            if (rStateID == STATE_ComboSpinSlash) { return true; }
            if (rStateID == STATE_CrouchIn) { return true; }
            if (rStateID == STATE_CrouchOut) { return true; }
            if (rStateID == STATE_Blocked) { return true; }
            if (rStateID == STATE_Blocked2) { return true; }
            return false;
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID, int rTransitionID)
        {
            if (rStateID == STATE_Slash) { return true; }
            if (rStateID == STATE_BackSlash) { return true; }
            if (rStateID == STATE_Idle) { return true; }
            if (rStateID == STATE_LowSlash) { return true; }
            if (rStateID == STATE_JumpSlash) { return true; }
            if (rStateID == STATE_SpinSlash) { return true; }
            if (rStateID == STATE_PommelBash) { return true; }
            if (rStateID == STATE_ComboSpinSlash) { return true; }
            if (rStateID == STATE_CrouchIn) { return true; }
            if (rStateID == STATE_CrouchOut) { return true; }
            if (rStateID == STATE_Blocked) { return true; }
            if (rStateID == STATE_Blocked2) { return true; }
            if (rTransitionID == TRANS_AnyState_Slash) { return true; }
            if (rTransitionID == TRANS_EntryState_Slash) { return true; }
            if (rTransitionID == TRANS_AnyState_BackSlash) { return true; }
            if (rTransitionID == TRANS_EntryState_BackSlash) { return true; }
            if (rTransitionID == TRANS_AnyState_JumpSlash) { return true; }
            if (rTransitionID == TRANS_EntryState_JumpSlash) { return true; }
            if (rTransitionID == TRANS_AnyState_SpinSlash) { return true; }
            if (rTransitionID == TRANS_EntryState_SpinSlash) { return true; }
            if (rTransitionID == TRANS_AnyState_PommelBash) { return true; }
            if (rTransitionID == TRANS_EntryState_PommelBash) { return true; }
            if (rTransitionID == TRANS_AnyState_ComboSpinSlash) { return true; }
            if (rTransitionID == TRANS_EntryState_ComboSpinSlash) { return true; }
            if (rTransitionID == TRANS_AnyState_CrouchIn) { return true; }
            if (rTransitionID == TRANS_EntryState_CrouchIn) { return true; }
            if (rTransitionID == TRANS_Slash_Idle) { return true; }
            if (rTransitionID == TRANS_Slash_Blocked) { return true; }
            if (rTransitionID == TRANS_Slash_BackSlash) { return true; }
            if (rTransitionID == TRANS_BackSlash_Idle) { return true; }
            if (rTransitionID == TRANS_BackSlash_Slash) { return true; }
            if (rTransitionID == TRANS_BackSlash_Blocked2) { return true; }
            if (rTransitionID == TRANS_LowSlash_CrouchOut) { return true; }
            if (rTransitionID == TRANS_LowSlash_Blocked) { return true; }
            if (rTransitionID == TRANS_JumpSlash_Idle) { return true; }
            if (rTransitionID == TRANS_SpinSlash_Idle) { return true; }
            if (rTransitionID == TRANS_SpinSlash_Blocked2) { return true; }
            if (rTransitionID == TRANS_PommelBash_Idle) { return true; }
            if (rTransitionID == TRANS_ComboSpinSlash_Idle) { return true; }
            if (rTransitionID == TRANS_CrouchIn_LowSlash) { return true; }
            if (rTransitionID == TRANS_CrouchOut_Idle) { return true; }
            if (rTransitionID == TRANS_Blocked_Idle) { return true; }
            if (rTransitionID == TRANS_Blocked2_Idle) { return true; }
            return false;
        }

        /// <summary>
        /// Preprocess any animator data so the motion can use it later
        /// </summary>
        public override void LoadAnimatorData()
        {
            TRANS_AnyState_Slash = mMotionController.AddAnimatorName("AnyState -> Base Layer.PSS_BasicAttacks-SM.Slash");
            TRANS_EntryState_Slash = mMotionController.AddAnimatorName("Entry -> Base Layer.PSS_BasicAttacks-SM.Slash");
            TRANS_AnyState_BackSlash = mMotionController.AddAnimatorName("AnyState -> Base Layer.PSS_BasicAttacks-SM.Back Slash");
            TRANS_EntryState_BackSlash = mMotionController.AddAnimatorName("Entry -> Base Layer.PSS_BasicAttacks-SM.Back Slash");
            TRANS_AnyState_JumpSlash = mMotionController.AddAnimatorName("AnyState -> Base Layer.PSS_BasicAttacks-SM.Jump Slash");
            TRANS_EntryState_JumpSlash = mMotionController.AddAnimatorName("Entry -> Base Layer.PSS_BasicAttacks-SM.Jump Slash");
            TRANS_AnyState_SpinSlash = mMotionController.AddAnimatorName("AnyState -> Base Layer.PSS_BasicAttacks-SM.Spin Slash");
            TRANS_EntryState_SpinSlash = mMotionController.AddAnimatorName("Entry -> Base Layer.PSS_BasicAttacks-SM.Spin Slash");
            TRANS_AnyState_PommelBash = mMotionController.AddAnimatorName("AnyState -> Base Layer.PSS_BasicAttacks-SM.Pommel Bash");
            TRANS_EntryState_PommelBash = mMotionController.AddAnimatorName("Entry -> Base Layer.PSS_BasicAttacks-SM.Pommel Bash");
            TRANS_AnyState_ComboSpinSlash = mMotionController.AddAnimatorName("AnyState -> Base Layer.PSS_BasicAttacks-SM.Combo Spin Slash");
            TRANS_EntryState_ComboSpinSlash = mMotionController.AddAnimatorName("Entry -> Base Layer.PSS_BasicAttacks-SM.Combo Spin Slash");
            TRANS_AnyState_CrouchIn = mMotionController.AddAnimatorName("AnyState -> Base Layer.PSS_BasicAttacks-SM.Crouch In");
            TRANS_EntryState_CrouchIn = mMotionController.AddAnimatorName("Entry -> Base Layer.PSS_BasicAttacks-SM.Crouch In");
            STATE_Slash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Slash");
            TRANS_Slash_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Slash -> Base Layer.PSS_BasicAttacks-SM.Idle");
            TRANS_Slash_Blocked = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Slash -> Base Layer.PSS_BasicAttacks-SM.Blocked");
            TRANS_Slash_BackSlash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Slash -> Base Layer.PSS_BasicAttacks-SM.Back Slash");
            STATE_BackSlash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Back Slash");
            TRANS_BackSlash_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Back Slash -> Base Layer.PSS_BasicAttacks-SM.Idle");
            TRANS_BackSlash_Slash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Back Slash -> Base Layer.PSS_BasicAttacks-SM.Slash");
            TRANS_BackSlash_Blocked2 = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Back Slash -> Base Layer.PSS_BasicAttacks-SM.Blocked 2");
            STATE_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Idle");
            STATE_LowSlash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Low Slash");
            TRANS_LowSlash_CrouchOut = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Low Slash -> Base Layer.PSS_BasicAttacks-SM.Crouch Out");
            TRANS_LowSlash_Blocked = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Low Slash -> Base Layer.PSS_BasicAttacks-SM.Blocked");
            STATE_JumpSlash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Jump Slash");
            TRANS_JumpSlash_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Jump Slash -> Base Layer.PSS_BasicAttacks-SM.Idle");
            STATE_SpinSlash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Spin Slash");
            TRANS_SpinSlash_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Spin Slash -> Base Layer.PSS_BasicAttacks-SM.Idle");
            TRANS_SpinSlash_Blocked2 = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Spin Slash -> Base Layer.PSS_BasicAttacks-SM.Blocked 2");
            STATE_PommelBash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Pommel Bash");
            TRANS_PommelBash_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Pommel Bash -> Base Layer.PSS_BasicAttacks-SM.Idle");
            STATE_ComboSpinSlash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Combo Spin Slash");
            TRANS_ComboSpinSlash_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Combo Spin Slash -> Base Layer.PSS_BasicAttacks-SM.Idle");
            STATE_CrouchIn = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Crouch In");
            TRANS_CrouchIn_LowSlash = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Crouch In -> Base Layer.PSS_BasicAttacks-SM.Low Slash");
            STATE_CrouchOut = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Crouch Out");
            TRANS_CrouchOut_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Crouch Out -> Base Layer.PSS_BasicAttacks-SM.Idle");
            STATE_Blocked = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Blocked");
            TRANS_Blocked_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Blocked -> Base Layer.PSS_BasicAttacks-SM.Idle");
            STATE_Blocked2 = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Blocked 2");
            TRANS_Blocked2_Idle = mMotionController.AddAnimatorName("Base Layer.PSS_BasicAttacks-SM.Blocked 2 -> Base Layer.PSS_BasicAttacks-SM.Idle");
        }

#if UNITY_EDITOR

        private AnimationClip m18504 = null;
        private AnimationClip m16032 = null;
        private AnimationClip m12314 = null;
        private AnimationClip m12688 = null;
        private AnimationClip m15756 = null;
        private AnimationClip m16156 = null;
        private AnimationClip m12494 = null;
        private AnimationClip m14928 = null;
        private AnimationClip m10740 = null;
        private AnimationClip m19110 = null;

        /// <summary>
        /// Creates the animator substate machine for this motion.
        /// </summary>
        protected override void CreateStateMachine()
        {
            // Grab the root sm for the layer
            UnityEditor.Animations.AnimatorStateMachine lRootStateMachine = _EditorAnimatorController.layers[mMotionLayer.AnimatorLayerIndex].stateMachine;
            UnityEditor.Animations.AnimatorStateMachine lSM_20856 = _EditorAnimatorController.layers[mMotionLayer.AnimatorLayerIndex].stateMachine;
            UnityEditor.Animations.AnimatorStateMachine lRootSubStateMachine = null;

            // If we find the sm with our name, remove it
            for (int i = 0; i < lRootStateMachine.stateMachines.Length; i++)
            {
                // Look for a sm with the matching name
                if (lRootStateMachine.stateMachines[i].stateMachine.name == _EditorAnimatorSMName)
                {
                    lRootSubStateMachine = lRootStateMachine.stateMachines[i].stateMachine;

                    // Allow the user to stop before we remove the sm
                    if (!UnityEditor.EditorUtility.DisplayDialog("Motion Controller", _EditorAnimatorSMName + " already exists. Delete and recreate it?", "Yes", "No"))
                    {
                        return;
                    }

                    // Remove the sm
                    //lRootStateMachine.RemoveStateMachine(lRootStateMachine.stateMachines[i].stateMachine);
                    break;
                }
            }

            UnityEditor.Animations.AnimatorStateMachine lSM_20876 = lRootSubStateMachine;
            if (lSM_20876 != null)
            {
                for (int i = lSM_20876.entryTransitions.Length - 1; i >= 0; i--)
                {
                    lSM_20876.RemoveEntryTransition(lSM_20876.entryTransitions[i]);
                }

                for (int i = lSM_20876.anyStateTransitions.Length - 1; i >= 0; i--)
                {
                    lSM_20876.RemoveAnyStateTransition(lSM_20876.anyStateTransitions[i]);
                }

                for (int i = lSM_20876.states.Length - 1; i >= 0; i--)
                {
                    lSM_20876.RemoveState(lSM_20876.states[i].state);
                }

                for (int i = lSM_20876.stateMachines.Length - 1; i >= 0; i--)
                {
                    lSM_20876.RemoveStateMachine(lSM_20876.stateMachines[i].stateMachine);
                }
            }
            else
            {
                lSM_20876 = lSM_20856.AddStateMachine(_EditorAnimatorSMName, new Vector3(408, 60, 0));
            }

            UnityEditor.Animations.AnimatorState lS_21112 = lSM_20876.AddState("Slash", new Vector3(336, 48, 0));
            lS_21112.speed = 1.2f;
            lS_21112.motion = m18504;

            UnityEditor.Animations.AnimatorState lS_21114 = lSM_20876.AddState("Back Slash", new Vector3(336, 120, 0));
            lS_21114.speed = 1.4f;
            lS_21114.motion = m16032;

            UnityEditor.Animations.AnimatorState lS_21396 = lSM_20876.AddState("Idle", new Vector3(768, 264, 0));
            lS_21396.speed = 0.3f;
            lS_21396.motion = m12314;

            UnityEditor.Animations.AnimatorState lS_21398 = lSM_20876.AddState("Low Slash", new Vector3(336, 480, 0));
            lS_21398.speed = 1.3f;
            lS_21398.motion = m12688;

            UnityEditor.Animations.AnimatorState lS_21160 = lSM_20876.AddState("Jump Slash", new Vector3(336, 336, 0));
            lS_21160.speed = 1.2f;
            lS_21160.motion = m15756;

            UnityEditor.Animations.AnimatorState lS_21162 = lSM_20876.AddState("Spin Slash", new Vector3(336, 192, 0));
            lS_21162.speed = 1.2f;
            lS_21162.motion = m16156;

            UnityEditor.Animations.AnimatorState lS_21164 = lSM_20876.AddState("Pommel Bash", new Vector3(336, 264, 0));
            lS_21164.speed = 1f;
            lS_21164.motion = m12494;

            UnityEditor.Animations.AnimatorState lS_21166 = lSM_20876.AddState("Combo Spin Slash", new Vector3(336, 408, 0));
            lS_21166.speed = 1.3f;
            lS_21166.motion = m14928;

            UnityEditor.Animations.AnimatorState lS_21168 = lSM_20876.AddState("Crouch In", new Vector3(108, 480, 0));
            lS_21168.speed = 1f;
            lS_21168.motion = m10740;

            UnityEditor.Animations.AnimatorState lS_21400 = lSM_20876.AddState("Crouch Out", new Vector3(564, 480, 0));
            lS_21400.speed = 1f;
            lS_21400.motion = m19110;

            UnityEditor.Animations.AnimatorState lS_21402 = lSM_20876.AddState("Blocked", new Vector3(636, 48, 0));
            lS_21402.speed = 0.8f;
            lS_21402.motion = m12494;

            UnityEditor.Animations.AnimatorState lS_N156514 = lSM_20876.AddState("Blocked 2", new Vector3(732, 120, 0));
            lS_N156514.speed = -1f;
            lS_N156514.motion = m16032;

            // Create the transition from the any state. Note that 'AnyState' transitions have to be added to the root
            UnityEditor.Animations.AnimatorStateTransition lT_20970 = lRootStateMachine.AddAnyStateTransition(lS_21112);
            lT_20970.hasExitTime = false;
            lT_20970.hasFixedDuration = true;
            lT_20970.exitTime = 0.9f;
            lT_20970.duration = 0.1f;
            lT_20970.offset = 0f;
            lT_20970.mute = false;
            lT_20970.solo = false;
            lT_20970.canTransitionToSelf = false;
            lT_20970.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32040f, "L0MotionPhase");
            lT_20970.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 0f, "L0MotionParameter");

            // Create the transition from the any state. Note that 'AnyState' transitions have to be added to the root
            UnityEditor.Animations.AnimatorStateTransition lT_20972 = lRootStateMachine.AddAnyStateTransition(lS_21114);
            lT_20972.hasExitTime = false;
            lT_20972.hasFixedDuration = true;
            lT_20972.exitTime = 0.9f;
            lT_20972.duration = 0.1f;
            lT_20972.offset = 0f;
            lT_20972.mute = false;
            lT_20972.solo = false;
            lT_20972.canTransitionToSelf = false;
            lT_20972.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32040f, "L0MotionPhase");
            lT_20972.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 1f, "L0MotionParameter");

            // Create the transition from the any state. Note that 'AnyState' transitions have to be added to the root
            UnityEditor.Animations.AnimatorStateTransition lT_21026 = lRootStateMachine.AddAnyStateTransition(lS_21160);
            lT_21026.hasExitTime = false;
            lT_21026.hasFixedDuration = true;
            lT_21026.exitTime = 0.9f;
            lT_21026.duration = 0.1f;
            lT_21026.offset = 0f;
            lT_21026.mute = false;
            lT_21026.solo = false;
            lT_21026.canTransitionToSelf = true;
            lT_21026.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32040f, "L0MotionPhase");
            lT_21026.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 5f, "L0MotionParameter");

            // Create the transition from the any state. Note that 'AnyState' transitions have to be added to the root
            UnityEditor.Animations.AnimatorStateTransition lT_21028 = lRootStateMachine.AddAnyStateTransition(lS_21162);
            lT_21028.hasExitTime = false;
            lT_21028.hasFixedDuration = true;
            lT_21028.exitTime = 0.9f;
            lT_21028.duration = 0.1f;
            lT_21028.offset = 0f;
            lT_21028.mute = false;
            lT_21028.solo = false;
            lT_21028.canTransitionToSelf = true;
            lT_21028.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32040f, "L0MotionPhase");
            lT_21028.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 2f, "L0MotionParameter");

            // Create the transition from the any state. Note that 'AnyState' transitions have to be added to the root
            UnityEditor.Animations.AnimatorStateTransition lT_21030 = lRootStateMachine.AddAnyStateTransition(lS_21164);
            lT_21030.hasExitTime = false;
            lT_21030.hasFixedDuration = true;
            lT_21030.exitTime = 0.9f;
            lT_21030.duration = 0.1f;
            lT_21030.offset = 0f;
            lT_21030.mute = false;
            lT_21030.solo = false;
            lT_21030.canTransitionToSelf = true;
            lT_21030.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32040f, "L0MotionPhase");
            lT_21030.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 4f, "L0MotionParameter");

            // Create the transition from the any state. Note that 'AnyState' transitions have to be added to the root
            UnityEditor.Animations.AnimatorStateTransition lT_21032 = lRootStateMachine.AddAnyStateTransition(lS_21166);
            lT_21032.hasExitTime = false;
            lT_21032.hasFixedDuration = true;
            lT_21032.exitTime = 0.9f;
            lT_21032.duration = 0.1f;
            lT_21032.offset = 0f;
            lT_21032.mute = false;
            lT_21032.solo = false;
            lT_21032.canTransitionToSelf = true;
            lT_21032.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32040f, "L0MotionPhase");
            lT_21032.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 6f, "L0MotionParameter");

            // Create the transition from the any state. Note that 'AnyState' transitions have to be added to the root
            UnityEditor.Animations.AnimatorStateTransition lT_21034 = lRootStateMachine.AddAnyStateTransition(lS_21168);
            lT_21034.hasExitTime = false;
            lT_21034.hasFixedDuration = true;
            lT_21034.exitTime = 0.9f;
            lT_21034.duration = 0.09999999f;
            lT_21034.offset = 0.319487f;
            lT_21034.mute = false;
            lT_21034.solo = false;
            lT_21034.canTransitionToSelf = true;
            lT_21034.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32040f, "L0MotionPhase");
            lT_21034.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3f, "L0MotionParameter");

            UnityEditor.Animations.AnimatorStateTransition lT_21404 = lS_21112.AddTransition(lS_21396);
            lT_21404.hasExitTime = true;
            lT_21404.hasFixedDuration = true;
            lT_21404.exitTime = 1f;
            lT_21404.duration = 0f;
            lT_21404.offset = 0f;
            lT_21404.mute = false;
            lT_21404.solo = false;
            lT_21404.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_21406 = lS_21112.AddTransition(lS_21402);
            lT_21406.hasExitTime = false;
            lT_21406.hasFixedDuration = true;
            lT_21406.exitTime = 0.8333334f;
            lT_21406.duration = 0.1f;
            lT_21406.offset = 0.4947726f;
            lT_21406.mute = false;
            lT_21406.solo = false;
            lT_21406.canTransitionToSelf = true;
            lT_21406.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32045f, "L0MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lT_21408 = lS_21112.AddTransition(lS_21114);
            lT_21408.hasExitTime = false;
            lT_21408.hasFixedDuration = true;
            lT_21408.exitTime = 0.423186f;
            lT_21408.duration = 0.15f;
            lT_21408.offset = 0.2619449f;
            lT_21408.mute = false;
            lT_21408.solo = false;
            lT_21408.canTransitionToSelf = true;
            lT_21408.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32041f, "L0MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lT_21410 = lS_21114.AddTransition(lS_21396);
            lT_21410.hasExitTime = true;
            lT_21410.hasFixedDuration = true;
            lT_21410.exitTime = 1f;
            lT_21410.duration = 0f;
            lT_21410.offset = 0f;
            lT_21410.mute = false;
            lT_21410.solo = false;
            lT_21410.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_21412 = lS_21114.AddTransition(lS_21112);
            lT_21412.hasExitTime = false;
            lT_21412.hasFixedDuration = true;
            lT_21412.exitTime = 0.6925371f;
            lT_21412.duration = 0.15f;
            lT_21412.offset = 0.1328259f;
            lT_21412.mute = false;
            lT_21412.solo = false;
            lT_21412.canTransitionToSelf = true;
            lT_21412.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32041f, "L0MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lT_N158208 = lS_21114.AddTransition(lS_N156514);
            lT_N158208.hasExitTime = false;
            lT_N158208.hasFixedDuration = true;
            lT_N158208.exitTime = 0.4270249f;
            lT_N158208.duration = 0.15f;
            lT_N158208.offset = 0.4603814f;
            lT_N158208.mute = false;
            lT_N158208.solo = false;
            lT_N158208.canTransitionToSelf = true;
            lT_N158208.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32045f, "L0MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lT_21414 = lS_21398.AddTransition(lS_21400);
            lT_21414.hasExitTime = true;
            lT_21414.hasFixedDuration = true;
            lT_21414.exitTime = 0.9306567f;
            lT_21414.duration = 0.1f;
            lT_21414.offset = 0.1f;
            lT_21414.mute = false;
            lT_21414.solo = false;
            lT_21414.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_N1280690 = lS_21398.AddTransition(lS_21402);
            lT_N1280690.hasExitTime = false;
            lT_N1280690.hasFixedDuration = true;
            lT_N1280690.exitTime = 0.4432549f;
            lT_N1280690.duration = 0.1f;
            lT_N1280690.offset = 0.4947726f;
            lT_N1280690.mute = false;
            lT_N1280690.solo = false;
            lT_N1280690.canTransitionToSelf = true;
            lT_N1280690.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32045f, "L0MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lT_21416 = lS_21160.AddTransition(lS_21396);
            lT_21416.hasExitTime = true;
            lT_21416.hasFixedDuration = true;
            lT_21416.exitTime = 1f;
            lT_21416.duration = 0f;
            lT_21416.offset = 0f;
            lT_21416.mute = false;
            lT_21416.solo = false;
            lT_21416.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_21418 = lS_21162.AddTransition(lS_21396);
            lT_21418.hasExitTime = true;
            lT_21418.hasFixedDuration = true;
            lT_21418.exitTime = 0.8434141f;
            lT_21418.duration = 0.15f;
            lT_21418.offset = 0f;
            lT_21418.mute = false;
            lT_21418.solo = false;
            lT_21418.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_N1225966 = lS_21162.AddTransition(lS_N156514);
            lT_N1225966.hasExitTime = false;
            lT_N1225966.hasFixedDuration = true;
            lT_N1225966.exitTime = 0.8557693f;
            lT_N1225966.duration = 0.15f;
            lT_N1225966.offset = 0.4603814f;
            lT_N1225966.mute = false;
            lT_N1225966.solo = false;
            lT_N1225966.canTransitionToSelf = true;
            lT_N1225966.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 32045f, "L0MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lT_21420 = lS_21164.AddTransition(lS_21396);
            lT_21420.hasExitTime = true;
            lT_21420.hasFixedDuration = true;
            lT_21420.exitTime = 1f;
            lT_21420.duration = 0f;
            lT_21420.offset = 0f;
            lT_21420.mute = false;
            lT_21420.solo = false;
            lT_21420.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_21422 = lS_21166.AddTransition(lS_21396);
            lT_21422.hasExitTime = true;
            lT_21422.hasFixedDuration = true;
            lT_21422.exitTime = 1f;
            lT_21422.duration = 0f;
            lT_21422.offset = 0f;
            lT_21422.mute = false;
            lT_21422.solo = false;
            lT_21422.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_21424 = lS_21168.AddTransition(lS_21398);
            lT_21424.hasExitTime = true;
            lT_21424.hasFixedDuration = true;
            lT_21424.exitTime = 0.5796737f;
            lT_21424.duration = 0.1f;
            lT_21424.offset = 0.1159924f;
            lT_21424.mute = false;
            lT_21424.solo = false;
            lT_21424.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_21426 = lS_21400.AddTransition(lS_21396);
            lT_21426.hasExitTime = true;
            lT_21426.hasFixedDuration = true;
            lT_21426.exitTime = 0.8f;
            lT_21426.duration = 0.1f;
            lT_21426.offset = 0f;
            lT_21426.mute = false;
            lT_21426.solo = false;
            lT_21426.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_21428 = lS_21402.AddTransition(lS_21396);
            lT_21428.hasExitTime = true;
            lT_21428.hasFixedDuration = true;
            lT_21428.exitTime = 0.873613f;
            lT_21428.duration = 0.1000001f;
            lT_21428.offset = 0f;
            lT_21428.mute = false;
            lT_21428.solo = false;
            lT_21428.canTransitionToSelf = true;

            UnityEditor.Animations.AnimatorStateTransition lT_N158472 = lS_N156514.AddTransition(lS_21396);
            lT_N158472.hasExitTime = true;
            lT_N158472.hasFixedDuration = true;
            lT_N158472.exitTime = 0.9644228f;
            lT_N158472.duration = 0.04999995f;
            lT_N158472.offset = 110.637f;
            lT_N158472.mute = false;
            lT_N158472.solo = false;
            lT_N158472.canTransitionToSelf = true;

        }

        /// <summary>
        /// Gathers the animations so we can use them when creating the sub-state machine.
        /// </summary>
        public override void FindAnimations()
        {
            m18504 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash.fbx/sword_and_shield_slash.anim", "sword_and_shield_slash");
            m16032 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash_2.fbx/sword_and_shield_slash_2.anim", "sword_and_shield_slash_2");
            m12314 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_idle.fbx/PSS_IdlePose.anim", "PSS_IdlePose");
            m12688 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash_4.fbx/sword_and_shield_slash_4.anim", "sword_and_shield_slash_4");
            m15756 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_attack.fbx/JumpSlash.anim", "JumpSlash");
            m16156 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_attack_2.fbx/sword_and_shield_attack_2.anim", "sword_and_shield_attack_2");
            m12494 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_attack_3.fbx/sword_and_shield_attack_3.anim", "sword_and_shield_attack_3");
            m14928 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash_1.fbx/Spin Jump Slash.anim", "Spin Jump Slash");
            m10740 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_crouch.fbx/sword_and_shield_crouch.anim", "sword_and_shield_crouch");
            m19110 = FindAnimationClip("Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_crouching.fbx/sword_and_shield_crouching.anim", "sword_and_shield_crouching");

            // Add the remaining functionality
            base.FindAnimations();
        }

        /// <summary>
        /// Used to show the settings that allow us to generate the animator setup.
        /// </summary>
        public override void OnSettingsGUI()
        {
            UnityEditor.EditorGUILayout.IntField(new GUIContent("Phase ID", "Phase ID used to transition to the state."), PHASE_START);
            m18504 = CreateAnimationField("Slash.sword_and_shield_slash", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash.fbx/sword_and_shield_slash.anim", "sword_and_shield_slash", m18504);
            m16032 = CreateAnimationField("Back Slash.sword_and_shield_slash_2", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash_2.fbx/sword_and_shield_slash_2.anim", "sword_and_shield_slash_2", m16032);
            m12314 = CreateAnimationField("Idle.PSS_IdlePose", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_idle.fbx/PSS_IdlePose.anim", "PSS_IdlePose", m12314);
            m12688 = CreateAnimationField("Low Slash.sword_and_shield_slash_4", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash_4.fbx/sword_and_shield_slash_4.anim", "sword_and_shield_slash_4", m12688);
            m15756 = CreateAnimationField("Jump Slash.JumpSlash", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_attack.fbx/JumpSlash.anim", "JumpSlash", m15756);
            m16156 = CreateAnimationField("Spin Slash.sword_and_shield_attack_2", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_attack_2.fbx/sword_and_shield_attack_2.anim", "sword_and_shield_attack_2", m16156);
            m12494 = CreateAnimationField("Pommel Bash.sword_and_shield_attack_3", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_attack_3.fbx/sword_and_shield_attack_3.anim", "sword_and_shield_attack_3", m12494);
            m14928 = CreateAnimationField("Combo Spin Slash.Spin Jump Slash", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_slash_1.fbx/Spin Jump Slash.anim", "Spin Jump Slash", m14928);
            m10740 = CreateAnimationField("Crouch In.sword_and_shield_crouch", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_crouch.fbx/sword_and_shield_crouch.anim", "sword_and_shield_crouch", m10740);
            m19110 = CreateAnimationField("Crouch Out.sword_and_shield_crouching", "Assets/ootii/MotionControllerPacks/SwordShield/Content/Animations/Mixamo/Y_Bot@sword_and_shield_crouching.fbx/sword_and_shield_crouching.anim", "sword_and_shield_crouching", m19110);

            // Add the remaining functionality
            base.OnSettingsGUI();
        }

#endif

        // ************************************ END AUTO GENERATED ************************************
#endregion
    }
}
