﻿// Author: Thomas Reiffer
// Last Edit: 19/09/18
// Summary: Elain's FSM, which sends commands to both the NavMeshInputSource and MotionController.
//          Contains both navigation and combat states, along with state-scoped logic. 
// 
// TODO:    Locally scope comboIterator somehow because constantly resetting it is a bit of a pain.
//          Link CircleTimer to dialogue duration if there's dialogue playing.
//          Contain more state logic within the states themselves - eg hitstun-ending logic.
//          Cleanup and properly understand dodge state. See comments.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Input;
using com.ootii.Actors.Combat;
using com.ootii.Reactors;
using com.ootii.Messages;
using com.ootii.MotionControllerPacks;
using com.ootii.Actors.LifeCores;
using com.ootii.Actors;
using com.ootii.Actors.AnimationControllers;
using com.ootii.Actors.Attributes;
using NFTS.TimeManager;
using com.ootii.Actors.Inventory;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class Elain_StateMachine : NavMeshInputSource
{
    // Variables and references.
    // ----------------------------------------------
    #region PlayerReferences
    //We're indirectly reading the player's inputs because we're scumbags.

    public GameObject playerObject;
    public MotionController pMotionController;
    private ActorController pActorController;
    private Combatant pCombatant;
    public BasicMeleeAttack pAttack;
    public CombatEvade pEvade;

    //Transform targets that are attached to the player here.

    public Transform CircleLeftTransform;
    public Transform CircleRightTransform;
    public Transform RetreatTransform;
    public Transform ThrowTargetTrans;
    public PositionManager positionManager;
    #endregion

    // ----------------------------------------------

    //Locally scoped variables.
    #region Internal State References
    
    //Dialogue control references
    private bool hasDialogued = false;

    private bool _comboStutterBool = false;
    public bool ComboStutterBool
    {
        set
        {
            _comboStutterBool = value;
        }
        get
        {
            return _comboStutterBool;
        }
    }

    public bool ExteriorMode;

    public bool isInterruptable = true;

    public int damageInstanceCounter;
    #endregion


    // References for character animation components.
    // ----------------------------------------------
    #region MotionController References

    public BasicMeleeAttack basicMeleeAttack;
    public CombatEvade combatEvade;
    public PSS_Damaged damaged;
    public Animator mAnim;
    public ActorCore mActorCore;
    public BasicWalkRunPivot locomotion;
    public BasicInventory AI_Inventory;
    public Combatant mCombatant;
    private BasicAttributes mBasicAttributes;

    #endregion

    // ----------------------------------------------

    //  public bool GateDodge = false;

    //Post-processing reference
    public PostProcessingManager PPM;

    // Non-animation combat references
    // ----------------------------------------------

    #region Combat
    
    // For deciding which combo to use.
    AI_Brain_Attack CombatDecisionBrain;

    //For calculating HP loss to progress arenas & difficulty;
    public float StartingHP;
    public float CurrentHP;

    float HP
    {
        get { return mBasicAttributes.GetAttributeValue<float>("Health"); }
        set { mBasicAttributes.SetAttributeValue<float>("Health", value); }
    }

    float MaxHP = 2000;

    //Iterator for moving through the different attacks in a combo while attacking.
    public int comboIterator = 0;

    //Are we dodging, how many times in a row? Are we countering, and how many times in a row?
    public bool AIDodgeActive = false;
    private int _AIDodgeChain = 0;
    public int MaxCounters = 0;
    public int DodgeThreshold = 8;
    public int AIDodgeChain
    {
        get
        {
            //Debug.Log("_AIDodgeChain = " + _AIDodgeChain);
            return _AIDodgeChain;
        }
        set
        {
            _AIDodgeChain = value;
        }
    }

    #endregion

    // ----------------------------------------------

    // State declaration and state-switching property.
    // ----------------------------------------------

    #region State Logic

    public enum COMBATSTATE
    {
        ATTACKING = 0,
        CIRCLING = 1,
        ADVANCING = 2,
        RETREATING = 3,
        //DIALOGUE = 4,
        TAUNTING = 5,
        STUNNED = 6,
        DODGING = 7,
        GATE = 8,
        THROW = 9,
        CUTSCENE = 10
    };

    private COMBATSTATE _CurrentCombatState;
    public COMBATSTATE CurrentCombatState
    {

        get { return _CurrentCombatState; }

        set
        {
            // If we aren't interruptable, change nothing.
            if (!isInterruptable && value != COMBATSTATE.CUTSCENE)
            {
                return;
            }

            StopAllCoroutines();
            _CurrentCombatState = value;

            // If we're NOT in a cutscene, we should be ready to fight.
            if (value != COMBATSTATE.CUTSCENE)
            {
                if (!AI_Inventory.IsWeaponSetEquipped(AI_Inventory.ActiveWeaponSet))
                {
                    AI_Inventory.EquipWeaponSet(AI_Inventory.ActiveWeaponSet);
                }
            } 
            else // If we ARE in a cutscene, stop Elain beating her sister up when we don't want her to.
            {
                if (AI_Inventory.IsWeaponSetEquipped(AI_Inventory.ActiveWeaponSet))
                {
                    AI_Inventory.StoreWeaponSet(AI_Inventory.ActiveWeaponSet);
                }
            }

            switch (value)
            {
                case COMBATSTATE.ATTACKING:
                    StartCoroutine(Attacking());
                    break;

                case COMBATSTATE.CIRCLING:
                    StartCoroutine(Circling());
                    break;

                case COMBATSTATE.ADVANCING:
                    StartCoroutine(Advancing());
                    break;

                case COMBATSTATE.RETREATING:
                    StartCoroutine(Retreating());
                    break;

                /*case COMBATSTATE.DIALOGUE:
                    StartCoroutine(Dialogue());
                    break;
                    */

                case COMBATSTATE.DODGING:
                    StartCoroutine(Dodging());
                    break;
                    

                case COMBATSTATE.TAUNTING:
                    StartCoroutine(Taunting());
                    break;

                case COMBATSTATE.STUNNED:
                    //Debug.Log("Stunning");
                    StartCoroutine(Stunned());
                    break;

                case COMBATSTATE.THROW:
                    StartCoroutine(ProjectileThrow());
                    break;

                    // Unused
                case COMBATSTATE.GATE:
                    CurrentCombatState = COMBATSTATE.RETREATING;
                    break;

                case COMBATSTATE.CUTSCENE:
                    StartCoroutine(Cutscene());
                    break;
            }
        }

    }

    #endregion

    // ----------------------------------------------

    // Functions that intersect/depend on combat messages.
    // ----------------------------------------------

    #region Reactors and Messages

     // Called on each combo to prevent infinite looping of combo iterator.
    public void ComboStutterer()
    {
        ComboStutterBool = false;
        CombatDataCentre.AIAggression -= 0.5f;
    }

    // ReactorAction that's called on each combat message the ActorController sends. Reads if message is a combat message.
    public void ComboStuttererReactor(ReactorAction rAction)
    {
        if (rAction == null || rAction.Message == null) { return; }

        CombatMessage lCombatMessage = rAction.Message as CombatMessage;

        if (lCombatMessage == null) { return; }

        ComboStutterBool = false;
    }

    //If Elain is taking damage, then we change her 
    public void DamageIterator(IMessage message)
    {
        // If we're won, go ahead and reset for the next player.
        if (CurrentHP <= 0) { SceneManager.LoadScene(0); }

        // If we've been struck, reset dodge and damage params.
        AIDodgeChain = 0;
        CurrentHP = HP;
        
        // Increase the number of counters we can do in a row according to progression of fight.
        MaxCounters = Mathf.RoundToInt( (StartingHP - CurrentHP)/ 200);

        //Find the music, and up the intensity according to Elain's difficulty progression.
        FindObjectOfType<FMOD_MusicManager>().ChangeFMODParam(FindObjectOfType<FMOD_MusicManager>().Intensity,MaxCounters / 10);

        // Get closer to starting a dodge volley.
        DodgeThreshold--;

        // Manage tutorialisation

        if (CurrentHP <= 1900f && TutorialManager.IsFirstDodge == true)
        {
            TutorialManager.AttackTutorialDone();
            TutorialManager.DodgeTutorial();
            TutorialManager.IsFirstDodge = false;
        }

        if (CurrentHP <= 1750f && TutorialManager.IsFirstGoldSpawn == true)
        {
            TutorialManager.GOLDTutorial();
            TutorialManager.ThrowTut();
            TutorialManager.IsFirstGoldSpawn = false;
        }

        // After all of this, increase the pressure that the AI is experiencing and send Elain into a hitstun state that locks her out of combat input.

        if (CurrentCombatState != COMBATSTATE.GATE && CurrentCombatState != COMBATSTATE.CUTSCENE)
        {
            //print("STUNNING");
            CurrentCombatState = COMBATSTATE.STUNNED;
            CombatDataCentre.AIPressure += 1f;
        }
    }
    #endregion
    /* Taken out of EGX build. Kept for further development.
    void NextArena()
    {
        switch (arenaProgressor.i)
        {
            case 0:
                combatEvade.IsEnabled = true;
                HP = 2000f;
                CurrentHP = HP;
                CurrentCombatState = COMBATSTATE.CUTSCENE;
                TutorialManager.AttackTutorialDone();
                CutsceneManager.PlayCutscene(1);
                pMotionController.GetMotion<CombatEvade>(true).IsEnabled = true;
                break;

            case 1:
                
                HP = 2000f;
                CurrentHP = HP;
                CutsceneManager.PlayCutscene(2);
                CurrentCombatState = COMBATSTATE.CUTSCENE;
                TutorialManager.DodgeTutorialDone();
                break;

            case 2:
                
                TutorialManager.CounterTutorialDone();
                CutsceneManager.PlayCutscene(3);
                CurrentCombatState = COMBATSTATE.CUTSCENE;
                isInterruptable = false;
                mCombatant.IsTargetLocked = false;
                HP = 2000f;
                CurrentHP = HP;
                break;

            default:
                Debug.Log("ERROR: No case to fall to. i = " + arenaProgressor.i);
                return;
        }
    }
    */

    // ----------------------------------------------

    // Functions

    // ----------------------------------------------
    // Here we basically just fetch stuff.
    public override void Start()
    {

        base.Start();


        //Find the player object.
        playerObject = FindObjectOfType<PlayerManager>().gameObject;
        //arenaProgressor = GetComponent<ArenaProgressor>();

        //Find all the player's stuff.
        pMotionController = playerObject.GetComponent<MotionController>();
        pActorController = pMotionController.ActorController;
        pCombatant = playerObject.GetComponent<Combatant>();
        pAttack = pMotionController.GetMotion<BasicMeleeAttack>();
        pEvade = pMotionController.GetMotion<CombatEvade>();

        //Identify Elain's combatant and cores.
        mCombatant = mMotionController.gameObject.GetComponent<Combatant>();
        mActorCore = mMotionController.gameObject.GetComponent<ActorCore>();
        mBasicAttributes = GetComponent<BasicAttributes>();
        CombatDecisionBrain = FindObjectOfType<AI_Brain_Attack>();
        AI_Inventory = GetComponent<BasicInventory>();

        //Identify the NavMeshInput location targets
        CircleLeftTransform = FindObjectOfType<CircleLeftTransIdent>().gameObject.transform;
        CircleRightTransform = FindObjectOfType<CircleRightTransIdent>().gameObject.transform;
        RetreatTransform = FindObjectOfType<RetreatTransIdent>().gameObject.transform;
        positionManager = FindObjectOfType<PositionManager>();

        //Get combat-salient motions.
        basicMeleeAttack = mMotionController.GetMotion<BasicMeleeAttack>();
        combatEvade = mMotionController.GetMotion<CombatEvade>();
        damaged = mMotionController.GetMotion<PSS_Damaged>();
        damaged.EnemyBMA = pAttack;

        //Get the animator for slo-mo purposes.
        mAnim = gameObject.GetComponent<Animator>();

        //Set health.
        StartingHP = HP;
        CurrentHP = HP;

        //Set target as first combat arena, and hold Elain until we tell her to start throwing punches.
        CurrentCombatState = COMBATSTATE.CUTSCENE;
        Target = playerObject.transform;

        //Get ready to apply more bloom than we probably should.
        PPM = FindObjectOfType<PostProcessingManager>();
    }

    // ----------------------------------------------

    /// <summary>
    /// Evaluates all transforms available for retreating, and returns the one farthest away to retreat to.
    /// </summary>
    /// <returns></returns>
    public Transform WhichRetreatTransform()
    {
        //Debug.Log("Going into retreat loop");
        Transform FarthestTarget = RetreatTransform;

        for (int i = 0; i < positionManager.RetreatTransforms.Length; i++)
        {
            if (Vector3.Distance(gameObject.transform.position, positionManager.RetreatTransforms[i].position) > Vector3.Distance(gameObject.transform.position, FarthestTarget.position))
            {
                FarthestTarget = positionManager.RetreatTransforms[i];
            }
        }
        return FarthestTarget;
    }

    // ----------------------------------------------

    /// <summary>
    /// If Elain wants to teleport close to the player, she can arrive in three different ways.
    /// </summary>
    /// <returns></returns>
    public MotionControllerMotion WhichTeleportArrival()
    {
        if (CombatDataCentre.DistanceBetweenCombatants <= StopDistance)
        {
            // If the player is attacking, dodge.
            if (pAttack.IsInMotionState)
            {
                return combatEvade;

            }
            else // Otherwise, attack.
            {
                //print("Attack confirmed");
                return basicMeleeAttack;
            }
        }
        else // If outside of StopDistance, just slide.
        {
            return null;
        }
    }

    // ----------------------------------------------

    /// <summary>
    /// Returns attack form taken from player's combat message.
    /// </summary>
    /// <returns></returns>
    public int PassCapturedCombatForm()
    {
        return playerObject.GetComponent<ActorCore>().CurrentAttackForm;
    }

    // ----------------------------------------------

    #region FSM

    // In this instance, Update is only used for identifying if we should be dodging, and for re-targeting the player if we're ever caught without a target.
    private void Update()
    {
        //Debug.Log("checking if we should dodge: " + AIDodgeActive + " " + SlowTimeManager.bIsAttacking);

        if (AIDodgeActive)
        { 
            if (combatEvade.IsEnabled && CurrentCombatState != COMBATSTATE.STUNNED)
            {
                CurrentCombatState = COMBATSTATE.DODGING;
            }
        }

        if (!Target)
        {
            Target = playerObject.transform;
            mCombatant.Target = playerObject.transform;
            mCombatant.IsTargetLocked = true;
        }
    }

    // ----------------------------------------------

    
    /// <summary>
    /// Simple heuristic for deciding what the AI FSM should be transitioning to next.
    /// </summary>
    /// <returns></returns>
    public COMBATSTATE DecideNextState()
    {
        // If we're deciding what to do, we've exited the ATTACKING state and should clear the comboIterator.
        comboIterator = 0;
        //print("DecideNextState entered");

        if (isInterruptable == false)
        {
            //print("uninterruptable state, returning samestate");
            return CurrentCombatState;
        }

        if (CombatDataCentre.AIPressure < 1.5f && CombatDataCentre.AIPressure >= -0.5f && CombatDataCentre.AIDifficulty <= 1.5f && CombatDataCentre.DistanceBetweenCombatants >= SlowDistance)
        {
            //print("CIRCLING state decided");
            return COMBATSTATE.CIRCLING;
        }

        if (CombatDataCentre.AIPressure > CombatDataCentre.AIDifficulty && CombatDataCentre.DistanceBetweenCombatants >= StopDistance)
        {
            //print("RETREATING state decided");
            return COMBATSTATE.RETREATING;
        }

        
        if (CombatDataCentre.DistanceBetweenCombatants >= 10f && CurrentCombatState != COMBATSTATE.THROW)
        {
            //print("DIALOGUE state decided");
            return COMBATSTATE.THROW;
        }

        if (CombatDataCentre.DistanceBetweenCombatants >= StopDistance && CombatDataCentre.AIAggression > CombatDataCentre.AIPressure)
        {
            //print("ADVANCE state decided");
            return COMBATSTATE.ADVANCING;
        }

        // Give the AI as much chance to not attack as possible to prevent stunlocking the player cheaply.
        if (CombatDataCentre.DistanceBetweenCombatants <= StopDistance)
        {
            //print("ATTACKING state decided");
            ComboStutterBool = false;
            return COMBATSTATE.ATTACKING;
        }

        // If we've gotten all the way through, just make her circle.

        // Debug.Log("Warning: Circling due to default.");
        return COMBATSTATE.CIRCLING;
    }

    // ----------------------------------------------

    /// <summary>
    /// State that starts the attacking combo chain and listens for reasons to stop the combo.
    /// </summary>
    /// <returns></returns>
    IEnumerator Attacking()
    {
        // If the player is too far away, immediately cancel the state.
        if (CombatDataCentre.DistanceBetweenCombatants >= StopDistance)
        {
            CurrentCombatState = COMBATSTATE.ADVANCING;
        }

        // Make sure we're orienting towards the player.
        Target = playerObject.transform;

        // Ask the CombatDecisionBrain which combo it thinks we should do.
        AI_Combo CurrentCombo = CombatDecisionBrain.TakeAction();
        basicMeleeAttack.CurrentCombo = CurrentCombo;

        // Cancel dialogue if we interrupt ourselves.
        hasDialogued = false;

        // Make doublesure we're not jumping into a combo at anything other than i = 0.
        comboIterator = 0;

        // Animate the first attack via the MotionController. 
        // The rest of the combo logic is dealt with through the basicMeleeAttack motion through Unity's animation events.
        mMotionController.ActivateMotion(basicMeleeAttack, CurrentCombo.ListOfAttacksInCombo[comboIterator].ReturnAttackStyleInt());

        // Here we listen for if the player is too far away, and if we've hit the end of the combo.
        while (CurrentCombatState == COMBATSTATE.ATTACKING)
        {
            yield return null;

            if (CombatDataCentre.DistanceBetweenCombatants >= StopDistance)
            {
                CurrentCombatState = COMBATSTATE.ADVANCING;
                yield break;
            }
            else if (comboIterator >= CurrentCombo.ListOfAttacksInCombo.Length - 1)
            { 
                // Debug.Log("Combo ended!");
                CurrentCombatState = COMBATSTATE.RETREATING;
                yield break;
            }
        }
    }

    // ----------------------------------------------

    /// <summary>
    /// State for maintaining distance. Normally used for gameplay downtime and dialogue.
    /// </summary>
    /// <returns></returns>
    IEnumerator Circling()
    {
        // Timer for making sure we don't circle for too long. 
        float CircleTimer = 0f;

        // Set circle target, chill circling for time relative to difficulty, and listen for reasons we should stop circling.
        while (CurrentCombatState == COMBATSTATE.CIRCLING)
        {
            yield return null;

            Target = CircleRightTransform;

           CircleTimer += CombatDataCentre.AIDifficulty * Time.deltaTime;
           if (CircleTimer >= 5f)
           {
               CurrentCombatState = DecideNextState();
               yield break;
           }
           if (CombatDataCentre.DistanceBetweenCombatants <= StopDistance)
           {    
                //Debug.Log("Distance less than stop distance, starting attack");
               CurrentCombatState = COMBATSTATE.ATTACKING;
               yield break;
           }
          
        }
    }

    // ----------------------------------------------

    /// <summary>
    /// <DEPRECATED> State that just waits for instruction. NOT a true idle!
    /// </summary>
    /// <returns></returns>
    IEnumerator Idle()
    {
        // Debug.Log("Warning: idle state reached. Something's probably gone wrong.");
        while (true) { yield return null; }
    }

    // ----------------------------------------------

    /// <summary>
    /// State for getting closer to the NavMeshAgent's target. Contains logic for deciding to throw/teleport to target instead of walking.
    /// </summary>
    /// <returns></returns>
    IEnumerator Advancing()
    {
        // How long we've been advancing for.
        float wait = 0;

        // Make sure we're targeting the player and the NavMeshInput knows that we've changed targets this frame.
        Target = playerObject.transform;
        mHasArrived = false;
        
        // Iterates on wait time and listens for reasons to stop advancing.
        while (CurrentCombatState == COMBATSTATE.ADVANCING)
        {
            yield return null;
            if (!mHasArrived)
            {
                wait += Time.deltaTime;
                Target = playerObject.transform;
                if (CombatDataCentre.DistanceBetweenCombatants >= 10f && GameManager.CurrentGameState == GameManager.GAMESTATE.INTERIOR)
                {
                    //Debug.Log("I ain't walking that far! State change to throw");
                    CurrentCombatState = COMBATSTATE.THROW;
                    yield break;
                }
            }
            if (wait > 2f)
            {
                CurrentCombatState = DecideNextState();
                yield break;
            }

            if (CombatDataCentre.DistanceBetweenCombatants <= StopDistance + 0.01)
            {
                //print("Should be attacking");
                CurrentCombatState = COMBATSTATE.ATTACKING;
                yield break;
            }

        }
    }

    // ----------------------------------------------

    /// <summary>
    /// State for moving toward a retreat target. Keeps Elain looking at the player and identifies 
    /// </summary>
    /// <returns></returns>
    IEnumerator Retreating()
    {
        // Inform the NavMeshInput that we're changing targets.
        mHasArrived = false;

        // Select which target to use.
        Target = WhichRetreatTransform();

        // Listen for reasons to stop retreating until timer hits a maximum.

        float Timer = 0f;

        while (CurrentCombatState == COMBATSTATE.RETREATING)
        {
            yield return null;

            // If we have enough space, go ahead and show off that neat throw you made.
            if (CombatDataCentre.DistanceBetweenCombatants >= StopDistance && GameManager.CurrentGameState == GameManager.GAMESTATE.INTERIOR)
            {
                CurrentCombatState = COMBATSTATE.THROW;
                yield break;
            }

            // Find out if we've been retreating for too long.
            Timer += Time.deltaTime;
            if (Timer >= 3f)
            {
                CurrentCombatState = DecideNextState();
            }
        }
    }

    // ----------------------------------------------

    /// <summary>
    /// <DEPRECATED> State for taunting on successful counter, or just because.
    /// </summary>
    /// <returns></returns>
    IEnumerator Taunting()
    {

        print("taunting");
        yield return new WaitForSeconds(2);
        CurrentCombatState = DecideNextState();

        yield return null;
    }


    /// <summary>
    /// State for hitstunning Elain so that she is locked out of other combat decision logic.
    /// </summary>
    /// <returns></returns>
    IEnumerator Stunned()
    {
        yield return null;

        // If we've been struck, reset where we are in our combo.
        comboIterator = 0;

        // Lock the state.
        isInterruptable = false;

        // Reset any combat-sensitive postprocessing.
        PPM.ResetVignIntensity();

        // Logic for transitioning out of stunned state is contained in PSS_Damaged.cs
        while (CurrentCombatState == COMBATSTATE.STUNNED)
        {
            yield return null;
        }

    }

    /// <summary>
    /// <DEPRECATED> State to move from arena to arena.
    /// </summary>
    /// <returns></returns>
    IEnumerator Gate()
    {
        yield return null;
        /*
        combatEvade.Priority = 18f;
        Target = arenaProgressor.ProgressionArray[arenaProgressor.i].WaitPoint;
        //Advance to next checkpoint
        mHasArrived = false;
        Debug.Log("Looping traversal");
        isInterruptable = false;
        while (mHasArrived == false)
        {
            Target = arenaProgressor.ProgressionArray[arenaProgressor.i].WaitPoint;
            GetComponent<Combatant>().IsTargetLocked = false;
            yield return null;
        }
        GetComponent<Combatant>().IsTargetLocked = true;
        print("Arrived at arena centre, waiting!");
        while (CombatDataCentre.DistanceBetweenCombatants > 5f)
        {
            yield return null;
        }

        isInterruptable = true;

        CurrentCombatState = DecideNextState();

        arenaProgressor.i++;
        */
        CurrentCombatState = DecideNextState();
        
    }


    /// <summary>
    /// State for throwing spear and 
    /// </summary>
    /// <returns></returns>
    IEnumerator ProjectileThrow()
    {
        //First, can make sure that we can actually throw.
        if (mMotionController.GetMotion<TSS_ThrowWeapon>().IsEnabled)
        {
            // Go get the weapon we're throwing and the throw motion.
            WeaponInventory mWeaponInventory = GetComponent<WeaponInventory>();
            TSS_ThrowWeapon ThrowMotion = mMotionController.GetMotion<TSS_ThrowWeapon>();

            // If we're circling, re-target to the player.
            if (Target == CircleLeftTransform || Target == CircleRightTransform)
            {
                Target = playerObject.transform;
            }

            // Force rotation to the target via the combatant.
            mCombatant.Target = Target;
            ThrowTargetTrans = Target;

            // Activate throw.
            mMotionController.ActivateMotion(ThrowMotion);
            while (CurrentCombatState == COMBATSTATE.THROW)
            {
                //Exiting this state is handled by TSS_ThrowWeapon.Deactivate().
                yield return null;
            }

        }
        else // If we can't throw, go do something else.
        {
            CurrentCombatState = DecideNextState();
        }

    }

    /// <summary>
    /// State for locking Elain into a state where her animations can safely be controlled by Timeline.
    /// </summary>
    /// <returns></returns>
    IEnumerator Cutscene()
    {
        while (GameManager.CurrentGameState == GameManager.GAMESTATE.CUTSCENE)
        {
            yield return null;
        }

    }

    /// <summary>
    /// State dodging and countering. While in this state, Elain decides if she wants to counter and if she wants to chain-dodge.
    /// </summary>
    /// <returns></returns>
    IEnumerator Dodging()
    {
        // If we're dodging, we might have interrupted a combo. Reset combo iterator.
        comboIterator = 0;
        
        // Very important bool I think?
        bool Dodgetrigger = false;

        // Let Update know that we've started dodging.
        AIDodgeActive = false;


        // Here, we wait for a bit and then 
        while (CurrentCombatState == COMBATSTATE.DODGING)
        {
            yield return null;

            // I cannot remember why this gate needs to exist, but if I take it out we end up in an infinite dodging loop.
            while (Dodgetrigger == false)
            {
                Dodgetrigger = true;

                // Activate the dodging motion.
                mMotionController.ActivateMotion(combatEvade, PassCapturedCombatForm());
                yield return null;
            }

            // Pause for a frame or fifteen, start a check to find if we're still in the dodge motion. 

            yield return new WaitForSeconds(0.5f);

            if (mMotionController.ActiveMotion != combatEvade)
            {
                // If we're not in the combat evade state, decide what to do next.
                CurrentCombatState = DecideNextState();
            }
        }
    }

#endregion
}