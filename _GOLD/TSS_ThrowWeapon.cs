﻿//Author: Thomas Reiffer
//Last Edit: Thomas Reiffer 18/09/18
//Summary: Determinates activation of, and then guides, 3rd-person character controller's navigation through the ThrowWeapon mecanim. Works for either player or Elain (AI opponent).
//         Collision and teleportation logic is contained within CollisionMessager.cs.

using UnityEngine;
using com.ootii.Actors.AnimationControllers;
using com.ootii.Actors.Combat;
using com.ootii.Actors.LifeCores;
using com.ootii.Cameras;
using com.ootii.Geometry;
using com.ootii.Helpers;
using com.ootii.Messages;
using com.ootii.Timing;
using NFTS.TimeManager;
using NFTS.CM;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace com.ootii.MotionControllerPacks
{
   
    /// <summary>
    /// Motion for aiming and then throwing associated weapon to targeted location.
    /// </summary>
    [MotionName("TSS_ThrowWeapon")]
    [MotionDescription("Aiming and then throwing weapon to target.")]
    public class TSS_ThrowWeapon : PSS_MotionBase
    {
        #region Variables
        /// <summary>
        /// Trigger values for the motion within mecanim.
        /// </summary>
        public int PHASE_SPEAR = 3300;
        public int PHASE_THROW = 3301;
        public int PHASE_GLIDE = 3302;
        public int PHASE_END = 3305;
        public int PHASE_AXE = 3306;

        /// <summary>
        /// Strings for identifying animation events
        /// </summary>
        public static string EVENT_THROW = "throw";

        /// <summary>
        /// Determines if we're using the IsInMotion() function to verify that
        /// the transition in the animator has occurred for this motion.
        /// </summary>
        public override bool VerifyTransition
        {
            get { return false; }
        }

        /// <summary>
        /// Identifies if we're chaining out of the throw into a different motion.
        /// </summary>
        bool mIsChainActive = false;

        /// <summary>
        /// Determines if we rotate by ourselves
        /// </summary>
        public bool _RotateWithInput = false;
        public bool RotateWithInput
        {
            get { return _RotateWithInput; }
            set { _RotateWithInput = value; }
        }

        /// <summary>
        /// Determines if we rotate to match the camera
        /// </summary>
        public bool _RotateWithCamera = false;
        public bool RotateWithCamera
        {
            get { return _RotateWithCamera; }
            set { _RotateWithCamera = value; }
        }


        /// <summary>
        /// Direction of the attack relative to the character's forward
        /// </summary>
        public Vector3 _Forward = new Vector3(0.1f, 0.02f, 0.9232365f);
        public Vector3 Forward
        {
            get { return _Forward; }
            set { _Forward = value; }
        }

        /// <summary>
        /// Horizontal field-of-attack centered on the Forward. This determines
        /// the horizontal range of the attack.
        /// </summary>
        public float _HorizontalFOA = 55f;
        public float HorizontalFOA
        {
            get { return _HorizontalFOA; }
            set { _HorizontalFOA = value; }
        }

        /// <summary>
        /// Vertical field-of-attack centered on the Forward. This determines
        /// the vertical range of the attack.
        /// </summary>
        public float _VerticalFOA = 55f;
        public float VerticalFOA
        {
            get { return _VerticalFOA; }
            set { _VerticalFOA = value; }
        }
        
        /// <summary>
        /// Stops blocking when the alias is released
        /// </summary>
        public bool _DeactivateOnAliasRelease = true;
        public bool DeactivateOnAliasRelease
        {
            get { return _DeactivateOnAliasRelease; }
            set { _DeactivateOnAliasRelease = value; }
        }


        /// <summary>
        /// Fields to help smooth out the mouse rotation
        /// </summary>
        protected float mYaw = 0f;
        protected float mYawTarget = 0f;
        protected float mYawVelocity = 0f;

        /// <summary>
        /// Determines if the actor rotation should be linked to the camera
        /// </summary>
        protected bool mLinkRotation = false;

        /// <summary>
        /// Weapon to throw's movement and collision logic.
        /// </summary>
        CollisionMessager WeaponThrowLogic;

        /// <summary>
        /// Weapon object childed to hand transform.
        /// </summary>
        GameObject WeaponHold;

        /// <summary>
        /// Projectile object that contains collision and teleportation logic.
        /// </summary>
        GameObject WeaponThrow;

        /// <summary>
        /// Character glow to be activated upon target.
        /// </summary>
        GameObject ScanlinesObject;

        /// <summary>
        /// Transform of Mixamo rig's hand so that we can return our javelin on activation;
        /// </summary>
        Transform HandParent;

        /// <summary>
        /// The game object that we'll be using to aim our throw.
        /// </summary>
        ThrowAimTargeting ThrowAimTarget = null;

        /// <summary>
        /// Default constructor
        /// </summary>
        public TSS_ThrowWeapon()
            : base()
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE_BLOCK;

            _Priority = 15;
            _ActionAlias = "Combat Block";

            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "BasicMeleeBlock-SM"; }
        }

        /// <summary>
        /// Controller constructor
        /// </summary>
        /// <param name="rController">Controller the motion belongs to</param>
        public TSS_ThrowWeapon(MotionController rController)
            : base(rController)
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE_BLOCK;

            _Priority = 15;
            _ActionAlias = "ThrowWeapon";

            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "ThrowWeapon-SM"; }
        }

        /// <summary>
        /// Awake is called after all objects are initialized so you can safely speak to other objects. This is where
        /// reference can be associated.
        /// </summary>
        public override void Awake()
        {
            base.Awake();
            //Fetch dependencies. Improve with dependency injection?
            ThrowAimTarget = mMotionController.GetComponentInChildren<ThrowAimTargeting>();
            HandParent = mMotionController.gameObject.GetComponentInChildren<WeaponParentIdent>().transform;
            WeaponHold = mMotionController.gameObject.GetComponent<WeaponInventory>().WeaponToHold;
            WeaponThrow = mMotionController.gameObject.GetComponent<WeaponInventory>().WeaponToThrow;
            WeaponThrowLogic = WeaponThrow.GetComponent<CollisionMessager>();
            WeaponThrowLogic.ThrowWeaponMotion = this;
            
            ScanlinesObject = mMotionController.GetComponentInChildren<ScanlinesIdent>().gameObject;
        }


        #endregion

        //-----------------------------------------------------------------------

        /// <summary>
        /// Tests if this motion should be started. However, the motion
        /// isn't actually started. Called every frame by the MotionController.
        /// </summary>
        /// <returns></returns>
        public override bool TestActivate()
        {
            //If the motion has been disabled for any reason, return.
            if (!mIsStartable)
            {
                return false;
            }

            //If the ActorController is not on the ground, return.
            if (!mActorController.IsGrounded)
            {
                return false;
            }

            // Check if the ActionAlias input has been called via the InputSource associated with the MotionController.
            if (_ActionAlias.Length > 0 && mMotionController.InputSource != null)
            {
       
                if (mMotionController.InputSource.IsPressed(_ActionAlias))
                {
                    //Only now do we call activate the motion the motion.
                    return true;
                }
       
            }

            //Return false every frame by default.
            return false;
        }

        //-----------------------------------------------------------------------

        /// <summary>
        /// Tests if the motion should continue. If it shouldn't, the motion
        /// is typically disabled. Called every frame the motion is active.
        /// </summary>
        /// <returns></returns>
        public override bool TestUpdate()
        {
            // If we've reached the exit state, leave.
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.IsTag("Exit"))
            {
                return false;
            }

            return true;
        }

        //-----------------------------------------------------------------------

        /// <summary>
        /// Called to start the specific motion when TestActivate() returns TRUE. If the motion
        /// were something like 'jump', this would start the jumping process
        /// </summary>
        /// <param name="rPrevMotion">Motion that this motion is taking over from</param>
        public override bool Activate(MotionControllerMotion rPrevMotion)
        {

            // Identifies which form to use while activating the motion for combat messaging. This will always return 0 as this motion does not use
            // combat messaging, but inherits from a class that does.
            int lForm = (_Form > 0 ? _Form : mMotionController.CurrentForm);

            // Manage weapon appearance - while aiming is active, the held weapon is active and scanlines are on.
            WeaponHold.SetActive(true);
            WeaponThrow.SetActive(false);
            ScanlinesObject.SetActive(true);

            // "Summon" the projectile weapon to hand to reset for next throw.
            WeaponThrow.transform.parent = HandParent;
            WeaponThrow.transform.position = WeaponHold.transform.position;
            WeaponThrow.transform.rotation = WeaponHold.transform.rotation;

            // Call function to enable targeting and throwing for the player.
            if (mMotionController.gameObject.tag == "Player")
            {
                ThrowAimTarget.ThrowingEnabled();
                // Set animation to axe aiming.
                mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_AXE, lForm, 0, true);
            }
            else // or, if we're Elain, make her look at her target after getting it through her _StateMachine component, which also decides the target.
            {
                Elain_StateMachine AI_Source = mMotionController.gameObject.GetComponent<Elain_StateMachine>();

                ThrowAimTarget.TargetingUI.transform.position = AI_Source.ThrowTargetTrans.position;

                // Now set animation to spear aiming.
                mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_SPEAR, lForm, 0, false);
            }

            // Now, activate the motion
            return base.Activate(rPrevMotion);
        }

        //-----------------------------------------------------------------------

        /// <summary>
        /// Raised when we shut the motion down
        /// </summary>
        public override void Deactivate()
        {
            //Deactivate targeting objects.
            WeaponHold.SetActive(false);
            ScanlinesObject.SetActive(false);

            // Allow combat interruption again, and return control over rotation back to combatant.
            mIsInterruptible = true;
            mMotionController.gameObject.GetComponent<Combatant>().ForceActorRotation = true;

            //If we're Elain, reset Elain's target and allow the Combatant to control rotation again.
            if (mMotionController.GetComponent<Elain_StateMachine>())
            {
                mMotionController.GetComponent<Elain_StateMachine>().mCombatant.IsTargetLocked = true;
                mMotionController.GetComponent<Elain_StateMachine>().Target = mMotionController.GetComponent<Elain_StateMachine>().playerObject.transform;
                mMotionController.GetComponent<Elain_StateMachine>().mCombatant.Target = mMotionController.GetComponent<Elain_StateMachine>().playerObject.transform;
            }
            
            //Debug.Log("Deactivating");

            // If we're the player: return time speed to normal, disable the throwing target ask the CinemachineManager to take us back to Lockon. 
            if (mMotionController.gameObject.tag == "Player")
            {
                SlowTimeManager.SetGlobalSpeedNormal();
                ThrowAimTarget.ThrowingDisabled();
                CinemachineManager.SwitchCameraTo(CinemachineManager.VCO_LockOn);
            }
            else // If we aren't, just ask the AI what it would like to do next.
            {
                Elain_StateMachine AI = mMotionController.gameObject.GetComponent<Elain_StateMachine>();
                AI.CurrentCombatState = AI.DecideNextState();
            }
 
            base.Deactivate();
        }

        //-----------------------------------------------------------------------

        /// <summary>
        /// Allows the motion to modify the velocity before it is applied.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        /// <param name="rMovement">Amount of movement caused by root motion this frame</param>
        /// <param name="rRotation">Amount of rotation caused by root motion this frame</param>
        /// <returns></returns>
        public override void UpdateRootMotion(float rDeltaTime, int rUpdateIndex, ref Vector3 rMovement, ref Quaternion rRotation)
        {
            
        }

        //-----------------------------------------------------------------------

        /// <summary>
        /// Updates the motion over time. This is called by the controller
        /// every update cycle so animations and stages can be updated.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        public override void Update(float rDeltaTime, int rUpdateIndex)
        {
            mMovement = Vector3.zero;
            mRotation = Quaternion.identity;

            //If we're the player, we listen for inputs to determine how the motion continues.
            if (mMotionController.gameObject.tag == "Player")
            {
                if (_ActionAlias.Length > 0 && mMotionController.InputSource != null)
                {
                    //If the player is in-flight, then they can exit the teleport state in three ways: Default slide, chaining a heavy attack, or chaining an evasion move.
                    if (mIsChainActive)
                    {
                        // If the dodge button is pressed, queue the CombatEvade motion for activation after the teleportation ends.
                        if (mMotionController.InputSource.IsJustPressed("Cancel"))
                        {
                            WeaponThrowLogic.QueuedMotion = mMotionController.GetMotion<CombatEvade>();
                        }

                        // If the heavy attack button is pressed, queue the BasicMeleeAttack motion for activation after the teleportation ends.
                        if (mMotionController.InputSource.IsJustPressed("BreakoutAlias"))
                        {
                            WeaponThrowLogic.QueuedMotion = mMotionController.GetMotion<BasicMeleeAttack>();
                        }
                    }

                    //If mIsChainActive is FALSE, then we are not in-flight and are targeting instead. Here, we listen to begin the throw.

                    if (mMotionController.InputSource.IsJustPressed("Submit"))
                    {
                        mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_THROW, true);

                        //If we are the player, then we reset time speed and prevent further inputs from interrupting the teleportation.
                        if (mMotionController.gameObject.tag == "Player")
                        {
                            mIsInterruptible = false;
                            SlowTimeManager.SetGlobalSpeedNormal();
                        }
                    }

                    // If the player releases the aiming button, de-activate aiming.
                    if (_DeactivateOnAliasRelease)
                    {
                        if (!mMotionController.InputSource.IsPressed(_ActionAlias))
                        {
                            //Send animation phase to its end state.
                            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_END, true);

                            if (mMotionController.gameObject.tag == "Player")
                            {
                                //If we are the player, reset time speed.
                                SlowTimeManager.SetGlobalSpeedNormal();
                            }
                        }
                    }
                }
                
            }

            //If we are Elain, we just throw the spear to the target.
            if (mMotionController.gameObject.GetComponent<Elain_StateMachine>() != null)
            {
                //But only after a deliberate delay to prevent glitching.
                if (mAge >= 1f && mAge <= 1.2f)
                {
                    mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_THROW, false);
                }
            }

            base.Update(rDeltaTime, rUpdateIndex);
        }


        //-----------------------------------------------------------------------

        /// <summary>
        /// Create a rotation velocity that rotates the character based on input
        /// </summary>
        /// <param name="rDeltaTime"></param>
        /// <param name="rAngularVelocity"></param>
        private void RotateUsingInput(float rDeltaTime, ref Quaternion rRotation)
        {
            // If we don't have an input source, stop
            if (mMotionController.InputSource == null) { return; }

            // Determine this frame's rotation
            float lYawDelta = 0f;
            float lYawSmoothing = 0.1f;


            mYawTarget = mYawTarget + lYawDelta;

            // Smooth the rotation
            lYawDelta = (lYawSmoothing <= 0f ? mYawTarget : Mathf.SmoothDampAngle(mYaw, mYawTarget, ref mYawVelocity, lYawSmoothing)) - mYaw;
            mYaw = mYaw + lYawDelta;

            // Use this frame's smoothed rotation
            if (lYawDelta != 0f)
            {
                rRotation = Quaternion.Euler(0f, lYawDelta, 0f);
            }
        }

        //-----------------------------------------------------------------------

        /// <summary>
        /// Here, we listen for animation events within the FBX files for frame-sensitive logic.
        /// </summary>
        /// <param name="rEvent"></param>
        public override void OnAnimationEvent(AnimationEvent rEvent)
        {
            // If the event is empty, it's probably been created in error.
            if (rEvent == null) { return; }

            // If the animation has fired the throw event...
            if (StringHelper.CleanString(rEvent.stringParameter) == TSS_ThrowWeapon.EVENT_THROW)
            {
                // ... Prevent the teleportation from being interrupted or deactivated...
                _DeactivateOnAliasRelease = false;
                IsInterruptible = false;

                // ... prevent the combatant from forcing rotation mid-teleport...
                mMotionController.gameObject.GetComponent<Combatant>().ForceActorRotation = false;

                // ... set the animation state to teleporting...
                mMotionController.SetAnimatorMotionPhase(0, PHASE_GLIDE);

                // ...activate the projectile weapon, unparent it, and deactivate the held weapon.
                WeaponHold.SetActive(false);
                WeaponThrow.SetActive(true);
                WeaponThrow.transform.SetParent(null, true);


                // If we're Elain...
                if (mMotionController.gameObject.GetComponent<Elain_StateMachine>() != null)
                {
                    // ... decide where we're going to be throwing the spear...
                    ThrowAimTarget.TargetingUI.transform.position = mMotionController.GetComponent<Elain_StateMachine>().WhichRetreatTransform().position;
                    // ... set the NavMeshAgent's target to this location also...
                    mMotionController.gameObject.GetComponent<Elain_StateMachine>().Target = mMotionController.GetComponent<Elain_StateMachine>().WhichRetreatTransform();
                    // and prevent the Combatant from ruining it all with target locking.
                    mMotionController.GetComponent<Elain_StateMachine>().mCombatant.IsTargetLocked = false;
                }

                // Finally, call the function that starts the projectile's movement and collision detection...
                WeaponThrowLogic.StartCoroutine(WeaponThrowLogic.ProjectileInFlight(ThrowAimTarget.TargetingUI.transform.position));

                // ... and force the weapon to orient itself towards the target.
                WeaponThrow.transform.LookAt(ThrowAimTarget.TargetingUI.gameObject.transform);
                
                // Inform the STM that we're throwing.
                SlowTimeManager.IsThrowing = true;

            }
        

            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_BEGIN_DODGE)
            {
                //TSEDIT 21/01/18
                //Debug.Log("DODGE START");
                SlowTimeManager.bIsAttacking = true;
            }

            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_END_DODGE)
            {
                //TSEDIT 21/01/18
                //Debug.Log("DODGE END");
                SlowTimeManager.bIsAttacking = false;
            }

            // Determine if we are beginning a chain
            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_BEGIN_CHAIN)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " begin chain");
                //Debug.Log("Chain detected");
                mIsChainActive = true;
                //Debug.Log("CHAIN START");
            }
            // Determine if we are ending
            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_END_CHAIN)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " end chain");
                mIsChainActive = false;
                //Debug.Log("DODGE END");
            }
        }

        //-----------------------------------------------------------------------

        #region Editor Functions

        // **************************************************************************************************
        // Following properties and function only valid while editing
        // **************************************************************************************************

#if UNITY_EDITOR

        /// <summary>
        /// Allow the constraint to render it's own GUI
        /// </summary>
        /// <returns>Reports if the object's value was changed</returns>
        public override bool OnInspectorGUI()
        {
            bool lIsDirty = false;

            if (EditorHelper.IntField("Form", "Form defines the animation type to use for this motion.", Form, mMotionController))
            {
                lIsDirty = true;
                Form = EditorHelper.FieldIntValue;
            }

            if (EditorHelper.TextField("Focus Alias", "Action alias for activating weapon.", ActionAlias, mMotionController))
            {
                lIsDirty = true;
                ActionAlias = EditorHelper.FieldStringValue;
            }

            if (EditorHelper.BoolField("Stop On Release", "Determines if the block is stopped when the alias is released.", DeactivateOnAliasRelease, mMotionController))
            {
                lIsDirty = true;
                DeactivateOnAliasRelease = EditorHelper.FieldBoolValue;
            }

            GUILayout.Space(5f);

            if (EditorHelper.BoolField("Rotate With Input", "Determines if we rotate based on user input.", RotateWithInput, mMotionController))
            {
                lIsDirty = true;
                RotateWithInput = EditorHelper.FieldBoolValue;
            }

            if (EditorHelper.BoolField("Rotate With Camera", "Determines if we rotate to match the camera.", RotateWithCamera, mMotionController))
            {
                lIsDirty = true;
                RotateWithCamera = EditorHelper.FieldBoolValue;
            }

            GUILayout.Space(5f);

            if (EditorHelper.Vector3Field("Defense Forward", "Normalized center of the attack. FOA values are based on this.", Forward, mMotionController))
            {
                lIsDirty = true;
                Forward = EditorHelper.FieldVector3Value;
            }

            EditorGUILayout.BeginHorizontal();

            EditorHelper.LabelField("Field of Defense", "Horizontal and vertical field of attack when colliders are not used.", EditorGUIUtility.labelWidth - 4f);

            if (EditorHelper.FloatField(HorizontalFOA, "Horizontal FOA", mMotionController, 0f, 31f))
            {
                lIsDirty = true;
                HorizontalFOA = EditorHelper.FieldFloatValue;
            }

            if (EditorHelper.FloatField(VerticalFOA, "Vertical FOA", mMotionController, 0f, 31f))
            {
                lIsDirty = true;
                VerticalFOA = EditorHelper.FieldFloatValue;
            }

            EditorGUILayout.EndHorizontal();

            if (lIsDirty)
            {
                SceneView.RepaintAll();
                InternalEditorUtility.RepaintAllViews();
            }

            return lIsDirty;
        }

        /// <summary>
        /// Enables the Editor to handle an event in the scene view.
        /// </summary>
        public override void OnSceneGUI()
        {
            if (Application.isPlaying) { return; }

            Combatant lCombatant = mMotionController.gameObject.GetComponent<Combatant>();
            if (lCombatant != null)
            {
                float lMinReach = lCombatant.MinMeleeReach + 0f;
                float lMaxReach = lCombatant.MaxMeleeReach + 1f;
                Graphics.GraphicsManager.DrawSolidFrustum(lCombatant.CombatOrigin, lCombatant.transform.rotation * Quaternion.LookRotation(Forward, lCombatant.transform.up), HorizontalFOA, VerticalFOA, lMinReach, lMaxReach, Color.gray);
            }
        }

        #region Auto-Generated
        // ************************************ START AUTO GENERATED ************************************

        /// <summary>
        /// These declarations go inside the class so you can test for which state
        /// and transitions are active. Testing hash values is much faster than strings.
        /// </summary>
        public int STATE_Start = -1;
        public int STATE_PSSIdlePose = -1;
        public int STATE_AimPose = -1;
        public int STATE_Throw = -1;
        public int STATE_Windup = -1;
        public int TRANS_AnyState_AimPose = -1;
        public int TRANS_EntryState_AimPose = -1;
        public int TRANS_AimPose_Throw = -1;
        public int TRANS_AimPose_PSSIdlePose = -1;
        public int TRANS_Throw_Windup = -1;
        public int TRANS_Windup_PSSIdlePose = -1;

        /// <summary>
        /// Determines if we're using auto-generated code
        /// </summary>
        public override bool HasAutoGeneratedCode
        {
            get { return true; }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsInMotionState
        {
            get
            {
                int lStateID = mMotionLayer._AnimatorStateID;
                int lTransitionID = mMotionLayer._AnimatorTransitionID;

                if (lTransitionID == 0)
                {
                    if (lStateID == STATE_Start) { return true; }
                    if (lStateID == STATE_PSSIdlePose) { return true; }
                    if (lStateID == STATE_AimPose) { return true; }
                    if (lStateID == STATE_Throw) { return true; }
                    if (lStateID == STATE_Windup) { return true; }
                }

                if (lTransitionID == TRANS_AnyState_AimPose) { return true; }
                if (lTransitionID == TRANS_EntryState_AimPose) { return true; }
                if (lTransitionID == TRANS_AimPose_Throw) { return true; }
                if (lTransitionID == TRANS_AimPose_PSSIdlePose) { return true; }
                if (lTransitionID == TRANS_Throw_Windup) { return true; }
                if (lTransitionID == TRANS_Windup_PSSIdlePose) { return true; }
                return false;
            }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID)
        {
            if (rStateID == STATE_Start) { return true; }
            if (rStateID == STATE_PSSIdlePose) { return true; }
            if (rStateID == STATE_AimPose) { return true; }
            if (rStateID == STATE_Throw) { return true; }
            if (rStateID == STATE_Windup) { return true; }
            return false;
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID, int rTransitionID)
        {
            if (rTransitionID == 0)
            {
                if (rStateID == STATE_Start) { return true; }
                if (rStateID == STATE_PSSIdlePose) { return true; }
                if (rStateID == STATE_AimPose) { return true; }
                if (rStateID == STATE_Throw) { return true; }
                if (rStateID == STATE_Windup) { return true; }
            }

            if (rTransitionID == TRANS_AnyState_AimPose) { return true; }
            if (rTransitionID == TRANS_EntryState_AimPose) { return true; }
            if (rTransitionID == TRANS_AimPose_Throw) { return true; }
            if (rTransitionID == TRANS_AimPose_PSSIdlePose) { return true; }
            if (rTransitionID == TRANS_Throw_Windup) { return true; }
            if (rTransitionID == TRANS_Windup_PSSIdlePose) { return true; }
            return false;
        }

        /// <summary>
        /// Preprocess any animator data so the motion can use it later
        /// </summary>
        public override void LoadAnimatorData()
        {
            string lLayer = mMotionController.Animator.GetLayerName(mMotionLayer._AnimatorLayerIndex);
            TRANS_AnyState_AimPose = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".ThrowWeapon-SM.AimPose");
            TRANS_EntryState_AimPose = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".ThrowWeapon-SM.AimPose");
            STATE_Start = mMotionController.AddAnimatorName("" + lLayer + ".Start");
            STATE_PSSIdlePose = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.PSS Idle Pose");
            STATE_AimPose = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.AimPose");
            TRANS_AimPose_Throw = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.AimPose -> " + lLayer + ".ThrowWeapon-SM.Throw");
            TRANS_AimPose_PSSIdlePose = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.AimPose -> " + lLayer + ".ThrowWeapon-SM.PSS Idle Pose");
            STATE_Throw = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.Throw");
            TRANS_Throw_Windup = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.Throw -> " + lLayer + ".ThrowWeapon-SM.Windup");
            STATE_Windup = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.Windup");
            TRANS_Windup_PSSIdlePose = mMotionController.AddAnimatorName("" + lLayer + ".ThrowWeapon-SM.Windup -> " + lLayer + ".ThrowWeapon-SM.PSS Idle Pose");
        }


        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
        protected override void CreateStateMachine()
        {
            int rLayerIndex = mMotionLayer._AnimatorLayerIndex;
            MotionController rMotionController = mMotionController;

            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_35688 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "ThrowWeapon-SM");
            if (lSSM_35688 == null) { lSSM_35688 = lLayerStateMachine.AddStateMachine("ThrowWeapon-SM", new Vector3(624, -1008, 0)); }

            UnityEditor.Animations.AnimatorState lState_37278 = MotionControllerMotion.EditorFindState(lSSM_35688, "PSS Idle Pose");
            if (lState_37278 == null) { lState_37278 = lSSM_35688.AddState("PSS Idle Pose", new Vector3(408, 348, 0)); }
            lState_37278.speed = 1f;
            lState_37278.mirror = false;
            lState_37278.tag = "Exit";
            lState_37278.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Idle_2");

            UnityEditor.Animations.AnimatorState lState_36250 = MotionControllerMotion.EditorFindState(lSSM_35688, "AimPose");
            if (lState_36250 == null) { lState_36250 = lSSM_35688.AddState("AimPose", new Vector3(276, 192, 0)); }
            lState_36250.speed = 1f;
            lState_36250.mirror = false;
            lState_36250.tag = "";
            lState_36250.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Female Action Pose (1).fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_37280 = MotionControllerMotion.EditorFindState(lSSM_35688, "Throw");
            if (lState_37280 == null) { lState_37280 = lSSM_35688.AddState("Throw", new Vector3(516, 192, 0)); }
            lState_37280.speed = 2f;
            lState_37280.mirror = false;
            lState_37280.tag = "";
            lState_37280.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Specials.fbx", "KB_KnifeThrow");

            UnityEditor.Animations.AnimatorState lState_37282 = MotionControllerMotion.EditorFindState(lSSM_35688, "Windup");
            if (lState_37282 == null) { lState_37282 = lSSM_35688.AddState("Windup", new Vector3(744, 204, 0)); }
            lState_37282.speed = 1f;
            lState_37282.mirror = false;
            lState_37282.tag = "";
            lState_37282.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Windup.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_35984 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_36250, 0);
            if (lAnyTransition_35984 == null) { lAnyTransition_35984 = lLayerStateMachine.AddAnyStateTransition(lState_36250); }
            lAnyTransition_35984.isExit = false;
            lAnyTransition_35984.hasExitTime = false;
            lAnyTransition_35984.hasFixedDuration = true;
            lAnyTransition_35984.exitTime = 0.75f;
            lAnyTransition_35984.duration = 0.25f;
            lAnyTransition_35984.offset = 0f;
            lAnyTransition_35984.mute = false;
            lAnyTransition_35984.solo = false;
            lAnyTransition_35984.canTransitionToSelf = false;
            lAnyTransition_35984.orderedInterruption = true;
            lAnyTransition_35984.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_35984.conditions.Length - 1; i >= 0; i--) { lAnyTransition_35984.RemoveCondition(lAnyTransition_35984.conditions[i]); }
            lAnyTransition_35984.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3300f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_35984.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lTransition_37290 = MotionControllerMotion.EditorFindTransition(lState_36250, lState_37280, 0);
            if (lTransition_37290 == null) { lTransition_37290 = lState_36250.AddTransition(lState_37280); }
            lTransition_37290.isExit = false;
            lTransition_37290.hasExitTime = false;
            lTransition_37290.hasFixedDuration = true;
            lTransition_37290.exitTime = 0.188751f;
            lTransition_37290.duration = 0.1407029f;
            lTransition_37290.offset = 0f;
            lTransition_37290.mute = false;
            lTransition_37290.solo = false;
            lTransition_37290.canTransitionToSelf = true;
            lTransition_37290.orderedInterruption = true;
            lTransition_37290.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_37290.conditions.Length - 1; i >= 0; i--) { lTransition_37290.RemoveCondition(lTransition_37290.conditions[i]); }
            lTransition_37290.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3301f, "L" + rLayerIndex + "MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lTransition_37292 = MotionControllerMotion.EditorFindTransition(lState_36250, lState_37278, 0);
            if (lTransition_37292 == null) { lTransition_37292 = lState_36250.AddTransition(lState_37278); }
            lTransition_37292.isExit = false;
            lTransition_37292.hasExitTime = true;
            lTransition_37292.hasFixedDuration = true;
            lTransition_37292.exitTime = 0f;
            lTransition_37292.duration = 0.25f;
            lTransition_37292.offset = 0f;
            lTransition_37292.mute = false;
            lTransition_37292.solo = false;
            lTransition_37292.canTransitionToSelf = true;
            lTransition_37292.orderedInterruption = true;
            lTransition_37292.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_37292.conditions.Length - 1; i >= 0; i--) { lTransition_37292.RemoveCondition(lTransition_37292.conditions[i]); }
            lTransition_37292.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3305f, "L" + rLayerIndex + "MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lTransition_37294 = MotionControllerMotion.EditorFindTransition(lState_37280, lState_37282, 0);
            if (lTransition_37294 == null) { lTransition_37294 = lState_37280.AddTransition(lState_37282); }
            lTransition_37294.isExit = false;
            lTransition_37294.hasExitTime = true;
            lTransition_37294.hasFixedDuration = true;
            lTransition_37294.exitTime = 0.4965208f;
            lTransition_37294.duration = 0.1006894f;
            lTransition_37294.offset = 0f;
            lTransition_37294.mute = false;
            lTransition_37294.solo = false;
            lTransition_37294.canTransitionToSelf = true;
            lTransition_37294.orderedInterruption = true;
            lTransition_37294.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_37294.conditions.Length - 1; i >= 0; i--) { lTransition_37294.RemoveCondition(lTransition_37294.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_N818656 = MotionControllerMotion.EditorFindTransition(lState_37282, lState_37278, 0);
            if (lTransition_N818656 == null) { lTransition_N818656 = lState_37282.AddTransition(lState_37278); }
            lTransition_N818656.isExit = false;
            lTransition_N818656.hasExitTime = true;
            lTransition_N818656.hasFixedDuration = true;
            lTransition_N818656.exitTime = 0f;
            lTransition_N818656.duration = 0.25f;
            lTransition_N818656.offset = 0f;
            lTransition_N818656.mute = false;
            lTransition_N818656.solo = false;
            lTransition_N818656.canTransitionToSelf = true;
            lTransition_N818656.orderedInterruption = true;
            lTransition_N818656.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_N818656.conditions.Length - 1; i >= 0; i--) { lTransition_N818656.RemoveCondition(lTransition_N818656.conditions[i]); }


            // Run any post processing after creating the state machine
            OnStateMachineCreated();
        }


        // ************************************ END AUTO GENERATED ************************************

        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
        public static void ExtendBasicMotion(MotionController rMotionController, int rLayerIndex)
        {
            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_35688 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "ThrowWeapon-SM");
            if (lSSM_35688 == null) { lSSM_35688 = lLayerStateMachine.AddStateMachine("ThrowWeapon-SM", new Vector3(624, -1008, 0)); }

            UnityEditor.Animations.AnimatorState lState_37278 = MotionControllerMotion.EditorFindState(lSSM_35688, "PSS Idle Pose");
            if (lState_37278 == null) { lState_37278 = lSSM_35688.AddState("PSS Idle Pose", new Vector3(408, 348, 0)); }
            lState_37278.speed = 1f;
            lState_37278.mirror = false;
            lState_37278.tag = "Exit";
            lState_37278.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Idle_2");

            UnityEditor.Animations.AnimatorState lState_36250 = MotionControllerMotion.EditorFindState(lSSM_35688, "AimPose");
            if (lState_36250 == null) { lState_36250 = lSSM_35688.AddState("AimPose", new Vector3(276, 192, 0)); }
            lState_36250.speed = 1f;
            lState_36250.mirror = false;
            lState_36250.tag = "";
            lState_36250.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Female Action Pose (1).fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_37280 = MotionControllerMotion.EditorFindState(lSSM_35688, "Throw");
            if (lState_37280 == null) { lState_37280 = lSSM_35688.AddState("Throw", new Vector3(516, 192, 0)); }
            lState_37280.speed = 2f;
            lState_37280.mirror = false;
            lState_37280.tag = "";
            lState_37280.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Specials.fbx", "KB_KnifeThrow");

            UnityEditor.Animations.AnimatorState lState_37282 = MotionControllerMotion.EditorFindState(lSSM_35688, "Windup");
            if (lState_37282 == null) { lState_37282 = lSSM_35688.AddState("Windup", new Vector3(744, 204, 0)); }
            lState_37282.speed = 1f;
            lState_37282.mirror = false;
            lState_37282.tag = "";
            lState_37282.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Windup.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_35984 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_36250, 0);
            if (lAnyTransition_35984 == null) { lAnyTransition_35984 = lLayerStateMachine.AddAnyStateTransition(lState_36250); }
            lAnyTransition_35984.isExit = false;
            lAnyTransition_35984.hasExitTime = false;
            lAnyTransition_35984.hasFixedDuration = true;
            lAnyTransition_35984.exitTime = 0.75f;
            lAnyTransition_35984.duration = 0.25f;
            lAnyTransition_35984.offset = 0f;
            lAnyTransition_35984.mute = false;
            lAnyTransition_35984.solo = false;
            lAnyTransition_35984.canTransitionToSelf = false;
            lAnyTransition_35984.orderedInterruption = true;
            lAnyTransition_35984.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_35984.conditions.Length - 1; i >= 0; i--) { lAnyTransition_35984.RemoveCondition(lAnyTransition_35984.conditions[i]); }
            lAnyTransition_35984.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3300f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_35984.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lTransition_37290 = MotionControllerMotion.EditorFindTransition(lState_36250, lState_37280, 0);
            if (lTransition_37290 == null) { lTransition_37290 = lState_36250.AddTransition(lState_37280); }
            lTransition_37290.isExit = false;
            lTransition_37290.hasExitTime = false;
            lTransition_37290.hasFixedDuration = true;
            lTransition_37290.exitTime = 0.188751f;
            lTransition_37290.duration = 0.1407029f;
            lTransition_37290.offset = 0f;
            lTransition_37290.mute = false;
            lTransition_37290.solo = false;
            lTransition_37290.canTransitionToSelf = true;
            lTransition_37290.orderedInterruption = true;
            lTransition_37290.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_37290.conditions.Length - 1; i >= 0; i--) { lTransition_37290.RemoveCondition(lTransition_37290.conditions[i]); }
            lTransition_37290.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3301f, "L" + rLayerIndex + "MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lTransition_37292 = MotionControllerMotion.EditorFindTransition(lState_36250, lState_37278, 0);
            if (lTransition_37292 == null) { lTransition_37292 = lState_36250.AddTransition(lState_37278); }
            lTransition_37292.isExit = false;
            lTransition_37292.hasExitTime = true;
            lTransition_37292.hasFixedDuration = true;
            lTransition_37292.exitTime = 0f;
            lTransition_37292.duration = 0.25f;
            lTransition_37292.offset = 0f;
            lTransition_37292.mute = false;
            lTransition_37292.solo = false;
            lTransition_37292.canTransitionToSelf = true;
            lTransition_37292.orderedInterruption = true;
            lTransition_37292.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_37292.conditions.Length - 1; i >= 0; i--) { lTransition_37292.RemoveCondition(lTransition_37292.conditions[i]); }
            lTransition_37292.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3305f, "L" + rLayerIndex + "MotionPhase");

            UnityEditor.Animations.AnimatorStateTransition lTransition_37294 = MotionControllerMotion.EditorFindTransition(lState_37280, lState_37282, 0);
            if (lTransition_37294 == null) { lTransition_37294 = lState_37280.AddTransition(lState_37282); }
            lTransition_37294.isExit = false;
            lTransition_37294.hasExitTime = true;
            lTransition_37294.hasFixedDuration = true;
            lTransition_37294.exitTime = 0.4965208f;
            lTransition_37294.duration = 0.1006894f;
            lTransition_37294.offset = 0f;
            lTransition_37294.mute = false;
            lTransition_37294.solo = false;
            lTransition_37294.canTransitionToSelf = true;
            lTransition_37294.orderedInterruption = true;
            lTransition_37294.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_37294.conditions.Length - 1; i >= 0; i--) { lTransition_37294.RemoveCondition(lTransition_37294.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_N818656 = MotionControllerMotion.EditorFindTransition(lState_37282, lState_37278, 0);
            if (lTransition_N818656 == null) { lTransition_N818656 = lState_37282.AddTransition(lState_37278); }
            lTransition_N818656.isExit = false;
            lTransition_N818656.hasExitTime = true;
            lTransition_N818656.hasFixedDuration = true;
            lTransition_N818656.exitTime = 0f;
            lTransition_N818656.duration = 0.25f;
            lTransition_N818656.offset = 0f;
            lTransition_N818656.mute = false;
            lTransition_N818656.solo = false;
            lTransition_N818656.canTransitionToSelf = true;
            lTransition_N818656.orderedInterruption = true;
            lTransition_N818656.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_N818656.conditions.Length - 1; i >= 0; i--) { lTransition_N818656.RemoveCondition(lTransition_N818656.conditions[i]); }

        }

        #endregion
#endif
        #endregion
    }


}
