﻿using System;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Actors.AnimationControllers;
using com.ootii.Actors.Attributes;
using com.ootii.Actors.Combat;
using com.ootii.Actors.Inventory;
using com.ootii.Actors.LifeCores;
using com.ootii.Data.Serializers;
using com.ootii.Helpers;
using com.ootii.Geometry;
using com.ootii.Messages;
using NFTS.TimeManager;
using NFTS.CM;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace com.ootii.MotionControllerPacks
{
    /// <summary>
    /// Basic motion for a sword attack
    /// </summary>
    [MotionName("Basic Melee Attack")]
    [MotionDescription("Basic melee attacks that use a 'style' to determine which attack takes place.")]
    public class BasicMeleeAttack : PSS_MotionBase
    {

        /// <summary>
        /// Preallocates string for the event tests
        /// </summary>
        public static string EVENT_BEGIN_FOA = "beginfoa";
        public static string EVENT_END_FOA = "endfoa";
        public static string EVENT_BEGIN_CHAIN = "beginchain";
        public static string EVENT_END_CHAIN = "endchain";
        public static string EVENT_HIT = "hit";
        public static string EVENT_BEGIN_DODGE = "begindodge";
        public static string EVENT_END_DODGE = "enddodge";
        public static string EVENT_DRAW_WEAPON = "draw";
        public static string EVENT_SHEATHE_WEAPON = "sheathe";
        public static string EVENT_SLOMO = "slomo";
        public static string EVENT_END_SLOMO = "endslomo";

        private FMOD.Studio.EventInstance instance;
        private FMOD.Studio.EventDescription eventDescription;
        private FMODUnity.StudioEventEmitter emitter;

        /// <summary>
        /// Weapon to throw's movement and collision logic.
        /// </summary>
        CollisionMessager WeaponThrowLogic;

        /// <summary>
        /// Weapon object in hand
        /// </summary>
        GameObject WeaponHold;

        /// <summary>
        /// Weapon object in hand
        /// </summary>
        GameObject WeaponThrow;

        GameObject ScanlinesObject;

        /// <summary>
        /// Transform of Mixamo rig's hand so that we can return our javelin on activation;
        /// </summary>
        Transform HandParent;

        /// <summary>
        /// The game object that we'll be using to aim our throw.
        /// </summary>
        ThrowAimTargeting ThrowAimTarget = null;

        /// <summary>
        /// Trigger values for th emotion
        /// </summary>
        public int PHASE_UNKNOWN = 0;
        public int PHASE_START = 3200;
        public int PHASE_START_CHAIN = 3200;
        public int PHASE_START_INTERRUPT = 3205;

        ///<summary>
        ///Integer values for combat styles.
        ///</summary>

        public int COMBO_1 = 0;
        public int COMBO_2 = 1;
        public int COMBO_3 = 2;
        public int COMBO_4 = 3;
        //public int SUPERMAN = 4;
        public int PUSHKICK = 5;
        public int BREAKOUT_0 = 6;
        public int BREAKOUT_1 = 7;
        public int BREAKOUT_2 = 8;
        public int BREAKOUT_3 = 9;
        public int BREAKOUT_4 = 10;
        public int BLINK_STRIKE = 11;
        public int COUNTER_1 = 12;
        public int COUNTER_2 = 13;
        public int COUNTER_3 = 14;
        public int COUNTER_4 = 15;
        public int COUNTER_5 = 16;
        public int COUNTER_HEAVY_01 = 17;
        public int COUNTER_HEAVY_02 = 18;
        public int COUNTER_HEAVY_03 = 19;

        public Elain_StateMachine AISource;
        public AI_Combo CurrentCombo;

        /// <summary>
        /// Determines if we're using the IsInMotion() function to verify that
        /// the transition in the animator has occurred for this motion.
        /// </summary>
        public override bool VerifyTransition
        {
            get { return false; }
        }

        /// <summary>
        /// Determines if the impact test is done continuously through the FOA
        /// </summary>
        public bool _TestContinuously = true;
        public bool TestContinuously
        {
            get { return _TestContinuously; }
            set { _TestContinuously = value; }
        }

        public float _ExitTime = 1f;
        public float ExitTime
        {
            get { return _ExitTime; }
            set { _ExitTime = value; }
        }

        /// <summary>
        /// Desired degrees of rotation per second
        /// </summary>
        public float _RotationSpeed = 360f;
        public float RotationSpeed
        {
            get { return _RotationSpeed; }

            set
            {
                _RotationSpeed = value;
                mDegreesPer60FPSTick = _RotationSpeed / 60f;
            }
        }

        /// <summary>
        /// Determine if we use the default form to select the weapon style
        /// </summary>
        public bool _UseDefaultForm = true;
        public bool UseDefaultForm
        {
            get { return _UseDefaultForm; }
            set { _UseDefaultForm = value; }
        }

        /// <summary>
        /// Index of the default attack style to use
        /// </summary>
        public int _DefaultAttackStyleIndex = 0;
        public int DefaultAttackStyleIndex
        {
            get { return _DefaultAttackStyleIndex; }
            set { _DefaultAttackStyleIndex = value; }
        }

        /// <summary>
        /// Index of the current attack style
        /// </summary>
        protected int mCurrentAttackStyleIndex = -1;
        public int CurrentAttackStyleIndex
        {
            get { return mCurrentAttackStyleIndex; }
            set { mCurrentAttackStyleIndex = value; }
        }

        /// <summary>
        /// Form of the current attack style
        /// </summary>
        protected int mCurrentAttackStyleForm = 0;
        public int CurrentAttackStyleForm
        {
            get { return mCurrentAttackStyleForm; }
            set { mCurrentAttackStyleForm = value; }
        }

        ///<summary>
        ///Unique action alias for transitioning into a breakout attack.
        ///</summary>
        protected string _BreakoutActionAlias = "BreakoutAlias";
        public string BreakoutActionAlias
        {
            get {return _BreakoutActionAlias; }
            set { _BreakoutActionAlias = value; }
        }

        /// <summary>
        /// Attack styles built into the motion. This allows us to create different 
        /// capabilities and tie them to motions.
        /// </summary>
        public List<AttackStyle> AttackStyles = new List<AttackStyle>();

        /// <summary>
        /// Active attack style
        /// </summary>
        public AttackStyle AttackStyle
        {
            get
            {
                if (mCurrentAttackStyleIndex >= 0 && mCurrentAttackStyleIndex < AttackStyles.Count)
                {
                    return AttackStyles[mCurrentAttackStyleIndex];
                }

                for (int i = AttackStyles.Count - 1; i >= 0; i--)
                {
                    if (AttackStyles[i].Form == mCurrentAttackStyleForm)
                    {
                        return AttackStyles[i];
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Determines if we can chain the next attack
        /// </summary>
        protected bool mIsChainActive = false;

        /// <summary>
        /// Defines the current index of the attack within the chain
        /// </summary>
        protected int mChainAttackIndex = 0;

        /// <summary>
        /// The next attack style in the chain
        /// </summary>
        protected int mChainAttackStyleIndex = -1;

        protected float mBreakoutAttackStyleIndex = -1;

        /// <summary>
        /// Represents the primary weapon for the attack
        /// </summary>
        protected IWeaponCore mWeaponCore = null;

        /// <summary>
        /// Keeps us from having to re-allocate
        /// </summary>
        protected List<CombatTarget> mCombatTargets = new List<CombatTarget>();

        /// <summary>
        /// Determines if the weapon is currently equipped
        /// </summary>
        //private bool mIsEquipped = false;

        /// <summary>
        /// Determines if the attack hit
        /// </summary>
        protected bool mHasHit = false;

        /// <summary>
        /// Determines if the attack has been interrupted or not
        /// </summary>
        protected bool mIsInterrupted = false;

        /// <summary>
        /// Speed we'll actually apply to the rotation. This is essencially the
        /// number of degrees per tick assuming we're running at 60 FPS
        /// </summary>
        protected float mDegreesPer60FPSTick = 1f;

        /// <summary>
        /// INTERNAL ONLY - Allows us to serialize our attack styles
        /// </summary>
        protected string mAttackStyleDefinitions = "";
        public string AttackStyleDefinitions
        {
            get { return mAttackStyleDefinitions; }
            set { mAttackStyleDefinitions = value; }
        }

        // Used to get attributes associated with the actor
        protected IAttributeSource mAttributes = null;

        protected float _ImpactPower = 0f;
        public float ImpactPower
        {
             get{ return _ImpactPower; } 
             set {_ImpactPower = value; }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public BasicMeleeAttack()
            : base()
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE;

            _Priority = 16;
            _ActionAlias = "Combat Attack";
            _BreakoutActionAlias = "BreakoutAlias";

            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "BasicMeleeAttack-SM"; }
        }

        /// <summary>
        /// Controller constructor
        /// </summary>
        /// <param name="rController">Controller the motion belongs to</param>
        public BasicMeleeAttack(MotionController rController)
            : base(rController)
        {
            _Pack = SwordShieldPackDefinition.PackName;
            _Category = EnumMotionCategories.COMBAT_MELEE;

            _Priority = 16;
            _ActionAlias = "Combat Attack";
            _BreakoutActionAlias = "BreakoutAlias";

            if (_EditorAnimatorSMName.Length == 0) { _EditorAnimatorSMName = "BasicMeleeAttack-SM"; }
        }

        /// <summary>
        /// Awake is called after all objects are initialized so you can safely speak to other objects. This is where
        /// reference can be associated.
        /// </summary>
        public override void Awake()
        {
            base.Awake();
            if (mMotionController.gameObject.GetComponent<Elain_StateMachine>() != null)
            {
                AISource = mMotionController.gameObject.GetComponent<Elain_StateMachine>();
            }
            // Default the speed we'll use to rotate
            mDegreesPer60FPSTick = _RotationSpeed / 60f;

            // Default the attack style
            if (mCombatant != null)
            {
                mCombatant.CombatStyle = AttackStyle;
            }

            // Grab attributes if the MC exists
            if (mMotionController != null)
            {
                mAttributes = mMotionController.gameObject.GetComponent<IAttributeSource>();
            }

            BreakoutActionAlias = "BreakoutAlias";

#if UNITY_EDITOR

            // Recreate our list for rendering
            InstantiateAttackStyleList();

#endif

            ThrowAimTarget = mMotionController.GetComponentInChildren<ThrowAimTargeting>();
            HandParent = mMotionController.gameObject.GetComponentInChildren<WeaponParentIdent>().transform;
            WeaponHold = mMotionController.gameObject.GetComponent<WeaponInventory>().WeaponToHold;
            WeaponThrow = mMotionController.gameObject.GetComponent<WeaponInventory>().WeaponToThrow;
            ScanlinesObject = mMotionController.GetComponentInChildren<ScanlinesIdent>().gameObject;
        }

        /// <summary>
        /// Tests if this motion should be started. However, the motion
        /// isn't actually started.
        /// </summary>
        /// <returns></returns>
        public override bool TestActivate()
        {
            if (!mIsStartable)
            {
                return false;
            }

            if (!mActorController.IsGrounded)
            {
                return false;
            }

            // This is only valid if we're in combat mode
            if (mMotionController.Stance != EnumControllerStance.COMBAT_MELEE)
            {
                return false;
            }

            // Check if we've been activated
            if (_ActionAlias.Length > 0 && mMotionController._InputSource != null)
            {
                if (mMotionController._InputSource.IsJustPressed(_ActionAlias))
                {
                    mParameter = GetDefaultAttackStyleIndex();
                    if (mParameter < 0) { return false; }

                    return true;
                }
            }

            if (mMotionController._InputSource.IsJustPressed(_BreakoutActionAlias))
            {
                float inputY = mMotionController.Animator.GetFloat("InputY");
                float inputMagnitude = mMotionController.Animator.GetFloat("InputMagnitude");
                //  Debug.Log("InputX =" + " " + inputY);
                //  Debug.Log("InputMagnitude =" + " " + inputMagnitude);

                /*if (inputY >= 0.2f)
                {
                    mParameter = SUPERMAN;
                    return true;
                }
                else */if (inputY <= -0.2f)
                {
                    mParameter = PUSHKICK;
                    return true;
                }


                mParameter = GetDefaultBreakoutAttackStyleIndex();
                if (mParameter < 0) { return false; }
                else
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Tests if the motion should continue. If it shouldn't, the motion
        /// is typically disabled
        /// </summary>
        /// <returns></returns>
        public override bool TestUpdate()
        {
            if (mIsActivatedFrame) { return true; }
            //if (!mMotionController.IsGrounded) { Debug.Log("Deactivating due to grounding");  return false; }
            //if (mActorController.State.Stance != EnumControllerStance.COMBAT_MELEE_SWORD_SHIELD) { return false; }

            //Check if we're in the exit state with an exit time.

            
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.IsTag("Exit"))
            {
                if (mMotionLayer._AnimatorStateID == STATE_Idle)
                {
                    if (mMotionLayer._AnimatorTransitionID == 0)
                    {
                        //Debug.Log("Deactivating due to Exit tag");
                        return false;
                    }
                }
            }
            

            // Ensure we're in the animation
            if (mIsAnimatorActive && !IsInMotionState)
            {
                //Debug.Log("Deactivating due to not being in an appropriate animator state");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Called to start the specific motion. If the motion
        /// were something like 'jump', this would start the jumping process
        /// </summary>
        /// <param name="rPrevMotion">Motion that this motion is taking over from</param>
        public override bool Activate(MotionControllerMotion rPrevMotion)
        {

            //Debug.Log("Activation called with param " + mParameter);
            mHasHit = false;
            mIsInterrupted = false;
            mIsChainActive = false;
            mChainAttackIndex = 0;
            mChainAttackStyleIndex = -1;

            // Determine the attack style
            if (AttackStyles.Count <= 0) { return false; }

            mCurrentAttackStyleIndex = mParameter;
            mParameter = 0;

            if (mCurrentAttackStyleIndex < 0 || mCurrentAttackStyleIndex >= AttackStyles.Count)
            {
                Debug.Log("Cancelling activation");
                return false;
            }

            if (AISource && mCurrentAttackStyleIndex >= 12 && mCurrentAttackStyleIndex < 15)
            {
                //instance = FMODUnity.RuntimeManager.CreateInstance("event:/FX/Interior/Combat/Elain Heavy Strike Attempt/Dry/Speed 1");
                //eventDescription = FMODUnity.RuntimeManager.GetEventDescription("event:/FX/Interior/Combat/Elain Heavy Strike Attempt/Dry/Speed 1");
                FMODUnity.RuntimeManager.PlayOneShot("event:/FX/Interior/Combat/Elain Heavy Strike Attempt/Dry/Speed 1",mMotionController.gameObject.transform.position);
                //instance.start();
            }

            // Allow the combat style to be modified before the attack occurs
            CombatMessage lMessage = CombatMessage.Allocate();
            lMessage.ID = CombatMessage.MSG_ATTACKER_PRE_ATTACK;

            

            lMessage.Attacker = mMotionController.gameObject;
            lMessage.StyleIndex = mCurrentAttackStyleIndex;
            lMessage.CombatStyle = new AttackStyle(AttackStyles[mCurrentAttackStyleIndex]);
            //Debug.Log("BasicMeleeAttack: CombatStyle.Form read as " + lMessage.CombatStyle.Form);
            lMessage.CombatMotion = this;
            lMessage.Data = this;

            IActorCore lActorCore = mMotionController.gameObject.GetComponent<ActorCore>();
            if (lActorCore != null)
            {
                lActorCore.SendMessage(lMessage);

                if (lMessage.StyleIndex >= 0 && lMessage.StyleIndex < AttackStyles.Count)
                {
                    mCurrentAttackStyleIndex = lMessage.StyleIndex;
                }
            }

#if USE_MESSAGE_DISPATCHER || OOTII_MD
            MessageDispatcher.SendMessage(lMessage);

            if (lMessage.StyleIndex >= 0 && lMessage.StyleIndex < AttackStyles.Count)
            {
                    mCurrentAttackStyleIndex = lMessage.StyleIndex;
            }
#endif

            //CombatMessage.Release(lMessage);

            // Use the final attack style as needed
            if (mCurrentAttackStyleIndex < 0 || mCurrentAttackStyleIndex >= AttackStyles.Count)
            { //Debug.Log("cancelling activation"); 
                return false; }

                mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;
                if (mCurrentAttackStyleForm < 0) { return false; }

                AttackStyles[mCurrentAttackStyleIndex].LastAttackTime = Time.time;

                mParameter = AttackStyles[mCurrentAttackStyleIndex].Parameter;

                // Determine the attack weapon
                mWeaponCore = null;

                // Grab the weapon from the weapon core
                if (mWeaponCore == null && AttackStyles[mCurrentAttackStyleIndex].InventorySlotID.Length > 0)
                {
                    IInventorySource lInventory = mMotionController.gameObject.GetComponent<IInventorySource>();
                    if (lInventory != null)
                    {
                        string lItemID = lInventory.GetItemID(AttackStyles[mCurrentAttackStyleIndex].InventorySlotID, false);
                        if (lItemID.Length > 0)
                        {
                            GameObject lWeapon = lInventory.GetItemPropertyValue<GameObject>(lItemID, "instance");
                            if (lWeapon != null)
                            {
                                mWeaponCore = lWeapon.GetComponent<IWeaponCore>();
                            }
                        }
                    }
                }

                // Grab the weapon from the combatant
                if (mWeaponCore == null && mCombatant != null && mCombatant.PrimaryWeapon != null)
                {
                    mWeaponCore = mCombatant.PrimaryWeapon;
                }

                // Grab the weapon through the attributes
                if (mWeaponCore == null && mAttributes != null)
                {
                    GameObject lWeapon = mAttributes.GetAttributeValue<GameObject>(EnumAttributeIDs.MELEE_PRIMARY_WEAPON);
                    if (lWeapon != null) { mWeaponCore = lWeapon.GetComponent<IWeaponCore>(); }
                }

                // Run the approapriate attack
                mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START, mCurrentAttackStyleForm, mParameter, true);

            //Debug.Log("Full activation cleared!");
            if (mCurrentAttackStyleForm >= 300)
            {
                lMessage.bIsCrit = true;
            }

            if (mCurrentAttackStyleForm == 400)
            {
                //Debug.Log("AND MY AXE");
                SlowTimeManager.SetElainSpeedSlow();
                WeaponHold.SetActive(true);
                WeaponThrow.SetActive(false);
                mMotionController.GetComponent<BasicAttributes>().SetAttributeValue<int>("GOLD", mMotionController.GetComponent<BasicAttributes>().GetAttributeValue<int>("GOLD") - 1);
            }
            // Now, activate the motion
            return base.Activate(rPrevMotion);
            
        }

        /// <summary>
        /// Called to interrupt the motion if it is currently active. This
        /// gives the motion a chance to stop itself how it sees fit. The motion
        /// may simply ignore the call.
        /// </summary>
        /// <param name="rParameter">Any value you wish to pass</param>
        /// <returns>Boolean determining if the motion accepts the interruption. It doesn't mean it will deactivate.</returns>
        public override bool Interrupt(object rParameter)
        {
            if (mIsActive && !mIsInterrupted)
            {
                mIsInterrupted = true;

                return true;
            }

            //If the AI asks to interrupt this motion, always return true.
            if (mMotionController.gameObject.GetComponent<Elain_StateMachine>() != null)
            {
                return true;
            }

            return false;
        }

        public void ContinueCombo(int ComboStyleInt)
        {
            //Debug.Log("Chaining!");
            mHasHit = false;
            mIsChainActive = false;
            mCurrentAttackStyleIndex = ComboStyleInt;
            mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;

            // Allow the combat style to be modified before the attack occurs
            CombatMessage lMessage = CombatMessage.Allocate();
            lMessage.ID = CombatMessage.MSG_ATTACKER_PRE_ATTACK;
            lMessage.Attacker = mMotionController.gameObject;
            lMessage.AttackIndex = mChainAttackIndex;
            lMessage.StyleIndex = mCurrentAttackStyleIndex;
            lMessage.CombatStyle = new AttackStyle(AttackStyles[mCurrentAttackStyleIndex]);
            lMessage.CombatMotion = this;
            lMessage.Data = this;

            IActorCore lActorCore = mMotionController.gameObject.GetComponent<ActorCore>();
            if (lActorCore != null)
            {
                lActorCore.SendMessage(lMessage);

                if (lMessage.StyleIndex >= 0 && lMessage.StyleIndex < AttackStyles.Count)
                {
                    mCurrentAttackStyleIndex = lMessage.StyleIndex;
                    mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;
                    mParameter = AttackStyles[mCurrentAttackStyleIndex].Parameter;
                }
            }

            //Debug.Log("Setting motion phase to START with form and param of: " + mCurrentAttackStyleForm + ", " + mParameter);
            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START_CHAIN, mCurrentAttackStyleForm, mParameter, false);
        }

        /// <summary>
        /// Raised when we shut the motion down
        /// </summary>
        public override void Deactivate()
        {
            //Debug.Log("Deactivating");
            // Clear our current attack style
            mCurrentAttackStyleIndex = -1;
            SlowTimeManager.SetGlobalSpeedNormal();
            // Don't hold onto our targets
            mCombatTargets.Clear();

            // Report that the attack has finished
            CombatMessage lMessage = CombatMessage.Allocate();
            lMessage.ID = CombatMessage.MSG_ATTACKER_POST_ATTACK;
            lMessage.Attacker = mMotionController.gameObject;
            lMessage.CombatMotion = this;
            lMessage.Data = this;

            IActorCore lActorCore = mMotionController.gameObject.GetComponent<ActorCore>();
            if (lActorCore != null)
            {
                lActorCore.SendMessage(lMessage);
            }

#if USE_MESSAGE_DISPATCHER || OOTII_MD
            MessageDispatcher.SendMessage(lMessage);
#endif

            CombatMessage.Release(lMessage);

            if (mMotionController.gameObject.GetComponent<Elain_StateMachine>() != null)
            {
                //Debug.Log("AI suttered");
                mMotionController.gameObject.GetComponent<Elain_StateMachine>().ComboStutterer();
            }

            mCombatant.ForceActorRotation = true;
            //Debug.Log("DEACTIVATING AXE");
            WeaponHold.SetActive(false);
            SlowTimeManager.SetGlobalSpeedNormal();
            /*
            FMOD.Studio.PLAYBACK_STATE state;
            instance.getPlaybackState(out state);
            if (state == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                instance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            }
            */
            // Continue
            base.Deactivate();
        }

        /// <summary>
        /// Allows the motion to modify the velocity before it is applied.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        /// <param name="rMovement">Amount of movement caused by root motion this frame</param>
        /// <param name="rRotation">Amount of rotation caused by root motion this frame</param>
        /// <returns></returns>
        public override void UpdateRootMotion(float rDeltaTime, int rUpdateIndex, ref Vector3 rMovement, ref Quaternion rRotation)
        {
            //int lStateID = mMotionLayer._AnimatorStateID;
            //int lTransitionID = mMotionLayer._AnimatorTransitionID;

            //if (lTransitionID == TRANS_AnyState_SpinSlash || lStateID == STATE_SpinSlash)
            //{
            //    rMovement.x = 0f;
            //    rMovement.z = 0f;
            //}
            //else if (lTransitionID == TRANS_AnyState_JumpSlash || lStateID == STATE_JumpSlash)
            //{
            //    rMovement.x = 0f;
            //    rMovement.z = 0f;
            //}
            //else if (lTransitionID == TRANS_AnyState_ComboSpinSlash || lStateID == STATE_ComboSpinSlash)
            //{
            //    rMovement.x = 0f;
            //    rMovement.z = 0f;
            //}
            //else if (lTransitionID == TRANS_AnyState_Slash || lStateID == STATE_Slash)
            //{
            //    rMovement = Vector3.zero;
            //    rRotation = Quaternion.identity;
            //}
            //else if (lTransitionID == TRANS_AnyState_BackSlash || lStateID == STATE_BackSlash)
            //{
            //    rMovement = Vector3.zero;
            //    rRotation = Quaternion.identity;
            //}
            //else if (lStateID == STATE_LowSlash)
            //{
            //    rMovement = Vector3.zero;
            //    rRotation = Quaternion.identity;
            //}
            //else if (lTransitionID == TRANS_AnyState_PommelBash || lStateID == STATE_PommelBash)
            //{
            //    rMovement = Vector3.zero;
            //    rRotation = Quaternion.identity;
            //}
        }

        /// <summary>
        /// Updates the motion over time. This is called by the controller
        /// every update cycle so animations and stages can be updated.
        /// </summary>
        /// <param name="rDeltaTime">Time since the last frame (or fixed update call)</param>
        /// <param name="rUpdateIndex">Index of the update to help manage dynamic/fixed updates. [0: Invalid update, >=1: Valid update]</param>
        public override void Update(float rDeltaTime, int rUpdateIndex)
        {
            mMovement = Vector3.zero;
            mRotation = Quaternion.identity;

            // Ensure we face the target if we're meant to
            //if (mCombatant != null && mCombatant.IsTargetLocked)
            //{
                //RotateCameraToTarget(mCombatant.Target, _ToTargetCameraRotationSpeed);
                //RotateToTarget(mCombatant.Target, _ToTargetRotationSpeed, rDeltaTime, ref mRotation);
            //}

            // Clear the motion phase if we need to
            if (IsInBlockedState())
            {
                mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, 0);
            }

            if (IsInChainState())
            {
                mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, 0);
            }

            // Check if we've got a hit while the weapon is active
            if (_TestContinuously && mCurrentAttackStyleForm >= 0 && !mIsInterrupted)
            {
                if (mWeaponCore != null && mWeaponCore.IsActive && !mWeaponCore.HasColliders)
                {
                    AttackStyle lAttackStyle = AttackStyle;
                    int lTargetCount = mCombatant.QueryCombatTargets(lAttackStyle, mCombatTargets);

                    if (lTargetCount > 0)
                    {
                        WeaponCore lWeaponCore = mWeaponCore as WeaponCore;
                        if (lWeaponCore != null)
                        {
                            int lImpactCount = lWeaponCore.TestImpact(mCombatTargets, lAttackStyle);
                            if (lImpactCount > 0) { mWeaponCore.IsActive = false; }
                        }

                        mCombatTargets.Clear();
                    }
                }
            }

            // Check if we are chaining an attack
            if (mIsChainActive && mChainAttackStyleIndex >= 0 && mChainAttackStyleIndex < AttackStyles.Count && !mIsInterrupted)
            {
                if (_ActionAlias.Length > 0 && mMotionController._InputSource != null)
                {
                    if (mMotionController._InputSource.IsJustPressed(_ActionAlias))
                    {
                        //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " chained");

                        mHasHit = false;
                        mIsChainActive = false;
                        mChainAttackIndex++;
                        mCurrentAttackStyleIndex = mChainAttackStyleIndex;
                        mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;

                        // Allow the combat style to be modified before the attack occurs
                        CombatMessage lMessage = CombatMessage.Allocate();
                        lMessage.ID = CombatMessage.MSG_ATTACKER_PRE_ATTACK;
                        lMessage.Attacker = mMotionController.gameObject;
                        lMessage.AttackIndex = mChainAttackIndex;
                        lMessage.StyleIndex = mCurrentAttackStyleIndex;
                        lMessage.CombatStyle = new AttackStyle(AttackStyles[mCurrentAttackStyleIndex]);
                        lMessage.CombatMotion = this;
                        lMessage.Data = this;

                        IActorCore lActorCore = mMotionController.gameObject.GetComponent<ActorCore>();
                        if (lActorCore != null)
                        {
                            lActorCore.SendMessage(lMessage);

                            if (lMessage.StyleIndex >= 0 && lMessage.StyleIndex < AttackStyles.Count)
                            {
                                mCurrentAttackStyleIndex = lMessage.StyleIndex;
                                mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;
                                mParameter = AttackStyles[mCurrentAttackStyleIndex].Parameter;
                            }
                        }

#if USE_MESSAGE_DISPATCHER || OOTII_MD
                        MessageDispatcher.SendMessage(lMessage);

                        if (lMessage.StyleIndex >= 0 && lMessage.StyleIndex < AttackStyles.Count)
                        {
                            mCurrentAttackStyleIndex = lMessage.StyleIndex;
                            mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;
                            mParameter = AttackStyles[mCurrentAttackStyleIndex].Parameter;
                        }
#endif

                        CombatMessage.Release(lMessage);

                        if (mCurrentAttackStyleForm >= 0)
                        {
                            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START_CHAIN, mCurrentAttackStyleForm, mParameter, false);
                        }
                    }
                }
                if (mIsChainActive && mBreakoutAttackStyleIndex >= 0 && mBreakoutAttackStyleIndex < AttackStyles.Count && !mIsInterrupted)
                {

                    if (mMotionController._InputSource.IsJustPressed(_BreakoutActionAlias))
                    {
                        //Debug.Log("Chain input detected");
                        //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " chained");

                        mHasHit = false;
                        mIsChainActive = false;
                        mChainAttackIndex++;
                        mCurrentAttackStyleIndex = Mathf.RoundToInt(mBreakoutAttackStyleIndex);
                        mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;

                        // Allow the combat style to be modified before the attack occurs
                        CombatMessage lMessage = CombatMessage.Allocate();
                        lMessage.ID = CombatMessage.MSG_ATTACKER_PRE_ATTACK;
                        lMessage.Attacker = mMotionController.gameObject;
                        lMessage.AttackIndex = mChainAttackIndex;
                        lMessage.StyleIndex = mCurrentAttackStyleIndex;
                        lMessage.CombatStyle = new AttackStyle(AttackStyles[mCurrentAttackStyleIndex]);
                        lMessage.CombatMotion = this;
                        lMessage.Data = this;

                        IActorCore lActorCore = mMotionController.gameObject.GetComponent<ActorCore>();
                        if (lActorCore != null)
                        {
                            lActorCore.SendMessage(lMessage);

                            if (lMessage.StyleIndex >= 0 && lMessage.StyleIndex < AttackStyles.Count)
                            {
                                mCurrentAttackStyleIndex = lMessage.StyleIndex;
                                mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;
                                mParameter = AttackStyles[mCurrentAttackStyleIndex].Parameter;
                            }
                        }


#if USE_MESSAGE_DISPATCHER || OOTII_MD
                        MessageDispatcher.SendMessage(lMessage);

                        if (lMessage.StyleIndex >= 0 && lMessage.StyleIndex < AttackStyles.Count)
                        {
                            mCurrentAttackStyleIndex = lMessage.StyleIndex;
                            mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;
                            mParameter = AttackStyles[mCurrentAttackStyleIndex].Parameter;
                        }
#endif

                        CombatMessage.Release(lMessage);

                        if (mCurrentAttackStyleForm >= 0)
                        {
                            mMotionController.SetAnimatorMotionPhase(mMotionLayer.AnimatorLayerIndex, PHASE_START_CHAIN, mCurrentAttackStyleForm, mParameter, false);
                        }
                    }

                }

#if UNITY_EDITOR
                if (ShowDebug)
                {
                    if (mWeaponCore != null)
                    {
                        Color lColor = (mHasHit ? Color.red : (mWeaponCore.IsActive ? Color.green : Color.gray));
                        DrawWeaponDebug(mWeaponCore, AttackStyle, lColor);
                    }

                    if (mIsChainActive)
                    {
                        Graphics.GraphicsManager.DrawSolidCircle(mMotionController._Transform.position + (mMotionController._Transform.up * 0.1f), 0.5f, Color.green);
                    }
                }
#endif
            }
            // Allow the base class to render debug info
            base.Update(rDeltaTime, rUpdateIndex);
        }

        /// <summary>
        /// Raised by the animation when an event occurs
        /// </summary>
        public override void OnAnimationEvent(AnimationEvent rEvent)
        {
            if (rEvent == null) { return; }

            // If the animation is running backwards (say in response to a block), don't fire.
#if UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3
#else
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.speed <= 0f) { return; }
#endif

            // Test the event type
            if (rEvent.stringParameter.Length == 0 || StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_HIT)
            {
                // Test if we can cause damage
                if (mCurrentAttackStyleForm >= 0 && !mIsInterrupted && !_TestContinuously && mCombatant != null)
                {
                    if (mWeaponCore != null && !mWeaponCore.HasColliders)
                    {
                        AttackStyle lAttackStyle = AttackStyle;
                        int lTargetCount = mCombatant.QueryCombatTargets(lAttackStyle, mCombatTargets);

                        if (lTargetCount > 0)
                        {
                            WeaponCore lWeaponCore = mWeaponCore as WeaponCore;
                            if (lWeaponCore != null)
                            {
                                lWeaponCore.TestImpact(mCombatTargets, lAttackStyle);
                            }
                        }
                        mCombatTargets.Clear();
                    }
                }

            }
            // Determine if we are starting the attack
            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_BEGIN_FOA)
            {
                // Grab the weapon through the combatant
                if (mWeaponCore != null)
                {
                    mWeaponCore.IsActive = true;

                    WeaponCore lWeaponCore = mWeaponCore as WeaponCore;
                    if (lWeaponCore != null)
                    {
                        lWeaponCore.mCombatStyle = AttackStyle;

                        SwordCore lSwordCore = lWeaponCore as SwordCore;
                        if (lSwordCore != null) { lSwordCore.OnStartSwing(null); }

                        mCombatant.ForceActorRotation = false;

                    }
                }
            }
            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_BEGIN_DODGE)
            {
                //TSEDIT 21/01/18
                SlowTimeManager.bIsAttacking = true;
                if (AISource)
                {
                    if (AISource.playerObject.GetComponent<MotionController>().ActiveMotion != AISource.playerObject.GetComponent<MotionController>().GetMotion<PSS_Damaged>()) { }
                    //CinemachineManager.SwitchCameraTo(CinemachineManager.VCO_DodgePrompt);
                    if (AISource.pMotionController.ActiveMotion != AISource.pMotionController.GetMotion<PSS_Damaged>())
                    {
                        SlowTimeManager.SetGlobalSpeedSlow();

                    }
                    
                }
                if (!AISource)
                {
                    Elain_StateMachine AI = mMotionController.gameObject.GetComponent<PlayerManager>().ElainsBrain;
                    if (AI.MaxCounters > AI.AIDodgeChain)
                    {
                        //Debug.Log(AI.DodgeThreshold);
                        if (AI.combatEvade.IsEnabled)
                        {
                            AI.AIDodgeActive = true;
                            AI.AIDodgeChain++;
                        }
                    }
                }
            }

            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_END_DODGE)
            {
                //TSEDIT 21/01/18
                SlowTimeManager.bIsAttacking = false;
                SlowTimeManager.SetGlobalSpeedNormal();
            }

            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_SLOMO)
            {
                SlowTimeManager.SetGlobalSpeedSlow();
                CinemachineManager.SwitchCameraTo(CinemachineManager.VCO_Payoff_Axe);
            }

            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_END_SLOMO)
            {
                SlowTimeManager.SetGlobalSpeedNormal();
                //CinemachineManager.SwitchCameraTo(CinemachineManager.VCO_LockOn);
            }

            // Determine if we are ending the attack
            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_END_FOA)
            {
                if (mWeaponCore != null)
                {
                    mWeaponCore.IsActive = false;

                    WeaponCore lWeaponCore = mWeaponCore as WeaponCore;
                    if (lWeaponCore != null)
                    {
                        lWeaponCore.mCombatStyle = null;

                        SwordCore lSwordCore = lWeaponCore as SwordCore;
                        if (lSwordCore != null) { lSwordCore.OnEndSwing(null); }

                        //TSEDIT 15/02/18
                        mCombatant.ForceActorRotation = true;
                        SlowTimeManager.ResetTime();

                        if (CinemachineManager.VCO_activeCamera != CinemachineManager.VCO_LockOn)
                        {
                            CinemachineManager.SwitchCameraTo(CinemachineManager.VCO_LockOn);
                        }
                        //CinemachineManager.SwitchCameraTo(CinemachineManager.VCO_Cached);
                    }
                }

            }
            // Determine if we are beginning a chain
            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_BEGIN_CHAIN)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " begin chain");
                //Debug.Log("Chain detected");
                mIsChainActive = true;
                mChainAttackStyleIndex = rEvent.intParameter;
                mBreakoutAttackStyleIndex = rEvent.floatParameter;

                if (AttackStyle != null)
                {
                    mChainAttackStyleIndex = AttackStyle.NextAttackStyleIndex;
                    if (mChainAttackStyleIndex < 0) { mChainAttackStyleIndex = mCurrentAttackStyleIndex; }
                }


            }
            // Determine if we are ending
            else if (StringHelper.CleanString(rEvent.stringParameter) == BasicMeleeAttack.EVENT_END_CHAIN)
            {
                //Utilities.Debug.Log.FileWrite(mMotionController.name + " " + AttackStyle.Name + " end chain");
                if (mMotionController.gameObject.GetComponent<Elain_StateMachine>() != null)
                {

                    Elain_StateMachine AI = mMotionController.gameObject.GetComponent<Elain_StateMachine>();
                    AI.comboIterator++;
                    if (AI.comboIterator < CurrentCombo.ListOfAttacksInCombo.Length && AI.CurrentCombatState == Elain_StateMachine.COMBATSTATE.ATTACKING)
                    {
                        //Debug.Log("AI combo recognised");
                        ContinueCombo(CurrentCombo.ListOfAttacksInCombo[AI.comboIterator].ReturnAttackStyleInt());
                    }
                }
                mIsChainActive = false;
            }
        }

        /// <summary>
        /// Raised by the controller when a message is received
        /// </summary>
        public override void OnMessageReceived(IMessage rMessage)
        {
            if (rMessage == null) { return; }
            if (mMotionController.Stance != EnumControllerStance.COMBAT_MELEE) { return; }

            CombatMessage lCombatMessage = rMessage as CombatMessage;
            if (lCombatMessage != null)
            {
                // Attack messages
                if (lCombatMessage.Attacker == mMotionController.gameObject)
                {
                    // Call for an attack
                    if (rMessage.ID == CombatMessage.MSG_COMBATANT_ATTACK)
                    {
                        if (!mIsActive && mMotionLayer._AnimatorTransitionID == 0)
                        {
                            int lAttackStyleIndex = (lCombatMessage.StyleIndex >= 0 ? lCombatMessage.StyleIndex : _DefaultAttackStyleIndex);
                            if (lAttackStyleIndex >= 0 && lAttackStyleIndex < AttackStyles.Count)
                            {
                                mCurrentAttackStyleIndex = lAttackStyleIndex;
                                mCurrentAttackStyleForm = AttackStyles[mCurrentAttackStyleIndex].Form;

                                //if (lCombatMessage.CombatStyle != null)
                                //{
                                //    for (int i = 0; i < AttackStyles.Count; i++)
                                //    {
                                //        if (AttackStyles[i] == lCombatMessage.CombatStyle)
                                //        {
                                //            break;
                                //        }
                                //    }
                                //}

                                lCombatMessage.IsHandled = true;
                                lCombatMessage.Recipient = this;
                                lCombatMessage.Damage = lCombatMessage.Damage * AttackStyles[lAttackStyleIndex].DamageModifier;
                                mMotionController.ActivateMotion(this, lAttackStyleIndex);
                            }
                        }
                    }
                    // Attack has been activated and impacts
                    else if (rMessage.ID == CombatMessage.MSG_ATTACKER_ATTACKED)
                    {
                        if (mIsActive)
                        { 
                            lCombatMessage.Damage = lCombatMessage.Damage * (AttackStyle != null ? AttackStyle.DamageModifier : 1f);
                        }
                    }
                    // Gives us a chance to respond to the defender's reaction (post attack)
                    else if (rMessage.ID == CombatMessage.MSG_DEFENDER_ATTACKED_BLOCKED)
                    {
                        if (mIsActive)
                        {
                            mIsInterrupted = true;
                            mMotionController.SetAnimatorMotionPhase(mMotionLayer._AnimatorLayerIndex, PHASE_START_INTERRUPT);

                            // Ensure the weapon isn't active any more.
                            if (mWeaponCore != null)
                            {
                                mWeaponCore.IsActive = false;
                            }
                        }
                    }
                    //// Final hit (post attack)
                    //else if (rMessage.ID == CombatMessage.MSG_DEFENDER_PARRIED ||
                    //         rMessage.ID == CombatMessage.MSG_DEFENDER_DAMAGED || 
                    //         rMessage.ID == CombatMessage.MSG_DEFENDER_KILLED)
                    //{
                    //    OnWeaponHit(lCombatMessage, lCombatMessage.Weapon);
                    //}
                }
                // Defender messages
                else if (lCombatMessage.Defender == mMotionController.gameObject)
                {
                }
            }
        }

        ///// <summary>
        ///// Determines what happens when a hit occurs. We could ignore it, reduce damage, etc.
        ///// </summary>
        ///// <param name="rCombatMessage"></param>
        //protected virtual void OnWeaponHit(CombatMessage rCombatMessage, IWeaponCore rWeaponCore)
        //{
        //    //com.ootii.Utilities.Debug.Log.FileWrite(mMotionController._Transform.name + ".OnWeaponHit()");

        //    mHasHit = true;

        //    if (rWeaponCore != null)
        //    {
        //        WeaponCore lWeaponCore = rWeaponCore as WeaponCore;
        //        if (lWeaponCore != null)
        //        {
        //            lWeaponCore.OnImpactComplete(rCombatMessage);
        //        }
        //    }
        //}

        /// <summary>
        /// Test if we're in a blocked state
        /// </summary>
        /// <returns>Determines if we're in a blocked state</returns>
        protected bool IsInBlockedState()
        {
            // Test based on the state tag
            if (mMotionController.State.AnimatorStates[mMotionLayer._AnimatorLayerIndex].StateInfo.IsTag("MeleeBlock")) { return true; }

            // Test based on the state hash
            int lStateID = mMotionLayer._AnimatorStateID;
            if (lStateID == GetStateID("Blocked")) { return true; }
            if (lStateID == GetStateID("Blocked 2")) { return true; }

            // Test based on the transition hash
            int lTransitionID = mMotionLayer._AnimatorTransitionID;
            if (lTransitionID == GetTransitionID("Slash", "Blocked")) { return true; }
            if (lTransitionID == GetTransitionID("Low Slash", "Blocked")) { return true; }
            if (lTransitionID == GetTransitionID("Back Slash", "Blocked 2")) { return true; }
            if (lTransitionID == GetTransitionID("Spin Slash", "Blocked 2")) { return true; }

            return false;
        }

        /// <summary>
        /// Test if we're in a chain state
        /// </summary>
        /// <returns>Determines if we're in a blocked state</returns>
        protected bool IsInChainState()
        {
            int lTransitionID = mMotionLayer._AnimatorTransitionID;

            string lTransitionName = string.Format("{0}.{1}.Back Slash -> {0}.{1}.Slash", mMotionLayer.AnimatorLayerName, _EditorAnimatorSMName);
            if (lTransitionID == Animator.StringToHash(lTransitionName)) { return true; }

            lTransitionName = string.Format("{0}.{1}.Slash -> {0}.{1}.Back Slash", mMotionLayer.AnimatorLayerName, _EditorAnimatorSMName);
            if (lTransitionID == Animator.StringToHash(lTransitionName)) { return true; }

            return false;
        }

        /// <summary>
        /// Gets the default attack style index to use
        /// </summary>
        /// <returns></returns>
        public int GetDefaultAttackStyleIndex()
        {
            int lAttackStyleIndex = -1;

            if (_UseDefaultForm)
            {
                lAttackStyleIndex = FindAttackStyleIndexFromForm(mMotionController.CurrentForm);
                if (lAttackStyleIndex >= 0 && lAttackStyleIndex < AttackStyles.Count)
                {
                    return lAttackStyleIndex;
                }
            }

            if (_DefaultAttackStyleIndex >= 0 && _DefaultAttackStyleIndex < AttackStyles.Count)
            {
                return _DefaultAttackStyleIndex;
            }

            return -1;
        }

        public int GetDefaultBreakoutAttackStyleIndex()
        {

            int lAttackStyleIndex = -1;

            if (_UseDefaultForm)
            {
                lAttackStyleIndex = FindAttackStyleIndexFromForm(mMotionController.CurrentForm);
                if (lAttackStyleIndex >= 0 && lAttackStyleIndex < AttackStyles.Count)
                {
                    return lAttackStyleIndex;
                }
            }

            return 6;
        }

        /// <summary>
        /// Finds the attack style index given the specified form
        /// </summary>
        /// <param name="rForm">Form that is associated with the attack style</param>
        /// <returns>Index of the attack style if found or -1</returns>
        public int FindAttackStyleIndexFromForm(int rForm)
        {
            for (int i = 0; i < AttackStyles.Count; i++)
            {
                if (AttackStyles[i].Form == rForm)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Creates a JSON string that represents the motion's serialized state. We
        /// do this since Unity can't handle putting lists of derived objects into
        /// prefabs.
        /// </summary>
        /// <returns>JSON string representing the object</returns>
        public override string SerializeMotion()
        {
            // Transfer the parameter info over to the form
            for (int i = 0; i < AttackStyles.Count; i++)
            {
                AttackStyles[i].Form = AttackStyles[i]._ParameterID;
            }

            // Serialize our attack styles
            if (AttackStyles != null && AttackStyles.Count > 0)
            {
                mAttackStyleDefinitions = JSONSerializer.SerializeValue("AttackStyles", AttackStyles);
                mAttackStyleDefinitions = mAttackStyleDefinitions.Replace("\"", "\\\"");
            }
            else
            {
                mAttackStyleDefinitions = "";
            }

            // Now serialize everything
            return base.SerializeMotion();
        }

        /// <summary>
        /// Gieven a JSON string that is the definition of the object, we parse
        /// out the properties and set them.
        /// </summary>
        /// <param name="rDefinition">JSON string</param>
        public override void DeserializeMotion(string rDefinition)
        {
            // Deserialize everything
            base.DeserializeMotion(rDefinition);

            // Now deserialize our attack styles
            if (mAttackStyleDefinitions.Length > 0)
            {
                try
                {
                    AttackStyles = JSONSerializer.DeserializeValue<List<AttackStyle>>(mAttackStyleDefinitions);
                }
                catch
                {
                    AttackStyles.Clear();
                    mAttackStyleDefinitions = "";
                }
            }

            // Transfer the parameter info over to the form
            for (int i = 0; i < AttackStyles.Count; i++)
            {
                AttackStyles[i].Form = AttackStyles[i]._ParameterID;
            }
        }



        // **************************************************************************************************
        // Following properties and function only valid while editing
        // **************************************************************************************************

#if UNITY_EDITOR
        #region Editor Functions
        /// <summary>
        /// Attack style list we'll modify
        /// </summary>
        public ReorderableList mAttackStyleList;

        /// <summary>
        /// Allows us to re-open the last selected motor
        /// </summary>
        public int mEditorAttackStyleIndex = 0;

        /// <summary>
        /// Determines if the motion is dirty
        /// </summary>
        public bool mIsDirty = false;

        /// <summary>
        /// Reset to default values. Reset is called when the user hits the Reset button in the Inspector's 
        /// context menu or when adding the component the first time. This function is only called in editor mode.
        /// </summary>
        public override void Reset()
        {
            AttackStyleListReset();
        }

        /// <summary>
        /// Allow the constraint to render it's own GUI
        /// </summary>
        /// <returns>Reports if the object's value was changed</returns>
        public override bool OnInspectorGUI()
        {
            mIsDirty = false;

            if (EditorHelper.TextField("Attack Alias", "Action alias that is required to trigger the attack.", ActionAlias, mMotionController))
            {
                mIsDirty = true;
                ActionAlias = EditorHelper.FieldStringValue;
            }

            if (EditorHelper.FloatField("Rotation Speed", "Degrees per second to rotate the actor.", RotationSpeed, mMotionController))
            {
                mIsDirty = true;
                RotationSpeed = EditorHelper.FieldFloatValue;
            }

            GUILayout.Space(5f);

            if (EditorHelper.BoolField("Use Default Form", "Determines if we use the Default Form (from the weapon sets) to determine the default attack style.", UseDefaultForm, mMotionController))
            {
                mIsDirty = true;
                UseDefaultForm = EditorHelper.FieldBoolValue;
            }

            if (!UseDefaultForm)
            {
                if (EditorHelper.IntField("Default Attack Style", "Default attack style index to use.", DefaultAttackStyleIndex, mMotionController))
                {
                    mIsDirty = true;
                    DefaultAttackStyleIndex = EditorHelper.FieldIntValue;
                }
            }

            if (EditorHelper.BoolField("Test Continuously", "Determines if we test for impact continuously between the field-of-attack or just at the animation event.", TestContinuously, mMotionController))
            {
                mIsDirty = true;
                TestContinuously = EditorHelper.FieldBoolValue;
            }

            GUILayout.Space(5f);

            EditorGUILayout.LabelField("Attack Styles", EditorStyles.boldLabel, GUILayout.Height(16f));

            GUILayout.BeginVertical(EditorHelper.GroupBox);
            EditorHelper.DrawInspectorDescription("Attack Styles determine how different attack animations play and the effect.", MessageType.None);

            if (mAttackStyleList == null) { InstantiateAttackStyleList(); }
            mAttackStyleList.DoLayoutList();

            if (mAttackStyleList.index >= 0)
            {
                GUILayout.Space(5f);
                GUILayout.BeginVertical(EditorHelper.Box);

                bool lListIsDirty = DrawAttackStyleDetailItem(AttackStyles[mAttackStyleList.index]);
                if (lListIsDirty) { mIsDirty = true; }

                GUILayout.EndVertical();
            }

            EditorGUILayout.EndVertical();

            if (mIsDirty)
            {
                // Serialize our attack styles
                if (AttackStyles != null && AttackStyles.Count > 0)
                {
                    mAttackStyleDefinitions = JSONSerializer.SerializeValue("AttackStyles", AttackStyles);
                    mAttackStyleDefinitions = mAttackStyleDefinitions.Replace("\"", "\\\"");
                }
                else
                {
                    mAttackStyleDefinitions = "";
                }

                InternalEditorUtility.RepaintAllViews();
            }

            return mIsDirty;
        }

        /// <summary>
        /// Enables the Editor to handle an event in the scene view.
        /// </summary>
        public override void OnSceneGUI()
        {
            if (Application.isPlaying) { return; }

            Combatant lCombatant = mMotionController.gameObject.GetComponent<Combatant>();
            if (lCombatant != null)
            {
                if (mAttackStyleList != null && mAttackStyleList.index >= 0 && mAttackStyleList.index < AttackStyles.Count)
                {
                    AttackStyle lAttackStyle = AttackStyles[mAttackStyleList.index];

                    float lMinReach = (lAttackStyle.MinRange > 0f ? lAttackStyle.MinRange : lCombatant.MinMeleeReach + 0f);
                    float lMaxReach = (lAttackStyle.MaxRange > 0f ? lAttackStyle.MaxRange : lCombatant.MaxMeleeReach + 1f);
                    Graphics.GraphicsManager.DrawSolidFrustum(lCombatant.CombatOrigin, lCombatant.transform.rotation * Quaternion.LookRotation(lAttackStyle.Forward, lCombatant.transform.up), lAttackStyle.HorizontalFOA, lAttackStyle.VerticalFOA, lMinReach, lMaxReach, Color.gray);
                }
            }
        }

        #region Attack Styles

        /// <summary>
        /// Create the reorderable list
        /// </summary>
        private void InstantiateAttackStyleList()
        {
            if (mAttackStyleList != null)
            {
                mAttackStyleList.drawHeaderCallback -= DrawAttackStyleListHeader;
                mAttackStyleList.drawFooterCallback -= DrawAttackStyleListFooter;
                mAttackStyleList.drawElementCallback -= DrawAttackStyleListItem;
                mAttackStyleList.onAddCallback -= OnAttackStyleListItemAdd;
                mAttackStyleList.onRemoveCallback -= OnAttackStyleListItemRemove;
                mAttackStyleList.onSelectCallback -= OnAttackStyleListItemSelect;
                mAttackStyleList.onReorderCallback -= OnAttackStyleListReorder;
                mAttackStyleList.list.Clear();
            }

            mAttackStyleList = new ReorderableList(AttackStyles, typeof(AttackStyle), true, true, true, true);
            mAttackStyleList.drawHeaderCallback += DrawAttackStyleListHeader;
            mAttackStyleList.drawFooterCallback += DrawAttackStyleListFooter;
            mAttackStyleList.drawElementCallback += DrawAttackStyleListItem;
            mAttackStyleList.onAddCallback += OnAttackStyleListItemAdd;
            mAttackStyleList.onRemoveCallback += OnAttackStyleListItemRemove;
            mAttackStyleList.onSelectCallback += OnAttackStyleListItemSelect;
            mAttackStyleList.onReorderCallback += OnAttackStyleListReorder;
            mAttackStyleList.headerHeight = 18f;
            mAttackStyleList.footerHeight = 17f;

            if (mEditorAttackStyleIndex >= 0 && mEditorAttackStyleIndex < mAttackStyleList.count)
            {
                mAttackStyleList.index = mEditorAttackStyleIndex;

                SceneView.RepaintAll();
                InternalEditorUtility.RepaintAllViews();
            }
        }

        /// <summary>
        /// Header for the list
        /// </summary>
        /// <param name="rRect"></param>
        private void DrawAttackStyleListHeader(Rect rRect)
        {
            EditorGUI.LabelField(rRect, "Attack Styles");

            Rect lClickRect = new Rect(30f, rRect.y, rRect.width - 45f, rRect.height);
            if (GUI.Button(lClickRect, "", EditorStyles.label))
            {
                mAttackStyleList.index = -1;
                OnAttackStyleListItemSelect(mAttackStyleList);
            }

            Rect lResetRect = new Rect(lClickRect.x + lClickRect.width + 3f, rRect.y + 2f, 45f, rRect.height - 3f);
            if (GUI.Button(lResetRect, "Reset", EditorStyles.miniButton))
            {
                mIsDirty = true;
                mAttackStyleList.index = -1;
                AttackStyleListReset();
            }

            //Rect lNoteRect = new Rect(rRect.width + 19f, rRect.y, 11f, rRect.height);
            //EditorGUI.LabelField(lNoteRect, "X", EditorStyles.miniLabel);
        }

        /// <summary>
        /// Allows us to draw each AttackStyle in the list
        /// </summary>
        /// <param name="rRect"></param>
        /// <param name="rIndex"></param>
        /// <param name="rIsActive"></param>
        /// <param name="rIsFocused"></param>
        private void DrawAttackStyleListItem(Rect rRect, int rIndex, bool rIsActive, bool rIsFocused)
        {
            if (rIndex < AttackStyles.Count)
            {
                AttackStyle lAttackStyle = AttackStyles[rIndex];

                rRect.y += 2;

                Rect lItemRect = new Rect(rRect.x, rRect.y, rRect.width, EditorGUIUtility.singleLineHeight);
                EditorGUI.LabelField(lItemRect, lAttackStyle.Name);
            }
        }

        /// <summary>
        /// Footer for the list
        /// </summary>
        /// <param name="rRect"></param>
        private void DrawAttackStyleListFooter(Rect rRect)
        {
            Rect lAddRect = new Rect(rRect.x + rRect.width - 28 - 28 - 1, rRect.y + 1, 28, 15);
            if (GUI.Button(lAddRect, new GUIContent("+", "Add Style."), EditorStyles.miniButtonLeft)) { OnAttackStyleListItemAdd(mAttackStyleList); }

            Rect lDeleteRect = new Rect(lAddRect.x + lAddRect.width, lAddRect.y, 28, 15);
            if (GUI.Button(lDeleteRect, new GUIContent("-", "Delete Style."), EditorStyles.miniButtonRight)) { OnAttackStyleListItemRemove(mAttackStyleList); };
        }

        /// <summary>
        /// Allows us to add to a list
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListItemAdd(ReorderableList rList)
        {
            AttackStyle lAttackStyle = new AttackStyle();
            //lAttackStyle.ParameterID = AttackStyles.Count;

            AttackStyles.Add(lAttackStyle);

            mAttackStyleList.index = AttackStyles.Count - 1;
            OnAttackStyleListItemSelect(rList);

            mIsDirty = true;
        }

        /// <summary>
        /// Allows us process when a list is selected
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListItemSelect(ReorderableList rList)
        {
            mEditorAttackStyleIndex = rList.index;

            SceneView.RepaintAll();
            InternalEditorUtility.RepaintAllViews();
        }

        /// <summary>
        /// Allows us to stop before removing the item
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListItemRemove(ReorderableList rList)
        {
            if (EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete the item?", "Yes", "No"))
            {
                int rIndex = rList.index;

                rList.index--;
                AttackStyles.RemoveAt(rIndex);

                OnAttackStyleListItemSelect(rList);

                mIsDirty = true;
            }
        }

        /// <summary>
        /// Allows us to process after the motions are reordered
        /// </summary>
        /// <param name="rList"></param>
        private void OnAttackStyleListReorder(ReorderableList rList)
        {
            mIsDirty = true;
        }

        /// <summary>
        /// Renders the currently selected step
        /// </summary>
        /// <param name="rStep"></param>
        private bool DrawAttackStyleDetailItem(AttackStyle rAttackStyle)
        {
            bool lIsDirty = false;

            EditorHelper.DrawSmallTitle(rAttackStyle.Name.Length > 0 ? rAttackStyle.Name : "Attack Style");

            if (EditorHelper.TextField("Name", "ID of the attack style.", rAttackStyle.Name, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Name = EditorHelper.FieldStringValue;
            }

            if (EditorHelper.TextField("Item Type", "Type of item this attack style is valid for.", rAttackStyle.ItemType, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.ItemType = EditorHelper.FieldStringValue;
            }

            if (EditorHelper.IntField("Form", "Motion form used to determine the animation to play.", rAttackStyle.Form, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Form = EditorHelper.FieldIntValue;
            }

            if (EditorHelper.IntField("Parameter", "Parameter value sent to the animator to help determine the animation to play.", rAttackStyle.Parameter, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Parameter = EditorHelper.FieldIntValue;
            }

            GUILayout.Space(5f);

            if (EditorHelper.TextField("Slot ID", "Inventory slot ID that defines the weapon used for the attack.", rAttackStyle.InventorySlotID, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.InventorySlotID = EditorHelper.FieldStringValue;
            }

            if (EditorHelper.FloatField("Delay", "Time (in seconds) before the attack can be used again.", rAttackStyle.Delay, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Delay = EditorHelper.FieldFloatValue;
            }

            if (EditorHelper.BoolField("Is Blockable", "Determines if the attack can be blocked", rAttackStyle.IsInterruptible, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.IsInterruptible = EditorHelper.FieldBoolValue;
            }

            GUILayout.Space(5f);

            if (EditorHelper.Vector3Field("Attack Forward", "Normalized center of the attack. FOA values are based on this.", rAttackStyle.Forward, mMotionController))
            {
                lIsDirty = true;
                rAttackStyle.Forward = EditorHelper.FieldVector3Value.normalized;
            }

            EditorGUILayout.BeginHorizontal();

            EditorHelper.LabelField("Field of Attack", "Horizontal and vertical field of attack when colliders are not used.", EditorGUIUtility.labelWidth - 4f);

            if (EditorHelper.FloatField(rAttackStyle.HorizontalFOA, "Horizontal FOA", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.HorizontalFOA = EditorHelper.FieldFloatValue;
            }

            if (EditorHelper.FloatField(rAttackStyle.VerticalFOA, "Vertical FOA", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.VerticalFOA = EditorHelper.FieldFloatValue;
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorHelper.LabelField("Range", "Min and Max range of the attack. Set to 0 to use combatant + weapon range.", EditorGUIUtility.labelWidth - 4f);

            if (EditorHelper.FloatField(rAttackStyle.MinRange, "Min Range", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.MinRange = EditorHelper.FieldFloatValue;
            }

            if (EditorHelper.FloatField(rAttackStyle.MaxRange, "Max Range", mMotionController, 0f, 31f))
            {
                mIsDirty = true;
                rAttackStyle.MaxRange = EditorHelper.FieldFloatValue;
            }

            EditorGUILayout.EndHorizontal();

            if (EditorHelper.FloatField("Damage Modifier", "Multiplier that this style applies to the weapon's damage.", rAttackStyle.DamageModifier, mMotionController))
            {
                mIsDirty = true;
                rAttackStyle.DamageModifier = EditorHelper.FieldFloatValue;
            }

            if (EditorHelper.IntField("Next Attack Style", "When chaining attacks, the next attack style that we'll default to.", rAttackStyle.NextAttackStyleIndex, mMotionController))
            {
                mIsDirty = true;
                rAttackStyle.NextAttackStyleIndex = EditorHelper.FieldIntValue;
            }

            if (lIsDirty)
            {
                SceneView.RepaintAll();
                InternalEditorUtility.RepaintAllViews();
            }

            return lIsDirty;
        }

        /// <summary>
        /// Reset to default values. 
        /// </summary>
        public void AttackStyleListReset()
        {
            AttackStyles.Clear();

            AttackStyle lStyle = new AttackStyle();
            lStyle.Name = "Forward Slash";
            //lStyle.ParameterID = 0;
            lStyle.Form = 100;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 70f;
            lStyle.VerticalFOA = 40f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            lStyle.NextAttackStyleIndex = 1;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Back Slash";
            //lStyle.ParameterID = 1;
            lStyle.Form = 101;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 160f;
            lStyle.VerticalFOA = 30f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            lStyle.NextAttackStyleIndex = 0;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Spin Slash";
            //lStyle.ParameterID = 2;
            lStyle.Form = 102;
            lStyle.Forward = new Vector3(0f, -0.24f, 0.968f);
            lStyle.HorizontalFOA = 210f;
            lStyle.VerticalFOA = 20f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Low Forward Slash";
            //lStyle.ParameterID = 3;
            lStyle.Form = 103;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 120f;
            lStyle.VerticalFOA = 30f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Pommel Blash";
            //lStyle.ParameterID = 4;
            lStyle.Form = 104;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 30f;
            lStyle.VerticalFOA = 30f;
            lStyle.MinRange = 0.1f;
            lStyle.MaxRange = 1.0f;
            lStyle.DamageModifier = 0.5f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Jump Slash";
            //lStyle.ParameterID = 5;
            lStyle.Form = 105;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 20f;
            lStyle.VerticalFOA = 50f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = true;
            AttackStyles.Add(lStyle);

            lStyle = new AttackStyle();
            lStyle.Name = "Jump Spin Slash";
            //lStyle.ParameterID = 6;
            lStyle.Form = 106;
            lStyle.Forward = Vector3.forward;
            lStyle.HorizontalFOA = 70f;
            lStyle.VerticalFOA = 40f;
            lStyle.DamageModifier = 1f;
            lStyle.IsInterruptible = false;
            AttackStyles.Add(lStyle);
        }

        #endregion

        #endregion

#endif

        #region Auto-Generated
        // ************************************ START AUTO GENERATED ************************************

        /// <summary>
        /// These declarations go inside the class so you can test for which state
        /// and transitions are active. Testing hash values is much faster than strings.
        /// </summary>
        public int STATE_Start = -1;
        public int STATE_Cross_100 = -1;
        public int STATE_LUppercut_101 = -1;
        public int STATE_RHook_102 = -1;
        public int STATE_Idle = -1;
        public int STATE_LPushKick_105 = -1;
        public int STATE_Overhand = -1;
        public int STATE_RAxe_99 = -1;
        public int STATE_LKnee_110 = -1;
        public int STATE_RRabbitKick_111 = -1;
        public int STATE_RLowSweep_112 = -1;
        public int STATE_RMidRound_113 = -1;
        public int STATE_KB_m_HighKickRound_R_1 = -1;
        public int STATE_KB_m_SpinningHeelRight = -1;
        public int STATE_KB_Superpunch = -1;
        public int STATE_KB_m_KickUppercut_R = -1;
        public int STATE_KB_m_BackfistRound_L = -1;
        public int STATE_standing_melee_attack_360_high = -1;
        public int STATE_standing_melee_attack_backhand = -1;
        public int STATE_standing_melee_combo_attack_ver_2 = -1;
        public int STATE_standing_melee_combo_attack_ver_1 = -1;
        public int STATE_standing_melee_run_jump_attack = -1;
        public int STATE_Pointing = -1;
        public int STATE_NoNoNo = -1;
        public int STATE_StandingClap = -1;
        public int STATE_Victory = -1;
        public int STATE_AwwwYeah = -1;
        public int TRANS_AnyState_Cross_100 = -1;
        public int TRANS_EntryState_Cross_100 = -1;
        public int TRANS_AnyState_LUppercut_101 = -1;
        public int TRANS_EntryState_LUppercut_101 = -1;
        public int TRANS_AnyState_RHook_102 = -1;
        public int TRANS_EntryState_RHook_102 = -1;
        public int TRANS_AnyState_LPushKick_105 = -1;
        public int TRANS_EntryState_LPushKick_105 = -1;
        public int TRANS_AnyState_Overhand = -1;
        public int TRANS_EntryState_Overhand = -1;
        public int TRANS_AnyState_RAxe_99 = -1;
        public int TRANS_EntryState_RAxe_99 = -1;
        public int TRANS_AnyState_RRabbitKick_111 = -1;
        public int TRANS_EntryState_RRabbitKick_111 = -1;
        public int TRANS_AnyState_RLowSweep_112 = -1;
        public int TRANS_EntryState_RLowSweep_112 = -1;
        public int TRANS_AnyState_RMidRound_113 = -1;
        public int TRANS_EntryState_RMidRound_113 = -1;
        public int TRANS_AnyState_LKnee_110 = -1;
        public int TRANS_EntryState_LKnee_110 = -1;
        public int TRANS_AnyState_KB_m_KickUppercut_R = -1;
        public int TRANS_EntryState_KB_m_KickUppercut_R = -1;
        public int TRANS_AnyState_KB_m_HighKickRound_R_1 = -1;
        public int TRANS_EntryState_KB_m_HighKickRound_R_1 = -1;
        public int TRANS_AnyState_KB_m_SpinningHeelRight = -1;
        public int TRANS_EntryState_KB_m_SpinningHeelRight = -1;
        public int TRANS_AnyState_KB_Superpunch = -1;
        public int TRANS_EntryState_KB_Superpunch = -1;
        public int TRANS_AnyState_KB_m_BackfistRound_L = -1;
        public int TRANS_EntryState_KB_m_BackfistRound_L = -1;
        public int TRANS_AnyState_standing_melee_attack_360_high = -1;
        public int TRANS_EntryState_standing_melee_attack_360_high = -1;
        public int TRANS_AnyState_standing_melee_attack_backhand = -1;
        public int TRANS_EntryState_standing_melee_attack_backhand = -1;
        public int TRANS_AnyState_standing_melee_combo_attack_ver_2 = -1;
        public int TRANS_EntryState_standing_melee_combo_attack_ver_2 = -1;
        public int TRANS_AnyState_standing_melee_combo_attack_ver_1 = -1;
        public int TRANS_EntryState_standing_melee_combo_attack_ver_1 = -1;
        public int TRANS_AnyState_standing_melee_run_jump_attack = -1;
        public int TRANS_EntryState_standing_melee_run_jump_attack = -1;
        public int TRANS_Cross_100_Idle = -1;
        public int TRANS_LUppercut_101_Idle = -1;
        public int TRANS_RHook_102_Idle = -1;
        public int TRANS_LPushKick_105_Idle = -1;
        public int TRANS_Overhand_Idle = -1;
        public int TRANS_RAxe_99_Idle = -1;
        public int TRANS_LKnee_110_Idle = -1;
        public int TRANS_RRabbitKick_111_Idle = -1;
        public int TRANS_RLowSweep_112_Idle = -1;
        public int TRANS_RMidRound_113_Idle = -1;
        public int TRANS_KB_m_HighKickRound_R_1_AwwwYeah = -1;
        public int TRANS_KB_m_SpinningHeelRight_StandingClap = -1;
        public int TRANS_KB_Superpunch_Pointing = -1;
        public int TRANS_KB_m_KickUppercut_R_NoNoNo = -1;
        public int TRANS_KB_m_BackfistRound_L_Victory = -1;
        public int TRANS_standing_melee_attack_360_high_Idle = -1;
        public int TRANS_standing_melee_attack_backhand_Idle = -1;
        public int TRANS_standing_melee_combo_attack_ver_2_Idle = -1;
        public int TRANS_standing_melee_combo_attack_ver_1_Idle = -1;
        public int TRANS_standing_melee_run_jump_attack_Idle = -1;
        public int TRANS_Pointing_Idle = -1;
        public int TRANS_NoNoNo_Idle = -1;
        public int TRANS_StandingClap_Idle = -1;
        public int TRANS_Victory_Idle = -1;
        public int TRANS_AwwwYeah_Idle = -1;

        /// <summary>
        /// Determines if we're using auto-generated code
        /// </summary>
        public override bool HasAutoGeneratedCode
        {
            get { return true; }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsInMotionState
        {
            get
            {
                int lStateID = mMotionLayer._AnimatorStateID;
                int lTransitionID = mMotionLayer._AnimatorTransitionID;

                if (lTransitionID == 0)
                {
                    if (lStateID == STATE_Start) { return true; }
                    if (lStateID == STATE_Cross_100) { return true; }
                    if (lStateID == STATE_LUppercut_101) { return true; }
                    if (lStateID == STATE_RHook_102) { return true; }
                    if (lStateID == STATE_Idle) { return true; }
                    if (lStateID == STATE_LPushKick_105) { return true; }
                    if (lStateID == STATE_Overhand) { return true; }
                    if (lStateID == STATE_RAxe_99) { return true; }
                    if (lStateID == STATE_LKnee_110) { return true; }
                    if (lStateID == STATE_RRabbitKick_111) { return true; }
                    if (lStateID == STATE_RLowSweep_112) { return true; }
                    if (lStateID == STATE_RMidRound_113) { return true; }
                    if (lStateID == STATE_KB_m_HighKickRound_R_1) { return true; }
                    if (lStateID == STATE_KB_m_SpinningHeelRight) { return true; }
                    if (lStateID == STATE_KB_Superpunch) { return true; }
                    if (lStateID == STATE_KB_m_KickUppercut_R) { return true; }
                    if (lStateID == STATE_KB_m_BackfistRound_L) { return true; }
                    if (lStateID == STATE_standing_melee_attack_360_high) { return true; }
                    if (lStateID == STATE_standing_melee_attack_backhand) { return true; }
                    if (lStateID == STATE_standing_melee_combo_attack_ver_2) { return true; }
                    if (lStateID == STATE_standing_melee_combo_attack_ver_1) { return true; }
                    if (lStateID == STATE_standing_melee_run_jump_attack) { return true; }
                    if (lStateID == STATE_Pointing) { return true; }
                    if (lStateID == STATE_NoNoNo) { return true; }
                    if (lStateID == STATE_StandingClap) { return true; }
                    if (lStateID == STATE_Victory) { return true; }
                    if (lStateID == STATE_AwwwYeah) { return true; }
                }

                if (lTransitionID == TRANS_AnyState_Cross_100) { return true; }
                if (lTransitionID == TRANS_EntryState_Cross_100) { return true; }
                if (lTransitionID == TRANS_AnyState_LUppercut_101) { return true; }
                if (lTransitionID == TRANS_EntryState_LUppercut_101) { return true; }
                if (lTransitionID == TRANS_AnyState_RHook_102) { return true; }
                if (lTransitionID == TRANS_EntryState_RHook_102) { return true; }
                if (lTransitionID == TRANS_AnyState_LPushKick_105) { return true; }
                if (lTransitionID == TRANS_EntryState_LPushKick_105) { return true; }
                if (lTransitionID == TRANS_AnyState_Overhand) { return true; }
                if (lTransitionID == TRANS_EntryState_Overhand) { return true; }
                if (lTransitionID == TRANS_AnyState_RAxe_99) { return true; }
                if (lTransitionID == TRANS_EntryState_RAxe_99) { return true; }
                if (lTransitionID == TRANS_AnyState_RRabbitKick_111) { return true; }
                if (lTransitionID == TRANS_EntryState_RRabbitKick_111) { return true; }
                if (lTransitionID == TRANS_AnyState_RLowSweep_112) { return true; }
                if (lTransitionID == TRANS_EntryState_RLowSweep_112) { return true; }
                if (lTransitionID == TRANS_AnyState_RMidRound_113) { return true; }
                if (lTransitionID == TRANS_EntryState_RMidRound_113) { return true; }
                if (lTransitionID == TRANS_AnyState_LKnee_110) { return true; }
                if (lTransitionID == TRANS_EntryState_LKnee_110) { return true; }
                if (lTransitionID == TRANS_AnyState_KB_m_KickUppercut_R) { return true; }
                if (lTransitionID == TRANS_EntryState_KB_m_KickUppercut_R) { return true; }
                if (lTransitionID == TRANS_AnyState_KB_m_HighKickRound_R_1) { return true; }
                if (lTransitionID == TRANS_EntryState_KB_m_HighKickRound_R_1) { return true; }
                if (lTransitionID == TRANS_AnyState_KB_m_SpinningHeelRight) { return true; }
                if (lTransitionID == TRANS_EntryState_KB_m_SpinningHeelRight) { return true; }
                if (lTransitionID == TRANS_AnyState_KB_Superpunch) { return true; }
                if (lTransitionID == TRANS_EntryState_KB_Superpunch) { return true; }
                if (lTransitionID == TRANS_AnyState_KB_m_BackfistRound_L) { return true; }
                if (lTransitionID == TRANS_EntryState_KB_m_BackfistRound_L) { return true; }
                if (lTransitionID == TRANS_AnyState_standing_melee_attack_360_high) { return true; }
                if (lTransitionID == TRANS_EntryState_standing_melee_attack_360_high) { return true; }
                if (lTransitionID == TRANS_AnyState_standing_melee_attack_backhand) { return true; }
                if (lTransitionID == TRANS_EntryState_standing_melee_attack_backhand) { return true; }
                if (lTransitionID == TRANS_AnyState_standing_melee_combo_attack_ver_2) { return true; }
                if (lTransitionID == TRANS_EntryState_standing_melee_combo_attack_ver_2) { return true; }
                if (lTransitionID == TRANS_AnyState_standing_melee_combo_attack_ver_1) { return true; }
                if (lTransitionID == TRANS_EntryState_standing_melee_combo_attack_ver_1) { return true; }
                if (lTransitionID == TRANS_AnyState_standing_melee_run_jump_attack) { return true; }
                if (lTransitionID == TRANS_EntryState_standing_melee_run_jump_attack) { return true; }
                if (lTransitionID == TRANS_AnyState_standing_melee_run_jump_attack) { return true; }
                if (lTransitionID == TRANS_EntryState_standing_melee_run_jump_attack) { return true; }
                if (lTransitionID == TRANS_Cross_100_Idle) { return true; }
                if (lTransitionID == TRANS_LUppercut_101_Idle) { return true; }
                if (lTransitionID == TRANS_RHook_102_Idle) { return true; }
                if (lTransitionID == TRANS_LPushKick_105_Idle) { return true; }
                if (lTransitionID == TRANS_Overhand_Idle) { return true; }
                if (lTransitionID == TRANS_RAxe_99_Idle) { return true; }
                if (lTransitionID == TRANS_LKnee_110_Idle) { return true; }
                if (lTransitionID == TRANS_RRabbitKick_111_Idle) { return true; }
                if (lTransitionID == TRANS_RLowSweep_112_Idle) { return true; }
                if (lTransitionID == TRANS_RMidRound_113_Idle) { return true; }
                if (lTransitionID == TRANS_KB_m_HighKickRound_R_1_AwwwYeah) { return true; }
                if (lTransitionID == TRANS_KB_m_SpinningHeelRight_StandingClap) { return true; }
                if (lTransitionID == TRANS_KB_Superpunch_Pointing) { return true; }
                if (lTransitionID == TRANS_KB_m_KickUppercut_R_NoNoNo) { return true; }
                if (lTransitionID == TRANS_KB_m_BackfistRound_L_Victory) { return true; }
                if (lTransitionID == TRANS_standing_melee_attack_360_high_Idle) { return true; }
                if (lTransitionID == TRANS_standing_melee_attack_backhand_Idle) { return true; }
                if (lTransitionID == TRANS_standing_melee_combo_attack_ver_2_Idle) { return true; }
                if (lTransitionID == TRANS_standing_melee_combo_attack_ver_1_Idle) { return true; }
                if (lTransitionID == TRANS_standing_melee_run_jump_attack_Idle) { return true; }
                if (lTransitionID == TRANS_Pointing_Idle) { return true; }
                if (lTransitionID == TRANS_NoNoNo_Idle) { return true; }
                if (lTransitionID == TRANS_StandingClap_Idle) { return true; }
                if (lTransitionID == TRANS_Victory_Idle) { return true; }
                if (lTransitionID == TRANS_AwwwYeah_Idle) { return true; }
                return false;
            }
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID)
        {
            if (rStateID == STATE_Start) { return true; }
            if (rStateID == STATE_Cross_100) { return true; }
            if (rStateID == STATE_LUppercut_101) { return true; }
            if (rStateID == STATE_RHook_102) { return true; }
            if (rStateID == STATE_Idle) { return true; }
            if (rStateID == STATE_LPushKick_105) { return true; }
            if (rStateID == STATE_Overhand) { return true; }
            if (rStateID == STATE_RAxe_99) { return true; }
            if (rStateID == STATE_LKnee_110) { return true; }
            if (rStateID == STATE_RRabbitKick_111) { return true; }
            if (rStateID == STATE_RLowSweep_112) { return true; }
            if (rStateID == STATE_RMidRound_113) { return true; }
            if (rStateID == STATE_KB_m_HighKickRound_R_1) { return true; }
            if (rStateID == STATE_KB_m_SpinningHeelRight) { return true; }
            if (rStateID == STATE_KB_Superpunch) { return true; }
            if (rStateID == STATE_KB_m_KickUppercut_R) { return true; }
            if (rStateID == STATE_KB_m_BackfistRound_L) { return true; }
            if (rStateID == STATE_standing_melee_attack_360_high) { return true; }
            if (rStateID == STATE_standing_melee_attack_backhand) { return true; }
            if (rStateID == STATE_standing_melee_combo_attack_ver_2) { return true; }
            if (rStateID == STATE_standing_melee_combo_attack_ver_1) { return true; }
            if (rStateID == STATE_standing_melee_run_jump_attack) { return true; }
            if (rStateID == STATE_Pointing) { return true; }
            if (rStateID == STATE_NoNoNo) { return true; }
            if (rStateID == STATE_StandingClap) { return true; }
            if (rStateID == STATE_Victory) { return true; }
            if (rStateID == STATE_AwwwYeah) { return true; }
            return false;
        }

        /// <summary>
        /// Used to determine if the actor is in one of the states for this motion
        /// </summary>
        /// <returns></returns>
        public override bool IsMotionState(int rStateID, int rTransitionID)
        {
            if (rTransitionID == 0)
            {
                if (rStateID == STATE_Start) { return true; }
                if (rStateID == STATE_Cross_100) { return true; }
                if (rStateID == STATE_LUppercut_101) { return true; }
                if (rStateID == STATE_RHook_102) { return true; }
                if (rStateID == STATE_Idle) { return true; }
                if (rStateID == STATE_LPushKick_105) { return true; }
                if (rStateID == STATE_Overhand) { return true; }
                if (rStateID == STATE_RAxe_99) { return true; }
                if (rStateID == STATE_LKnee_110) { return true; }
                if (rStateID == STATE_RRabbitKick_111) { return true; }
                if (rStateID == STATE_RLowSweep_112) { return true; }
                if (rStateID == STATE_RMidRound_113) { return true; }
                if (rStateID == STATE_KB_m_HighKickRound_R_1) { return true; }
                if (rStateID == STATE_KB_m_SpinningHeelRight) { return true; }
                if (rStateID == STATE_KB_Superpunch) { return true; }
                if (rStateID == STATE_KB_m_KickUppercut_R) { return true; }
                if (rStateID == STATE_KB_m_BackfistRound_L) { return true; }
                if (rStateID == STATE_standing_melee_attack_360_high) { return true; }
                if (rStateID == STATE_standing_melee_attack_backhand) { return true; }
                if (rStateID == STATE_standing_melee_combo_attack_ver_2) { return true; }
                if (rStateID == STATE_standing_melee_combo_attack_ver_1) { return true; }
                if (rStateID == STATE_standing_melee_run_jump_attack) { return true; }
                if (rStateID == STATE_Pointing) { return true; }
                if (rStateID == STATE_NoNoNo) { return true; }
                if (rStateID == STATE_StandingClap) { return true; }
                if (rStateID == STATE_Victory) { return true; }
                if (rStateID == STATE_AwwwYeah) { return true; }
            }

            if (rTransitionID == TRANS_AnyState_Cross_100) { return true; }
            if (rTransitionID == TRANS_EntryState_Cross_100) { return true; }
            if (rTransitionID == TRANS_AnyState_LUppercut_101) { return true; }
            if (rTransitionID == TRANS_EntryState_LUppercut_101) { return true; }
            if (rTransitionID == TRANS_AnyState_RHook_102) { return true; }
            if (rTransitionID == TRANS_EntryState_RHook_102) { return true; }
            if (rTransitionID == TRANS_AnyState_LPushKick_105) { return true; }
            if (rTransitionID == TRANS_EntryState_LPushKick_105) { return true; }
            if (rTransitionID == TRANS_AnyState_Overhand) { return true; }
            if (rTransitionID == TRANS_EntryState_Overhand) { return true; }
            if (rTransitionID == TRANS_AnyState_RAxe_99) { return true; }
            if (rTransitionID == TRANS_EntryState_RAxe_99) { return true; }
            if (rTransitionID == TRANS_AnyState_RRabbitKick_111) { return true; }
            if (rTransitionID == TRANS_EntryState_RRabbitKick_111) { return true; }
            if (rTransitionID == TRANS_AnyState_RLowSweep_112) { return true; }
            if (rTransitionID == TRANS_EntryState_RLowSweep_112) { return true; }
            if (rTransitionID == TRANS_AnyState_RMidRound_113) { return true; }
            if (rTransitionID == TRANS_EntryState_RMidRound_113) { return true; }
            if (rTransitionID == TRANS_AnyState_LKnee_110) { return true; }
            if (rTransitionID == TRANS_EntryState_LKnee_110) { return true; }
            if (rTransitionID == TRANS_AnyState_KB_m_KickUppercut_R) { return true; }
            if (rTransitionID == TRANS_EntryState_KB_m_KickUppercut_R) { return true; }
            if (rTransitionID == TRANS_AnyState_KB_m_HighKickRound_R_1) { return true; }
            if (rTransitionID == TRANS_EntryState_KB_m_HighKickRound_R_1) { return true; }
            if (rTransitionID == TRANS_AnyState_KB_m_SpinningHeelRight) { return true; }
            if (rTransitionID == TRANS_EntryState_KB_m_SpinningHeelRight) { return true; }
            if (rTransitionID == TRANS_AnyState_KB_Superpunch) { return true; }
            if (rTransitionID == TRANS_EntryState_KB_Superpunch) { return true; }
            if (rTransitionID == TRANS_AnyState_KB_m_BackfistRound_L) { return true; }
            if (rTransitionID == TRANS_EntryState_KB_m_BackfistRound_L) { return true; }
            if (rTransitionID == TRANS_AnyState_standing_melee_attack_360_high) { return true; }
            if (rTransitionID == TRANS_EntryState_standing_melee_attack_360_high) { return true; }
            if (rTransitionID == TRANS_AnyState_standing_melee_attack_backhand) { return true; }
            if (rTransitionID == TRANS_EntryState_standing_melee_attack_backhand) { return true; }
            if (rTransitionID == TRANS_AnyState_standing_melee_combo_attack_ver_2) { return true; }
            if (rTransitionID == TRANS_EntryState_standing_melee_combo_attack_ver_2) { return true; }
            if (rTransitionID == TRANS_AnyState_standing_melee_combo_attack_ver_1) { return true; }
            if (rTransitionID == TRANS_EntryState_standing_melee_combo_attack_ver_1) { return true; }
            if (rTransitionID == TRANS_AnyState_standing_melee_run_jump_attack) { return true; }
            if (rTransitionID == TRANS_EntryState_standing_melee_run_jump_attack) { return true; }
            if (rTransitionID == TRANS_AnyState_standing_melee_run_jump_attack) { return true; }
            if (rTransitionID == TRANS_EntryState_standing_melee_run_jump_attack) { return true; }
            if (rTransitionID == TRANS_Cross_100_Idle) { return true; }
            if (rTransitionID == TRANS_LUppercut_101_Idle) { return true; }
            if (rTransitionID == TRANS_RHook_102_Idle) { return true; }
            if (rTransitionID == TRANS_LPushKick_105_Idle) { return true; }
            if (rTransitionID == TRANS_Overhand_Idle) { return true; }
            if (rTransitionID == TRANS_RAxe_99_Idle) { return true; }
            if (rTransitionID == TRANS_LKnee_110_Idle) { return true; }
            if (rTransitionID == TRANS_RRabbitKick_111_Idle) { return true; }
            if (rTransitionID == TRANS_RLowSweep_112_Idle) { return true; }
            if (rTransitionID == TRANS_RMidRound_113_Idle) { return true; }
            if (rTransitionID == TRANS_KB_m_HighKickRound_R_1_AwwwYeah) { return true; }
            if (rTransitionID == TRANS_KB_m_SpinningHeelRight_StandingClap) { return true; }
            if (rTransitionID == TRANS_KB_Superpunch_Pointing) { return true; }
            if (rTransitionID == TRANS_KB_m_KickUppercut_R_NoNoNo) { return true; }
            if (rTransitionID == TRANS_KB_m_BackfistRound_L_Victory) { return true; }
            if (rTransitionID == TRANS_standing_melee_attack_360_high_Idle) { return true; }
            if (rTransitionID == TRANS_standing_melee_attack_backhand_Idle) { return true; }
            if (rTransitionID == TRANS_standing_melee_combo_attack_ver_2_Idle) { return true; }
            if (rTransitionID == TRANS_standing_melee_combo_attack_ver_1_Idle) { return true; }
            if (rTransitionID == TRANS_standing_melee_run_jump_attack_Idle) { return true; }
            if (rTransitionID == TRANS_Pointing_Idle) { return true; }
            if (rTransitionID == TRANS_NoNoNo_Idle) { return true; }
            if (rTransitionID == TRANS_StandingClap_Idle) { return true; }
            if (rTransitionID == TRANS_Victory_Idle) { return true; }
            if (rTransitionID == TRANS_AwwwYeah_Idle) { return true; }
            return false;
        }

        /// <summary>
        /// Preprocess any animator data so the motion can use it later
        /// </summary>
        public override void LoadAnimatorData()
        {
            string lLayer = mMotionController.Animator.GetLayerName(mMotionLayer._AnimatorLayerIndex);
            TRANS_AnyState_Cross_100 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.Cross_100");
            TRANS_EntryState_Cross_100 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.Cross_100");
            TRANS_AnyState_LUppercut_101 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.LUppercut_101");
            TRANS_EntryState_LUppercut_101 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.LUppercut_101");
            TRANS_AnyState_RHook_102 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.RHook_102");
            TRANS_EntryState_RHook_102 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.RHook_102");
            TRANS_AnyState_LPushKick_105 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.LPushKick_105");
            TRANS_EntryState_LPushKick_105 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.LPushKick_105");
            TRANS_AnyState_Overhand = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.Overhand");
            TRANS_EntryState_Overhand = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.Overhand");
            TRANS_AnyState_RAxe_99 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.RAxe_99");
            TRANS_EntryState_RAxe_99 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.RAxe_99");
            TRANS_AnyState_RRabbitKick_111 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.RRabbitKick_111");
            TRANS_EntryState_RRabbitKick_111 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.RRabbitKick_111");
            TRANS_AnyState_RLowSweep_112 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.RLowSweep_112");
            TRANS_EntryState_RLowSweep_112 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.RLowSweep_112");
            TRANS_AnyState_RMidRound_113 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.RMidRound_113");
            TRANS_EntryState_RMidRound_113 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.RMidRound_113");
            TRANS_AnyState_LKnee_110 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.LKnee_110");
            TRANS_EntryState_LKnee_110 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.LKnee_110");
            TRANS_AnyState_KB_m_KickUppercut_R = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_KickUppercut_R");
            TRANS_EntryState_KB_m_KickUppercut_R = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_KickUppercut_R");
            TRANS_AnyState_KB_m_HighKickRound_R_1 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_HighKickRound_R_1");
            TRANS_EntryState_KB_m_HighKickRound_R_1 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_HighKickRound_R_1");
            TRANS_AnyState_KB_m_SpinningHeelRight = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_SpinningHeelRight");
            TRANS_EntryState_KB_m_SpinningHeelRight = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_SpinningHeelRight");
            TRANS_AnyState_KB_Superpunch = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.KB_Superpunch");
            TRANS_EntryState_KB_Superpunch = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.KB_Superpunch");
            TRANS_AnyState_KB_m_BackfistRound_L = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_BackfistRound_L");
            TRANS_EntryState_KB_m_BackfistRound_L = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.KB_m_BackfistRound_L");
            TRANS_AnyState_standing_melee_attack_360_high = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_360_high");
            TRANS_EntryState_standing_melee_attack_360_high = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_360_high");
            TRANS_AnyState_standing_melee_attack_backhand = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_backhand");
            TRANS_EntryState_standing_melee_attack_backhand = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_backhand");
            TRANS_AnyState_standing_melee_combo_attack_ver_2 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_2");
            TRANS_EntryState_standing_melee_combo_attack_ver_2 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_2");
            TRANS_AnyState_standing_melee_combo_attack_ver_1 = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_1");
            TRANS_EntryState_standing_melee_combo_attack_ver_1 = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_1");
            TRANS_AnyState_standing_melee_run_jump_attack = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_run_jump_attack");
            TRANS_EntryState_standing_melee_run_jump_attack = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_run_jump_attack");
            TRANS_AnyState_standing_melee_run_jump_attack = mMotionController.AddAnimatorName("AnyState -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_run_jump_attack");
            TRANS_EntryState_standing_melee_run_jump_attack = mMotionController.AddAnimatorName("Entry -> " + lLayer + ".BasicMeleeAttack-SM.standing_melee_run_jump_attack");
            STATE_Start = mMotionController.AddAnimatorName("" + lLayer + ".Start");
            STATE_Cross_100 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Cross_100");
            TRANS_Cross_100_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Cross_100 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_LUppercut_101 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.LUppercut_101");
            TRANS_LUppercut_101_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.LUppercut_101 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_RHook_102 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RHook_102");
            TRANS_RHook_102_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RHook_102 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_LPushKick_105 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.LPushKick_105");
            TRANS_LPushKick_105_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.LPushKick_105 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_Overhand = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Overhand");
            TRANS_Overhand_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Overhand -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_RAxe_99 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RAxe_99");
            TRANS_RAxe_99_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RAxe_99 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_LKnee_110 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.LKnee_110");
            TRANS_LKnee_110_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.LKnee_110 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_RRabbitKick_111 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RRabbitKick_111");
            TRANS_RRabbitKick_111_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RRabbitKick_111 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_RLowSweep_112 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RLowSweep_112");
            TRANS_RLowSweep_112_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RLowSweep_112 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_RMidRound_113 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RMidRound_113");
            TRANS_RMidRound_113_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.RMidRound_113 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_KB_m_HighKickRound_R_1 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_HighKickRound_R_1");
            TRANS_KB_m_HighKickRound_R_1_AwwwYeah = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_HighKickRound_R_1 -> " + lLayer + ".BasicMeleeAttack-SM.AwwwYeah");
            STATE_KB_m_SpinningHeelRight = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_SpinningHeelRight");
            TRANS_KB_m_SpinningHeelRight_StandingClap = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_SpinningHeelRight -> " + lLayer + ".BasicMeleeAttack-SM.StandingClap");
            STATE_KB_Superpunch = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_Superpunch");
            TRANS_KB_Superpunch_Pointing = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_Superpunch -> " + lLayer + ".BasicMeleeAttack-SM.Pointing");
            STATE_KB_m_KickUppercut_R = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_KickUppercut_R");
            TRANS_KB_m_KickUppercut_R_NoNoNo = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_KickUppercut_R -> " + lLayer + ".BasicMeleeAttack-SM.NoNoNo");
            STATE_KB_m_BackfistRound_L = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_BackfistRound_L");
            TRANS_KB_m_BackfistRound_L_Victory = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.KB_m_BackfistRound_L -> " + lLayer + ".BasicMeleeAttack-SM.Victory");
            STATE_standing_melee_attack_360_high = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_360_high");
            TRANS_standing_melee_attack_360_high_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_360_high -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_standing_melee_attack_backhand = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_backhand");
            TRANS_standing_melee_attack_backhand_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_attack_backhand -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_standing_melee_combo_attack_ver_2 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_2");
            TRANS_standing_melee_combo_attack_ver_2_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_2 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_standing_melee_combo_attack_ver_1 = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_1");
            TRANS_standing_melee_combo_attack_ver_1_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_combo_attack_ver_1 -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_standing_melee_run_jump_attack = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_run_jump_attack");
            TRANS_standing_melee_run_jump_attack_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.standing_melee_run_jump_attack -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_Pointing = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Pointing");
            TRANS_Pointing_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Pointing -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_NoNoNo = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.NoNoNo");
            TRANS_NoNoNo_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.NoNoNo -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_StandingClap = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.StandingClap");
            TRANS_StandingClap_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.StandingClap -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_Victory = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Victory");
            TRANS_Victory_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.Victory -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
            STATE_AwwwYeah = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.AwwwYeah");
            TRANS_AwwwYeah_Idle = mMotionController.AddAnimatorName("" + lLayer + ".BasicMeleeAttack-SM.AwwwYeah -> " + lLayer + ".BasicMeleeAttack-SM.Idle");
        }

#if UNITY_EDITOR

        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
        protected override void CreateStateMachine()
        {
            int rLayerIndex = mMotionLayer._AnimatorLayerIndex;
            MotionController rMotionController = mMotionController;

            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_113762 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "BasicMeleeAttack-SM");
            if (lSSM_113762 == null) { lSSM_113762 = lLayerStateMachine.AddStateMachine("BasicMeleeAttack-SM", new Vector3(672, -420, 0)); }

            UnityEditor.Animations.AnimatorState lState_114302 = MotionControllerMotion.EditorFindState(lSSM_113762, "Cross_100");
            if (lState_114302 == null) { lState_114302 = lSSM_113762.AddState("Cross_100", new Vector3(336, 48, 0)); }
            lState_114302.speed = 1.2f;
            lState_114302.mirror = false;
            lState_114302.tag = "";
            lState_114302.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Jab_R");

            UnityEditor.Animations.AnimatorState lState_114304 = MotionControllerMotion.EditorFindState(lSSM_113762, "LUppercut_101");
            if (lState_114304 == null) { lState_114304 = lSSM_113762.AddState("LUppercut_101", new Vector3(336, 120, 0)); }
            lState_114304.speed = 1f;
            lState_114304.mirror = false;
            lState_114304.tag = "";
            lState_114304.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Uppercut_L");

            UnityEditor.Animations.AnimatorState lState_114306 = MotionControllerMotion.EditorFindState(lSSM_113762, "RHook_102");
            if (lState_114306 == null) { lState_114306 = lSSM_113762.AddState("RHook_102", new Vector3(336, 192, 0)); }
            lState_114306.speed = 1.5f;
            lState_114306.mirror = false;
            lState_114306.tag = "";
            lState_114306.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Hook_R");

            UnityEditor.Animations.AnimatorState lState_115416 = MotionControllerMotion.EditorFindState(lSSM_113762, "Idle");
            if (lState_115416 == null) { lState_115416 = lSSM_113762.AddState("Idle", new Vector3(24, -192, 0)); }
            lState_115416.speed = 1f;
            lState_115416.mirror = false;
            lState_115416.tag = "Exit";
            lState_115416.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Idle_2");

            UnityEditor.Animations.AnimatorState lState_114324 = MotionControllerMotion.EditorFindState(lSSM_113762, "LPushKick_105");
            if (lState_114324 == null) { lState_114324 = lSSM_113762.AddState("LPushKick_105", new Vector3(336, 372, 0)); }
            lState_114324.speed = 1.5f;
            lState_114324.mirror = false;
            lState_114324.tag = "";
            lState_114324.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Specials.fbx", "KB_FootShotgun");

            UnityEditor.Animations.AnimatorState lState_114326 = MotionControllerMotion.EditorFindState(lSSM_113762, "Overhand");
            if (lState_114326 == null) { lState_114326 = lSSM_113762.AddState("Overhand", new Vector3(336, 264, 0)); }
            lState_114326.speed = 1f;
            lState_114326.mirror = false;
            lState_114326.tag = "";
            lState_114326.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Overhand_R");

            UnityEditor.Animations.AnimatorState lState_114328 = MotionControllerMotion.EditorFindState(lSSM_113762, "RAxe_99");
            if (lState_114328 == null) { lState_114328 = lSSM_113762.AddState("RAxe_99", new Vector3(336, -24, 0)); }
            lState_114328.speed = 0.8f;
            lState_114328.mirror = false;
            lState_114328.tag = "";
            lState_114328.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_AxeKick");

            UnityEditor.Animations.AnimatorState lState_114336 = MotionControllerMotion.EditorFindState(lSSM_113762, "LKnee_110");
            if (lState_114336 == null) { lState_114336 = lSSM_113762.AddState("LKnee_110", new Vector3(600, 48, 0)); }
            lState_114336.speed = 1f;
            lState_114336.mirror = false;
            lState_114336.tag = "";
            lState_114336.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_KneeLeft");

            UnityEditor.Animations.AnimatorState lState_114330 = MotionControllerMotion.EditorFindState(lSSM_113762, "RRabbitKick_111");
            if (lState_114330 == null) { lState_114330 = lSSM_113762.AddState("RRabbitKick_111", new Vector3(600, 120, 0)); }
            lState_114330.speed = 1.5f;
            lState_114330.mirror = false;
            lState_114330.tag = "";
            lState_114330.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_MidRabbitKick_R");

            UnityEditor.Animations.AnimatorState lState_114332 = MotionControllerMotion.EditorFindState(lSSM_113762, "RLowSweep_112");
            if (lState_114332 == null) { lState_114332 = lSSM_113762.AddState("RLowSweep_112", new Vector3(600, 192, 0)); }
            lState_114332.speed = 1.2f;
            lState_114332.mirror = false;
            lState_114332.tag = "";
            lState_114332.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Crouched.fbx", "KB_crouch_m_LowKickRound_R");

            UnityEditor.Animations.AnimatorState lState_114334 = MotionControllerMotion.EditorFindState(lSSM_113762, "RMidRound_113");
            if (lState_114334 == null) { lState_114334 = lSSM_113762.AddState("RMidRound_113", new Vector3(600, 264, 0)); }
            lState_114334.speed = 1f;
            lState_114334.mirror = false;
            lState_114334.tag = "";
            lState_114334.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_MidKickRoud_R_1");

            UnityEditor.Animations.AnimatorState lState_114398 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_HighKickRound_R_1");
            if (lState_114398 == null) { lState_114398 = lSSM_113762.AddState("KB_m_HighKickRound_R_1", new Vector3(-288, 24, 0)); }
            lState_114398.speed = 1f;
            lState_114398.mirror = false;
            lState_114398.tag = "";
            lState_114398.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_HighKickRound_R_1");

            UnityEditor.Animations.AnimatorState lState_114400 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_SpinningHeelRight");
            if (lState_114400 == null) { lState_114400 = lSSM_113762.AddState("KB_m_SpinningHeelRight", new Vector3(-288, 84, 0)); }
            lState_114400.speed = 1f;
            lState_114400.mirror = false;
            lState_114400.tag = "";
            lState_114400.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_SpinningHeelRight");

            UnityEditor.Animations.AnimatorState lState_114402 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_Superpunch");
            if (lState_114402 == null) { lState_114402 = lSSM_113762.AddState("KB_Superpunch", new Vector3(-288, 144, 0)); }
            lState_114402.speed = 1f;
            lState_114402.mirror = false;
            lState_114402.tag = "";
            lState_114402.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/FightingUnityChan_FreeAsset/FightingUnityChan_FreeAsset/Animations/FUCM05_0022_M_RISING_P.fbx", "RISING_P");

            UnityEditor.Animations.AnimatorState lState_114396 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_KickUppercut_R");
            if (lState_114396 == null) { lState_114396 = lSSM_113762.AddState("KB_m_KickUppercut_R", new Vector3(-288, -36, 0)); }
            lState_114396.speed = 1f;
            lState_114396.mirror = false;
            lState_114396.tag = "";
            lState_114396.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_KickUppercut_R");

            UnityEditor.Animations.AnimatorState lState_114404 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_BackfistRound_L");
            if (lState_114404 == null) { lState_114404 = lSSM_113762.AddState("KB_m_BackfistRound_L", new Vector3(-288, -96, 0)); }
            lState_114404.speed = 1f;
            lState_114404.mirror = false;
            lState_114404.tag = "";
            lState_114404.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_BackfistRound_L");

            UnityEditor.Animations.AnimatorState lState_114410 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_attack_360_high");
            if (lState_114410 == null) { lState_114410 = lSSM_113762.AddState("standing_melee_attack_360_high", new Vector3(-312, 264, 0)); }
            lState_114410.speed = 1.2f;
            lState_114410.mirror = false;
            lState_114410.tag = "";
            lState_114410.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Sword And Shield Attack.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114412 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_attack_backhand");
            if (lState_114412 == null) { lState_114412 = lSSM_113762.AddState("standing_melee_attack_backhand", new Vector3(-216, 312, 0)); }
            lState_114412.speed = 1.2f;
            lState_114412.mirror = false;
            lState_114412.tag = "";
            lState_114412.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee attack backhand.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114414 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_combo_attack_ver_2");
            if (lState_114414 == null) { lState_114414 = lSSM_113762.AddState("standing_melee_combo_attack_ver_2", new Vector3(-120, 372, 0)); }
            lState_114414.speed = 1.2f;
            lState_114414.mirror = false;
            lState_114414.tag = "";
            lState_114414.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee combo attack ver. 2.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114416 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_combo_attack_ver_1");
            if (lState_114416 == null) { lState_114416 = lSSM_113762.AddState("standing_melee_combo_attack_ver_1", new Vector3(144, 468, 0)); }
            lState_114416.speed = 1.2f;
            lState_114416.mirror = false;
            lState_114416.tag = "";
            lState_114416.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee combo attack ver. 1.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114418 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_run_jump_attack");
            if (lState_114418 == null) { lState_114418 = lSSM_113762.AddState("standing_melee_run_jump_attack", new Vector3(12, 420, 0)); }
            lState_114418.speed = 1.5f;
            lState_114418.mirror = false;
            lState_114418.tag = "";
            lState_114418.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee run jump attack.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115418 = MotionControllerMotion.EditorFindState(lSSM_113762, "Pointing");
            if (lState_115418 == null) { lState_115418 = lSSM_113762.AddState("Pointing", new Vector3(228, -240, 0)); }
            lState_115418.speed = 1f;
            lState_115418.mirror = false;
            lState_115418.tag = "";
            lState_115418.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Pointing.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115420 = MotionControllerMotion.EditorFindState(lSSM_113762, "NoNoNo");
            if (lState_115420 == null) { lState_115420 = lSSM_113762.AddState("NoNoNo", new Vector3(-96, -276, 0)); }
            lState_115420.speed = 1f;
            lState_115420.mirror = false;
            lState_115420.tag = "";
            lState_115420.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/No.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115422 = MotionControllerMotion.EditorFindState(lSSM_113762, "StandingClap");
            if (lState_115422 == null) { lState_115422 = lSSM_113762.AddState("StandingClap", new Vector3(132, -276, 0)); }
            lState_115422.speed = 1f;
            lState_115422.mirror = false;
            lState_115422.tag = "";
            lState_115422.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Standing Clap.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115424 = MotionControllerMotion.EditorFindState(lSSM_113762, "Victory");
            if (lState_115424 == null) { lState_115424 = lSSM_113762.AddState("Victory", new Vector3(-168, -240, 0)); }
            lState_115424.speed = 1f;
            lState_115424.mirror = false;
            lState_115424.tag = "";
            lState_115424.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Victory.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115426 = MotionControllerMotion.EditorFindState(lSSM_113762, "AwwwYeah");
            if (lState_115426 == null) { lState_115426 = lSSM_113762.AddState("AwwwYeah", new Vector3(24, -312, 0)); }
            lState_115426.speed = 1f;
            lState_115426.mirror = false;
            lState_115426.tag = "";
            lState_115426.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Rallying.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113968 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114302, 0);
            if (lAnyTransition_113968 == null) { lAnyTransition_113968 = lLayerStateMachine.AddAnyStateTransition(lState_114302); }
            lAnyTransition_113968.isExit = false;
            lAnyTransition_113968.hasExitTime = false;
            lAnyTransition_113968.hasFixedDuration = false;
            lAnyTransition_113968.exitTime = 0.9f;
            lAnyTransition_113968.duration = 0f;
            lAnyTransition_113968.offset = 0f;
            lAnyTransition_113968.mute = false;
            lAnyTransition_113968.solo = false;
            lAnyTransition_113968.canTransitionToSelf = false;
            lAnyTransition_113968.orderedInterruption = true;
            lAnyTransition_113968.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113968.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113968.RemoveCondition(lAnyTransition_113968.conditions[i]); }
            lAnyTransition_113968.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113968.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113970 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114304, 0);
            if (lAnyTransition_113970 == null) { lAnyTransition_113970 = lLayerStateMachine.AddAnyStateTransition(lState_114304); }
            lAnyTransition_113970.isExit = false;
            lAnyTransition_113970.hasExitTime = false;
            lAnyTransition_113970.hasFixedDuration = false;
            lAnyTransition_113970.exitTime = 1.523494E-08f;
            lAnyTransition_113970.duration = 0.05999994f;
            lAnyTransition_113970.offset = 0f;
            lAnyTransition_113970.mute = false;
            lAnyTransition_113970.solo = false;
            lAnyTransition_113970.canTransitionToSelf = false;
            lAnyTransition_113970.orderedInterruption = true;
            lAnyTransition_113970.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113970.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113970.RemoveCondition(lAnyTransition_113970.conditions[i]); }
            lAnyTransition_113970.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113970.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 101f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113972 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114306, 0);
            if (lAnyTransition_113972 == null) { lAnyTransition_113972 = lLayerStateMachine.AddAnyStateTransition(lState_114306); }
            lAnyTransition_113972.isExit = false;
            lAnyTransition_113972.hasExitTime = false;
            lAnyTransition_113972.hasFixedDuration = false;
            lAnyTransition_113972.exitTime = 0.9000001f;
            lAnyTransition_113972.duration = 0.01443848f;
            lAnyTransition_113972.offset = 0f;
            lAnyTransition_113972.mute = false;
            lAnyTransition_113972.solo = false;
            lAnyTransition_113972.canTransitionToSelf = false;
            lAnyTransition_113972.orderedInterruption = true;
            lAnyTransition_113972.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113972.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113972.RemoveCondition(lAnyTransition_113972.conditions[i]); }
            lAnyTransition_113972.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113972.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 102f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113994 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114324, 0);
            if (lAnyTransition_113994 == null) { lAnyTransition_113994 = lLayerStateMachine.AddAnyStateTransition(lState_114324); }
            lAnyTransition_113994.isExit = false;
            lAnyTransition_113994.hasExitTime = false;
            lAnyTransition_113994.hasFixedDuration = false;
            lAnyTransition_113994.exitTime = 0f;
            lAnyTransition_113994.duration = 0.05910589f;
            lAnyTransition_113994.offset = 0f;
            lAnyTransition_113994.mute = false;
            lAnyTransition_113994.solo = false;
            lAnyTransition_113994.canTransitionToSelf = true;
            lAnyTransition_113994.orderedInterruption = true;
            lAnyTransition_113994.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113994.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113994.RemoveCondition(lAnyTransition_113994.conditions[i]); }
            lAnyTransition_113994.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113994.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 105f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113996 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114326, 0);
            if (lAnyTransition_113996 == null) { lAnyTransition_113996 = lLayerStateMachine.AddAnyStateTransition(lState_114326); }
            lAnyTransition_113996.isExit = false;
            lAnyTransition_113996.hasExitTime = false;
            lAnyTransition_113996.hasFixedDuration = false;
            lAnyTransition_113996.exitTime = 0.4168403f;
            lAnyTransition_113996.duration = 0.03748399f;
            lAnyTransition_113996.offset = 0.08077718f;
            lAnyTransition_113996.mute = false;
            lAnyTransition_113996.solo = false;
            lAnyTransition_113996.canTransitionToSelf = false;
            lAnyTransition_113996.orderedInterruption = true;
            lAnyTransition_113996.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113996.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113996.RemoveCondition(lAnyTransition_113996.conditions[i]); }
            lAnyTransition_113996.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113996.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 103f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113998 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114328, 0);
            if (lAnyTransition_113998 == null) { lAnyTransition_113998 = lLayerStateMachine.AddAnyStateTransition(lState_114328); }
            lAnyTransition_113998.isExit = false;
            lAnyTransition_113998.hasExitTime = false;
            lAnyTransition_113998.hasFixedDuration = false;
            lAnyTransition_113998.exitTime = 0.75f;
            lAnyTransition_113998.duration = 0f;
            lAnyTransition_113998.offset = 0f;
            lAnyTransition_113998.mute = false;
            lAnyTransition_113998.solo = false;
            lAnyTransition_113998.canTransitionToSelf = false;
            lAnyTransition_113998.orderedInterruption = true;
            lAnyTransition_113998.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113998.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113998.RemoveCondition(lAnyTransition_113998.conditions[i]); }
            lAnyTransition_113998.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113998.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114000 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114330, 0);
            if (lAnyTransition_114000 == null) { lAnyTransition_114000 = lLayerStateMachine.AddAnyStateTransition(lState_114330); }
            lAnyTransition_114000.isExit = false;
            lAnyTransition_114000.hasExitTime = false;
            lAnyTransition_114000.hasFixedDuration = false;
            lAnyTransition_114000.exitTime = 1.575326E-09f;
            lAnyTransition_114000.duration = 0.05f;
            lAnyTransition_114000.offset = 0f;
            lAnyTransition_114000.mute = false;
            lAnyTransition_114000.solo = false;
            lAnyTransition_114000.canTransitionToSelf = false;
            lAnyTransition_114000.orderedInterruption = true;
            lAnyTransition_114000.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114000.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114000.RemoveCondition(lAnyTransition_114000.conditions[i]); }
            lAnyTransition_114000.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114000.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 111f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114002 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114332, 0);
            if (lAnyTransition_114002 == null) { lAnyTransition_114002 = lLayerStateMachine.AddAnyStateTransition(lState_114332); }
            lAnyTransition_114002.isExit = false;
            lAnyTransition_114002.hasExitTime = false;
            lAnyTransition_114002.hasFixedDuration = false;
            lAnyTransition_114002.exitTime = 0f;
            lAnyTransition_114002.duration = 0.1786151f;
            lAnyTransition_114002.offset = 0.04548877f;
            lAnyTransition_114002.mute = false;
            lAnyTransition_114002.solo = false;
            lAnyTransition_114002.canTransitionToSelf = false;
            lAnyTransition_114002.orderedInterruption = true;
            lAnyTransition_114002.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114002.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114002.RemoveCondition(lAnyTransition_114002.conditions[i]); }
            lAnyTransition_114002.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114002.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 112f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114004 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114334, 0);
            if (lAnyTransition_114004 == null) { lAnyTransition_114004 = lLayerStateMachine.AddAnyStateTransition(lState_114334); }
            lAnyTransition_114004.isExit = false;
            lAnyTransition_114004.hasExitTime = false;
            lAnyTransition_114004.hasFixedDuration = false;
            lAnyTransition_114004.exitTime = 0f;
            lAnyTransition_114004.duration = 0.05169718f;
            lAnyTransition_114004.offset = 0f;
            lAnyTransition_114004.mute = false;
            lAnyTransition_114004.solo = false;
            lAnyTransition_114004.canTransitionToSelf = false;
            lAnyTransition_114004.orderedInterruption = true;
            lAnyTransition_114004.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114004.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114004.RemoveCondition(lAnyTransition_114004.conditions[i]); }
            lAnyTransition_114004.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114004.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 113f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114006 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114336, 0);
            if (lAnyTransition_114006 == null) { lAnyTransition_114006 = lLayerStateMachine.AddAnyStateTransition(lState_114336); }
            lAnyTransition_114006.isExit = false;
            lAnyTransition_114006.hasExitTime = false;
            lAnyTransition_114006.hasFixedDuration = false;
            lAnyTransition_114006.exitTime = 0.75f;
            lAnyTransition_114006.duration = 0.25f;
            lAnyTransition_114006.offset = 0f;
            lAnyTransition_114006.mute = false;
            lAnyTransition_114006.solo = false;
            lAnyTransition_114006.canTransitionToSelf = false;
            lAnyTransition_114006.orderedInterruption = true;
            lAnyTransition_114006.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114006.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114006.RemoveCondition(lAnyTransition_114006.conditions[i]); }
            lAnyTransition_114006.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114006.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 110f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114070 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114396, 0);
            if (lAnyTransition_114070 == null) { lAnyTransition_114070 = lLayerStateMachine.AddAnyStateTransition(lState_114396); }
            lAnyTransition_114070.isExit = false;
            lAnyTransition_114070.hasExitTime = false;
            lAnyTransition_114070.hasFixedDuration = true;
            lAnyTransition_114070.exitTime = 1.606756E-09f;
            lAnyTransition_114070.duration = 0.05f;
            lAnyTransition_114070.offset = 0f;
            lAnyTransition_114070.mute = false;
            lAnyTransition_114070.solo = false;
            lAnyTransition_114070.canTransitionToSelf = false;
            lAnyTransition_114070.orderedInterruption = true;
            lAnyTransition_114070.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114070.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114070.RemoveCondition(lAnyTransition_114070.conditions[i]); }
            lAnyTransition_114070.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114070.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 301f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114072 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114398, 0);
            if (lAnyTransition_114072 == null) { lAnyTransition_114072 = lLayerStateMachine.AddAnyStateTransition(lState_114398); }
            lAnyTransition_114072.isExit = false;
            lAnyTransition_114072.hasExitTime = false;
            lAnyTransition_114072.hasFixedDuration = true;
            lAnyTransition_114072.exitTime = 8.422393E-09f;
            lAnyTransition_114072.duration = 0.05f;
            lAnyTransition_114072.offset = 0f;
            lAnyTransition_114072.mute = false;
            lAnyTransition_114072.solo = false;
            lAnyTransition_114072.canTransitionToSelf = false;
            lAnyTransition_114072.orderedInterruption = true;
            lAnyTransition_114072.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114072.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114072.RemoveCondition(lAnyTransition_114072.conditions[i]); }
            lAnyTransition_114072.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114072.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 302f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114074 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114400, 0);
            if (lAnyTransition_114074 == null) { lAnyTransition_114074 = lLayerStateMachine.AddAnyStateTransition(lState_114400); }
            lAnyTransition_114074.isExit = false;
            lAnyTransition_114074.hasExitTime = false;
            lAnyTransition_114074.hasFixedDuration = true;
            lAnyTransition_114074.exitTime = 2.142341E-09f;
            lAnyTransition_114074.duration = 0.03685686f;
            lAnyTransition_114074.offset = 0.03569532f;
            lAnyTransition_114074.mute = false;
            lAnyTransition_114074.solo = false;
            lAnyTransition_114074.canTransitionToSelf = false;
            lAnyTransition_114074.orderedInterruption = true;
            lAnyTransition_114074.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114074.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114074.RemoveCondition(lAnyTransition_114074.conditions[i]); }
            lAnyTransition_114074.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114074.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 303f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114076 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114402, 0);
            if (lAnyTransition_114076 == null) { lAnyTransition_114076 = lLayerStateMachine.AddAnyStateTransition(lState_114402); }
            lAnyTransition_114076.isExit = false;
            lAnyTransition_114076.hasExitTime = false;
            lAnyTransition_114076.hasFixedDuration = true;
            lAnyTransition_114076.exitTime = 1.981666E-08f;
            lAnyTransition_114076.duration = 0.03598962f;
            lAnyTransition_114076.offset = 0f;
            lAnyTransition_114076.mute = false;
            lAnyTransition_114076.solo = false;
            lAnyTransition_114076.canTransitionToSelf = false;
            lAnyTransition_114076.orderedInterruption = true;
            lAnyTransition_114076.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114076.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114076.RemoveCondition(lAnyTransition_114076.conditions[i]); }
            lAnyTransition_114076.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114076.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 304f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114078 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114404, 0);
            if (lAnyTransition_114078 == null) { lAnyTransition_114078 = lLayerStateMachine.AddAnyStateTransition(lState_114404); }
            lAnyTransition_114078.isExit = false;
            lAnyTransition_114078.hasExitTime = false;
            lAnyTransition_114078.hasFixedDuration = true;
            lAnyTransition_114078.exitTime = 0.7500002f;
            lAnyTransition_114078.duration = 0.06643014f;
            lAnyTransition_114078.offset = 0.02266629f;
            lAnyTransition_114078.mute = false;
            lAnyTransition_114078.solo = false;
            lAnyTransition_114078.canTransitionToSelf = false;
            lAnyTransition_114078.orderedInterruption = true;
            lAnyTransition_114078.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114078.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114078.RemoveCondition(lAnyTransition_114078.conditions[i]); }
            lAnyTransition_114078.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114078.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 300f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114106 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114410, 0);
            if (lAnyTransition_114106 == null) { lAnyTransition_114106 = lLayerStateMachine.AddAnyStateTransition(lState_114410); }
            lAnyTransition_114106.isExit = false;
            lAnyTransition_114106.hasExitTime = false;
            lAnyTransition_114106.hasFixedDuration = true;
            lAnyTransition_114106.exitTime = 0f;
            lAnyTransition_114106.duration = 0.05f;
            lAnyTransition_114106.offset = 0.02365446f;
            lAnyTransition_114106.mute = false;
            lAnyTransition_114106.solo = false;
            lAnyTransition_114106.canTransitionToSelf = true;
            lAnyTransition_114106.orderedInterruption = true;
            lAnyTransition_114106.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114106.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114106.RemoveCondition(lAnyTransition_114106.conditions[i]); }
            lAnyTransition_114106.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114106.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 0f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114108 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114412, 0);
            if (lAnyTransition_114108 == null) { lAnyTransition_114108 = lLayerStateMachine.AddAnyStateTransition(lState_114412); }
            lAnyTransition_114108.isExit = false;
            lAnyTransition_114108.hasExitTime = false;
            lAnyTransition_114108.hasFixedDuration = true;
            lAnyTransition_114108.exitTime = 0.75f;
            lAnyTransition_114108.duration = 0.25f;
            lAnyTransition_114108.offset = 0.1220909f;
            lAnyTransition_114108.mute = false;
            lAnyTransition_114108.solo = false;
            lAnyTransition_114108.canTransitionToSelf = true;
            lAnyTransition_114108.orderedInterruption = true;
            lAnyTransition_114108.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114108.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114108.RemoveCondition(lAnyTransition_114108.conditions[i]); }
            lAnyTransition_114108.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114108.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 403f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114110 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114414, 0);
            if (lAnyTransition_114110 == null) { lAnyTransition_114110 = lLayerStateMachine.AddAnyStateTransition(lState_114414); }
            lAnyTransition_114110.isExit = false;
            lAnyTransition_114110.hasExitTime = false;
            lAnyTransition_114110.hasFixedDuration = true;
            lAnyTransition_114110.exitTime = 0f;
            lAnyTransition_114110.duration = 0.25f;
            lAnyTransition_114110.offset = 0.4168376f;
            lAnyTransition_114110.mute = false;
            lAnyTransition_114110.solo = false;
            lAnyTransition_114110.canTransitionToSelf = true;
            lAnyTransition_114110.orderedInterruption = true;
            lAnyTransition_114110.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114110.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114110.RemoveCondition(lAnyTransition_114110.conditions[i]); }
            lAnyTransition_114110.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114110.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 402f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114112 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114416, 0);
            if (lAnyTransition_114112 == null) { lAnyTransition_114112 = lLayerStateMachine.AddAnyStateTransition(lState_114416); }
            lAnyTransition_114112.isExit = false;
            lAnyTransition_114112.hasExitTime = false;
            lAnyTransition_114112.hasFixedDuration = true;
            lAnyTransition_114112.exitTime = 0.75f;
            lAnyTransition_114112.duration = 0.05f;
            lAnyTransition_114112.offset = 0.4555707f;
            lAnyTransition_114112.mute = false;
            lAnyTransition_114112.solo = false;
            lAnyTransition_114112.canTransitionToSelf = false;
            lAnyTransition_114112.orderedInterruption = true;
            lAnyTransition_114112.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114112.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114112.RemoveCondition(lAnyTransition_114112.conditions[i]); }
            lAnyTransition_114112.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114112.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 0f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114114 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114418, 0);
            if (lAnyTransition_114114 == null) { lAnyTransition_114114 = lLayerStateMachine.AddAnyStateTransition(lState_114418); }
            lAnyTransition_114114.isExit = false;
            lAnyTransition_114114.hasExitTime = false;
            lAnyTransition_114114.hasFixedDuration = true;
            lAnyTransition_114114.exitTime = 1.31314f;
            lAnyTransition_114114.duration = 0.05290114f;
            lAnyTransition_114114.offset = 0.1896213f;
            lAnyTransition_114114.mute = false;
            lAnyTransition_114114.solo = false;
            lAnyTransition_114114.canTransitionToSelf = true;
            lAnyTransition_114114.orderedInterruption = true;
            lAnyTransition_114114.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114114.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114114.RemoveCondition(lAnyTransition_114114.conditions[i]); }
            lAnyTransition_114114.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114114.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 400f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114120 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114418, 1);
            if (lAnyTransition_114120 == null) { lAnyTransition_114120 = lLayerStateMachine.AddAnyStateTransition(lState_114418); }
            lAnyTransition_114120.isExit = false;
            lAnyTransition_114120.hasExitTime = false;
            lAnyTransition_114120.hasFixedDuration = true;
            lAnyTransition_114120.exitTime = 0.75f;
            lAnyTransition_114120.duration = 0.25f;
            lAnyTransition_114120.offset = 0f;
            lAnyTransition_114120.mute = false;
            lAnyTransition_114120.solo = false;
            lAnyTransition_114120.canTransitionToSelf = true;
            lAnyTransition_114120.orderedInterruption = true;
            lAnyTransition_114120.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114120.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114120.RemoveCondition(lAnyTransition_114120.conditions[i]); }
            lAnyTransition_114120.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114120.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 200f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lTransition_115428 = MotionControllerMotion.EditorFindTransition(lState_114302, lState_115416, 0);
            if (lTransition_115428 == null) { lTransition_115428 = lState_114302.AddTransition(lState_115416); }
            lTransition_115428.isExit = false;
            lTransition_115428.hasExitTime = true;
            lTransition_115428.hasFixedDuration = false;
            lTransition_115428.exitTime = 0.6875f;
            lTransition_115428.duration = 0.1833009f;
            lTransition_115428.offset = 0f;
            lTransition_115428.mute = false;
            lTransition_115428.solo = false;
            lTransition_115428.canTransitionToSelf = true;
            lTransition_115428.orderedInterruption = true;
            lTransition_115428.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115428.conditions.Length - 1; i >= 0; i--) { lTransition_115428.RemoveCondition(lTransition_115428.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115430 = MotionControllerMotion.EditorFindTransition(lState_114304, lState_115416, 0);
            if (lTransition_115430 == null) { lTransition_115430 = lState_114304.AddTransition(lState_115416); }
            lTransition_115430.isExit = false;
            lTransition_115430.hasExitTime = true;
            lTransition_115430.hasFixedDuration = false;
            lTransition_115430.exitTime = 0.6959836f;
            lTransition_115430.duration = 0.1397124f;
            lTransition_115430.offset = 0.005498699f;
            lTransition_115430.mute = false;
            lTransition_115430.solo = false;
            lTransition_115430.canTransitionToSelf = true;
            lTransition_115430.orderedInterruption = true;
            lTransition_115430.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115430.conditions.Length - 1; i >= 0; i--) { lTransition_115430.RemoveCondition(lTransition_115430.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115432 = MotionControllerMotion.EditorFindTransition(lState_114306, lState_115416, 0);
            if (lTransition_115432 == null) { lTransition_115432 = lState_114306.AddTransition(lState_115416); }
            lTransition_115432.isExit = false;
            lTransition_115432.hasExitTime = true;
            lTransition_115432.hasFixedDuration = false;
            lTransition_115432.exitTime = 0.7499999f;
            lTransition_115432.duration = 0.1880713f;
            lTransition_115432.offset = 0f;
            lTransition_115432.mute = false;
            lTransition_115432.solo = false;
            lTransition_115432.canTransitionToSelf = true;
            lTransition_115432.orderedInterruption = true;
            lTransition_115432.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115432.conditions.Length - 1; i >= 0; i--) { lTransition_115432.RemoveCondition(lTransition_115432.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115434 = MotionControllerMotion.EditorFindTransition(lState_114324, lState_115416, 0);
            if (lTransition_115434 == null) { lTransition_115434 = lState_114324.AddTransition(lState_115416); }
            lTransition_115434.isExit = false;
            lTransition_115434.hasExitTime = true;
            lTransition_115434.hasFixedDuration = false;
            lTransition_115434.exitTime = 0.5228129f;
            lTransition_115434.duration = 0.1242818f;
            lTransition_115434.offset = 0.06203142f;
            lTransition_115434.mute = false;
            lTransition_115434.solo = false;
            lTransition_115434.canTransitionToSelf = true;
            lTransition_115434.orderedInterruption = true;
            lTransition_115434.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115434.conditions.Length - 1; i >= 0; i--) { lTransition_115434.RemoveCondition(lTransition_115434.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115436 = MotionControllerMotion.EditorFindTransition(lState_114326, lState_115416, 0);
            if (lTransition_115436 == null) { lTransition_115436 = lState_114326.AddTransition(lState_115416); }
            lTransition_115436.isExit = false;
            lTransition_115436.hasExitTime = true;
            lTransition_115436.hasFixedDuration = false;
            lTransition_115436.exitTime = 1.004305f;
            lTransition_115436.duration = 0.05591125f;
            lTransition_115436.offset = 0.4601691f;
            lTransition_115436.mute = false;
            lTransition_115436.solo = false;
            lTransition_115436.canTransitionToSelf = true;
            lTransition_115436.orderedInterruption = true;
            lTransition_115436.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115436.conditions.Length - 1; i >= 0; i--) { lTransition_115436.RemoveCondition(lTransition_115436.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115438 = MotionControllerMotion.EditorFindTransition(lState_114328, lState_115416, 0);
            if (lTransition_115438 == null) { lTransition_115438 = lState_114328.AddTransition(lState_115416); }
            lTransition_115438.isExit = false;
            lTransition_115438.hasExitTime = true;
            lTransition_115438.hasFixedDuration = false;
            lTransition_115438.exitTime = 0.3266955f;
            lTransition_115438.duration = 0.1307748f;
            lTransition_115438.offset = 0f;
            lTransition_115438.mute = false;
            lTransition_115438.solo = false;
            lTransition_115438.canTransitionToSelf = true;
            lTransition_115438.orderedInterruption = true;
            lTransition_115438.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115438.conditions.Length - 1; i >= 0; i--) { lTransition_115438.RemoveCondition(lTransition_115438.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115440 = MotionControllerMotion.EditorFindTransition(lState_114336, lState_115416, 0);
            if (lTransition_115440 == null) { lTransition_115440 = lState_114336.AddTransition(lState_115416); }
            lTransition_115440.isExit = false;
            lTransition_115440.hasExitTime = true;
            lTransition_115440.hasFixedDuration = false;
            lTransition_115440.exitTime = 0.4517571f;
            lTransition_115440.duration = 0.1076995f;
            lTransition_115440.offset = 0f;
            lTransition_115440.mute = false;
            lTransition_115440.solo = false;
            lTransition_115440.canTransitionToSelf = true;
            lTransition_115440.orderedInterruption = true;
            lTransition_115440.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115440.conditions.Length - 1; i >= 0; i--) { lTransition_115440.RemoveCondition(lTransition_115440.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115442 = MotionControllerMotion.EditorFindTransition(lState_114330, lState_115416, 0);
            if (lTransition_115442 == null) { lTransition_115442 = lState_114330.AddTransition(lState_115416); }
            lTransition_115442.isExit = false;
            lTransition_115442.hasExitTime = true;
            lTransition_115442.hasFixedDuration = false;
            lTransition_115442.exitTime = 0.8369565f;
            lTransition_115442.duration = 0.1716502f;
            lTransition_115442.offset = 0f;
            lTransition_115442.mute = false;
            lTransition_115442.solo = false;
            lTransition_115442.canTransitionToSelf = true;
            lTransition_115442.orderedInterruption = true;
            lTransition_115442.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115442.conditions.Length - 1; i >= 0; i--) { lTransition_115442.RemoveCondition(lTransition_115442.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115444 = MotionControllerMotion.EditorFindTransition(lState_114332, lState_115416, 0);
            if (lTransition_115444 == null) { lTransition_115444 = lState_114332.AddTransition(lState_115416); }
            lTransition_115444.isExit = false;
            lTransition_115444.hasExitTime = true;
            lTransition_115444.hasFixedDuration = true;
            lTransition_115444.exitTime = 0.5712763f;
            lTransition_115444.duration = 0.262871f;
            lTransition_115444.offset = 0f;
            lTransition_115444.mute = false;
            lTransition_115444.solo = false;
            lTransition_115444.canTransitionToSelf = true;
            lTransition_115444.orderedInterruption = true;
            lTransition_115444.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115444.conditions.Length - 1; i >= 0; i--) { lTransition_115444.RemoveCondition(lTransition_115444.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115446 = MotionControllerMotion.EditorFindTransition(lState_114334, lState_115416, 0);
            if (lTransition_115446 == null) { lTransition_115446 = lState_114334.AddTransition(lState_115416); }
            lTransition_115446.isExit = false;
            lTransition_115446.hasExitTime = true;
            lTransition_115446.hasFixedDuration = false;
            lTransition_115446.exitTime = 0.7046111f;
            lTransition_115446.duration = 0.136108f;
            lTransition_115446.offset = 0f;
            lTransition_115446.mute = false;
            lTransition_115446.solo = false;
            lTransition_115446.canTransitionToSelf = true;
            lTransition_115446.orderedInterruption = true;
            lTransition_115446.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115446.conditions.Length - 1; i >= 0; i--) { lTransition_115446.RemoveCondition(lTransition_115446.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115448 = MotionControllerMotion.EditorFindTransition(lState_114398, lState_115426, 0);
            if (lTransition_115448 == null) { lTransition_115448 = lState_114398.AddTransition(lState_115426); }
            lTransition_115448.isExit = false;
            lTransition_115448.hasExitTime = true;
            lTransition_115448.hasFixedDuration = true;
            lTransition_115448.exitTime = 0.7858688f;
            lTransition_115448.duration = 0.7122293f;
            lTransition_115448.offset = 0.2814808f;
            lTransition_115448.mute = false;
            lTransition_115448.solo = false;
            lTransition_115448.canTransitionToSelf = true;
            lTransition_115448.orderedInterruption = true;
            lTransition_115448.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115448.conditions.Length - 1; i >= 0; i--) { lTransition_115448.RemoveCondition(lTransition_115448.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115450 = MotionControllerMotion.EditorFindTransition(lState_114400, lState_115422, 0);
            if (lTransition_115450 == null) { lTransition_115450 = lState_114400.AddTransition(lState_115422); }
            lTransition_115450.isExit = false;
            lTransition_115450.hasExitTime = true;
            lTransition_115450.hasFixedDuration = true;
            lTransition_115450.exitTime = 0.84375f;
            lTransition_115450.duration = 0.6329349f;
            lTransition_115450.offset = 0.1360608f;
            lTransition_115450.mute = false;
            lTransition_115450.solo = false;
            lTransition_115450.canTransitionToSelf = true;
            lTransition_115450.orderedInterruption = true;
            lTransition_115450.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115450.conditions.Length - 1; i >= 0; i--) { lTransition_115450.RemoveCondition(lTransition_115450.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115452 = MotionControllerMotion.EditorFindTransition(lState_114402, lState_115418, 0);
            if (lTransition_115452 == null) { lTransition_115452 = lState_114402.AddTransition(lState_115418); }
            lTransition_115452.isExit = false;
            lTransition_115452.hasExitTime = true;
            lTransition_115452.hasFixedDuration = true;
            lTransition_115452.exitTime = 0.9480702f;
            lTransition_115452.duration = 0.25f;
            lTransition_115452.offset = 0.07633905f;
            lTransition_115452.mute = false;
            lTransition_115452.solo = false;
            lTransition_115452.canTransitionToSelf = true;
            lTransition_115452.orderedInterruption = true;
            lTransition_115452.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115452.conditions.Length - 1; i >= 0; i--) { lTransition_115452.RemoveCondition(lTransition_115452.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115454 = MotionControllerMotion.EditorFindTransition(lState_114396, lState_115420, 0);
            if (lTransition_115454 == null) { lTransition_115454 = lState_114396.AddTransition(lState_115420); }
            lTransition_115454.isExit = false;
            lTransition_115454.hasExitTime = true;
            lTransition_115454.hasFixedDuration = true;
            lTransition_115454.exitTime = 0.6875741f;
            lTransition_115454.duration = 0.4866321f;
            lTransition_115454.offset = 0.05391639f;
            lTransition_115454.mute = false;
            lTransition_115454.solo = false;
            lTransition_115454.canTransitionToSelf = true;
            lTransition_115454.orderedInterruption = true;
            lTransition_115454.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115454.conditions.Length - 1; i >= 0; i--) { lTransition_115454.RemoveCondition(lTransition_115454.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115456 = MotionControllerMotion.EditorFindTransition(lState_114404, lState_115424, 0);
            if (lTransition_115456 == null) { lTransition_115456 = lState_114404.AddTransition(lState_115424); }
            lTransition_115456.isExit = false;
            lTransition_115456.hasExitTime = true;
            lTransition_115456.hasFixedDuration = true;
            lTransition_115456.exitTime = 0.6064144f;
            lTransition_115456.duration = 0.320307f;
            lTransition_115456.offset = 0.1004388f;
            lTransition_115456.mute = false;
            lTransition_115456.solo = false;
            lTransition_115456.canTransitionToSelf = true;
            lTransition_115456.orderedInterruption = true;
            lTransition_115456.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115456.conditions.Length - 1; i >= 0; i--) { lTransition_115456.RemoveCondition(lTransition_115456.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115458 = MotionControllerMotion.EditorFindTransition(lState_114410, lState_115416, 0);
            if (lTransition_115458 == null) { lTransition_115458 = lState_114410.AddTransition(lState_115416); }
            lTransition_115458.isExit = false;
            lTransition_115458.hasExitTime = true;
            lTransition_115458.hasFixedDuration = true;
            lTransition_115458.exitTime = 0.9210526f;
            lTransition_115458.duration = 0.25f;
            lTransition_115458.offset = 0f;
            lTransition_115458.mute = false;
            lTransition_115458.solo = false;
            lTransition_115458.canTransitionToSelf = true;
            lTransition_115458.orderedInterruption = true;
            lTransition_115458.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115458.conditions.Length - 1; i >= 0; i--) { lTransition_115458.RemoveCondition(lTransition_115458.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115460 = MotionControllerMotion.EditorFindTransition(lState_114412, lState_115416, 0);
            if (lTransition_115460 == null) { lTransition_115460 = lState_114412.AddTransition(lState_115416); }
            lTransition_115460.isExit = false;
            lTransition_115460.hasExitTime = true;
            lTransition_115460.hasFixedDuration = true;
            lTransition_115460.exitTime = 0.5893119f;
            lTransition_115460.duration = 0.25f;
            lTransition_115460.offset = 0.1023891f;
            lTransition_115460.mute = false;
            lTransition_115460.solo = false;
            lTransition_115460.canTransitionToSelf = true;
            lTransition_115460.orderedInterruption = true;
            lTransition_115460.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115460.conditions.Length - 1; i >= 0; i--) { lTransition_115460.RemoveCondition(lTransition_115460.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115462 = MotionControllerMotion.EditorFindTransition(lState_114414, lState_115416, 0);
            if (lTransition_115462 == null) { lTransition_115462 = lState_114414.AddTransition(lState_115416); }
            lTransition_115462.isExit = false;
            lTransition_115462.hasExitTime = true;
            lTransition_115462.hasFixedDuration = true;
            lTransition_115462.exitTime = 0.8253455f;
            lTransition_115462.duration = 0.25f;
            lTransition_115462.offset = 0f;
            lTransition_115462.mute = false;
            lTransition_115462.solo = false;
            lTransition_115462.canTransitionToSelf = true;
            lTransition_115462.orderedInterruption = true;
            lTransition_115462.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115462.conditions.Length - 1; i >= 0; i--) { lTransition_115462.RemoveCondition(lTransition_115462.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115464 = MotionControllerMotion.EditorFindTransition(lState_114416, lState_115416, 0);
            if (lTransition_115464 == null) { lTransition_115464 = lState_114416.AddTransition(lState_115416); }
            lTransition_115464.isExit = false;
            lTransition_115464.hasExitTime = true;
            lTransition_115464.hasFixedDuration = true;
            lTransition_115464.exitTime = 0.8055411f;
            lTransition_115464.duration = 0.25f;
            lTransition_115464.offset = 0f;
            lTransition_115464.mute = false;
            lTransition_115464.solo = false;
            lTransition_115464.canTransitionToSelf = true;
            lTransition_115464.orderedInterruption = true;
            lTransition_115464.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115464.conditions.Length - 1; i >= 0; i--) { lTransition_115464.RemoveCondition(lTransition_115464.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115466 = MotionControllerMotion.EditorFindTransition(lState_114418, lState_115416, 0);
            if (lTransition_115466 == null) { lTransition_115466 = lState_114418.AddTransition(lState_115416); }
            lTransition_115466.isExit = false;
            lTransition_115466.hasExitTime = true;
            lTransition_115466.hasFixedDuration = true;
            lTransition_115466.exitTime = 0.9318182f;
            lTransition_115466.duration = 0.25f;
            lTransition_115466.offset = 0f;
            lTransition_115466.mute = false;
            lTransition_115466.solo = false;
            lTransition_115466.canTransitionToSelf = true;
            lTransition_115466.orderedInterruption = true;
            lTransition_115466.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115466.conditions.Length - 1; i >= 0; i--) { lTransition_115466.RemoveCondition(lTransition_115466.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115468 = MotionControllerMotion.EditorFindTransition(lState_115418, lState_115416, 0);
            if (lTransition_115468 == null) { lTransition_115468 = lState_115418.AddTransition(lState_115416); }
            lTransition_115468.isExit = false;
            lTransition_115468.hasExitTime = true;
            lTransition_115468.hasFixedDuration = true;
            lTransition_115468.exitTime = 0.5354011f;
            lTransition_115468.duration = 0.5603523f;
            lTransition_115468.offset = 0f;
            lTransition_115468.mute = false;
            lTransition_115468.solo = false;
            lTransition_115468.canTransitionToSelf = true;
            lTransition_115468.orderedInterruption = true;
            lTransition_115468.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115468.conditions.Length - 1; i >= 0; i--) { lTransition_115468.RemoveCondition(lTransition_115468.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115470 = MotionControllerMotion.EditorFindTransition(lState_115420, lState_115416, 0);
            if (lTransition_115470 == null) { lTransition_115470 = lState_115420.AddTransition(lState_115416); }
            lTransition_115470.isExit = false;
            lTransition_115470.hasExitTime = true;
            lTransition_115470.hasFixedDuration = true;
            lTransition_115470.exitTime = 0.5016305f;
            lTransition_115470.duration = 0.6861773f;
            lTransition_115470.offset = 0f;
            lTransition_115470.mute = false;
            lTransition_115470.solo = false;
            lTransition_115470.canTransitionToSelf = true;
            lTransition_115470.orderedInterruption = true;
            lTransition_115470.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115470.conditions.Length - 1; i >= 0; i--) { lTransition_115470.RemoveCondition(lTransition_115470.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115472 = MotionControllerMotion.EditorFindTransition(lState_115422, lState_115416, 0);
            if (lTransition_115472 == null) { lTransition_115472 = lState_115422.AddTransition(lState_115416); }
            lTransition_115472.isExit = false;
            lTransition_115472.hasExitTime = true;
            lTransition_115472.hasFixedDuration = true;
            lTransition_115472.exitTime = 0.4122928f;
            lTransition_115472.duration = 0.6213312f;
            lTransition_115472.offset = 0f;
            lTransition_115472.mute = false;
            lTransition_115472.solo = false;
            lTransition_115472.canTransitionToSelf = true;
            lTransition_115472.orderedInterruption = true;
            lTransition_115472.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115472.conditions.Length - 1; i >= 0; i--) { lTransition_115472.RemoveCondition(lTransition_115472.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115474 = MotionControllerMotion.EditorFindTransition(lState_115424, lState_115416, 0);
            if (lTransition_115474 == null) { lTransition_115474 = lState_115424.AddTransition(lState_115416); }
            lTransition_115474.isExit = false;
            lTransition_115474.hasExitTime = true;
            lTransition_115474.hasFixedDuration = true;
            lTransition_115474.exitTime = 0.3997446f;
            lTransition_115474.duration = 0.8489763f;
            lTransition_115474.offset = 0.4929846f;
            lTransition_115474.mute = false;
            lTransition_115474.solo = false;
            lTransition_115474.canTransitionToSelf = true;
            lTransition_115474.orderedInterruption = true;
            lTransition_115474.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115474.conditions.Length - 1; i >= 0; i--) { lTransition_115474.RemoveCondition(lTransition_115474.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115476 = MotionControllerMotion.EditorFindTransition(lState_115426, lState_115416, 0);
            if (lTransition_115476 == null) { lTransition_115476 = lState_115426.AddTransition(lState_115416); }
            lTransition_115476.isExit = false;
            lTransition_115476.hasExitTime = true;
            lTransition_115476.hasFixedDuration = true;
            lTransition_115476.exitTime = 0.5749115f;
            lTransition_115476.duration = 0.6802311f;
            lTransition_115476.offset = 0.7088763f;
            lTransition_115476.mute = false;
            lTransition_115476.solo = false;
            lTransition_115476.canTransitionToSelf = true;
            lTransition_115476.orderedInterruption = true;
            lTransition_115476.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115476.conditions.Length - 1; i >= 0; i--) { lTransition_115476.RemoveCondition(lTransition_115476.conditions[i]); }


            // Run any post processing after creating the state machine
            OnStateMachineCreated();
        }

#endif

        // ************************************ END AUTO GENERATED ************************************
        #endregion
        /// <summary>
        /// New way to create sub-state machines without destroying what exists first.
        /// </summary>
#if UNITY_EDITOR
        public static void ExtendBasicMotion(MotionController rMotionController, int rLayerIndex)
        {
            UnityEditor.Animations.AnimatorController lController = null;

            Animator lAnimator = rMotionController.Animator;
            if (lAnimator == null) { lAnimator = rMotionController.gameObject.GetComponent<Animator>(); }
            if (lAnimator != null) { lController = lAnimator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController; }
            if (lController == null) { return; }

            while (lController.layers.Length <= rLayerIndex)
            {
                UnityEditor.Animations.AnimatorControllerLayer lNewLayer = new UnityEditor.Animations.AnimatorControllerLayer();
                lNewLayer.name = "Layer " + (lController.layers.Length + 1);
                lNewLayer.stateMachine = new UnityEditor.Animations.AnimatorStateMachine();
                lController.AddLayer(lNewLayer);
            }

            UnityEditor.Animations.AnimatorControllerLayer lLayer = lController.layers[rLayerIndex];

            UnityEditor.Animations.AnimatorStateMachine lLayerStateMachine = lLayer.stateMachine;

            UnityEditor.Animations.AnimatorStateMachine lSSM_113762 = MotionControllerMotion.EditorFindSSM(lLayerStateMachine, "BasicMeleeAttack-SM");
            if (lSSM_113762 == null) { lSSM_113762 = lLayerStateMachine.AddStateMachine("BasicMeleeAttack-SM", new Vector3(672, -420, 0)); }

            UnityEditor.Animations.AnimatorState lState_114302 = MotionControllerMotion.EditorFindState(lSSM_113762, "Cross_100");
            if (lState_114302 == null) { lState_114302 = lSSM_113762.AddState("Cross_100", new Vector3(336, 48, 0)); }
            lState_114302.speed = 1.2f;
            lState_114302.mirror = false;
            lState_114302.tag = "";
            lState_114302.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Jab_R");

            UnityEditor.Animations.AnimatorState lState_114304 = MotionControllerMotion.EditorFindState(lSSM_113762, "LUppercut_101");
            if (lState_114304 == null) { lState_114304 = lSSM_113762.AddState("LUppercut_101", new Vector3(336, 120, 0)); }
            lState_114304.speed = 1f;
            lState_114304.mirror = false;
            lState_114304.tag = "";
            lState_114304.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Uppercut_L");

            UnityEditor.Animations.AnimatorState lState_114306 = MotionControllerMotion.EditorFindState(lSSM_113762, "RHook_102");
            if (lState_114306 == null) { lState_114306 = lSSM_113762.AddState("RHook_102", new Vector3(336, 192, 0)); }
            lState_114306.speed = 1.5f;
            lState_114306.mirror = false;
            lState_114306.tag = "";
            lState_114306.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Hook_R");

            UnityEditor.Animations.AnimatorState lState_115416 = MotionControllerMotion.EditorFindState(lSSM_113762, "Idle");
            if (lState_115416 == null) { lState_115416 = lSSM_113762.AddState("Idle", new Vector3(24, -192, 0)); }
            lState_115416.speed = 1f;
            lState_115416.mirror = false;
            lState_115416.tag = "Exit";
            lState_115416.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Movement.fbx", "KB_Idle_2");

            UnityEditor.Animations.AnimatorState lState_114324 = MotionControllerMotion.EditorFindState(lSSM_113762, "LPushKick_105");
            if (lState_114324 == null) { lState_114324 = lSSM_113762.AddState("LPushKick_105", new Vector3(336, 372, 0)); }
            lState_114324.speed = 1.5f;
            lState_114324.mirror = false;
            lState_114324.tag = "";
            lState_114324.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Specials.fbx", "KB_FootShotgun");

            UnityEditor.Animations.AnimatorState lState_114326 = MotionControllerMotion.EditorFindState(lSSM_113762, "Overhand");
            if (lState_114326 == null) { lState_114326 = lSSM_113762.AddState("Overhand", new Vector3(336, 264, 0)); }
            lState_114326.speed = 1f;
            lState_114326.mirror = false;
            lState_114326.tag = "";
            lState_114326.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_Overhand_R");

            UnityEditor.Animations.AnimatorState lState_114328 = MotionControllerMotion.EditorFindState(lSSM_113762, "RAxe_99");
            if (lState_114328 == null) { lState_114328 = lSSM_113762.AddState("RAxe_99", new Vector3(336, -24, 0)); }
            lState_114328.speed = 0.8f;
            lState_114328.mirror = false;
            lState_114328.tag = "";
            lState_114328.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_AxeKick");

            UnityEditor.Animations.AnimatorState lState_114336 = MotionControllerMotion.EditorFindState(lSSM_113762, "LKnee_110");
            if (lState_114336 == null) { lState_114336 = lSSM_113762.AddState("LKnee_110", new Vector3(600, 48, 0)); }
            lState_114336.speed = 1f;
            lState_114336.mirror = false;
            lState_114336.tag = "";
            lState_114336.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_KneeLeft");

            UnityEditor.Animations.AnimatorState lState_114330 = MotionControllerMotion.EditorFindState(lSSM_113762, "RRabbitKick_111");
            if (lState_114330 == null) { lState_114330 = lSSM_113762.AddState("RRabbitKick_111", new Vector3(600, 120, 0)); }
            lState_114330.speed = 1.5f;
            lState_114330.mirror = false;
            lState_114330.tag = "";
            lState_114330.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_MidRabbitKick_R");

            UnityEditor.Animations.AnimatorState lState_114332 = MotionControllerMotion.EditorFindState(lSSM_113762, "RLowSweep_112");
            if (lState_114332 == null) { lState_114332 = lSSM_113762.AddState("RLowSweep_112", new Vector3(600, 192, 0)); }
            lState_114332.speed = 1.2f;
            lState_114332.mirror = false;
            lState_114332.tag = "";
            lState_114332.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Crouched.fbx", "KB_crouch_m_LowKickRound_R");

            UnityEditor.Animations.AnimatorState lState_114334 = MotionControllerMotion.EditorFindState(lSSM_113762, "RMidRound_113");
            if (lState_114334 == null) { lState_114334 = lSSM_113762.AddState("RMidRound_113", new Vector3(600, 264, 0)); }
            lState_114334.speed = 1f;
            lState_114334.mirror = false;
            lState_114334.tag = "";
            lState_114334.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_MidKickRoud_R_1");

            UnityEditor.Animations.AnimatorState lState_114398 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_HighKickRound_R_1");
            if (lState_114398 == null) { lState_114398 = lSSM_113762.AddState("KB_m_HighKickRound_R_1", new Vector3(-288, 24, 0)); }
            lState_114398.speed = 1f;
            lState_114398.mirror = false;
            lState_114398.tag = "";
            lState_114398.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_HighKickRound_R_1");

            UnityEditor.Animations.AnimatorState lState_114400 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_SpinningHeelRight");
            if (lState_114400 == null) { lState_114400 = lSSM_113762.AddState("KB_m_SpinningHeelRight", new Vector3(-288, 84, 0)); }
            lState_114400.speed = 1f;
            lState_114400.mirror = false;
            lState_114400.tag = "";
            lState_114400.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_SpinningHeelRight");

            UnityEditor.Animations.AnimatorState lState_114402 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_Superpunch");
            if (lState_114402 == null) { lState_114402 = lSSM_113762.AddState("KB_Superpunch", new Vector3(-288, 144, 0)); }
            lState_114402.speed = 1f;
            lState_114402.mirror = false;
            lState_114402.tag = "";
            lState_114402.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/FightingUnityChan_FreeAsset/FightingUnityChan_FreeAsset/Animations/FUCM05_0022_M_RISING_P.fbx", "RISING_P");

            UnityEditor.Animations.AnimatorState lState_114396 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_KickUppercut_R");
            if (lState_114396 == null) { lState_114396 = lSSM_113762.AddState("KB_m_KickUppercut_R", new Vector3(-288, -36, 0)); }
            lState_114396.speed = 1f;
            lState_114396.mirror = false;
            lState_114396.tag = "";
            lState_114396.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Kicks.fbx", "KB_m_KickUppercut_R");

            UnityEditor.Animations.AnimatorState lState_114404 = MotionControllerMotion.EditorFindState(lSSM_113762, "KB_m_BackfistRound_L");
            if (lState_114404 == null) { lState_114404 = lSSM_113762.AddState("KB_m_BackfistRound_L", new Vector3(-288, -96, 0)); }
            lState_114404.speed = 1f;
            lState_114404.mirror = false;
            lState_114404.tag = "";
            lState_114404.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/_FightingAnimsetPro/Animations/KB_Punches.fbx", "KB_m_BackfistRound_L");

            UnityEditor.Animations.AnimatorState lState_114410 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_attack_360_high");
            if (lState_114410 == null) { lState_114410 = lSSM_113762.AddState("standing_melee_attack_360_high", new Vector3(-312, 264, 0)); }
            lState_114410.speed = 1.2f;
            lState_114410.mirror = false;
            lState_114410.tag = "";
            lState_114410.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Sword And Shield Attack.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114412 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_attack_backhand");
            if (lState_114412 == null) { lState_114412 = lSSM_113762.AddState("standing_melee_attack_backhand", new Vector3(-216, 312, 0)); }
            lState_114412.speed = 1.2f;
            lState_114412.mirror = false;
            lState_114412.tag = "";
            lState_114412.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee attack backhand.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114414 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_combo_attack_ver_2");
            if (lState_114414 == null) { lState_114414 = lSSM_113762.AddState("standing_melee_combo_attack_ver_2", new Vector3(-120, 372, 0)); }
            lState_114414.speed = 1.2f;
            lState_114414.mirror = false;
            lState_114414.tag = "";
            lState_114414.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee combo attack ver. 2.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114416 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_combo_attack_ver_1");
            if (lState_114416 == null) { lState_114416 = lSSM_113762.AddState("standing_melee_combo_attack_ver_1", new Vector3(144, 468, 0)); }
            lState_114416.speed = 1.2f;
            lState_114416.mirror = false;
            lState_114416.tag = "";
            lState_114416.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee combo attack ver. 1.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_114418 = MotionControllerMotion.EditorFindState(lSSM_113762, "standing_melee_run_jump_attack");
            if (lState_114418 == null) { lState_114418 = lSSM_113762.AddState("standing_melee_run_jump_attack", new Vector3(12, 420, 0)); }
            lState_114418.speed = 1.5f;
            lState_114418.mirror = false;
            lState_114418.tag = "";
            lState_114418.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/standing melee run jump attack.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115418 = MotionControllerMotion.EditorFindState(lSSM_113762, "Pointing");
            if (lState_115418 == null) { lState_115418 = lSSM_113762.AddState("Pointing", new Vector3(228, -240, 0)); }
            lState_115418.speed = 1f;
            lState_115418.mirror = false;
            lState_115418.tag = "";
            lState_115418.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Pointing.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115420 = MotionControllerMotion.EditorFindState(lSSM_113762, "NoNoNo");
            if (lState_115420 == null) { lState_115420 = lSSM_113762.AddState("NoNoNo", new Vector3(-96, -276, 0)); }
            lState_115420.speed = 1f;
            lState_115420.mirror = false;
            lState_115420.tag = "";
            lState_115420.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/No.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115422 = MotionControllerMotion.EditorFindState(lSSM_113762, "StandingClap");
            if (lState_115422 == null) { lState_115422 = lSSM_113762.AddState("StandingClap", new Vector3(132, -276, 0)); }
            lState_115422.speed = 1f;
            lState_115422.mirror = false;
            lState_115422.tag = "";
            lState_115422.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Standing Clap.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115424 = MotionControllerMotion.EditorFindState(lSSM_113762, "Victory");
            if (lState_115424 == null) { lState_115424 = lSSM_113762.AddState("Victory", new Vector3(-168, -240, 0)); }
            lState_115424.speed = 1f;
            lState_115424.mirror = false;
            lState_115424.tag = "";
            lState_115424.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Victory.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorState lState_115426 = MotionControllerMotion.EditorFindState(lSSM_113762, "AwwwYeah");
            if (lState_115426 == null) { lState_115426 = lSSM_113762.AddState("AwwwYeah", new Vector3(24, -312, 0)); }
            lState_115426.speed = 1f;
            lState_115426.mirror = false;
            lState_115426.tag = "";
            lState_115426.motion = MotionControllerMotion.EditorFindAnimationClip("Assets/Animation/Mixamo/Rallying.fbx", "mixamo.com");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113968 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114302, 0);
            if (lAnyTransition_113968 == null) { lAnyTransition_113968 = lLayerStateMachine.AddAnyStateTransition(lState_114302); }
            lAnyTransition_113968.isExit = false;
            lAnyTransition_113968.hasExitTime = false;
            lAnyTransition_113968.hasFixedDuration = false;
            lAnyTransition_113968.exitTime = 0.9f;
            lAnyTransition_113968.duration = 0f;
            lAnyTransition_113968.offset = 0f;
            lAnyTransition_113968.mute = false;
            lAnyTransition_113968.solo = false;
            lAnyTransition_113968.canTransitionToSelf = false;
            lAnyTransition_113968.orderedInterruption = true;
            lAnyTransition_113968.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113968.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113968.RemoveCondition(lAnyTransition_113968.conditions[i]); }
            lAnyTransition_113968.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113968.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 100f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113970 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114304, 0);
            if (lAnyTransition_113970 == null) { lAnyTransition_113970 = lLayerStateMachine.AddAnyStateTransition(lState_114304); }
            lAnyTransition_113970.isExit = false;
            lAnyTransition_113970.hasExitTime = false;
            lAnyTransition_113970.hasFixedDuration = false;
            lAnyTransition_113970.exitTime = 1.523494E-08f;
            lAnyTransition_113970.duration = 0.05999994f;
            lAnyTransition_113970.offset = 0f;
            lAnyTransition_113970.mute = false;
            lAnyTransition_113970.solo = false;
            lAnyTransition_113970.canTransitionToSelf = false;
            lAnyTransition_113970.orderedInterruption = true;
            lAnyTransition_113970.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113970.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113970.RemoveCondition(lAnyTransition_113970.conditions[i]); }
            lAnyTransition_113970.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113970.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 101f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113972 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114306, 0);
            if (lAnyTransition_113972 == null) { lAnyTransition_113972 = lLayerStateMachine.AddAnyStateTransition(lState_114306); }
            lAnyTransition_113972.isExit = false;
            lAnyTransition_113972.hasExitTime = false;
            lAnyTransition_113972.hasFixedDuration = false;
            lAnyTransition_113972.exitTime = 0.9000001f;
            lAnyTransition_113972.duration = 0.01443848f;
            lAnyTransition_113972.offset = 0f;
            lAnyTransition_113972.mute = false;
            lAnyTransition_113972.solo = false;
            lAnyTransition_113972.canTransitionToSelf = false;
            lAnyTransition_113972.orderedInterruption = true;
            lAnyTransition_113972.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113972.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113972.RemoveCondition(lAnyTransition_113972.conditions[i]); }
            lAnyTransition_113972.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113972.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 102f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113994 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114324, 0);
            if (lAnyTransition_113994 == null) { lAnyTransition_113994 = lLayerStateMachine.AddAnyStateTransition(lState_114324); }
            lAnyTransition_113994.isExit = false;
            lAnyTransition_113994.hasExitTime = false;
            lAnyTransition_113994.hasFixedDuration = false;
            lAnyTransition_113994.exitTime = 0f;
            lAnyTransition_113994.duration = 0.05910589f;
            lAnyTransition_113994.offset = 0f;
            lAnyTransition_113994.mute = false;
            lAnyTransition_113994.solo = false;
            lAnyTransition_113994.canTransitionToSelf = true;
            lAnyTransition_113994.orderedInterruption = true;
            lAnyTransition_113994.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113994.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113994.RemoveCondition(lAnyTransition_113994.conditions[i]); }
            lAnyTransition_113994.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113994.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 105f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113996 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114326, 0);
            if (lAnyTransition_113996 == null) { lAnyTransition_113996 = lLayerStateMachine.AddAnyStateTransition(lState_114326); }
            lAnyTransition_113996.isExit = false;
            lAnyTransition_113996.hasExitTime = false;
            lAnyTransition_113996.hasFixedDuration = false;
            lAnyTransition_113996.exitTime = 0.4168403f;
            lAnyTransition_113996.duration = 0.03748399f;
            lAnyTransition_113996.offset = 0.08077718f;
            lAnyTransition_113996.mute = false;
            lAnyTransition_113996.solo = false;
            lAnyTransition_113996.canTransitionToSelf = false;
            lAnyTransition_113996.orderedInterruption = true;
            lAnyTransition_113996.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113996.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113996.RemoveCondition(lAnyTransition_113996.conditions[i]); }
            lAnyTransition_113996.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113996.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 103f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_113998 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114328, 0);
            if (lAnyTransition_113998 == null) { lAnyTransition_113998 = lLayerStateMachine.AddAnyStateTransition(lState_114328); }
            lAnyTransition_113998.isExit = false;
            lAnyTransition_113998.hasExitTime = false;
            lAnyTransition_113998.hasFixedDuration = false;
            lAnyTransition_113998.exitTime = 0.75f;
            lAnyTransition_113998.duration = 0f;
            lAnyTransition_113998.offset = 0f;
            lAnyTransition_113998.mute = false;
            lAnyTransition_113998.solo = false;
            lAnyTransition_113998.canTransitionToSelf = false;
            lAnyTransition_113998.orderedInterruption = true;
            lAnyTransition_113998.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_113998.conditions.Length - 1; i >= 0; i--) { lAnyTransition_113998.RemoveCondition(lAnyTransition_113998.conditions[i]); }
            lAnyTransition_113998.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_113998.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 99f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114000 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114330, 0);
            if (lAnyTransition_114000 == null) { lAnyTransition_114000 = lLayerStateMachine.AddAnyStateTransition(lState_114330); }
            lAnyTransition_114000.isExit = false;
            lAnyTransition_114000.hasExitTime = false;
            lAnyTransition_114000.hasFixedDuration = false;
            lAnyTransition_114000.exitTime = 1.575326E-09f;
            lAnyTransition_114000.duration = 0.05f;
            lAnyTransition_114000.offset = 0f;
            lAnyTransition_114000.mute = false;
            lAnyTransition_114000.solo = false;
            lAnyTransition_114000.canTransitionToSelf = false;
            lAnyTransition_114000.orderedInterruption = true;
            lAnyTransition_114000.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114000.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114000.RemoveCondition(lAnyTransition_114000.conditions[i]); }
            lAnyTransition_114000.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114000.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 111f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114002 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114332, 0);
            if (lAnyTransition_114002 == null) { lAnyTransition_114002 = lLayerStateMachine.AddAnyStateTransition(lState_114332); }
            lAnyTransition_114002.isExit = false;
            lAnyTransition_114002.hasExitTime = false;
            lAnyTransition_114002.hasFixedDuration = false;
            lAnyTransition_114002.exitTime = 0f;
            lAnyTransition_114002.duration = 0.1786151f;
            lAnyTransition_114002.offset = 0.04548877f;
            lAnyTransition_114002.mute = false;
            lAnyTransition_114002.solo = false;
            lAnyTransition_114002.canTransitionToSelf = false;
            lAnyTransition_114002.orderedInterruption = true;
            lAnyTransition_114002.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114002.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114002.RemoveCondition(lAnyTransition_114002.conditions[i]); }
            lAnyTransition_114002.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114002.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 112f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114004 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114334, 0);
            if (lAnyTransition_114004 == null) { lAnyTransition_114004 = lLayerStateMachine.AddAnyStateTransition(lState_114334); }
            lAnyTransition_114004.isExit = false;
            lAnyTransition_114004.hasExitTime = false;
            lAnyTransition_114004.hasFixedDuration = false;
            lAnyTransition_114004.exitTime = 0f;
            lAnyTransition_114004.duration = 0.05169718f;
            lAnyTransition_114004.offset = 0f;
            lAnyTransition_114004.mute = false;
            lAnyTransition_114004.solo = false;
            lAnyTransition_114004.canTransitionToSelf = false;
            lAnyTransition_114004.orderedInterruption = true;
            lAnyTransition_114004.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114004.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114004.RemoveCondition(lAnyTransition_114004.conditions[i]); }
            lAnyTransition_114004.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114004.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 113f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114006 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114336, 0);
            if (lAnyTransition_114006 == null) { lAnyTransition_114006 = lLayerStateMachine.AddAnyStateTransition(lState_114336); }
            lAnyTransition_114006.isExit = false;
            lAnyTransition_114006.hasExitTime = false;
            lAnyTransition_114006.hasFixedDuration = false;
            lAnyTransition_114006.exitTime = 0.75f;
            lAnyTransition_114006.duration = 0.25f;
            lAnyTransition_114006.offset = 0f;
            lAnyTransition_114006.mute = false;
            lAnyTransition_114006.solo = false;
            lAnyTransition_114006.canTransitionToSelf = false;
            lAnyTransition_114006.orderedInterruption = true;
            lAnyTransition_114006.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114006.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114006.RemoveCondition(lAnyTransition_114006.conditions[i]); }
            lAnyTransition_114006.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114006.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 110f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114070 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114396, 0);
            if (lAnyTransition_114070 == null) { lAnyTransition_114070 = lLayerStateMachine.AddAnyStateTransition(lState_114396); }
            lAnyTransition_114070.isExit = false;
            lAnyTransition_114070.hasExitTime = false;
            lAnyTransition_114070.hasFixedDuration = true;
            lAnyTransition_114070.exitTime = 1.606756E-09f;
            lAnyTransition_114070.duration = 0.05f;
            lAnyTransition_114070.offset = 0f;
            lAnyTransition_114070.mute = false;
            lAnyTransition_114070.solo = false;
            lAnyTransition_114070.canTransitionToSelf = false;
            lAnyTransition_114070.orderedInterruption = true;
            lAnyTransition_114070.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114070.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114070.RemoveCondition(lAnyTransition_114070.conditions[i]); }
            lAnyTransition_114070.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114070.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 301f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114072 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114398, 0);
            if (lAnyTransition_114072 == null) { lAnyTransition_114072 = lLayerStateMachine.AddAnyStateTransition(lState_114398); }
            lAnyTransition_114072.isExit = false;
            lAnyTransition_114072.hasExitTime = false;
            lAnyTransition_114072.hasFixedDuration = true;
            lAnyTransition_114072.exitTime = 8.422393E-09f;
            lAnyTransition_114072.duration = 0.05f;
            lAnyTransition_114072.offset = 0f;
            lAnyTransition_114072.mute = false;
            lAnyTransition_114072.solo = false;
            lAnyTransition_114072.canTransitionToSelf = false;
            lAnyTransition_114072.orderedInterruption = true;
            lAnyTransition_114072.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114072.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114072.RemoveCondition(lAnyTransition_114072.conditions[i]); }
            lAnyTransition_114072.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114072.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 302f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114074 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114400, 0);
            if (lAnyTransition_114074 == null) { lAnyTransition_114074 = lLayerStateMachine.AddAnyStateTransition(lState_114400); }
            lAnyTransition_114074.isExit = false;
            lAnyTransition_114074.hasExitTime = false;
            lAnyTransition_114074.hasFixedDuration = true;
            lAnyTransition_114074.exitTime = 2.142341E-09f;
            lAnyTransition_114074.duration = 0.03685686f;
            lAnyTransition_114074.offset = 0.03569532f;
            lAnyTransition_114074.mute = false;
            lAnyTransition_114074.solo = false;
            lAnyTransition_114074.canTransitionToSelf = false;
            lAnyTransition_114074.orderedInterruption = true;
            lAnyTransition_114074.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114074.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114074.RemoveCondition(lAnyTransition_114074.conditions[i]); }
            lAnyTransition_114074.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114074.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 303f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114076 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114402, 0);
            if (lAnyTransition_114076 == null) { lAnyTransition_114076 = lLayerStateMachine.AddAnyStateTransition(lState_114402); }
            lAnyTransition_114076.isExit = false;
            lAnyTransition_114076.hasExitTime = false;
            lAnyTransition_114076.hasFixedDuration = true;
            lAnyTransition_114076.exitTime = 1.981666E-08f;
            lAnyTransition_114076.duration = 0.03598962f;
            lAnyTransition_114076.offset = 0f;
            lAnyTransition_114076.mute = false;
            lAnyTransition_114076.solo = false;
            lAnyTransition_114076.canTransitionToSelf = false;
            lAnyTransition_114076.orderedInterruption = true;
            lAnyTransition_114076.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114076.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114076.RemoveCondition(lAnyTransition_114076.conditions[i]); }
            lAnyTransition_114076.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114076.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 304f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114078 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114404, 0);
            if (lAnyTransition_114078 == null) { lAnyTransition_114078 = lLayerStateMachine.AddAnyStateTransition(lState_114404); }
            lAnyTransition_114078.isExit = false;
            lAnyTransition_114078.hasExitTime = false;
            lAnyTransition_114078.hasFixedDuration = true;
            lAnyTransition_114078.exitTime = 0.7500002f;
            lAnyTransition_114078.duration = 0.06643014f;
            lAnyTransition_114078.offset = 0.02266629f;
            lAnyTransition_114078.mute = false;
            lAnyTransition_114078.solo = false;
            lAnyTransition_114078.canTransitionToSelf = false;
            lAnyTransition_114078.orderedInterruption = true;
            lAnyTransition_114078.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114078.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114078.RemoveCondition(lAnyTransition_114078.conditions[i]); }
            lAnyTransition_114078.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114078.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 300f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114106 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114410, 0);
            if (lAnyTransition_114106 == null) { lAnyTransition_114106 = lLayerStateMachine.AddAnyStateTransition(lState_114410); }
            lAnyTransition_114106.isExit = false;
            lAnyTransition_114106.hasExitTime = false;
            lAnyTransition_114106.hasFixedDuration = true;
            lAnyTransition_114106.exitTime = 0f;
            lAnyTransition_114106.duration = 0.05f;
            lAnyTransition_114106.offset = 0.02365446f;
            lAnyTransition_114106.mute = false;
            lAnyTransition_114106.solo = false;
            lAnyTransition_114106.canTransitionToSelf = true;
            lAnyTransition_114106.orderedInterruption = true;
            lAnyTransition_114106.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114106.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114106.RemoveCondition(lAnyTransition_114106.conditions[i]); }
            lAnyTransition_114106.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114106.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 0f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114108 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114412, 0);
            if (lAnyTransition_114108 == null) { lAnyTransition_114108 = lLayerStateMachine.AddAnyStateTransition(lState_114412); }
            lAnyTransition_114108.isExit = false;
            lAnyTransition_114108.hasExitTime = false;
            lAnyTransition_114108.hasFixedDuration = true;
            lAnyTransition_114108.exitTime = 0.75f;
            lAnyTransition_114108.duration = 0.25f;
            lAnyTransition_114108.offset = 0.1220909f;
            lAnyTransition_114108.mute = false;
            lAnyTransition_114108.solo = false;
            lAnyTransition_114108.canTransitionToSelf = true;
            lAnyTransition_114108.orderedInterruption = true;
            lAnyTransition_114108.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114108.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114108.RemoveCondition(lAnyTransition_114108.conditions[i]); }
            lAnyTransition_114108.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114108.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 403f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114110 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114414, 0);
            if (lAnyTransition_114110 == null) { lAnyTransition_114110 = lLayerStateMachine.AddAnyStateTransition(lState_114414); }
            lAnyTransition_114110.isExit = false;
            lAnyTransition_114110.hasExitTime = false;
            lAnyTransition_114110.hasFixedDuration = true;
            lAnyTransition_114110.exitTime = 0f;
            lAnyTransition_114110.duration = 0.25f;
            lAnyTransition_114110.offset = 0.4168376f;
            lAnyTransition_114110.mute = false;
            lAnyTransition_114110.solo = false;
            lAnyTransition_114110.canTransitionToSelf = true;
            lAnyTransition_114110.orderedInterruption = true;
            lAnyTransition_114110.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114110.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114110.RemoveCondition(lAnyTransition_114110.conditions[i]); }
            lAnyTransition_114110.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114110.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 402f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114112 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114416, 0);
            if (lAnyTransition_114112 == null) { lAnyTransition_114112 = lLayerStateMachine.AddAnyStateTransition(lState_114416); }
            lAnyTransition_114112.isExit = false;
            lAnyTransition_114112.hasExitTime = false;
            lAnyTransition_114112.hasFixedDuration = true;
            lAnyTransition_114112.exitTime = 0.75f;
            lAnyTransition_114112.duration = 0.05f;
            lAnyTransition_114112.offset = 0.4555707f;
            lAnyTransition_114112.mute = false;
            lAnyTransition_114112.solo = false;
            lAnyTransition_114112.canTransitionToSelf = false;
            lAnyTransition_114112.orderedInterruption = true;
            lAnyTransition_114112.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114112.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114112.RemoveCondition(lAnyTransition_114112.conditions[i]); }
            lAnyTransition_114112.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114112.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 0f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114114 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114418, 0);
            if (lAnyTransition_114114 == null) { lAnyTransition_114114 = lLayerStateMachine.AddAnyStateTransition(lState_114418); }
            lAnyTransition_114114.isExit = false;
            lAnyTransition_114114.hasExitTime = false;
            lAnyTransition_114114.hasFixedDuration = true;
            lAnyTransition_114114.exitTime = 1.31314f;
            lAnyTransition_114114.duration = 0.05290114f;
            lAnyTransition_114114.offset = 0.1896213f;
            lAnyTransition_114114.mute = false;
            lAnyTransition_114114.solo = false;
            lAnyTransition_114114.canTransitionToSelf = true;
            lAnyTransition_114114.orderedInterruption = true;
            lAnyTransition_114114.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114114.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114114.RemoveCondition(lAnyTransition_114114.conditions[i]); }
            lAnyTransition_114114.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114114.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 400f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lAnyTransition_114120 = MotionControllerMotion.EditorFindAnyStateTransition(lLayerStateMachine, lState_114418, 1);
            if (lAnyTransition_114120 == null) { lAnyTransition_114120 = lLayerStateMachine.AddAnyStateTransition(lState_114418); }
            lAnyTransition_114120.isExit = false;
            lAnyTransition_114120.hasExitTime = false;
            lAnyTransition_114120.hasFixedDuration = true;
            lAnyTransition_114120.exitTime = 0.75f;
            lAnyTransition_114120.duration = 0.25f;
            lAnyTransition_114120.offset = 0f;
            lAnyTransition_114120.mute = false;
            lAnyTransition_114120.solo = false;
            lAnyTransition_114120.canTransitionToSelf = true;
            lAnyTransition_114120.orderedInterruption = true;
            lAnyTransition_114120.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lAnyTransition_114120.conditions.Length - 1; i >= 0; i--) { lAnyTransition_114120.RemoveCondition(lAnyTransition_114120.conditions[i]); }
            lAnyTransition_114120.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 3200f, "L" + rLayerIndex + "MotionPhase");
            lAnyTransition_114120.AddCondition(UnityEditor.Animations.AnimatorConditionMode.Equals, 200f, "L" + rLayerIndex + "MotionForm");

            UnityEditor.Animations.AnimatorStateTransition lTransition_115428 = MotionControllerMotion.EditorFindTransition(lState_114302, lState_115416, 0);
            if (lTransition_115428 == null) { lTransition_115428 = lState_114302.AddTransition(lState_115416); }
            lTransition_115428.isExit = false;
            lTransition_115428.hasExitTime = true;
            lTransition_115428.hasFixedDuration = false;
            lTransition_115428.exitTime = 0.6875f;
            lTransition_115428.duration = 0.1833009f;
            lTransition_115428.offset = 0f;
            lTransition_115428.mute = false;
            lTransition_115428.solo = false;
            lTransition_115428.canTransitionToSelf = true;
            lTransition_115428.orderedInterruption = true;
            lTransition_115428.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115428.conditions.Length - 1; i >= 0; i--) { lTransition_115428.RemoveCondition(lTransition_115428.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115430 = MotionControllerMotion.EditorFindTransition(lState_114304, lState_115416, 0);
            if (lTransition_115430 == null) { lTransition_115430 = lState_114304.AddTransition(lState_115416); }
            lTransition_115430.isExit = false;
            lTransition_115430.hasExitTime = true;
            lTransition_115430.hasFixedDuration = false;
            lTransition_115430.exitTime = 0.6959836f;
            lTransition_115430.duration = 0.1397124f;
            lTransition_115430.offset = 0.005498699f;
            lTransition_115430.mute = false;
            lTransition_115430.solo = false;
            lTransition_115430.canTransitionToSelf = true;
            lTransition_115430.orderedInterruption = true;
            lTransition_115430.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115430.conditions.Length - 1; i >= 0; i--) { lTransition_115430.RemoveCondition(lTransition_115430.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115432 = MotionControllerMotion.EditorFindTransition(lState_114306, lState_115416, 0);
            if (lTransition_115432 == null) { lTransition_115432 = lState_114306.AddTransition(lState_115416); }
            lTransition_115432.isExit = false;
            lTransition_115432.hasExitTime = true;
            lTransition_115432.hasFixedDuration = false;
            lTransition_115432.exitTime = 0.7499999f;
            lTransition_115432.duration = 0.1880713f;
            lTransition_115432.offset = 0f;
            lTransition_115432.mute = false;
            lTransition_115432.solo = false;
            lTransition_115432.canTransitionToSelf = true;
            lTransition_115432.orderedInterruption = true;
            lTransition_115432.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115432.conditions.Length - 1; i >= 0; i--) { lTransition_115432.RemoveCondition(lTransition_115432.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115434 = MotionControllerMotion.EditorFindTransition(lState_114324, lState_115416, 0);
            if (lTransition_115434 == null) { lTransition_115434 = lState_114324.AddTransition(lState_115416); }
            lTransition_115434.isExit = false;
            lTransition_115434.hasExitTime = true;
            lTransition_115434.hasFixedDuration = false;
            lTransition_115434.exitTime = 0.5228129f;
            lTransition_115434.duration = 0.1242818f;
            lTransition_115434.offset = 0.06203142f;
            lTransition_115434.mute = false;
            lTransition_115434.solo = false;
            lTransition_115434.canTransitionToSelf = true;
            lTransition_115434.orderedInterruption = true;
            lTransition_115434.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115434.conditions.Length - 1; i >= 0; i--) { lTransition_115434.RemoveCondition(lTransition_115434.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115436 = MotionControllerMotion.EditorFindTransition(lState_114326, lState_115416, 0);
            if (lTransition_115436 == null) { lTransition_115436 = lState_114326.AddTransition(lState_115416); }
            lTransition_115436.isExit = false;
            lTransition_115436.hasExitTime = true;
            lTransition_115436.hasFixedDuration = false;
            lTransition_115436.exitTime = 1.004305f;
            lTransition_115436.duration = 0.05591125f;
            lTransition_115436.offset = 0.4601691f;
            lTransition_115436.mute = false;
            lTransition_115436.solo = false;
            lTransition_115436.canTransitionToSelf = true;
            lTransition_115436.orderedInterruption = true;
            lTransition_115436.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115436.conditions.Length - 1; i >= 0; i--) { lTransition_115436.RemoveCondition(lTransition_115436.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115438 = MotionControllerMotion.EditorFindTransition(lState_114328, lState_115416, 0);
            if (lTransition_115438 == null) { lTransition_115438 = lState_114328.AddTransition(lState_115416); }
            lTransition_115438.isExit = false;
            lTransition_115438.hasExitTime = true;
            lTransition_115438.hasFixedDuration = false;
            lTransition_115438.exitTime = 0.3266955f;
            lTransition_115438.duration = 0.1307748f;
            lTransition_115438.offset = 0f;
            lTransition_115438.mute = false;
            lTransition_115438.solo = false;
            lTransition_115438.canTransitionToSelf = true;
            lTransition_115438.orderedInterruption = true;
            lTransition_115438.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115438.conditions.Length - 1; i >= 0; i--) { lTransition_115438.RemoveCondition(lTransition_115438.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115440 = MotionControllerMotion.EditorFindTransition(lState_114336, lState_115416, 0);
            if (lTransition_115440 == null) { lTransition_115440 = lState_114336.AddTransition(lState_115416); }
            lTransition_115440.isExit = false;
            lTransition_115440.hasExitTime = true;
            lTransition_115440.hasFixedDuration = false;
            lTransition_115440.exitTime = 0.4517571f;
            lTransition_115440.duration = 0.1076995f;
            lTransition_115440.offset = 0f;
            lTransition_115440.mute = false;
            lTransition_115440.solo = false;
            lTransition_115440.canTransitionToSelf = true;
            lTransition_115440.orderedInterruption = true;
            lTransition_115440.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115440.conditions.Length - 1; i >= 0; i--) { lTransition_115440.RemoveCondition(lTransition_115440.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115442 = MotionControllerMotion.EditorFindTransition(lState_114330, lState_115416, 0);
            if (lTransition_115442 == null) { lTransition_115442 = lState_114330.AddTransition(lState_115416); }
            lTransition_115442.isExit = false;
            lTransition_115442.hasExitTime = true;
            lTransition_115442.hasFixedDuration = false;
            lTransition_115442.exitTime = 0.8369565f;
            lTransition_115442.duration = 0.1716502f;
            lTransition_115442.offset = 0f;
            lTransition_115442.mute = false;
            lTransition_115442.solo = false;
            lTransition_115442.canTransitionToSelf = true;
            lTransition_115442.orderedInterruption = true;
            lTransition_115442.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115442.conditions.Length - 1; i >= 0; i--) { lTransition_115442.RemoveCondition(lTransition_115442.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115444 = MotionControllerMotion.EditorFindTransition(lState_114332, lState_115416, 0);
            if (lTransition_115444 == null) { lTransition_115444 = lState_114332.AddTransition(lState_115416); }
            lTransition_115444.isExit = false;
            lTransition_115444.hasExitTime = true;
            lTransition_115444.hasFixedDuration = true;
            lTransition_115444.exitTime = 0.5712763f;
            lTransition_115444.duration = 0.262871f;
            lTransition_115444.offset = 0f;
            lTransition_115444.mute = false;
            lTransition_115444.solo = false;
            lTransition_115444.canTransitionToSelf = true;
            lTransition_115444.orderedInterruption = true;
            lTransition_115444.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115444.conditions.Length - 1; i >= 0; i--) { lTransition_115444.RemoveCondition(lTransition_115444.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115446 = MotionControllerMotion.EditorFindTransition(lState_114334, lState_115416, 0);
            if (lTransition_115446 == null) { lTransition_115446 = lState_114334.AddTransition(lState_115416); }
            lTransition_115446.isExit = false;
            lTransition_115446.hasExitTime = true;
            lTransition_115446.hasFixedDuration = false;
            lTransition_115446.exitTime = 0.7046111f;
            lTransition_115446.duration = 0.136108f;
            lTransition_115446.offset = 0f;
            lTransition_115446.mute = false;
            lTransition_115446.solo = false;
            lTransition_115446.canTransitionToSelf = true;
            lTransition_115446.orderedInterruption = true;
            lTransition_115446.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115446.conditions.Length - 1; i >= 0; i--) { lTransition_115446.RemoveCondition(lTransition_115446.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115448 = MotionControllerMotion.EditorFindTransition(lState_114398, lState_115426, 0);
            if (lTransition_115448 == null) { lTransition_115448 = lState_114398.AddTransition(lState_115426); }
            lTransition_115448.isExit = false;
            lTransition_115448.hasExitTime = true;
            lTransition_115448.hasFixedDuration = true;
            lTransition_115448.exitTime = 0.7858688f;
            lTransition_115448.duration = 0.7122293f;
            lTransition_115448.offset = 0.2814808f;
            lTransition_115448.mute = false;
            lTransition_115448.solo = false;
            lTransition_115448.canTransitionToSelf = true;
            lTransition_115448.orderedInterruption = true;
            lTransition_115448.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115448.conditions.Length - 1; i >= 0; i--) { lTransition_115448.RemoveCondition(lTransition_115448.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115450 = MotionControllerMotion.EditorFindTransition(lState_114400, lState_115422, 0);
            if (lTransition_115450 == null) { lTransition_115450 = lState_114400.AddTransition(lState_115422); }
            lTransition_115450.isExit = false;
            lTransition_115450.hasExitTime = true;
            lTransition_115450.hasFixedDuration = true;
            lTransition_115450.exitTime = 0.84375f;
            lTransition_115450.duration = 0.6329349f;
            lTransition_115450.offset = 0.1360608f;
            lTransition_115450.mute = false;
            lTransition_115450.solo = false;
            lTransition_115450.canTransitionToSelf = true;
            lTransition_115450.orderedInterruption = true;
            lTransition_115450.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115450.conditions.Length - 1; i >= 0; i--) { lTransition_115450.RemoveCondition(lTransition_115450.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115452 = MotionControllerMotion.EditorFindTransition(lState_114402, lState_115418, 0);
            if (lTransition_115452 == null) { lTransition_115452 = lState_114402.AddTransition(lState_115418); }
            lTransition_115452.isExit = false;
            lTransition_115452.hasExitTime = true;
            lTransition_115452.hasFixedDuration = true;
            lTransition_115452.exitTime = 0.9480702f;
            lTransition_115452.duration = 0.25f;
            lTransition_115452.offset = 0.07633905f;
            lTransition_115452.mute = false;
            lTransition_115452.solo = false;
            lTransition_115452.canTransitionToSelf = true;
            lTransition_115452.orderedInterruption = true;
            lTransition_115452.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115452.conditions.Length - 1; i >= 0; i--) { lTransition_115452.RemoveCondition(lTransition_115452.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115454 = MotionControllerMotion.EditorFindTransition(lState_114396, lState_115420, 0);
            if (lTransition_115454 == null) { lTransition_115454 = lState_114396.AddTransition(lState_115420); }
            lTransition_115454.isExit = false;
            lTransition_115454.hasExitTime = true;
            lTransition_115454.hasFixedDuration = true;
            lTransition_115454.exitTime = 0.6875741f;
            lTransition_115454.duration = 0.4866321f;
            lTransition_115454.offset = 0.05391639f;
            lTransition_115454.mute = false;
            lTransition_115454.solo = false;
            lTransition_115454.canTransitionToSelf = true;
            lTransition_115454.orderedInterruption = true;
            lTransition_115454.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115454.conditions.Length - 1; i >= 0; i--) { lTransition_115454.RemoveCondition(lTransition_115454.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115456 = MotionControllerMotion.EditorFindTransition(lState_114404, lState_115424, 0);
            if (lTransition_115456 == null) { lTransition_115456 = lState_114404.AddTransition(lState_115424); }
            lTransition_115456.isExit = false;
            lTransition_115456.hasExitTime = true;
            lTransition_115456.hasFixedDuration = true;
            lTransition_115456.exitTime = 0.6064144f;
            lTransition_115456.duration = 0.320307f;
            lTransition_115456.offset = 0.1004388f;
            lTransition_115456.mute = false;
            lTransition_115456.solo = false;
            lTransition_115456.canTransitionToSelf = true;
            lTransition_115456.orderedInterruption = true;
            lTransition_115456.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115456.conditions.Length - 1; i >= 0; i--) { lTransition_115456.RemoveCondition(lTransition_115456.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115458 = MotionControllerMotion.EditorFindTransition(lState_114410, lState_115416, 0);
            if (lTransition_115458 == null) { lTransition_115458 = lState_114410.AddTransition(lState_115416); }
            lTransition_115458.isExit = false;
            lTransition_115458.hasExitTime = true;
            lTransition_115458.hasFixedDuration = true;
            lTransition_115458.exitTime = 0.9210526f;
            lTransition_115458.duration = 0.25f;
            lTransition_115458.offset = 0f;
            lTransition_115458.mute = false;
            lTransition_115458.solo = false;
            lTransition_115458.canTransitionToSelf = true;
            lTransition_115458.orderedInterruption = true;
            lTransition_115458.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115458.conditions.Length - 1; i >= 0; i--) { lTransition_115458.RemoveCondition(lTransition_115458.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115460 = MotionControllerMotion.EditorFindTransition(lState_114412, lState_115416, 0);
            if (lTransition_115460 == null) { lTransition_115460 = lState_114412.AddTransition(lState_115416); }
            lTransition_115460.isExit = false;
            lTransition_115460.hasExitTime = true;
            lTransition_115460.hasFixedDuration = true;
            lTransition_115460.exitTime = 0.5893119f;
            lTransition_115460.duration = 0.25f;
            lTransition_115460.offset = 0.1023891f;
            lTransition_115460.mute = false;
            lTransition_115460.solo = false;
            lTransition_115460.canTransitionToSelf = true;
            lTransition_115460.orderedInterruption = true;
            lTransition_115460.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115460.conditions.Length - 1; i >= 0; i--) { lTransition_115460.RemoveCondition(lTransition_115460.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115462 = MotionControllerMotion.EditorFindTransition(lState_114414, lState_115416, 0);
            if (lTransition_115462 == null) { lTransition_115462 = lState_114414.AddTransition(lState_115416); }
            lTransition_115462.isExit = false;
            lTransition_115462.hasExitTime = true;
            lTransition_115462.hasFixedDuration = true;
            lTransition_115462.exitTime = 0.8253455f;
            lTransition_115462.duration = 0.25f;
            lTransition_115462.offset = 0f;
            lTransition_115462.mute = false;
            lTransition_115462.solo = false;
            lTransition_115462.canTransitionToSelf = true;
            lTransition_115462.orderedInterruption = true;
            lTransition_115462.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115462.conditions.Length - 1; i >= 0; i--) { lTransition_115462.RemoveCondition(lTransition_115462.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115464 = MotionControllerMotion.EditorFindTransition(lState_114416, lState_115416, 0);
            if (lTransition_115464 == null) { lTransition_115464 = lState_114416.AddTransition(lState_115416); }
            lTransition_115464.isExit = false;
            lTransition_115464.hasExitTime = true;
            lTransition_115464.hasFixedDuration = true;
            lTransition_115464.exitTime = 0.8055411f;
            lTransition_115464.duration = 0.25f;
            lTransition_115464.offset = 0f;
            lTransition_115464.mute = false;
            lTransition_115464.solo = false;
            lTransition_115464.canTransitionToSelf = true;
            lTransition_115464.orderedInterruption = true;
            lTransition_115464.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115464.conditions.Length - 1; i >= 0; i--) { lTransition_115464.RemoveCondition(lTransition_115464.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115466 = MotionControllerMotion.EditorFindTransition(lState_114418, lState_115416, 0);
            if (lTransition_115466 == null) { lTransition_115466 = lState_114418.AddTransition(lState_115416); }
            lTransition_115466.isExit = false;
            lTransition_115466.hasExitTime = true;
            lTransition_115466.hasFixedDuration = true;
            lTransition_115466.exitTime = 0.9318182f;
            lTransition_115466.duration = 0.25f;
            lTransition_115466.offset = 0f;
            lTransition_115466.mute = false;
            lTransition_115466.solo = false;
            lTransition_115466.canTransitionToSelf = true;
            lTransition_115466.orderedInterruption = true;
            lTransition_115466.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115466.conditions.Length - 1; i >= 0; i--) { lTransition_115466.RemoveCondition(lTransition_115466.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115468 = MotionControllerMotion.EditorFindTransition(lState_115418, lState_115416, 0);
            if (lTransition_115468 == null) { lTransition_115468 = lState_115418.AddTransition(lState_115416); }
            lTransition_115468.isExit = false;
            lTransition_115468.hasExitTime = true;
            lTransition_115468.hasFixedDuration = true;
            lTransition_115468.exitTime = 0.5354011f;
            lTransition_115468.duration = 0.5603523f;
            lTransition_115468.offset = 0f;
            lTransition_115468.mute = false;
            lTransition_115468.solo = false;
            lTransition_115468.canTransitionToSelf = true;
            lTransition_115468.orderedInterruption = true;
            lTransition_115468.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115468.conditions.Length - 1; i >= 0; i--) { lTransition_115468.RemoveCondition(lTransition_115468.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115470 = MotionControllerMotion.EditorFindTransition(lState_115420, lState_115416, 0);
            if (lTransition_115470 == null) { lTransition_115470 = lState_115420.AddTransition(lState_115416); }
            lTransition_115470.isExit = false;
            lTransition_115470.hasExitTime = true;
            lTransition_115470.hasFixedDuration = true;
            lTransition_115470.exitTime = 0.5016305f;
            lTransition_115470.duration = 0.6861773f;
            lTransition_115470.offset = 0f;
            lTransition_115470.mute = false;
            lTransition_115470.solo = false;
            lTransition_115470.canTransitionToSelf = true;
            lTransition_115470.orderedInterruption = true;
            lTransition_115470.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115470.conditions.Length - 1; i >= 0; i--) { lTransition_115470.RemoveCondition(lTransition_115470.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115472 = MotionControllerMotion.EditorFindTransition(lState_115422, lState_115416, 0);
            if (lTransition_115472 == null) { lTransition_115472 = lState_115422.AddTransition(lState_115416); }
            lTransition_115472.isExit = false;
            lTransition_115472.hasExitTime = true;
            lTransition_115472.hasFixedDuration = true;
            lTransition_115472.exitTime = 0.4122928f;
            lTransition_115472.duration = 0.6213312f;
            lTransition_115472.offset = 0f;
            lTransition_115472.mute = false;
            lTransition_115472.solo = false;
            lTransition_115472.canTransitionToSelf = true;
            lTransition_115472.orderedInterruption = true;
            lTransition_115472.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115472.conditions.Length - 1; i >= 0; i--) { lTransition_115472.RemoveCondition(lTransition_115472.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115474 = MotionControllerMotion.EditorFindTransition(lState_115424, lState_115416, 0);
            if (lTransition_115474 == null) { lTransition_115474 = lState_115424.AddTransition(lState_115416); }
            lTransition_115474.isExit = false;
            lTransition_115474.hasExitTime = true;
            lTransition_115474.hasFixedDuration = true;
            lTransition_115474.exitTime = 0.3997446f;
            lTransition_115474.duration = 0.8489763f;
            lTransition_115474.offset = 0.4929846f;
            lTransition_115474.mute = false;
            lTransition_115474.solo = false;
            lTransition_115474.canTransitionToSelf = true;
            lTransition_115474.orderedInterruption = true;
            lTransition_115474.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115474.conditions.Length - 1; i >= 0; i--) { lTransition_115474.RemoveCondition(lTransition_115474.conditions[i]); }

            UnityEditor.Animations.AnimatorStateTransition lTransition_115476 = MotionControllerMotion.EditorFindTransition(lState_115426, lState_115416, 0);
            if (lTransition_115476 == null) { lTransition_115476 = lState_115426.AddTransition(lState_115416); }
            lTransition_115476.isExit = false;
            lTransition_115476.hasExitTime = true;
            lTransition_115476.hasFixedDuration = true;
            lTransition_115476.exitTime = 0.5749115f;
            lTransition_115476.duration = 0.6802311f;
            lTransition_115476.offset = 0.7088763f;
            lTransition_115476.mute = false;
            lTransition_115476.solo = false;
            lTransition_115476.canTransitionToSelf = true;
            lTransition_115476.orderedInterruption = true;
            lTransition_115476.interruptionSource = (UnityEditor.Animations.TransitionInterruptionSource)0;
            for (int i = lTransition_115476.conditions.Length - 1; i >= 0; i--) { lTransition_115476.RemoveCondition(lTransition_115476.conditions[i]); }

        }
#endif






    }

}
