﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//----------------------------------------------
public class MonsterScript : MonoBehaviour {

	public Transform playerTarget;
	public GameObject player;
	public float patrolSpeed = 2.5f;
	public float attackSpeed = 10f;
	public float aggroTimerMax = 120f;
	public float aggroTimer = 0f;
	public bool visibilityBool = false;
	public bool proximityBool = false;
	public bool angleBool = false;
	private NavMeshAgent monster;
	public enum AI_STATE {PATROL, ATTACK};
	public AI_STATE CurrentState = AI_STATE.PATROL;

	//----------------------------------------------
	void Start ()
	{
		monster = GetComponent<NavMeshAgent> ();
		ChangeState (CurrentState);
		aggroTimer = aggroTimerMax;
	}

	void Update()
	{
		visibilityBool = IsPlayerVisible (player);
		angleBool = AngleBool (player);

		//Debug.DrawRay (gameObject.transform.position, player.transform.position - gameObject.transform.position);
	}
	//----------------------------------------------
	#region State Machine

	void ChangeState(AI_STATE NewState)
	{
		StopAllCoroutines ();
		CurrentState = NewState;

		switch (NewState)
		{
		case AI_STATE.PATROL:
			monster.speed = patrolSpeed;
			StartCoroutine (Patrol ());
			break;

		case AI_STATE.ATTACK:
			monster.speed = attackSpeed;
			aggroTimer = aggroTimerMax;
			StartCoroutine (Attack ());
			break;
		}
	}
	#endregion
	//----------------------------------------------
	#region States
	public IEnumerator Patrol ()
	{
		while (CurrentState == AI_STATE.PATROL)
		{
			monster.SetDestination (playerTarget.position);

			yield return null;
		}
	}
		

	public IEnumerator Attack()
	{

		while (CurrentState == AI_STATE.ATTACK)
		{
			monster.SetDestination (player.transform.position);


			if (visibilityBool == false || proximityBool == false || angleBool == false)
			{
				aggroTimer -= 1f;
			}

			if (aggroTimer <= 0f)
			{
				ChangeState (AI_STATE.PATROL);
			}	
			yield return null;
		}
	}
	#endregion
	//----------------------------------------------
	#region Sight Logic
	public bool IsPlayerVisible (GameObject obj) 
	{
		RaycastHit hit;
		Ray rayToPlayer = new Ray (gameObject.transform.position, obj.transform.position - gameObject.transform.position);
		Physics.Raycast (rayToPlayer, out hit, 5000f);
		if (hit.collider.tag != "Player")
		{
			return false;
		}
		return true;
	}

    /*
    public bool IsPlayerVisible(GameObject obj)
    {
        //Get distance
        float Distance = Vector3.Distance(transform.position, obj.transform.position);

        if (Distance >= HorizonDistance) return false;

        Vector3 DirToPlayer = (transform.position - obj.transform.position).normalized;

        float ViewAngle = Vector3.Angle(transform.forward, DirToPlayer);

        if (ViewAngle > FOV) return false;

        RaycastHit hit;
        Ray rayToPlayer = new Ray(gameObject.transform.position, obj.transform.position - gameObject.transform.position);

        Physics.Raycast(rayToPlayer, out hit, 5000f);

        if (hit.collider.tag != "Player")
            return false;

        return true;
    }*/

    void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			proximityBool = false;
		}
	}

	void OnTriggerStay (Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			proximityBool = true;
			if (visibilityBool == true && angleBool == true)
			{
				ChangeState(AI_STATE.ATTACK);
			}
		}
	}

	bool AngleBool (GameObject obj)
	{
		if	(Mathf.Abs(Vector3.Angle(obj.transform.position, gameObject.transform.position)) <= 25f)
		{
			return true;
		}
		return false;
	}
	#endregion
	//----------------------------------------------
}