﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	private CharacterController ThisController = null;
	public float moveSpeed = 2f;
	public float jumpSpeed = 100f;
	private Vector3 velocity = new Vector3 (0f,0f,0f);

    //delegate void MyCustomDelegate();
    //MyCustomDelegate OnPlayerJump;

    // Use this for initialization
    void Start ()
    {
		ThisController = GetComponent<CharacterController> ();

        //OnPlayerJump = OnPlayerJump();
        //OnPlayerJump += OnPlayerJump2();
    }
	
	// Update is called once per frame
	void FixedUpdate () 
	{

		float Horz = Input.GetAxis ("Horizontal");
		gameObject.transform.position = new Vector3 (transform.position.x,0.5f,transform.position.y);

		//Calcluate new direction to nmove
		velocity += transform.right * moveSpeed * Horz;
	
		if (Input.GetButtonDown ("Jump"))
		{
			velocity += transform.up * jumpSpeed;
            //OnPlayerJump();
        }

		velocity += Physics.gravity;
		velocity *= Time.deltaTime;


		ThisController.Move (velocity);
	}

    /*()
    void OnPlayerJump()
    {

    }

    void OnPlayerJump2()
    {
    }*/

    /* //could be a regularly timed update function (custom time steps)
        public IEnumerator CustomUpdate()
        {
            while (true)
            {

                yield return new WaitForSeconds(0.5f);
            }
        }*/
}
