﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ZGravController : MonoBehaviour 
{

	//Movement stats.
	public float moveSpeed = 2f;
	public float jumpSpeed = 2f;
	private float castDistance = 0.25f;
	private float gravity = -9.84f;
	public float walljumpPenalty = 2f;
	public float airControlFactor = 0.1f;
	public float floatiness = 0.2f;

	//Raycast origin objects
	public GameObject botLeftObject;
	public GameObject topLeftObject;
	public GameObject botRightObject;
	public GameObject topRightObject;

	//Pause UI
	public GameObject pauseUI;

	//Raycast origin V3s.
	private Transform botLeft;
	private Transform topLeft;
	private Transform topRight;
	private Transform botRight;

	//Animator bools
	public bool faceRight;
	public bool faceLeft;
	public bool running;

	//Animator objects
	public GameObject root;

	//Bools that inform if controller is on the floor, at a left wall or at a right wall.
	public bool onFloor;
	private bool leftWall;
	private bool rightWall;
	private bool onCeiling;

	//Velocity to change position every frame.
	private Vector3 velocity = new Vector3 (0f,0f,0f);

	void Start ()
	{
		botLeft = botLeftObject.GetComponent<Transform> ();
		botRight = botRightObject.GetComponent<Transform> ();
		topLeft = topLeftObject.GetComponent<Transform> ();
		topRight = topRightObject.GetComponent<Transform> ();
	}


	void FixedUpdate()
	{
		CollisionCheckLeft ();
		CollisionCheckRight ();
		CollisionCheckCeiling ();
		CollisionCheckFloor ();

		castDistance = moveSpeed * Time.deltaTime;

		//-------------------------------------------------------------
		#region Movement and Collision Logic
		//If on floor: disables gravity, enables movement left and right
		if (onFloor)
		{

			velocity = new Vector3 (velocity.x, velocity.y, Mathf.Clamp (velocity.z,0f,Mathf.Infinity));

			if (Input.GetKey (KeyCode.A) && !leftWall)
			{
				velocity = new Vector3 (-moveSpeed, velocity.y, velocity.z);
				root.transform.rotation = Quaternion.Euler (0,-90,0);
			}
			else if (Input.GetKey (KeyCode.D) && !rightWall)
            {
            	velocity = new Vector3 (moveSpeed, velocity.y, velocity.z);
            	root.transform.rotation = Quaternion.Euler (0,-270,180);
            }
            else
            {
            	velocity = new Vector3 (0f, 0f, 0f);
            }

			if (Input.GetKeyDown (KeyCode.Space))
			{
				onFloor = false;
				velocity += new Vector3 (0f, 0f, jumpSpeed);
			}
		}

		//-------------------------------------------------------------
		if (onCeiling)
		{
			velocity = new Vector3 (velocity.x, velocity.y, 0f);
			onCeiling = false;
		}

		//-------------------------------------------------------------
		//Aerial movement restrictions
		if (!onFloor)
		{

			if (Input.GetKey (KeyCode.A) && !leftWall && !rightWall)
			{
				velocity += new Vector3 (-moveSpeed, 0f, 0f) * airControlFactor;
			}
			else if (Input.GetKey (KeyCode.D) && !rightWall && !leftWall)
			{
			    velocity += new Vector3 (moveSpeed, 0f, 0f) * airControlFactor;
			}

			if (leftWall && velocity.x < 0f)
			{
				velocity = new Vector3 (Mathf.Clamp (velocity.x,0f,moveSpeed), velocity.y, velocity.z);
			} 

			if (Input.GetKeyDown (KeyCode.Space) && leftWall)
			{
				//print ("Walljump from left");
				velocity = new Vector3 (jumpSpeed / walljumpPenalty, 0f, jumpSpeed);	
			}

			if (rightWall && velocity.x > 0f)
			{
				velocity = new Vector3 (Mathf.Clamp (velocity.x,-moveSpeed,0f), velocity.y, velocity.z);
			}

			if (Input.GetKeyDown (KeyCode.Space) && rightWall)
			{
				//print ("Walljump from right");
				velocity = new Vector3 (-jumpSpeed / walljumpPenalty, 0f, jumpSpeed);
			}

			if (!leftWall && !rightWall && velocity.z <= 0f)
			{
				velocity -= new Vector3 (0f, 0f, floatiness);
			}

			velocity += new Vector3 (0f, 0f, gravity) * Time.deltaTime;
		}

		//-------------------------------------------------------------

		//Resultant velocity calculations
		velocity = new Vector3 (Mathf.Clamp (velocity.x, -moveSpeed, moveSpeed), velocity.y, Mathf.Clamp (velocity.z, gravity, jumpSpeed));
		transform.position += velocity * Time.deltaTime;

		#endregion
	}

	void Update ()
	{

		//Pause function
		if (Input.GetKeyDown(KeyCode.P))
		{
			pauseUI.SetActive (true);
			Time.timeScale = 0;
		}
			
	}

	public void Upause ()
	{
		Time.timeScale = 1;
	}

	#region Collision Raycasts

	void CollisionCheckCeiling ()
	{
		RaycastHit hit;
		Ray upRayRight = new Ray (topRight.position, Vector3.forward);
		Ray upRayLeft = new Ray (topLeft.position, Vector3.forward);

		if (Physics.Raycast (upRayRight, out hit, castDistance) || Physics.Raycast (upRayLeft, out hit, castDistance))
		{
			if (hit.collider.tag == "environment")
			{
				//print ("Player on floor");
				onCeiling = true;
			}
		}
	}

	void CollisionCheckFloor()
	{

		Debug.DrawRay (botRight.position,Vector3.back);
		Debug.DrawRay (botLeft.position,Vector3.back);

		RaycastHit hit;
		Ray floorRayRight = new Ray (botRight.position,Vector3.back);
		Ray floorRayLeft = new Ray (botLeft.position,Vector3.back);

		if (Physics.Raycast (floorRayLeft, out hit, castDistance) || Physics.Raycast (floorRayRight, out hit, castDistance))
		{
			if (hit.collider.tag == "environment")
			{
				//print ("Player on floor");
				onFloor = true;
			}
		}
		else
		{
			onFloor = false;
		}
	}

	void CollisionCheckLeft()
	{
		RaycastHit hit;
		Ray wallRayLeftTop = new Ray (topLeft.position,Vector3.left);
		Ray wallRayLeftBot = new Ray (botLeft.position,Vector3.left);
		if (Physics.Raycast (wallRayLeftBot, out hit, castDistance) || Physics.Raycast (wallRayLeftTop, out hit, castDistance))
		{
			if (hit.collider.tag == "environment")
			{
				//print ("Player on wall - left");
				leftWall = true;
			}

		}
		else
		{
			leftWall = false;
		}
	}

	void CollisionCheckRight()
	{
		RaycastHit hit;
		Ray wallRayRightTop = new Ray (topRight.position,Vector3.right);
		Ray wallRayRightBot = new Ray (botRight.position,Vector3.right);

		if (Physics.Raycast (wallRayRightBot, out hit, castDistance) || Physics.Raycast (wallRayRightTop, out hit, castDistance))
		{
			if (hit.collider.tag == "environment")
			{
				//print ("Player on wall - right");
				rightWall = true;
			}
		}
		else
		{
			rightWall = false;
		}
	}

	#endregion

}