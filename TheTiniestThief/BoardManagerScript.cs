﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManagerScript : MonoBehaviour 
{
	public GameObject player;
	public GameObject[] roomPrefabs;
	public GameObject[] roomCoords;
	public int spawnCoord = 0;


	// Use this for initialization
	void Start () 
	{
		roomCoords = GameObject.FindGameObjectsWithTag ("Coord");

		Restart ();
	}

	public void Restart() 
	{
		for (int i = 0; i < roomPrefabs.Length; i++)
		{
			RoomRoller (i);
		}
	}

	public void RoomRoller(int i)
	{
		int roomNumber = Random.Range (1, roomPrefabs.Length + 1);

		for (int j = 0; j < roomPrefabs.Length - 1; j++)
		{
			RoomSpawn roomSpawn = roomPrefabs [j].GetComponent<RoomSpawn> ();
			if (roomNumber == roomSpawn.spawnedRoom) 
			{
				//print ("Room " + roomNumber + " already exists, rolling again.");
				RoomRoller (i);
				return;
			} 
		}
		//print ("Room number " + roomNumber + " is unique in the array. Assigning and spawning to room " + i);
		RoomSpawn targetSpawn = roomPrefabs [i].GetComponent<RoomSpawn> ();
		targetSpawn.SpawnRoom (roomNumber);
	}

}
