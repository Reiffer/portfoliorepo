﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomSpawn : MonoBehaviour {

	public int spawnedRoom = 0;
	public BoardManagerScript BMS;

	void Start()
	{
		BMS = GetComponentInParent<BoardManagerScript> ();
	}

	public void SpawnRoom(int roomNumber)
	{
		spawnedRoom = roomNumber;
		gameObject.transform.position = BMS.roomCoords [roomNumber - 1].transform.position;

	}
}
