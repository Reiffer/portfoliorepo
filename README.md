### Portfolio Repository for Thomas Reiffer. ###

Each folder represents a different project undertaken at the National Film and Television School. 

Within each folder is the project brief and a playthrough video of the final build, along with a sample of the source code for each project.

### Structure ###

First Year Modules:

* Hello World
* Code Camp
* App Factory
* Synthespians

Second Year Modules:

* FYP Grad Game: GOLD

### A note about the MotionController ###

The *MotionController* is an asset on the Unity Asset Store that is a fully implemented 3rd-Person Character Controller and combat messaging system. 
For my grad game, GOLD, I used this framework extensively not only to learn rapidly from a much more advanced developer's code (thanks a lot Tim!),
but also to demonstrate that I can operate within a complex codebase via its documentation. Because the MotionController is premium code, I am 
unable to post it here fully for context - but I can still show what I contributed. 

### Contact Info ###

* Employers and agencies: thomas.reiffer@gmail.com
* Prospective NFTS Games students: clockworkxvi@gmail.com